<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('test-file',function (){
//    $file = file_get_contents(asset("check-time.json"));
////    dd($file);
////    $data=json_decode($file);
//    $characters = json_decode($file, true);
//    foreach ($characters as $key => $val) {
//        $characters[$key]['date'] = $val['check_in'] !=null ? date('Y-m-d',strtotime($val['check_in'])) : date('Y-m-d',strtotime($val['check_out']));
//    }
//    try {
//        foreach ($characters as $key => $val) {
//            $exist = \App\Models\PeopleAttendance::where([['employee_id', '=', $val['employee_id']], ['date', '=', $val['date']]])->first();
//            if ($exist!=null) {
//                if($val['check_out'] !=null) {
//                    $exist->update(['check_out' => $val['check_out'],
//                        'updated_at'=>$val['check_out']]);
//                }
//            } else {
//                if($val['check_in'] !=null){
//                    \App\Models\PeopleAttendance::create([
//                        'employee_id'=>$val['employee_id'],
//                        'check_in'=>$val['check_in'],
//                        'date'=>$val['date'],
//                        'created_at'=>$val['check_in'],
//                        'updated_at'=>$val['check_in'],
//                        'check_out'=>$val['check_out']
//                    ]);
//                }
//
//            }
//        }
//    dd('done');
//    }catch (Exception $e){
//        dd($e);
//    }
//});
//Route::get('test-temp',function(){
//    $file = file_get_contents(asset("temperature.json"));
//    $characters = json_decode($file, true);
////    dd($characters);
//    try {
//        foreach ($characters as $key => $val) {
//            \App\Models\EmployeeTemperature::create([
//                'employee_id' => $val['employee_id'],
//                'temperature' => $val['temperature'],
//                'temp_date' => date('Y-m-d H:i:s',strtotime($val['temp_time'])),
//                'created_at'=>date('Y-m-d H:i:s',strtotime($val['temp_time'])),
//                'updated_at'=>date('Y-m-d H:i:s',strtotime($val['temp_time'])),
//                'is_guest'=>0
//            ]);
//        }
//        dd('done');
//    }catch (Exception $e){
//        dd($e);
//    }
//});

Route::get('testinfo',function (){
    echo phpinfo();
});
Route::get('test-until', function() {
    try {
        $requestPayload = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:TPAPIPosIntfU-ITPAPIPOS" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <urn:GetActiveTableInfo soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                             <Request xsi:type="urn:TGetActiveTableInfoRequest" xmlns:urn="urn:TPAPIPosIntfU">
                             <Password xsi:type="xsd:string">EatCard!61020</Password>
                             <UserName xsi:type="xsd:string">RON_EATCARD</UserName>
                             <AppToken xsi:type="xsd:string">37213cabd823</AppToken>
                             <AppName xsi:type="xsd:string"></AppName>
                                <TableNumber xsi:type="xsd:int">1</TableNumber>
                                <TablePart xsi:type="xsd:string">a</TablePart>
                                <Extra xsi:type="urn1:TExtraInfoArray" soapenc:arrayType="urn1:TExtraInfo[]" xmlns:urn1="urn:TPAPIPosTypesU"/>
                             </Request>
                          </urn:GetActiveTableInfo>
                       </soapenv:Body>
                    </soapenv:Envelope>';
        $headers = [
            "Content-type: text/xml;charset=utf-8",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($requestPayload),
        ];
        $soap_request = curl_init();
        curl_setopt($soap_request, CURLOPT_URL, "http://94.168.11.250:3063/soap/ITPAPIPOS");
        curl_setopt($soap_request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_request, CURLOPT_POST, true);
//      curl_setopt($soap_request, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_request, CURLOPT_POSTFIELDS, $requestPayload);
        curl_setopt($soap_request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($soap_request, CURLOPT_TIMEOUT, 10000); //timeout in seconds
        $soap_reposonse = curl_exec($soap_request);
        //          $parsedData = $this->parseXMLData($soap_reposonse);
        dd($soap_reposonse);
// Check the return value of curl_exec(), too
        if ($soap_reposonse == false) {
            throw new Exception(curl_error($soap_request), curl_errno($soap_request));
        }
        curl_close($soap_request);
// dd($soap_reposonse);
        $plainXML = parseXMLData(trim($soap_reposonse));
        $arrayResult = json_decode(json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $arrayResult;
    }catch (\Exception $e){
        // dd( $e->getCode(), $e->getMessage(), $e->getLine(), $e->getFile());
        Log::info('untill api call error '.json_encode($e->getMessage()));
        return [];
    }
});
Route::get('/clear/today-collision/{id}',function ($id){
   \App\CollisionAccess::where('from_user',$id)->whereDate('created_at',\Carbon\Carbon::today())->delete();
   return redirect('/dashboard')->with('success','Collision deleted');
});

Route::get('major-minor',function (){
    $users=\App\User::where([['major_id','=',null],['minor_id','=',null]])->get();
  try {
      foreach ($users as $user) {
          $ids = checkUser();
          $user->update(['major_id' => $ids['major'], 'minor_id' => $ids['minor']]);
      }

   return redirect('/');
  }catch (Exception $e){
      dd($e);
  }
});
Route::get('test-temperature',function (){
$data=\App\Models\EmployeeTemperature::whereBetween('temp_date',['2020-09-01','2020-09-04'])->get()->groupBy('employee_id')->toArray();
foreach ($data as $ey=>$val){
    foreach ($val as $k=>$v) {
        $val[$k]['temp_date'] = date('Y-m-d', strtotime($v['temp_date']));

    }
            $date = collect($val)->groupBy('temp_date')->toArray();
            foreach ($date as $d=>$t){
                if(count($t)>1){
                    for($j=0;$j<count($t)-1;$j++){
                        \App\Models\EmployeeTemperature::where('id',$t[$j]['id'])->delete();
                    }
                }
            }
}
return redirect('/');
});

Auth::routes();
Route::post('/login', [
    'uses'          => 'Auth\LoginController@login',
    'middleware'    => 'checkstatus',
]);

Route::get('/', function () {
    if (auth()->check()) {
        return redirect('/dashboard');
    } else {
        return redirect('/login');
    }
});

Route::get('/home', function () {
    return redirect('/dashboard');
});


Route::get('/test-mail', 'CronController@testMail');
Route::get('/cron/guest_user_end', 'CronController@endGuestUser');

/*
   |--------------------------------------------------------------------------
   | Routes Without authentication
   |--------------------------------------------------------------------------
 */
Route::get('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/forgot', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password_reset_save');
Route::post('/reset/password', 'Auth\ResetPasswordController@reset')->name('password.reset');


Auth::routes();

Route::get('register_company',['as' => 'company.register', 'uses' => 'RegisterCompanyController@register']);
Route::post('register_company',['as' => 'company.register', 'uses' => 'RegisterCompanyController@registerCreate']);

Route::group(['middleware' => 'auth'], function () {


    Route::group(['namespace' => 'Auth'], function () {

        Route::get('view-profile', 'ProfileController@showProfile')->name('view.profile');
        Route::get('update-profile', 'ProfileController@editProfile')->name('edit.profile');
        Route::post('update-profile', 'ProfileController@updateProfile')->name('update.profile');

        Route::get('change-password', 'ProfileController@changePassword')->name('change.password');
        Route::post('change-password', 'ProfileController@updatePassword')->name('update.password');

        Route::get('edit-address', 'ProfileController@editAddress')->name('address.change');
        Route::post('edit-address', 'ProfileController@updateAddress')->name('update.address');

        Route::get('edit-settings', 'ProfileController@settings')->name('profile.settings');
        Route::post('edit-settings', 'ProfileController@updateSettings')->name('update.settings');

        Route::get('logout', 'LoginController@logout')->name('logout');
    });


    /*
      |--------------------------------------------------------------------------
      | Super Admin Routes
      |--------------------------------------------------------------------------
    */
    Route::group(['namespace' => 'Backend'], function () {

        //After Login redirect to dashboard
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        Route::resource('users', 'UserController');
        Route::get('users/delete/{id}', ['as' => 'users.delete', 'uses' => 'UserController@destroy']);

        Route::resource('company', 'CompanyController');
        Route::get('company/delete/{id}', ['as' => 'company.delete', 'uses' => 'CompanyController@destroy']);
        Route::get('company/cancel/{id}', ['as' => 'company.cancel', 'uses' => 'CompanyController@cancel']);
        Route::get('company/block/{id}', ['as' => 'company.block', 'uses' => 'CompanyController@block']);
        Route::post('company/reset_password', ['as' => 'company.reset_password', 'uses' => 'CompanyController@resetPassword']);

        Route::resource('roles', 'RoleController');
        Route::get('roles/delete/{id}', ['as' => 'roles.delete', 'uses' => 'RoleController@destroy']);

        Route::resource('groups', 'GroupController');
        Route::get('groups/delete/{id}', ['as' => 'groups.delete', 'uses' => 'GroupController@destroy']);
        Route::post('groups/get_ajax_data', ['uses' => 'GroupController@get_ajax_data']);
        Route::post('groups/show_people', ['as' => 'groups.show_people','uses' => 'GroupController@show_people']);

        Route::resource('people_registry', 'PeopleRegistryController');
        Route::get('people_registry/delete/{id}', ['as' => 'people_registry.delete', 'uses' => 'PeopleRegistryController@destroy']);
        Route::post('people_registry/show_video/', ['as' => 'people_registry.show_video', 'uses' => 'PeopleRegistryController@show_video']);
        Route::post('people_registry/get_ajax_data/', ['uses' => 'PeopleRegistryController@get_ajax_data']);


        Route::resource('people_attendance', 'PeopleAttendanceController');
        Route::post('people_attendance/get_attendance', ['as' => 'people_registry.get_attendance', 'uses' => 'PeopleAttendanceController@get_attendance']);
        Route::post('people_attendance/ajax-attendance-log', ['as' => 'people_registry.ajax-attendance-log', 'uses' => 'PeopleAttendanceController@ajaxAttendanceLog']);

        Route::resource('devices', 'DeviceController');
        Route::get('devices/delete/{id}', ['as' => 'devices.delete', 'uses' => 'DeviceController@destroy']);
        Route::post('devices/get_ajax_data', ['as' => 'devices.get_ajax_data', 'uses' => 'DeviceController@get_ajax_data']);

        Route::resource('device_people', 'DevicePeopleController');
        Route::post('device_people/get_ajax_data', ['as' => 'device_people.get_ajax_data', 'uses' => 'DevicePeopleController@get_ajax_data']);

        //Route::get('devices/delete/{id}', ['as' => 'devices.delete', 'uses' => 'DeviceController@destroy']);

        Route::get('device_settings/settings', ['as' => 'device_settings.settings', 'uses' => 'DeviceSettingsController@settings']);
        Route::post('device_settings/update_settings/{id}', ['as' => 'device_settings.update_settings', 'uses' => 'DeviceSettingsController@updateSettings']);
        Route::resource('device_settings', 'DeviceSettingsController');



        Route::resource('building', 'BuildingController');
        Route::get('building/delete/{id}', ['as' => 'building.delete', 'uses' => 'BuildingController@destroy']);
        Route::post('building/get_ajax_data', ['as' => 'building.get_ajax_data', 'uses' => 'BuildingController@get_ajax_data']);
        Route::post('building/show_booking', ['as' => 'building.show_booking', 'uses' => 'BuildingController@show_booking']);

        Route::resource('building_policy', 'BuildingPolicyController');
        Route::get('building_policy/delete/{id}', ['as' => 'building_policy.delete', 'uses' => 'BuildingPolicyController@destroy']);

        Route::resource('bookings', 'BookingController');
        Route::post('bookings/get_ajax_data', ['as' => 'bookings.get_ajax_data', 'uses' => 'BookingController@get_ajax_data']);
        Route::get('bookings/delete/{id}', ['as' => 'bookings.delete', 'uses' => 'BookingController@destroy']);


        Route::get('contact_tracing', ['as'=>'contact_tracing.index','uses'=>'ContactTracingController@index']);
        Route::post('getContactTracing', ['as' => 'contact_tracing_data', 'uses' => 'ContactTracingController@getLists']);
        Route::post('contact_tracing/show_contact', ['as' => 'contact_tracing.show_contact', 'uses' => 'ContactTracingController@show']);
        Route::post('contact_tracing/show_other_contact', ['as' => 'contact_tracing.show_other_contact', 'uses' => 'ContactTracingController@show_other']);

        Route::get('risk_report', ['as'=>'risk_report.index','uses'=>'RiskController@index']);
        Route::post('getRiskLists', ['as'=>'risk_data','uses'=>'RiskController@getRiskLists']);

        Route::get('employee/export_data', ['as' => 'employee.export_data', 'uses' => 'EmployeeController@export_data']);
        Route::get('employee/import_data', ['middleware'=>'company_role','as' => 'employee.import_data', 'uses' => 'EmployeeController@import_data']);
        Route::post('employee/import_data_store', ['middleware'=>'company_role','as' => 'employee.import_data_store', 'uses' => 'EmployeeController@import_data_store']);
        Route::resource('employee', 'EmployeeController');
        Route::post('employee/get_ajax_data', 'EmployeeController@get_ajax_data');
        Route::get('employee/delete/{id}', ['as' => 'employee.delete', 'uses' => 'EmployeeController@destroy']);
        Route::get('employee/cancel/{id}', ['as' => 'employee.cancel', 'uses' => 'EmployeeController@cancel']);
        Route::get('employee/block/{id}', ['as' => 'employee.block', 'uses' => 'EmployeeController@block']);

        Route::resource('emp_temp', 'EmployeeTempratureController');
        Route::post('getEmpTemp', ['as' => 'emp_temp_list', 'uses' => 'EmployeeTempratureController@getLists']);
        Route::get('emp_temp/delete/{id}', ['as' => 'emp_temp.delete', 'uses' => 'EmployeeTempratureController@destroy']);


        Route::get('guest_users', ['as' => 'guest_users.index', 'uses' => 'GuestUsersController@index']);
        Route::post('guest_users/getGuestUsers', ['as' => 'guest_users.getGuestUsers', 'uses' => 'GuestUsersController@getLists']);
        Route::post('guest_users/show_video/', ['as' => 'guest_users.show_video', 'uses' => 'GuestUsersController@show_video']);
        Route::get('guest_users/delete/{id}', ['as' => 'guest_users.delete', 'uses' => 'GuestUsersController@destroy']);
//        Route::get('guest_users', ['as' => 'guest_users.index', 'uses' => 'GuestUsersController@index']);
//        Route::post('guest_users/getGuestUsers', ['as' => 'guest_users.getGuestUsers', 'uses' => 'GuestUsersController@getLists']);
//        Route::post('guest_users/show_video/', ['as' => 'guest_users.show_video', 'uses' => 'GuestUsersController@show_video']);
//        Route::get('guest_users/delete/{id}', ['as' => 'guest_users.delete', 'uses' => 'GuestUsersController@destroy']);

        Route::group(['prefix'=>'building-access'],function (){
          Route::get('/','BuildingAccessController@index')->name('booking-access.index');
          Route::post('/ajax-table','BuildingAccessController@tableAjax')->name('booking-access.table');
          Route::get('add-building','BuildingAccessController@create')->name('booking-access.create');
          Route::post('store','BuildingAccessController@store')->name('booking-access.store');
          Route::get('{id}/edit-building','BuildingAccessController@edit')->name('booking-access.edit');
          Route::Post('update-building/{id}','BuildingAccessController@update')->name('booking-access.update');
          Route::get('delete-building/{id}','BuildingAccessController@deleteBuilding')->name('booking-access.delete');
        });

        Route::group(['prefix'=>'location-access'],function (){
            Route::get('/','LocationAccessController@index')->name('location-access.index');
            Route::post('/ajax-table','LocationAccessController@tableAjax')->name('location-access.table');
            Route::post('/ajax-table2','LocationAccessController@tableAjax2')->name('location-access.table2');
            Route::get('add-location','LocationAccessController@create')->name('location-access.create');
            Route::post('store','LocationAccessController@store')->name('location-access.store');
            Route::get('{id}/edit-location','LocationAccessController@edit')->name('location-access.edit');
            Route::Post('update-location/{id}','LocationAccessController@update')->name('location-access.update');
            Route::get('delete-location/{id}','LocationAccessController@deleteLocation')->name('location-access.delete');
        });
        Route::group(['prefix'=>'timeslot-access'],function (){
            Route::get('/','TimeSlotController@index')->name('timeslot-access.index');
            Route::post('/ajax-table','TimeSlotController@tableAjax')->name('timeslot-access.table');
            Route::post('/ajax-table2','TimeSlotController@tableAjax2')->name('timeslot-access.table2');
            Route::get('add-timeslot','TimeSlotController@create')->name('timeslot-access.create');
            Route::get('location-ajax/{id}','TimeSlotController@LocationAjax')->name('timeslot-access.location');
            Route::post('store','TimeSlotController@store')->name('timeslot-access.store');
            Route::get('{id}/edit-timeslot','TimeSlotController@edit')->name('timeslot-access.edit');
            Route::Post('update-timeslot/{id}','TimeSlotController@update')->name('timeslot-access.update');
            Route::get('delete-timeslot/{id}','TimeSlotController@deletetimeslot')->name('timeslot-access.delete');
        });
        Route::group(['prefix'=>'reservations'],function (){
           Route::get('/','ReservationController@index')->name('reservation.index');
           Route::get('ajax-table','ReservationController@ajaxTable')->name('reservation.table');
           Route::get('details/{id}/{type}','ReservationController@details')->name('reservation.details');
        });
        Route::group(['prefix'=>'device_settings'],function (){
            Route::get('device_settings/create','DeviceSettingsController@create')->name('device_settings.create');
            Route::post('device_settings/store','DeviceSettingsController@store')->name('device_settings.store');
            Route::get('edit-settings/{id}',  'DeviceSettingsController@newSettings')->name('device_settings.settings-new');
            Route::post('settings-ajax','DeviceSettingsController@settingsAjax')->name('device_settings.settings-ajax');
            Route::get('delete/{id}','DeviceSettingsController@deleteBuilding')->name('device_settings.delete');
        });
        Route::group(['prefix'=>'booking-policy'],function (){
            Route::get('/','BookingPolicyController@index')->name('booking-policy.index');
            Route::get('/create','BookingPolicyController@create')->name('booking-policy.create');
            Route::post('/store','BookingPolicyController@store')->name('booking-policy.store');
            Route::post('/ajax-table','BookingPolicyController@ajaxTable')->name('booking-policy.table');
            Route::get('{id}/edit','BookingPolicyController@edit')->name('booking-policy.edit');
            Route::post('/update/{id}','BookingPolicyController@update')->name('booking-policy.update');
            Route::get('/delete/{id}','BookingPolicyController@deletePolicy')->name('booking-policy.delete');
        });
        Route::group(['prefix'=>'collision-report'],function (){
            Route::get('/','CollisionController@index')->name('collision.index');
            Route::post('ajax-table','CollisionController@tableAjax')->name('collision.table');
            Route::get('{id}/{date}/details','CollisionController@details')->name('collision.details');
            Route::post('ajax-table-view','CollisionController@viewTable')->name('collision.view');
        });
        Route::group(['prefix'=>'floor-access'],function (){
            Route::get('/','FloorAccessController@index')->name('floor-access.index');
            Route::post('ajax-table','FloorAccessController@tableAjax')->name('floor-access.table');
            Route::get('/create','FloorAccessController@create')->name('floor-access.create');
            Route::post('/store','FloorAccessController@store')->name('floor-access.store');
            Route::get('{id}/edit-floor','FloorAccessController@edit')->name('floor-access.edit');
            Route::post('/update-floor/{id}','FloorAccessController@update')->name('floor-access.update');
            Route::get('/delete-floor/{id}','FloorAccessController@deleteFloor')->name('floor-access.delete');
            Route::post('/ajax-table2','FloorAccessController@tableAjax2')->name('floor-access.table2');
        });
        Route::group(['prefix'=>'desk-access'],function (){
            Route::get('/','DeskAccessController@index')->name('desk-access.index');
            Route::post('ajax-table','DeskAccessController@tableAjax')->name('desk-access.table');
            Route::get('floor-ajax/{id}','DeskAccessController@floorAjax')->name('desk-access.floors');
            Route::get('/create','DeskAccessController@create')->name('desk-access.create');
            Route::post('/store','DeskAccessController@store')->name('desk-access.store');
            Route::get('{id}/edit-desk','DeskAccessController@edit')->name('desk-access.edit');
            Route::post('/update-desk/{id}','DeskAccessController@update')->name('desk-access.update');
            Route::get('/delete-desk/{id}','DeskAccessController@deleteDesk')->name('desk-access.delete');
            Route::post('/ajax-table2','DeskAccessController@tableAjax2')->name('desk-access.table2');
        });
        Route::group(['prefix'=>'desk-booking-policy'],function (){
                Route::get('/','DeskBookingPolicyController@index')->name('desk-booking-policy.index');
                Route::post('ajax-table','DeskBookingPolicyController@ajaxTable')->name('desk-booking-policy.table');
                Route::get('floor-ajax/{id}','DeskBookingPolicyController@floorAjax')->name('desk-booking-policy.floors');
                Route::get('/create','DeskBookingPolicyController@create')->name('desk-booking-policy.create');
                Route::post('/store','DeskBookingPolicyController@store')->name('desk-booking-policy.store');
                Route::get('{id}/edit-desk','DeskBookingPolicyController@edit')->name('desk-booking-policy.edit');
                Route::post('/update-desk/{id}','DeskBookingPolicyController@update')->name('desk-booking-policy.update');
                Route::get('/delete-desk/{id}','DeskBookingPolicyController@deletePolicy')->name('desk-booking-policy.delete');
                Route::post('/ajax-table2','DeskBookingPolicyController@tableAjax2')->name('desk-booking-policy.table2');
        });
        Route::post('report-email-update','EmployeeTempratureController@updateEmail')->name('temp_report.email.update');
    });
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

route::get('logout-test',function (){
    \Illuminate\Support\Facades\Auth::logout();
    return redirect('/');
});

