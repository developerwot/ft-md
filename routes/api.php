<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*Route::any('{slug}', [
    'uses' => 'ApiController@index'
])->where('slug', '([A-Za-z0-9\-\/]+)');*/

/*Route::group([
    'prefix' => 'data',
    'middleware' => ['log_request']
], function()
{*/
    Route::get('/test', function (Request $request) {
        return response()->json(['name' => 'test']);
    });
    Route::post('emp/login','EmployeeApiController@login');
    Route::post('emp_temp/store', 'EmployeeTempratureController@store');
    Route::post('emp/get_data', 'EmployeeApiController@get_data');
    Route::post('emp/data_store', 'EmployeeApiController@data_store');
    Route::post('emp/store', 'EmployeeApiController@store');
    Route::post('emp/profile', 'EmployeeApiController@profile');
    Route::post('emp_profile/update', 'EmployeeApiController@profileUpdate');
    Route::post('emp/password/update', 'EmployeeApiController@passwordUpdate');
    Route::post('emp/get_temperature', 'EmployeeApiController@employeeTemperature');
    Route::post('emp/get_temperature/monthWise', 'EmployeeApiController@monthWiseTemperature');
    Route::post('booking/get_buildings','BookingApiController@getbuildings');
    Route::post('booking/get_locations','BookingApiController@getLocations');
    Route::post('booking/get_timeslots','BookingApiController@getTimeSlots');
    Route::post('booking/invitation/send','BookingInvitationApiController@locationInvitation');
    Route::post('booking/invitation/get_invitations','BookingInvitationApiController@getInvitation');
    Route::post('booking/invitation/accept_invitation','BookingInvitationApiController@acceptInvitation');
    Route::post('booking/invitation/reject_invitation','BookingInvitationApiController@rejectInvitations');
    Route::post('booking/invitation/delete_invitation','BookingInvitationApiController@deleteInvitation');
    Route::post('booking/get_floors','BookingApiController@getFloors');
    Route::post('booking/get_desks','BookingApiController@getDesks');
    Route::post('booking/reservation/create','BookingApiController@bookTimeSlot');
    Route::post('booking/desk_booking','BookingApiController@bookDesk');
    Route::post('booking/reservation/qr_code','BookingApiController@scanQrCode');
    Route::post('booking/reservation/list','BookingApiController@bookings');
    Route::post('booking/reservation/cancel','BookingApiController@cancel');
    Route::post('guest_user/store', 'GuestUserApiController@store');
    Route::post('guest_user/complete', 'GuestUserApiController@complete');
    Route::post('guest_user/existing_data', 'GuestUserApiController@existing_data');
    Route::post('guest_user/document-upload', 'GuestUserApiController@document_upload');
    Route::post('guest_user/verify', 'GuestUserApiController@verify');
    Route::post('guest_user/invitation', 'GuestUserApiController@invitation');
    Route::post('guest_user/guest_users_lists', 'GuestUserApiController@guest_users_lists');
    Route::post('guest_user_detail/store', 'GuestUserDataApiController@store');
    Route::post('emp/attendance/store', 'PeopleApiAttendanceController@store');
    Route::post('people_registry/update', 'PeopleRegistryApiController@update');
    Route::post('people_registry/check', 'PeopleRegistryApiController@checkPeopleRegistry');
    Route::post('people_registry/getTrainedIds', 'PeopleRegistryApiController@getTrainedIds');
    Route::post('company/getDeviceSettings', 'CompanyApiController@getDeviceSettings');
    Route::post('company/login', 'CompanyApiController@login');
    Route::post('company/getLocationDeviceSettings', 'CompanyApiController@getLocationDeviceSetting');
    Route::post('devices/beacon/data', 'ContactTracingApiController@store');


    Route::post('collision/store', 'CollisionAccessApiController@storeCollision');
    Route::post('collision/stats/overall', 'CollisionAccessApiController@getOverAllStates');
    Route::post('collision/per-day-report', 'CollisionAccessApiController@getCollisionPerDayReport');

    Route::post('invitation/employees','EmployeeInviteApiController@getEmployees');
    Route::post('invitation/send','EmployeeInviteApiController@sendInvitations');

//    Route::any('/{any}', function ($any) {
//
//        return response()->json([
//            "status"=>1,
//        ], 200);
//    })->where('any', '.*');
//});
