<?php

use Illuminate\Database\Seeder;
use App\Role;

class AddEmployeeRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $registered_user = new Role();
        $registered_user->name = 'employee';
        $registered_user->display_name = 'Employee'; // optional
        $registered_user->description = 'Employee'; // optional
        $registered_user->save();
    }
}
