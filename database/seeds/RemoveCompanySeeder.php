<?php

use Illuminate\Database\Seeder;

class RemoveCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->truncate();
        DB::table('employee')->truncate();
        DB::table('employee_temperature')->truncate();
    }
}
