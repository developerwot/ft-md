<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt('123456');
        $data = [
            [
                'first_name' =>'Super admin',
                'last_name' =>'',
                'email' => 'admin@mailinator.com',
                'password' => $password,
                'status' => 1,
            ]
        ];


        \App\User::truncate();

        foreach ($data as $user){
            $user = \App\User::firstOrCreate($user);
            $user->syncRoles([1]);
        }
    }
}
