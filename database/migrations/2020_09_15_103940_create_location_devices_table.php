<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_devices', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->integer('building_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->string('camera_name')->nullable();
            $table->string('device_type')->nullable();
            $table->string('device_location')->nullable();
            $table->string('green_relay')->nullable();
            $table->string('green_relay_command')->nullable();
            $table->string('red_relay')->nullable();
            $table->string('red_relay_command')->nullable();
            $table->string('green_relay_duration')->nullable();
            $table->string('red_relay_duration')->nullable();
            $table->string('video_stream_url')->nullable();
            $table->string('host')->nullable();
            $table->string('port')->nullable();
            $table->integer('face_detection')->nullable();
            $table->integer('temperature_detection')->nullable();
            $table->string('face_detect_seconds')->nullable();
            $table->string('recognize_at_seconds')->nullable();
            $table->string('temperature_check')->nullable();
            $table->string('temperature_check_end')->nullable();
            $table->string('temperature_threshold')->nullable();
            $table->string('door_open_for_green_at')->nullable();
            $table->string('door_open_for_red_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_devices');
    }
}
