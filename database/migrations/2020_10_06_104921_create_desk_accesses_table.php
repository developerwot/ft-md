<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeskAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desk_accesses', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->integer('building_id')->nullable();
            $table->integer('floor_id')->nullable();
            $table->string('desk_name')->nullable();
            $table->integer('desk_status')->nullable();
            $table->string('desk_QR')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desk_accesses');
    }
}
