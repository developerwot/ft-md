<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_invitations', function (Blueprint $table) {
            $table->id();
            $table->integer('employee_id');
            $table->integer('company_id');
            $table->integer('invited_to');
            $table->integer('slot_id');
            $table->string('type');
            $table->string('status');
            $table->date('invited_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_invitations');
    }
}
