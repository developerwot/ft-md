<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRiskSetingsInSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->integer('low')->nullable()->after('hr_email');
            $table->integer('medium')->nullable()->after('low');
            $table->integer('high')->nullable()->after('medium');
            $table->string('low_color')->nullable()->after('high');
            $table->string('medium_color')->nullable()->after('low_color');
            $table->string('high_color')->nullable()->after('medium_color');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('low');
            $table->dropColumn('medium');
            $table->dropColumn('high');
            $table->dropColumn('low_color');
            $table->dropColumn('medium_color');
            $table->dropColumn('high_color');
        });
    }
}
