<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsPolicyAddedColumnToDeskAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('desk_accesses', function (Blueprint $table) {
            $table->integer('is_policy_added')->default('0')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('desk_accesses', function (Blueprint $table) {
           $table->dropColumn('is_policy_added');
        });
    }
}
