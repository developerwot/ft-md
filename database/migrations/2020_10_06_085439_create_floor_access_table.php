<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloorAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('floor_accesses', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->integer('building_access_id')->nullable();
            $table->string('floor_name')->nullable();
            $table->integer('floor_status')->nullable();
            $table->integer('floor_limit')->nullable();
            $table->string('floor_QR')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('floor_accesses');
    }
}
