<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCodeGuestUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guest_users', function (Blueprint $table) {
            $table->string('verify_code')->nullable()->after('email_address');
            $table->integer('is_verified')->nullable()->after('verify_code');
            $table->date('verify_at')->nullable()->after('is_verified');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guest_users', function (Blueprint $table) {
            $table->dropColumn('verify_code');
            $table->dropColumn('is_verified');
            $table->dropColumn('verify_at');
        });
    }
}
