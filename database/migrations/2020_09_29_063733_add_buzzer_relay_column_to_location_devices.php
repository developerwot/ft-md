<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBuzzerRelayColumnToLocationDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('location_devices', function (Blueprint $table) {
            $table->string('buzzer_relay')->after('red_relay_duration')->nullable();
            $table->string('buzzer_relay_duration')->after('buzzer_relay')->nullable();
            $table->string('buzzer_relay_command')->after('buzzer_relay_duration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location_devices', function (Blueprint $table) {
            $table->dropColumn('buzzer_relay');
            $table->dropColumn('buzzer_relay_duration');
            $table->dropColumn('buzzer_relay_command');
        });
    }
}
