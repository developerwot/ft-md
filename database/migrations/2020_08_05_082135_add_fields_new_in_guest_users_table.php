<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsNewInGuestUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guest_users', function (Blueprint $table) {
            $table->integer('is_dob')->nullable()->after('is_last_name');
            $table->integer('is_address_proof')->nullable()->after('is_telephone_number');
            $table->integer('is_identity_proof')->nullable()->after('is_address_proof');
            $table->integer('is_company_name')->nullable()->after('is_identity_proof');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guest_users', function (Blueprint $table) {
            $table->dropColumn('is_address_proof');
            $table->dropColumn('is_identity_proof');
            $table->dropColumn('is_company_name');
            $table->dropColumn('is_dob');
        });
    }
}
