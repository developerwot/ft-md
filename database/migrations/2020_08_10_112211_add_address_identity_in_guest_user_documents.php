<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressIdentityInGuestUserDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guest_user_documents', function (Blueprint $table) {
            $table->string('address_proof')->nullable()->after('guest_user_id');
            $table->string('identity_proof')->nullable()->after('address_proof');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guest_user_documents', function (Blueprint $table) {
            $table->dropColumn('address_proof');
            $table->dropColumn('identity_proof');
        });
    }
}
