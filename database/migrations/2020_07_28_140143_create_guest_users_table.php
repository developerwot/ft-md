<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_users', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('is_first_name')->nullable();
            $table->integer('is_last_name')->nullable();
            $table->date('is_dob')->nullable();
            $table->integer('is_address')->nullable();
            $table->integer('is_city')->nullable();
            $table->integer('is_postal_code')->nullable();
            $table->integer('is_telephone_number')->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->string('email_address')->nullable();
            $table->integer('receiver_employee_id');
            $table->text('instructions_upon_arrival');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_users');
    }
}
