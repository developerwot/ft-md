<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsRangeAlwaysBuildingGroupInAccessPolicy extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('building_access_polices', function (Blueprint $table) {
            $table->string('always_range')->nullable();
            $table->integer('group_id')->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->string('from_hour')->nullable();
            $table->string('to_hour')->nullable();
            $table->string('permit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('building_access_polices', function (Blueprint $table) {
            $table->dropColumn('always_range');
            $table->dropColumn('group_id');
            $table->dropColumn('from_date');
            $table->dropColumn('to_date');
            $table->dropColumn('from_hour');
            $table->dropColumn('to_hour');
            $table->dropColumn('permit');
        });
    }

}
