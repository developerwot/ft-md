<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_accesses', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('building_access_id');
            $table->string('location_name')->nullable();
            $table->string('location_status')->nullable();
            $table->string('location_limit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_accesses');
    }
}
