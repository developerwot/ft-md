<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeskBookingPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desk_booking_policies', function (Blueprint $table) {
            $table->id();
            $table->integer('building_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('floor_id')->nullable();
            $table->json('desk_ids')->nullable();
            $table->integer('is_permitted')->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desk_booking_policies');
    }
}
