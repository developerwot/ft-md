<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
           $table->string('host')->nullable()->after('hr_email');
           $table->string('port')->nullable()->after('host');
           $table->string('video_stream_url')->nullable()->after('port');
           $table->string('face_detect_at_seconds')->nullable()->after('video_stream_url');
           $table->string('recognize_at_seconds')->nullable()->after('face_detect_at_seconds');
           $table->string('temperature_check')->nullable()->after('recognize_at_seconds');
           $table->string('temperature_check_end')->nullable()->after('temperature_check');
           $table->string('temperature_threshold')->nullable()->after('temperature_check_end');
           $table->string('door_open_for_green_at')->nullable()->after('temperature_threshold');
           $table->string('door_open_for_red_at')->nullable()->after('door_open_for_green_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('host');
            $table->dropColumn('port');
            $table->dropColumn('video_stream_url');
            $table->dropColumn('face_detect_at_seconds');
            $table->dropColumn('recognize_at_seconds');
            $table->dropColumn('temperature_check');
            $table->dropColumn('temperature_check_end');
            $table->dropColumn('temperature_threshold');
            $table->dropColumn('door_open_for_green_at');
            $table->dropColumn('door_open_for_red_at');

        });
    }
}
