@extends('backend.layouts.auth_master')

@section('content')
    <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid wizard" id="kt_login" data-wizard-state="first">
        <!--begin::Content-->
        <div class="login-container d-flex flex-center flex-row flex-row-fluid order-2 order-lg-1 flex-row-fluid bg-white py-lg-0 pb-lg-0 pt-15 pb-12">
            <!--begin::Wrapper-->
            <!--begin::Signin-->
            <div class="login-content login-content-signup d-flex flex-column">
                <!--begin::Logo-->

                <div class="d-flex flex-column-auto flex-column px-10">
                    <!--begin::Aside header-->
                    <a href="{{ route('login') }}" class="login-logo pb-lg-4 pb-10">
                        {{--<img src="{{ asset('theme/media/logos/logo-4.png') }}" class="max-h-70px" alt="">--}}
                        <img src="{{ asset('images/site_logo.png') }}" class="max-h-150px" alt="">
                    </a>
                    <!--end::Aside header-->

                    <!--begin: Wizard Nav-->
                    <div class="wizard-nav  pb-10">
                        <!--begin::Wizard Steps-->
                        <div class="wizard-steps d-flex flex-column flex-sm-row">
                            <!--begin::Wizard Step 1 Nav-->
                            <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="current">
                                <div class="wizard-wrapper pr-7">
                                    <div class="wizard-icon">
                                        <i class="wizard-check ki ki-check"></i>
                                        <span class="wizard-number">1</span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">
                                            Account
                                        </h3>
                                        <div class="wizard-desc">
                                            Account details
                                        </div>
                                    </div>
                                    <span class="svg-icon pl-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
                                            <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
                                        </g>
                                    </svg>
                                </span>
                                </div>
                            </div>
                            <!--end::Wizard Step 1 Nav-->

                            <!--begin::Wizard Step 2 Nav-->
                            <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="pending">
                                <div class="wizard-wrapper pr-7">
                                    <div class="wizard-icon">
                                        <i class="wizard-check ki ki-check"></i>
                                        <span class="wizard-number">2</span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">
                                            Address
                                        </h3>
                                        <div class="wizard-desc">
                                            Residential address
                                        </div>
                                    </div>
                                    <span class="svg-icon pl-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
                                            <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
                                        </g>
                                    </svg>
                                </span>
                                </div>
                            </div>
                            <!--end::Wizard Step 2 Nav-->

                            <!--begin::Wizard Step 3 Nav-->

                            <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="pending">
                                <div class="wizard-wrapper">
                                    <div class="wizard-icon">
                                        <i class="wizard-check ki ki-check"></i>
                                        <span class="wizard-number">3</span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">
                                            Complete & Review
                                        </h3>
                                        <div class="wizard-desc">
                                            Submit form
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Wizard Step 3 Nav-->
                        </div>
                        <!--end::Wizard Steps-->
                    </div>
                    <!--end: Wizard Nav-->
                </div>
                <div class="login-form">
                    <!--begin::Form-->

                    <form class="form px-10 fv-plugins-bootstrap fv-plugins-framework" novalidate="novalidate" id="kt_login_signup_form"  method="POST" action="{{ route('company.register') }}" enctype="multipart/form-data">

                    @csrf
                    <!--begin: Wizard Step 1-->
                        <div class=" " data-wizard-type="step-content" data-wizard-state="current">
                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-12">
                                <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Create Account</h3>
                                <div class="text-muted font-weight-bold font-size-h4">
                                    Already have an Account ?
                                    <a href="{{ route('login') }}" class="text-primary font-weight-bolder">Sign In</a>
                                </div>
                            </div>
                            <!--begin::Title-->

                            <!--begin::Form Group-->
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group fv-plugins-icon-container">
                                        <label class="font-size-h6 font-weight-bolder text-dark">First Name</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="first_name" placeholder="First Name" value="{{ old('first_name') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-group fv-plugins-icon-container">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Last Name</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6 " name="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group fv-plugins-icon-container">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Email</label>
                                        <input type="email" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="email" placeholder="Email" value="{{ old('email') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Phone</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="phone" placeholder="Phone" value="{{ old('phone') }}">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Password</label>
                                        <input type="password" id="pwdId" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="password" placeholder="Password" >
                                        <div class="fv-plugins-message-container"></div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Confirm Password</label>
                                        <input type="password" id="cPwdId"  class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="password_confirmation" placeholder="Password">
                                        <div class="fv-plugins-message-container"></div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>--}}
                            <!--end::Form Group-->
                        </div>
                        <!--end: Wizard Step 1-->

                        <!--begin: Wizard Step 2-->
                        <div class="pb-5" data-wizard-type="step-content">
                            <!--begin::Title-->
                            <div class="pt-lg-0 pt-5 pb-15">
                                <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Address Details</h3>
                            </div>
                            <!--begin::Title-->

                            <!--begin::Row-->
                            <div class="row">
                                <div class="col-xl-12">
                                    <!--begin::Input-->
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Business Name</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6 " name="business_name" placeholder="Business Name" value="{{ old('business_name') }}">
                                        <div class="fv-plugins-message-container"></div>
                                    </div>
                                </div>
                                <!--end::Input-->
                            </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <!--begin::Input-->
                                    <div class="form-group">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Address </label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="address" placeholder="Address" value="{{ old('address')  }}">
                                        <div class="fv-plugins-message-container"></div>
                                    </div>
                                    <!--end::Input-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <!--begin::Input-->
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">City</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6 " name="city" placeholder="City" value="{{ old('city') }}">
                                        <div class="fv-plugins-message-container"></div></div>
                                    <!--end::Input-->
                                </div>
                                <div class="col-xl-6">
                                    <!--begin::Input-->
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Postal Code</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6 " name="postal_code" placeholder="Postal Code" value="{{ old('postal_code') }}">
                                        <div class="fv-plugins-message-container"></div></div>
                                    <!--end::Input-->
                                </div>
                            </div>
                            <!--end::Row-->
                            <!--begin::Row-->
                            <div class="row">
                                <div class="col-xl-6">
                                    <!--begin::Select-->
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Country</label>
                                        <select name="country" id="countryID" class="form-control form-control-solid h-auto py-7 px-5 border-0 rounded-lg font-size-h6 ">
                                            @php $countries = \App\Http\Controllers\Controller::getCountries(); @endphp
                                            <option value="">Select</option>
                                            @foreach($countries as $ckey=>$country)
                                                <option value="{{ $ckey }}" @if($ckey == old('country')) selected @endif >{{ $country }}</option>
                                            @endforeach
                                        </select>
                                        <div class="fv-plugins-message-container"></div></div>
                                    <!--end::Input-->
                                </div>
                                <div class="col-xl-6">
                                    <!--begin::Input-->
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Vat</label>
                                        <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6 " name="vat" placeholder="Vat" value="{{ old('vat') }}">
                                        <div class="fv-plugins-message-container"></div></div>
                                    <!--end::Input-->
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-xl-6">
                                    <!--begin::Input-->
                                    <div class="form-group fv-plugins-icon-container ">
                                        <label class="font-size-h6 font-weight-bolder text-dark">Company Logo</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input " name="logo" id="customFile" value="">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                        <div class="fv-plugins-message-container"></div>
                                    </div>
                                    <!--end::Input-->
                                </div>
                            </div>

                            <!--end::Row-->
                        </div>
                        <!--end: Wizard Step 2-->


                        <div class="pb-5" data-wizard-type="step-content">
                            <!--begin::Title-->
                            <div class="pt-lg-0 pt-5 pb-15">
                                <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Complete</h3>
                                <div class="text-muted font-weight-bold font-size-h4">
                                    Complete Your Signup And Become A Member!
                                </div>
                            </div>
                            <h4 class="font-weight-bolder mb-3">
                                Accoun Settings:
                            </h4>
                            <div class="text-dark-50 font-weight-bold line-height-lg mb-8 accountSettings">
                                <div class="userName"></div>
                                <div class="userPhone"></div>
                                <div class="userEmail"></div>
                                <div class="userBusiness"></div>
                            </div>
                            <h4 class="font-weight-bolder mb-3">
                                Address Details:
                            </h4>
                            <div class="text-dark-50 font-weight-bold line-height-lg mb-8">
                                <div class="userAddress"></div>
                                <div class="userCity"></div>
                                <div class="userCountry"></div>
                                <div class="postalCode"></div>
                                <div class="uservat"></div>
                            </div>


                        </div>
                        <!--end: Wizard Step 5-->

                        <!--begin: Wizard Actions-->
                        <div class="d-flex justify-content-between pt-7">
                            <div class="mr-2">
                                <button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 pr-8 pl-6 py-4 my-3 mr-3" data-wizard-type="action-prev">
                                <span class="svg-icon svg-icon-md mr-2"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo8/dist/assets/media/svg/icons/Navigation/Left-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"></rect>
        <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>                                Previous
                                </button>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-submit" id="kt_login_signup_form_submit_button">
                                    Submit
                                    <span class="svg-icon svg-icon-md ml-2"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo8/dist/assets/media/svg/icons/Navigation/Right-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </button>

                                <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">
                                    Next
                                    <span class="svg-icon svg-icon-md ml-2"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo8/dist/assets/media/svg/icons/Navigation/Right-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"></polygon>
        <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "></path>
    </g>
</svg><!--end::Svg Icon--></span>                            </button>
                            </div>
                        </div>
                        <!--end: Wizard Actions-->
                        <div></div><div></div></form>
                    <!--end::Form-->
                </div>
                <!--end::Logo-->


            </div>
            <!--end::Signin-->
            <!--end::Wrapper-->
        </div>
        <!--begin::Content-->

        <!--begin::Aside-->
        <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
            <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom" style="background-image: url({{--{{ asset('theme/media/svg/illustrations/login-visual-4.svg') }}--}});">
                <!--begin::Aside title-->
                <h3 class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
                    Access Control Portal
                </h3>
                <!--end::Aside title-->
            </div>
        </div>
        <!--end::Aside-->
    </div>

@endsection
@section('scripts')


@endsection