@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/site_logo.png') }}"  height="150px" border="0" alt=""/>
        @endcomponent
    @endslot
    @slot('subcopy')
        <br>
        Dear {{$email}},
        <br><br>
        <p> You have invited successfully , Click below link to get app.</p>
        <p><a href="{{$guest_user_link}}">App Link </a> </p>
        <p>Your app verify code <strong>{{ $verify_code }}</strong></p><br>


            Thanks!<br>
            {{config('app.name')}}

            @endslot
            {{-- Footer --}}
            @slot('footer')
                @component('mail::footer')
                    ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



