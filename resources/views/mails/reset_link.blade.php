@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/site_logo.png') }}"  height="150px" border="0" alt=""/>
        @endcomponent
    @endslot
    @slot('subcopy')
        <br>
        Dear {{$user->first_name}},
        <br><br>
        <p> Please click on below button to reset your password !!</p><br>

        @component('mail::button', ['url' => $reset_link])
            Reset Password
        @endcomponent

        Thanks!<br>
        {{config('app.name')}}

    @endslot
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



