@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/site_logo.png') }}"  height="150px" border="0" alt=""/>
        @endcomponent
    @endslot
    @slot('subcopy')
        <br>
        Dear {{$username}},
        <br><br>
        <p> You have register successfully </p><br>
        <p>Your Login Password <strong>{{ $newPassword }}</strong></p>


            Thanks!<br>
            {{config('app.name')}}

            @endslot
            {{-- Footer --}}
            @slot('footer')
                @component('mail::footer')
                    ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



