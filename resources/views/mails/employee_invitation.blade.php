@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/site_logo.png') }}"  height="150px" border="0" alt=""/>
        @endcomponent
    @endslot
    @slot('subcopy')
        <br>
        Dear {{$name}},
        <br><br>
        <p> You are invited by {{$inviterName.''.$inviterLastName}} on {{$locationName}} at {{$startTime}} </p>
            <p>Click below link to get app.</p>
        <p><a href="{{$link}}">App Link </a> </p>
        <p>Use your email & password to login ask your manager for these.</p>
        <p>Or you can reset password using link below</p>
        <p><a href="{{route('password.request')}}">Reset password</a> </p>
        Thanks!<br>
        {{config('app.name')}}

    @endslot
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



