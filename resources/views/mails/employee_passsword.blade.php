@component('mail::layout')
    {{-- Header --}}
    @slot('header')

    @endslot
    @slot('subcopy')
        <br>
        Dear {{$email}},
        <br><br>
        <p>{{$company->business_name}} Added you to check-in for secure access at its headquarters.</p>
        <p>If you haven't downloaded the intellyScan APP yet, you can do it now by clicking here.</p>
        <p><a href="{{$guest_user_link}}">App Link </a> </p>
        <p>Enter your company Email within the app and share the requested data directly with the company, no other data stored within the app will be shared.</p>
        <p>PASSWORD: <strong>{{ $password }}</strong></p><br>
        <p>Our innovative access control system will allow us to carry out a quick check-in at the entrance without an operator allowing you to safely access our company on the days indicated.</p>
        <p>We remind you that access will be allowed upon your recognition and verification of suitability (body temperature below {{$company->settings['temperature_threshold']}}°) which will be performed automatically by the intellyScan solution at the entrance.</p>
        <p>For more information on how our solution works, visit the intellyScan website by clicking here.</p>
        <p><a href="{{env('SITE_URL')}}">{{env('SITE_URL')}}</a></p>
        Thanks!<br>
        {{config('app.name')}}<br>
        {{env('APP_SLOGAN')}}<br>
        <img src="https://intellyscan.com/images/intellyScan_logo_testo150.png"  height="70px" border="0" alt=""/>


    @endslot
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



