@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/site_logo.png') }}"  height="150px" border="0" alt=""/>
        @endcomponent
    @endslot
    @slot('subcopy')
        <br>
        Dear {{$admin}},
        <br><br>
        <p> {{ $username }} has been reported with {{ $temp }} temperature today. Please take necessary action on this.</p><br>



        Thanks!<br>
        {{config('app.name')}}

    @endslot
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



