@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/site_logo.png') }}"  height="150px" border="0" alt=""/>
        @endcomponent
    @endslot
    @slot('subcopy')
        <br>
        <p>{!! $message !!}</p>

        Thanks!<br>
        {{config('app.name')}}<br>
        {{env('APP_SLOGAN')}}<br>
        <img src="https://intellyscan.com/images/intellyScan_logo_testo150.png"  height="70px" border="0" alt=""/>
    @endslot
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



