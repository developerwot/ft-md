@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            
        @endcomponent
    @endslot
    @slot('subcopy')
@foreach ($introLines as $line)
{!! $line !!}

@endforeach

            @endslot
            {{-- Footer --}}
            @slot('footer')
                @component('mail::footer')
                    ©{{ date('Y') }} {{config('app.name')}}. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



