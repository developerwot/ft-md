@extends('backend.layouts.master')
@section('content')
<div class="main d-flex flex-column flex-row-fluid">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($employee) ? 'Update People' : 'Add People' !!}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="{{ route('employee.index') }}" class="text-muted">People</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ isset($employee) ? route('employee.index').'/'.$employee->id.'/edit' : route('employee.create') }} " class="text-muted">{!! isset($employee) ? 'Update People' : 'Add People' !!}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Daterange-->
                <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                    <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                    <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                </a>
                <!--end::Daterange-->
                <!--begin::Dropdown-->

                <!--end::Dropdown-->
            </div>

            <!--end::Toolbar-->
        </div>

    </div>
    @if(isset($Employees))

    <div class="alert alert-warning" role="alert">
        {{auth()->user()->company->employee_limit!=null ? 'Company Employee limit is '.auth()->user()->company->employee_limit. ' & '. $Employees.' employees are already added!' : 'Employee limit is not added!'  }}
    </div>

    @endif
    <!--end::Subheader-->
    <div class="content flex-column-fluid" id="kt_content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">{!! isset($employee) ? 'Update People' : 'Add People' !!} </h3>

                    </div>

                    <!--begin::Form-->
                    <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                          action="{{ isset($employee) ? route('employee.update', $employee->id) : route('employee.store')}}"
                          method="POST" enctype="multipart/form-data">
                        @csrf
                        @if(isset($employee))
                        @method('PUT')
                        @endif
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="form-control-label">First Name</label>
                                    <input type="text" class="form-control"  name="first_name" id="first_name" value="{!! isset($employee) ? $employee->first_name: old('first_name') !!}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="form-control-label">Last Name</label>
                                    <input type="text" class="form-control"  name="last_name" id="last_name" value="{!! isset($employee) ? $employee->last_name: old('last_name') !!}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="form-control-label">Email</label>
                                    <input type="text" class="form-control"  name="email" id="email" value="{!! isset($employee) ? $employee->email: old('email') !!}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6 ">
                                    <label class="form-control-label">Group </label>
                                    <select class="form-control select2" id="group_id" name="group_id[]" multiple>
                                        <option value=""> Select Group</option>

                                        @if($groups)
                                        @foreach($groups as $group)
                                        <option {!! (isset($employee) && in_array($group->id,$employee->groups2))  ? 'selected': '' !!} value="{{ $group->id }}">{{ $group->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="form-control-label">Status</label>
                                    <select class="form-control" name="status">
                                        @php $statusArry = ['1'=>'Active','0'=>'Inactive'];@endphp
                                        @foreach($statusArry as $skey=>$status)
                                        <option value="{{ $skey }}" {!! ((isset($employee) && $employee->status == $skey))  ? 'selected': '' !!}>{{ $status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="form-control-label">Password</label>
                                    <input type="password" class="form-control"  name="password"  value="" />
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="form-control-label">Confirm Password</label>
                                    <input type="password" class="form-control"  name="password_confirmation"  value="" />
                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!--end::Code example-->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                            <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href ='{{ route('employee.index') }}'">Cancel</button>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Card-->
                <!--begin::Card-->

                <!--end::Card-->
            </div>

        </div>
    </div>
    <!--end::Content-->
</div>
@endsection

@section('scripts')
<script>
    var KTFormControls = function () {
        // Private functions
        var _initDemo1 = function () {
             $('#group_id').select2({placeholder: "Select Group"});
            FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            first_name: {
                                validators: {
                                    notEmpty: {
                                        message: 'First Name is required'
                                    },

                                }
                            },
                            last_name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Last Name is required'
                                    },

                                }
                            },
                            email: {
                                validators: {
                                    notEmpty: {
                                        message: 'Email is required'
                                    },
                                    emailAddress: {
                                        message: 'The value is not a valid email address'
                                    }
                                }
                            },
                        },

                        plugins: {//Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
            );
        }
        return {
            // public functions
            init: function () {
                _initDemo1();

            }
        };
    }();

    jQuery(document).ready(function () {
        KTFormControls.init();
    });


</script>
@endsection
