@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($group) ? 'Update Group' : 'Add Group' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('groups.index') }}" class="text-muted">Groups</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($group) ? route('groups.index').'/'.$group->id.'/edit' : route('groups.create') }} " class="text-muted">{!! isset($group) ? 'Update Group' : 'Add Group' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($group) ? 'Update Group' : 'Add Group' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($group) ? route('groups.update', $group->id) : route('groups.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            @if(isset($group))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Name</label>
                                        <input type="text" name="name" class="form-control" value="{!! isset($group) ? $group->name: old('name') !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Description</label>
                                        <input type="text" name="description" class="form-control" value="{!! isset($group) ? $group->description: old('description') !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select People</label>
                                        <select class="form-control select2" id="kt_select2_3" name="employee_id[]" multiple>

                                            @if(isset($group) && !empty($group->group_employees))
                                                @php $selected_emp = collect($group->group_employees)->pluck('employee_id')->toArray(); @endphp
                                            @endif
                                            @if($employees)
                                                @foreach($employees as $emp)
                                                    <option value="{{ $emp->id }}" @if(isset($group) && in_array($emp->id,$selected_emp)) selected @endif>{{ $emp->first_name.' '.$emp->last_name.' ('.$emp->email.')' }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('groups.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            employee_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'People is required'
                                    },

                                }
                            },
                            name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Group Name is required'
                                    },
                                }
                            },

                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });


    </script>
@endsection
