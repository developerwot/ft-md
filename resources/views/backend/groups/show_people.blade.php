<table class="table">
    @if(!empty($group->group_employees))
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        @foreach($group->group_employees as $group_emp)
            @if($group_emp->employee && $group_emp->employee->status == 1 )
                <tr>
                    <td>{{ $group_emp->employee->first_name }}  {{ $group_emp->employee->last_name }} </td>
                    <td>{{ $group_emp->employee->email }}</td>
                </tr>
            @endif
        @endforeach
        </tbody>
    @endif
</table>