<div class="container">
    <!--begin::Header Menu-->
    <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default header-menu-root-arrow">
        <!--begin::Header Nav-->
        <ul class="menu-nav">

            <li class="menu-item  menu-item-rel {{ (strpos(Route::currentRouteName(), 'dashboard') === 0) ? 'menu-item-open  menu-item-active' : '' }}" data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route('dashboard') }}" class="menu-link ">
                    <span class="menu-text">Dashboard</span>

                </a>

            </li>
            @if(Auth::user()->hasRole('admin'))

            <li class="menu-item  menu-item-submenu menu-item-rel {{ (strpos(Route::currentRouteName(), 'company') === 0) ? 'menu-item-open  menu-item-active' : '' }}  " data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route('company.index') }}" class="menu-link">
                    <span class="menu-text">Companies</span>
                    <span class="menu-desc"></span>

                </a>
            </li>
                {{--<li class="menu-item menu-item-submenu menu-item-rel {{ (strpos(Route::currentRouteName(), 'groups') === 0 || strpos(Route::currentRouteName(), 'employee') === 0 || strpos(Route::currentRouteName(), 'emp_temp') === 0) ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:void(0)" class="menu-link menu-toggle">
                        <span class="menu-text">Registries</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left" >
                        <ul class="menu-subnav">

                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'employee') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('employee.index') }}" class="menu-link ">
                                    <span class="menu-text">People Management</span>
                                </a>
                            </li>
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'emp_temp') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('emp_temp.index') }}" class="menu-link ">
                                    <span class="menu-text">People Temperature</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>--}}
            @endif
               
            @if( Auth::user()->hasRole('company'))
                <li class="menu-item menu-item-submenu menu-item-rel {{ ( strpos(Route::currentRouteName(), 'groups') === 0 || strpos(Route::currentRouteName(), 'employee') === 0 || strpos(Route::currentRouteName(), 'emp_temp') === 0  || strpos(Route::currentRouteName(), 'guest_users') === 0 )   ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:void(0)" class="menu-link menu-toggle">
                        <span class="menu-text">Registries</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left" >
                        <ul class="menu-subnav">
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'groups') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('groups.index') }}" class="menu-link ">
                                    <span class="menu-text">Group Management</span>
                                </a>
                            </li>
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'employee') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('employee.index') }}" class="menu-link ">
                                    <span class="menu-text">People Management</span>
                                </a>
                            </li>
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'emp_temp') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('emp_temp.index') }}" class="menu-link ">
                                    <span class="menu-text">People Temperature</span>
                                </a>
                            </li>@if(0)
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'guest_users') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('guest_users.index') }}" class="menu-link ">
                                    <span class="menu-text">Guest Users</span>
                                </a>
                            </li>
@endif
                        </ul>
                    </div>
                </li>
                {{--|| strpos(Route::currentRouteName(), 'employee') === 0 || strpos(Route::currentRouteName(), 'emp_temp') === 0  || strpos(Route::currentRouteName(), 'guest_users') === 0 )--}}
                <li class="menu-item menu-item-submenu menu-item-rel {{ ( strpos(Route::currentRouteName(), 'booking-policy') === 0 || strpos(Route::currentRouteName(), 'booking-access') === 0 || strpos(Route::currentRouteName(), 'location-access') === 0 || strpos(Route::currentRouteName(), 'timeslot-access') === 0 || strpos(Route::currentRouteName(), 'reservation') === 0 ||strpos(Route::currentRouteName(), 'floor-access') === 0 || strpos(Route::currentRouteName(), 'desk-access') === 0)   ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:void(0)" class="menu-link menu-toggle">
                        <span class="menu-text">Booking Management</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left" >
                        <ul class="menu-subnav">

                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'booking-access') === 0 ) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('booking-access.index') }}" class="menu-link ">
                                    <span class="menu-text">Building Management</span>
                                </a>
                            </li>
                            {{--<li class="menu-item {{ (strpos(Route::currentRouteName(), 'location-access') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="{{ route('location-access.index') }}" class="menu-link ">--}}
                                    {{--<span class="menu-text">Location Management</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            <li class="menu-item menu-item-submenu {{ ( strpos(Route::currentRouteName(), 'booking-policy') === 0 ||  strpos(Route::currentRouteName(), 'location-access') === 0 || strpos(Route::currentRouteName(), 'timeslot-access') === 0 )   ? 'menu-item-open  menu-item-active menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Location Management</span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-classic menu-submenu-right" data-hor-direction="menu-submenu-right">
                                    <ul class="menu-subnav">
                                        <li class="menu-item menu-item-submenu {{strpos(Route::currentRouteName(), 'location-access') === 0 ? 'menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ route('location-access.index') }}" class="menu-link">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Locations</span>
                                                {{--<i class="menu-arrow"></i>--}}
                                            </a>
                                            {{--<div class="menu-submenu menu-submenu-classic menu-submenu-right" data-hor-direction="menu-submenu-right">--}}
                                                {{--<ul class="menu-subnav">--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/controls/base.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">Base Inputs</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        </li>
                                        <li class="menu-item menu-item-submenu {{strpos(Route::currentRouteName(), 'timeslot-access') === 0 ? 'menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ route('timeslot-access.index') }}" class="menu-link ">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Time-slots</span>
                                            </a>
                                        </li>
                                        <li class="menu-item menu-item-submenu {{strpos(Route::currentRouteName(), 'booking-policy') === 0 ? 'menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ route('booking-policy.index') }}" class="menu-link">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Booking Policies</span>
                                                {{--<i class="menu-arrow"></i>--}}
                                            </a>
                                            {{--<div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
                                                {{--<ul class="menu-subnav">--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/widgets/nouislider.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">noUiSlider</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/widgets/form-repeater.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">Form Repeater</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/widgets/ion-range-slider.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">Ion Range Slider</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/widgets/input-mask.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">Input Masks</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/widgets/autosize.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">Autosize</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/widgets/clipboard.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">Clipboard</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="menu-item" aria-haspopup="true">--}}
                                                        {{--<a href="crud/forms/widgets/recaptcha.html" class="menu-link">--}}
                                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                                                {{--<span></span>--}}
                                                            {{--</i>--}}
                                                            {{--<span class="menu-text">Google reCaptcha</span>--}}
                                                        {{--</a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            {{--<li class="menu-item {{ (strpos(Route::currentRouteName(), 'timeslot-access') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="{{ route('timeslot-access.index') }}" class="menu-link ">--}}
                                    {{--<span class="menu-text">Time-slots Management</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            <li class="menu-item menu-item-submenu {{ ( strpos(Route::currentRouteName(), 'floor-access') === 0 ||strpos(Route::currentRouteName(), 'desk-access') === 0  ||strpos(Route::currentRouteName(), 'desk-booking-policy') === 0)   ? 'menu-item-open  menu-item-active menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Floor Management</span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-classic menu-submenu-right" data-hor-direction="menu-submenu-right">
                                    <ul class="menu-subnav">
                                        <li class="menu-item menu-item-submenu {{strpos(Route::currentRouteName(), 'floor-access') === 0 ? 'menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ route('floor-access.index') }}" class="menu-link">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Floors</span>
                                                {{--<i class="menu-arrow"></i>--}}
                                            </a>
                                            {{--<div class="menu-submenu menu-submenu-classic menu-submenu-right" data-hor-direction="menu-submenu-right">--}}
                                            {{--<ul class="menu-subnav">--}}
                                            {{--<li class="menu-item" aria-haspopup="true">--}}
                                            {{--<a href="crud/forms/controls/base.html" class="menu-link">--}}
                                            {{--<i class="menu-bullet menu-bullet-dot">--}}
                                            {{--<span></span>--}}
                                            {{--</i>--}}
                                            {{--<span class="menu-text">Base Inputs</span>--}}
                                            {{--</a>--}}
                                            {{--</li>--}}
                                            {{--</ul>--}}
                                            {{--</div>--}}
                                        </li>
                                        <li class="menu-item menu-item-submenu {{strpos(Route::currentRouteName(), 'desk-access') === 0 ? 'menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ route('desk-access.index') }}" class="menu-link ">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Desks</span>
                                            </a>
                                        </li>
                                        <li class="menu-item menu-item-submenu {{strpos(Route::currentRouteName(), 'desk-booking-policy') === 0 ? 'menu-item-hover' : ''}}" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ route('desk-booking-policy.index') }}" class="menu-link ">
                                                <i class="menu-bullet menu-bullet-dot">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Booking Policies</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'reservation') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('reservation.index') }}" class="menu-link ">
                                    <span class="menu-text">Reservations Management</span>
                                </a>
                            </li>
                            {{--<li class="menu-item {{ (strpos(Route::currentRouteName(), 'booking-policy') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="{{ route('booking-policy.index') }}" class="menu-link ">--}}
                                    {{--<span class="menu-text">Booking Policies</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                </li>


                <li class="menu-item menu-item-submenu menu-item-rel {{ (strpos(Route::currentRouteName(), 'building') === 0 || strpos(Route::currentRouteName(), 'building_policy') === 0 || strpos(Route::currentRouteName(), 'device_settings') === 0 || strpos(Route::currentRouteName(), 'bookings') === 0 ) ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:void(0)" class="menu-link menu-toggle">
                        <span class="menu-text">Access control</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left" >
                        <ul class="menu-subnav">
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'device_settings') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('device_settings.index') }}" class="menu-link ">
                                    <span class="menu-text">Gates Management </span>
                                </a>
                            </li>
                            {{--<li class="menu-item {{ (strpos(Route::currentRouteName(), 'building.index') === 0 || strpos(Route::currentRouteName(), 'building.create') === 0 || strpos(Route::currentRouteName(), 'building.edit') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="{{ route('building.index') }}" class="menu-link ">--}}
                                    {{--<span class="menu-text">Buildings Management</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="menu-item {{ Route::currentRouteName() }} {{ (strpos(Route::currentRouteName(), 'building_policy.index') === 0 || strpos(Route::currentRouteName(), 'building_policy.create') === 0 || strpos(Route::currentRouteName(), 'building_policy.edit') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="{{ route('building_policy.index') }}" class="menu-link ">--}}
                                    {{--<span class="menu-text">Access Policies</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="menu-item {{ (strpos(Route::currentRouteName(), 'health') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="javascript:void(0)" class="menu-link ">--}}
                                    {{--<span class="menu-text">Report Health Status </span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'bookings') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('bookings.index') }}" class="menu-link ">
                                    <span class="menu-text">Guest User Reservation</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                <li class="menu-item menu-item-submenu menu-item-rel {{ (strpos(Route::currentRouteName(), 'people_registry') === 0 || strpos(Route::currentRouteName(), 'people_attendance') === 0) ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:void(0)" class="menu-link menu-toggle">
                        <span class="menu-text">Facial Recognition</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left" >
                        <ul class="menu-subnav">
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'people_registry') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('people_registry.index') }}" class="menu-link ">
                                    <span class="menu-text">People Registry</span>
                                </a>
                            </li>
                            <li class="menu-item {{ (strpos(Route::currentRouteName(), 'people_attendance') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('people_attendance.index') }}" class="menu-link ">
                                    <span class="menu-text">Report Attendance</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>

            @endif
            @if(Auth::user()->hasRole('company'))
                <li class="menu-item menu-item-submenu menu-item-rel {{ (strpos(Route::currentRouteName(), 'devices') === 0 || strpos(Route::currentRouteName(), 'device_people') === 0 || strpos(Route::currentRouteName(), 'contact_tracing') === 0 || strpos(Route::currentRouteName(), 'risk_report') === 0 || strpos(Route::currentRouteName(), 'collision') === 0) ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:void(0)" class="menu-link menu-toggle">
                        <span class="menu-text">Track contacts</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left" >
                        <ul class="menu-subnav">
                            {{--<li class="menu-item {{ (strpos(Route::currentRouteName(), 'devices') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="{{ route('devices.index') }}" class="menu-link ">--}}
                                    {{--<span class="menu-text">Device Management</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="menu-item {{ (strpos(Route::currentRouteName(), 'device_people') === 0) ? 'menu-item-open  menu-item-active' : '' }}" aria-haspopup="true">--}}
                                {{--<a href="{{ route('device_people.index') }}" class="menu-link ">--}}
                                    {{--<span class="menu-text">Device People</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="menu-item  menu-item-rel  {{ (strpos(Route::currentRouteName(), 'contact_tracing') === 0) ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">--}}
                                {{--<a href="{{ route('contact_tracing.index') }}" class="menu-link">--}}
                                    {{--<span class="menu-text">Contact Tracing</span>--}}
                                    {{--<span class="menu-desc"></span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            <li class="menu-item  menu-item-rel  {{ (strpos(Route::currentRouteName(), 'collision') === 0) ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                                <a href="{{ route('collision.index') }}" class="menu-link">
                                    <span class="menu-text">Collision Report</span>
                                    <span class="menu-desc"></span>
                                </a>
                            </li>
                            <li class="menu-item  menu-item-rel  {{ (strpos(Route::currentRouteName(), 'risk_report') === 0) ? 'menu-item-open  menu-item-active' : '' }}"   data-menu-toggle="click" aria-haspopup="true">
                                <a href="{{ route('risk_report.index') }}" class="menu-link">
                                    <span class="menu-text">Risk Report</span>
                                    <span class="menu-desc"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif
        </ul>
        <!--end::Header Nav-->
    </div>
    <!--end::Header Menu-->
</div>
