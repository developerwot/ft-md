<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        {{config('app.name')}}
    </title>

    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>


    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->

    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('theme/css/pages/login/login-4.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('theme/plugins/global/plugins.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/css/style.bundle.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="{{ asset('images/intellyScan_logo_favicon.png') }}" type="image/x-icon"/>


    {{--<link href="{{ asset('theme/css/toastr.css')}}" rel="stylesheet" type="text/css"/>--}}
    <link href="https://codeseven.github.io/toastr/build/toastr.min.css" rel="stylesheet" type="text/css"/>

</head>
<!-- end::Head -->

<body id="kt_body" class="header-fixed subheader-enabled page-loading">
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    @yield('content')

<!--end::Main-->


<script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->

<!--begin::Global Theme Bundle(used by all pages)-->
<script src="{{ asset('theme/plugins/global/plugins.bundle.js?v=7.0.3') }}"></script>
<script src="{{ asset('theme/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.3') }}"></script>
<script src="{{ asset('theme/js/scripts.bundle.js?v=7.0.3') }}"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Vendors(used by this page)-->
<script src="{{ asset('theme/js/toastr.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/pages/custom/login/login-4.js?v=7.0.4') }}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>

<script>

    @if(session()->has('failure'))

        Swal.fire({
            html: "{{ html_entity_decode(session()->get('failure'), ENT_QUOTES, 'UTF-8') }}",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
                confirmButton: "btn font-weight-bold btn-light-primary"
            }
        }).then(function () {
            KTUtil.scrollTop();
        });
        @php
            session()->forget('failure');
        @endphp
   @endif

    @if(session()->has('success'))
            toastr.options = {
            "progressBar": true,
        };
        toastr.success("{{ session()->get('success') }}", "Success");

        @php
            //session()->forget('success');
        @endphp
   @endif
    @if(session('status'))
            toastr.options = {
            "progressBar": true,
        };
    toastr.success("{{ session('status') }}", "Success");

    @endif




    @if ($errors->any())
     @foreach ($errors->all() as $error)
        Swal.fire({
            html: "{{ html_entity_decode($error, ENT_QUOTES, 'UTF-8') }}",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
                confirmButton: "btn font-weight-bold btn-light-primary"
            }
        }).then(function () {
            KTUtil.scrollTop();
        });
    @endforeach
    @endif

</script>
@yield('scripts')
</body>
</html>
