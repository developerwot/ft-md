@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Companies</h5>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Search Form-->
                    <div class="d-flex align-items-center" id="kt_subheader_search">
                        <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{ $company->count() }} Total</span>
                        <form class="ml-5" action="javascript:void(0)">
                            <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                                <input type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search..." />
                                <div class="input-group-append">
														<span class="input-group-text">
															<span class="svg-icon">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																		<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                                            <!--<i class="flaticon2-search-1 icon-sm"></i>-->
														</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end::Search Form-->
                    <!--begin::Group Actions-->
                    <div class="d-flex- align-items-center flex-wrap mr-2 d-none" id="kt_subheader_group_actions">
                        <div class="text-dark-50 font-weight-bold">
                            <span id="kt_subheader_group_selected_rows">23</span>Selected:</div>
                        <div class="d-flex ml-6">
                            <div class="dropdown mr-2" id="kt_subheader_group_actions_status_change">
                                <button type="button" class="btn btn-light-primary font-weight-bolder btn-sm dropdown-toggle" data-toggle="dropdown">Update Status</button>
                                <div class="dropdown-menu p-0 m-0 dropdown-menu-sm">
                                    <ul class="navi navi-hover pt-3 pb-4">
                                        <li class="navi-header font-weight-bolder text-uppercase text-primary font-size-lg pb-0">Change status to:</li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link" data-toggle="status-change" data-status="1">
																	<span class="navi-text">
																		<span class="label label-light-success label-inline font-weight-bold">Approved</span>
																	</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link" data-toggle="status-change" data-status="2">
																	<span class="navi-text">
																		<span class="label label-light-danger label-inline font-weight-bold">Rejected</span>
																	</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link" data-toggle="status-change" data-status="3">
																	<span class="navi-text">
																		<span class="label label-light-warning label-inline font-weight-bold">Pending</span>
																	</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link" data-toggle="status-change" data-status="4">
																	<span class="navi-text">
																		<span class="label label-light-info label-inline font-weight-bold">On Hold</span>
																	</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <button class="btn btn-light-success font-weight-bolder btn-sm mr-2" id="kt_subheader_group_actions_fetch" data-toggle="modal" data-target="#kt_datatable_records_fetch_modal">Fetch Selected</button>
                            <button class="btn btn-light-danger font-weight-bolder btn-sm mr-2" id="kt_subheader_group_actions_delete_all">Delete All</button>
                        </div>
                    </div>
                    <!--end::Group Actions-->
                </div>
                <!--end::Details-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Button-->
                    <a href="#" class=""></a>
                    <!--end::Button-->
                    <!--begin::Button-->
                    <a href="{{ route('company.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2">Add Company</a>
                    <!--end::Button-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Companies Management
                            </h3>
                    </div>
                    <div class="card-toolbar">

                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="datatable datatable-bordered datatable-head-custom" id="user_datatable"></div>
                    <!--end: Datatable-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </div>


    @include('backend.snippets.delete')
@endsection
    @section('scripts')
    <script>

        $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('company.index')}}/delete/' + $(this).data('id'));
        });
        $(document).on('click', '.userBlock', function (e) {
            var blockUrl = '{{route("company.index")}}/block/' + $(this).data('id');
            location.href = blockUrl;

        });
        $(document).on('click', '.password-reset', function (e) {

            $.ajax({
                url: '{{route("company.reset_password")}}',
                type: "post",
                data: {"id": $(this).data('id'),"email":$(this).data('email')},
                success: function (res) {
                    toastr.options = {
                        "progressBar": true,
                    };
                    toastr.success(res.message, "Success");
                }
            })


        });
        $(document).on('click', '.cancel-button-action', function (e) {
            $('#myModal').modal();
            $(".modal-body").html('<p>Do you really want to Cancel Comapny Profile? </p>');
            $("#delete_btn").html('Yes');
            $(".cancel-btn").html('No');
            $('#delete_btn').attr('href', '{{route('company.index')}}/cancel/' + $(this).data('id'));

        });


        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('company.index')}}/' + $(this).data('id') + '/edit';
            location.href = editUrl;
        });

        var company = {!! $company !!};
        console.log('company', company);


        var DatatableDataLocalDemo = {
            init: function () {
                var e, a, i;
                e = company, a = $("#user_datatable").KTDatatable({
                    data: {
                        saveState :false,
                        type: "local",
                        source: e,
                        pageSize: 10
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#kt_subheader_search_form")},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        sortable: false,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },  {
                        field: "name",
                        title: "Name",
                        width: 150,
                        sortable: 1,
                        template: function (e, a, i) {

                            if(e.name != '')
                            {
                                return e.name + ' ' + e.last_name;
                            }
                            else
                            {
                                return '';
                            }
                        }
                        // textAlign: "center",
                    }, {
                        field: "email",
                        title: "Email",
                        width: 150,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },{
                        field: "business_name",
                        title: "Business",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },  {
                        field: "status",
                        title: "Status",
                        width: 60,
                        sortable: false,
                        overflow: "visible",
                        template: function (e, a, i) {
                            if(e.status == 1){
                               data = '<span class="label label-lg font-weight-bold label-light-success label-inline">Active</span>'

                            } else {
                                data = '<span class="label label-lg font-weight-bold label-light-danger label-inline">Inactive</span>'
                            }
                            return data;
                        }
                    }
                        ,  {
                            field: "Actions",
                            width: 120,
                            title: "Action",
                            sortable: false,
                            overflow: "visible",
                            template: function (e, a, i) {

                                var statusB = '<div class="dropdown dropdown-inline">'+
                                        '<a href="javascript:;" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" data-toggle="dropdown" title="User Action">'+
                                            '<span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo8\dist/../src/media/svg/icons\General\Settings-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                            '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                            '<rect x="0" y="0" width="24" height="24"/>'+
                                            '<path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>'+
                                            '</g>'+
                                            '</svg><!--end::Svg Icon--></span>'+
                                        '</a>'+
                                        '<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">'+
                                            '<ul class="navi flex-column navi-hover py-2">'+
                                                '<li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">'+
                                                    'Choose an action:'+
                                                '</li>'+
                                                '<li class="navi-item">'+
                                                    '<a href="javascript:void(0)" class="navi-link userBlock" data-id='+e.id+ '>'+
                                                        '<span class="navi-icon"><i class="la la-lock"></i></span>'+
                                                        '<span class="navi-text">';
                                                                if(e.status == 1) { statusB += 'Block' } else { statusB += 'Unblock'; }
                                            statusB +='</span>'+
                                                    '</a>'+
                                                '</li>'+
                                                '<li class="navi-item">'+
                                                    '<a href="javascript:void(0)" class="navi-link cancel-button-action" data-id='+e.id+ '>'+
                                                        '<span class="navi-icon"><i class="la la-user-slash"></i></span>'+
                                                        '<span class="navi-text">Cancel </span>'+
                                                    '</a>'+
                                                '</li>'+
                                                '<li class="navi-item">'+
                                                '<a href="javascript:void(0)" class="navi-link password-reset" data-id="'+e.id+ '"  data-email="'+e.email+'" >'+
                                                '<span class="navi-icon"><i class="la la-lock"></i></span>'+
                                                '<span class="navi-text">Password Reset </span>'+
                                                '</a>'+
                                                '</li>'+
                                            '</ul>'+
                                        '</div>'+
                                    '</div>';
                                var edit = ' <a href="javascript:;" data-id="' + e.id + '"'+ 'class=" edit-button btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>'+
                                    '<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>';
                                var deleteB = '<a href="javascript:;" data-id="' + e.id + '" class="delete-button-action btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon" title="Delete">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>'+
                                    '<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>';

                                if(e.user_id != 0){
                                    return statusB + edit + deleteB;
                                }
                                else {
                                    return  deleteB;
                                }


                            }
                        }
                    ]
                });

            }
        };
        jQuery(document).ready(function () {
            DatatableDataLocalDemo.init();
        });
    </script>
@endsection
