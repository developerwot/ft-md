@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> Add Company</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('company.index') }}" class="text-muted">Company</a>
                            </li>
                            <li class="breadcrumb-item">
                                @if(isset($company))
                                    <a href="{{ route('company.index').'/'.$company->id.'/edit' }}" class="text-muted">Update Company</a>
                                @else
                                    <a href="{{ route('company.create') }}" class="text-muted">Add Company</a>
                                @endif

                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            @if(isset($company))
                                <h3 class="card-title">Update Company </h3>
                             @else
                                <h3 class="card-title">Add Company Email To Register </h3>
                             @endif
                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($company) ? route('company.update', $company->id) : route('company.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            @if(isset($company))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                            @if(!isset($company))
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Email</label>
                                        <input type="text" class="form-control"  name="email" id="email"/>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Name</label>
                                        <input type="text" class="form-control"  name="name" id="name" value="{!! isset($company) ? $company->name :'' !!}"/>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Last Name</label>
                                        <input type="text" class="form-control"  name="last_name" id="last_name" value="{!! isset($company) ? $company->last_name :'' !!}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Phone</label>
                                        <input type="text" class="form-control"  name="phone" id="phone" value="{!! isset($user) ? $user->phone :'' !!}"/>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Busines Name</label>
                                        <input type="text" class="form-control"  name="business_name" id="business_name" value="{!! isset($company) ? $company->business_name :'' !!}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Address</label>
                                        <input type="text" class="form-control"  name="address" id="address" value="{!! isset($company) ? $company->address :'' !!}"/>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">City</label>
                                        <input type="text" class="form-control"  name="city" id="city" value="{!! isset($company) ? $company->city :'' !!}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Postal Code</label>
                                        <input type="text" class="form-control"  name="postal_code" id="postal_code" value="{!! isset($company) ? $company->postal_code :'' !!}"/>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Country</label>
                                        <select name="country" id="countryID" class="form-control">
                                            @php $countries = \App\Http\Controllers\Controller::getCountries(); @endphp
                                            <option value="">Select</option>
                                            @foreach($countries as $ckey=>$country)
                                                <option value="{{ $ckey }}" @if(isset($company) && $company->country == $ckey) selected @endif >{{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Vat</label>
                                        <input type="text" class="form-control"  name="vat" id="vat" value="{!! isset($company) ? $company->vat :'' !!}"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="form-check-label mt-10">Tracking service</label>
                                    </div>
                                    <div class="form-group col-lg-4">

                                        <br><input type="checkbox" name="is_tracking_enabled" id="tracking" class="form-control" data-toggle="toggle" data-on="Enabled" data-off="Disabled"{{isset($company) ?( $company->is_tracking_enbaled==1 ? 'checked' : '') :''}}>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Status</label>
                                        <select class="form-control" name="status">
                                            @php $statusArry = ['1'=>'Active','0'=>'Inactive'];@endphp
                                            @foreach($statusArry as $skey=>$status)
                                                <option value="{{ $skey }}" {!! ((isset($company) && $company->status == $skey))  ? 'selected': '' !!}>{{ $status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="form-check-label mt-10">Booking service</label>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <br><input type="checkbox" id="booking" name="is_booking_enabled" class="form-control" data-toggle="toggle" data-on="Enabled" data-off="Disabled" {{isset($company) ?( $company->is_booking_enabled==1 ? 'checked' : '') :''}}>
                                    </div>
                                </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label class="form-control-label">Employees limit</label>
                                            <input type="number" class="form-control"  name="employee_limit" id="employee_limit" value="{!! isset($company) ? $company->employee_limit :'' !!}"/>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="form-check-label mt-10">People Link</label>
                                        </div>
                                        <div class="form-group col-lg-4">
                                            <br><input type="checkbox"  id="peopleLink" name="people_link" class="form-control" data-toggle="toggle" data-on="Enabled" data-off="Disabled"{{isset($company) ?( $company->customer_id!=null ? 'checked' : '') :''}}>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label ">Gates limit</label>
                                        <input type="number" class="form-control"  name="gate_limit" id="gate_limit" value="{!! isset($company) ? $company->gate_limit :'' !!}"/>
                                    </div>
                                        <div class="form-group col-lg-6" id="customerID"   @if(isset($company) && $company->customer_id==null) style="display: none" @endif>
                                            <label class="form-control-label ">Customer id</label>
                                            <input type="text" class="form-control" id="customer_id" name="customer_id" value="{!! isset($company) ? $company->customer_id :'' !!}"/>
                                        </div>
                            </div>
                            @endif
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('company.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')


    <script>
        $(document).ready(function () {
            var tracking=   `@php echo isset($company) ?$company->is_tracking_enabled: 0 @endphp`;
            var booking= `@php echo isset($company) ?$company->is_booking_enabled: 0 @endphp`;
            var customerId= `@php echo isset($company) ?$company->people_link_status: 0 @endphp`;
            if(tracking && tracking==1){

                $('#tracking').prop('checked', true).change();
            }
            if(booking && booking==1){
                $('#booking').prop('checked', true).change();
            }
            if(customerId && customerId==1){
                $('#peopleLink').prop('checked', true).change();
            }else{
                $('#peopleLink').prop('checked', false).change();
                $('#customerID').hide();
            }
        });

//         $(document).on('toggle','#peopleLink',function () {
//             console.log(1);
// $('#customer_id').toggle()
//         });
//         $('#peopleLink').bootstrapToggle({
//             on: $('#customer_id').show(),
//             off: $('#customer_id').hide()
//         });
        $('#peopleLink').change(function() {
            $('#customerID').toggle();
        });
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            email: {
                                validators: {
                                    notEmpty: {
                                        message: 'Email is required'
                                    },
                                    emailAddress: {
                                        message: 'The value is not a valid email address'
                                    }
                                }
                            }
                            ,name: {
                                validators: {
                                    notEmpty: {
                                        message: 'First Name is required'
                                    },

                                }
                            } ,last_name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Last Name is required'
                                    },

                                }
                            } ,business_name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Busines Name is required'
                                    },

                                }
                            },address: {
                                validators: {
                                    notEmpty: {
                                        message: 'Address is required'
                                    },

                                }
                            },city: {
                                validators: {
                                    notEmpty: {
                                        message: 'City is required'
                                    },

                                }
                            },postal_code: {
                                validators: {
                                    notEmpty: {
                                        message: 'Postal Code is required'
                                    },

                                }
                            }
                            ,vat: {
                                validators: {
                                    notEmpty: {
                                        message: 'Vat is required'
                                    },

                                }
                            }
                            ,gate_limit: {
                                validators: {
                                    notEmpty: {
                                        message: 'Gate limit is required'
                                    },

                                }
                            }
                            ,employee_limit: {
                                validators: {
                                    notEmpty: {
                                        message: 'Employee limit is required'
                                    },

                                }
                            },
                           country: {
                                validators: {
                                    notEmpty: {
                                        message: 'Country is required'
                                    },

                                }
                            }
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });


    </script>
@endsection
