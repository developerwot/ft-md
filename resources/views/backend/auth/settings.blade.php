@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> User Profile</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('view.profile') }}" class="text-muted">Profile</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('profile.settings') }}" class="text-muted">Settings</a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="d-flex flex-row">
                <!--begin::Aside-->

            @include('backend.snippets.user_profile')
            <!--end::Aside-->
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8">
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch">
                            <!--begin::Header-->
                            <div class="card-header py-3">
                                <div class="card-title align-items-start flex-column">
                                    <h3 class="card-label font-weight-bolder text-dark">Settings</h3>
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update Settings</span>
                                </div>
                                <div class="card-toolbar">
                                    <button type="button" class="btn btn-success mr-2" id="saveButton">Save Changes</button>
                                    <button type="reset" class="btn btn-secondary" onclick="return window.location.href='{{ route('view.profile') }}'">Cancel</button>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Form-->
                            <form id="ProfileForm" class="form"
                                  method="POST" enctype="multipart/form-data"
                                  action="{{ route('update.settings') }}">
                            @csrf
                            <!--begin::Body-->
                                <div class="card-body">
                                    <!--begin::Alert-->

                                    <div class="form-group row">
                                        <label class="col-xl-4 col-lg-3 col-form-label text-alert">Hr Email</label>
                                        <div class="col-xl-8 ">
                                            <input type="email" class="form-control form-control-lg form-control-solid" value="{!! isset($settings) ? $settings->hr_email : old('hr_email') !!}" name="hr_email" placeholder="Hr Email" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-4 col-lg-3 col-form-label text-alert">Guest User App Link</label>
                                        <div class="col-xl-8 ">
                                            <input type="text" class="form-control form-control-lg form-control-solid" value="{!! isset($settings) ? $settings->guest_user_link : old('guest_user_link') !!}" name="guest_user_link" placeholder="Guest User Link" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-xl-3"></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <h5 class="font-weight-bold mt-10 mb-6">Risk Settings</h5>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group row col-lg-6">
                                            <label class="col-xl-4 col-lg-3 col-form-label text-alert">Low</label>
                                            <div class="col-xl-8 ">
                                                <input type="number" min="1" class="form-control form-control-lg form-control-solid" value="{!! isset($settings) ? $settings->low : old('low') !!}" name="low" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="form-group row col-lg-6">
                                            <label class="col-xl-4 col-lg-3 col-form-label text-alert">Low Color</label>
                                            <div class="col-xl-8 ">
                                                <input type="text" class="form-control form-control-lg form-control-solid demo" id="hue-demo" data-control="hue" value="{!! isset($settings) ? $settings->low_color : old('low_color') !!}" name="low_color" placeholder=""  autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group row col-lg-6">
                                            <label class="col-xl-4 col-lg-3 col-form-label text-alert">Medium</label>
                                            <div class="col-xl-8 ">
                                                <input type="number" min="1" class="form-control form-control-lg form-control-solid" value="{!! isset($settings) ? $settings->medium : old('medium') !!}"  name="medium" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="form-group row col-lg-6">
                                            <label class="col-xl-4 col-lg-3 col-form-label text-alert">Medium Color</label>
                                            <div class="col-xl-8 ">
                                                <input type="text" min="1" class="form-control form-control-lg form-control-solid demo" value="{!! isset($settings) ? $settings->medium_color : old('medium_color') !!}"  data-control="hue"  name="medium_color" placeholder="" autocomplete="off" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group row col-lg-6">
                                            <label class="col-xl-4 col-lg-3 col-form-label text-alert">High</label>
                                            <div class="col-xl-8 ">
                                                <input type="number" min="1" class="form-control form-control-lg form-control-solid" value="{!! isset($settings) ? $settings->high : old('high') !!}" name="high" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="form-group row col-lg-6">
                                            <label class="col-xl-4 col-lg-3 col-form-label text-alert">High Color</label>
                                            <div class="col-xl-8 ">
                                                <input type="text" class="form-control form-control-lg form-control-solid demo" value="{!! isset($settings) ? $settings->high_color : old('high_color') !!}" name="high_color" placeholder="" autocomplete="off" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Body-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <link rel="stylesheet" href="{{ asset('theme/css/jquery.minicolors.css') }}">
    <script src="{{ asset('theme/js/jquery.minicolors.js') }}"></script>

    <script>

        var KTFormControls = function () {

            $('.demo').each( function() {
                //
                // Dear reader, it's actually very easy to initialize MiniColors. For example:
                //
                //  $(selector).minicolors();
                //
                // The way I've done it below is just for the demo, so don't get confused
                // by it. Also, data- attributes aren't supported at this time...they're
                // only used for this demo.
                //
                $(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    defaultValue: $(this).attr('data-defaultValue') || '',
                    format: $(this).attr('data-format') || 'hex',
                    keywords: $(this).attr('data-keywords') || '',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: $(this).attr('data-letterCase') || 'lowercase',
                    opacity: $(this).attr('data-opacity'),
                    position: $(this).attr('data-position') || 'bottom',
                    swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
                    change: function(value, opacity) {
                        if( !value ) return;
                        if( opacity ) value += ', ' + opacity;
                        if( typeof console === 'object' ) {
                            console.log(value);
                        }
                    },
                    theme: 'bootstrap'

                });
            });
            // Private functions
            const loginButton = document.getElementById('saveButton');
            form =  document.getElementById('ProfileForm');
            var _initDemo1 = function () {
                const fv =  FormValidation.formValidation(
                    document.getElementById('ProfileForm'),
                    {
                        fields: {

                            hr_email: {
                                validators: {
                                    notEmpty: {
                                        message: 'Hr Email is required'
                                    },
                                    emailAddress:{
                                        message: 'Please add valid email address'
                                    }
                                }
                            },
                            low: {
                                validators: {
                                    notEmpty: {
                                        message: 'Low is required'
                                    },
                                    digits:{
                                        message: 'Low must be number'
                                    }
                                }
                            }
                            , medium: {
                                validators: {
                                    notEmpty: {
                                        message: 'Medium is required'
                                    },
                                    digits:{
                                        message: 'Medium must be number'
                                    }
                                }
                            },
                            high: {
                                validators: {
                                    notEmpty: {
                                        message: 'High is required'
                                    },
                                    digits:{
                                        message: 'High must be number'
                                    }
                                }
                            }
                            ,low_color: {
                                validators: {
                                    notEmpty: {
                                        message: 'Low Color is required'
                                    },

                                }
                            },
                             medium_color: {
                                validators: {
                                    notEmpty: {
                                        message: 'Medium Color is required'
                                    }
                                }
                            },
                            high_color: {
                                validators: {
                                    notEmpty: {
                                        message: 'High  color is required'
                                    },
                                }
                            }


                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
                loginButton.addEventListener('click', function() {
                    fv.validate().then(function(status) {
                        // Update the login button content based on the validation status
                        /* if (status == 'Valid') {

                         }
                         else {

                         }*/
                    });
                });
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>
@endsection