@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> User Profile</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('view.profile') }}" class="text-muted">Profile</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('address.change') }}" class="text-muted">Contact Information</a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="d-flex flex-row">
                <!--begin::Aside-->

            @include('backend.snippets.user_profile')
            <!--end::Aside-->
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8">
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch">
                            <!--begin::Header-->
                            <div class="card-header py-3">
                                <div class="card-title align-items-start flex-column">
                                    <h3 class="card-label font-weight-bolder text-dark">Contact Information</h3>
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update your address informaiton</span>
                                </div>
                                <div class="card-toolbar">
                                    <button type="button" class="btn btn-success mr-2" id="saveButton">Save Changes</button>
                                    <button type="reset" class="btn btn-secondary" onclick="return window.location.href='{{ route('view.profile') }}'">Cancel</button>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Form-->
                            <form id="ProfileForm" class="form"
                                  method="POST" enctype="multipart/form-data"
                                  action="{{ route('update.address', $user->id) }}">
                            @csrf
                            <!--begin::Body-->
                                <div class="card-body">
                                    <!--begin::Alert-->

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">Address</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" class="form-control form-control-lg form-control-solid" value="{!! isset($userCompany) ? $userCompany->address : old('address') !!}" name="address" placeholder="Address" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">City</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" class="form-control form-control-lg form-control-solid" value="{!! isset($userCompany) ? $userCompany->city : old('city') !!}" name="city" placeholder="City" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">Postal Code</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" class="form-control form-control-lg form-control-solid" value="{!! isset($userCompany) ? $userCompany->postal_code : old('postal_code') !!}" name="postal_code" placeholder="Postal Code" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">Vat</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" class="form-control form-control-lg form-control-solid" value="{!! isset($userCompany) ? $userCompany->vat : old('vat') !!}" name="vat" placeholder="Vat" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-alert">Country</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <select name="country" id="countryID" class="form-control form-control-lg form-control-solid">
                                                @php $countries = \App\Http\Controllers\Controller::getCountries(); @endphp
                                                <option value="">Select</option>
                                                @foreach($countries as $ckey=>$country)
                                                    <option value="{{ $ckey }}" @if(isset($userCompany) && $userCompany->country == $ckey) selected @endif >{{ $country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!--end::Body-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        var KTFormControls = function () {
            // Private functions
            const loginButton = document.getElementById('saveButton');
            form =  document.getElementById('ProfileForm');
            var _initDemo1 = function () {
                const fv =  FormValidation.formValidation(
                    document.getElementById('ProfileForm'),
                    {
                        fields: {

                            address: {
                                validators: {
                                    notEmpty: {
                                        message: 'Address is required'
                                    },


                                }
                            },
                            city: {
                                validators: {
                                    notEmpty: {
                                        message: 'City  is required'
                                    },


                                }
                            },
                            postal_code: {
                                validators: {
                                    notEmpty: {
                                        message: 'Postal Code  is required'
                                    },


                                }
                            },
                            country: {
                                validators: {
                                    notEmpty: {
                                        message: 'Country is required'
                                    },


                                }
                            },
                            vat: {
                                validators: {
                                    notEmpty: {
                                        message: 'Vat is required'
                                    },


                                }
                            }
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
                loginButton.addEventListener('click', function() {
                    fv.validate().then(function(status) {
                        // Update the login button content based on the validation status
                        /* if (status == 'Valid') {

                         }
                         else {

                         }*/
                    });
                });
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>
@endsection