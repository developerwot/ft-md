@extends('backend.layouts.master')
@section('content')
    <style>
        #timerange_picker_div .table-condensed {
            display: none !important;
        }
    </style>
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($booking_policy) ? 'Update Booking Policy' : 'Add Booking Policy' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('booking-policy.index') }}" class="text-muted">Booking Policies</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($booking_policy) ? route('building_policy.index').'/'.$booking_policy->id.'/edit' : route('building_policy.create') }} "
                                   class="text-muted">{!! isset($booking_policy) ? 'Update Booking Policy' : 'Add Booking Policy' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id=""
                       data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($booking_policy) ? 'Update Booking Policy' : 'Add Booking Policy' !!} </h3>
                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($booking_policy) ? route('desk-booking-policy.update', $booking_policy->id) : route('desk-booking-policy.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">Building</label>
                                        <select class="form-control  buildingId" id="kt_select2_4" name="building_id">
                                            <option value=""> Select Building</option>
                                            @if($buildings)
                                                @foreach($buildings as $building)
                                                    <option value="{{ $building->id }}"
                                                            @if( (isset($booking_policy) && $building->id == $booking_policy->building_id) || $building->id == old('building_id') )  selected @endif>{{ $building->building_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">Floors</label>
                                        <select class="form-control select2 locations" id="kt_select2_3"
                                                name="floor_id[]" multiple>
                                        </select>
                                    </div>
                                </div>
                                <option value="" id="options" style="display: none">Select Floors</option>
                                <div class="row range_date_div">
                                    <div class="form-group col-lg-6 ">
                                        <label>Date Range</label>
                                        <input class="form-control" name="date_range" id="kt_daterangepicker_1"
                                               readonly="readonly" placeholder="Select Date" type="text"/>
                                    </div>
                                </div>
                                {{--<div class="row  range_date_div">--}}
                                {{--<div class="form-group col-lg-6 " id="timerange_picker_div">--}}
                                {{--<label>Time Range</label>--}}
                                {{--<input class="form-control" name="time_range" id="timerange_picker" readonly="readonly" placeholder="Select time" type="text" value="{{isset($booking_policy) && $booking_policy->from_hour!='' ? $booking_policy->from_hour.' - '.$booking_policy->to_hour : old('time_range')}}" />--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="row">
                                    <div class="form-group col-lg-3 ">
                                        <label>Permit / No Permit</label>
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input checked type="radio" name="is_permitted" value="1">Permit
                                                <span></span></label>
                                            <label class="radio">
                                                <input type="radio"
                                                       @if( (isset($booking_policy) && $booking_policy->is_permitted=='0') || old('is_permitted')=='0' )  checked
                                                       @endif  name="is_permitted" value="0">No Permit
                                                <span></span></label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mt-2 form-group" id="DeskAdder" style="display: none">
                                        <button type="button" class="btn btn-success font-weight-bold add_desk">Add
                                            Desks
                                        </button>
                                    </div>
                                </div>
                                {{--<div class="row" >--}}

                                {{--</div>--}}
                                <div class="row" id="slots-box" style="display: none">
                                    <div class="form-group col-lg-6">
                                        <label>Desk name</label>
                                        <div style="display: flex">
                                            <input type="text" class="form-control mr-2 desk">  <a href="javascript:void(0)" class="trash-button btn btn-sm btn-default  btn-hover-danger btn-icon mr-2 ">
                                                <i class="la la-trash" style="font-size: 30px;color: red" title="delete"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="slots-container">
                                    @if(old('desks'))
                                        @foreach(old('desks') as $desk)
                                            <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label>Desk name</label>
                                                <div style="display: flex">
                                                    <input type="text" class="form-control mr-2 desks" name="desks[{{$loop->iteration-1}}]" value="{{$desk}}"> <a href="javascript:void(0)" class="trash-button btn btn-sm btn-default  btn-hover-danger btn-icon mr-2 ">
                                                        <i class="la la-trash" style="font-size: 30px;color: red" title="delete"></i></a>
                                                </div>
                                            </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if(isset($booking_policy) && empty(old('desks')) )
                                        @if($booking_policy->is_permitted==1)
                                            @foreach($desks as $desk)
                                                  <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label>Desk name</label>
                                                        <div style="display: flex">
                                                            <input type="text" class="form-control mr-2 desks" name="desks[{{$loop->iteration-1}}]" value="{{$desk->desk_name}}">
                                                            <a href="javascript:void(0)" class="trash-button btn btn-sm btn-default  btn-hover-danger btn-icon mr-2 ">
                                                                <i class="la la-trash" style="font-size: 30px;color: red" title="delete"></i></a>
                                                            <a href="javascript:;" onclick="return false;"  data-id="{{asset('public/storage/QR_codes').'/'.$desk->desk_QR.'.png'}}" class=" print-button btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Print QR code">
                                                            <span class="svg-icon svg-icon-md">
                                    <i class="la la-print"></i>
                                    </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                  </div>
                                            @endforeach
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold"
                                        onclick="return window.location.href ='{{ route('desk-booking-policy.index') }}'">
                                    Cancel
                                </button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')

    <!--<script src="{{ asset('theme/plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.3') }}"></script>-->
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.3') }}"></script>
    <script>
        var slotLocation = `@php echo isset($booking_policy) ?$booking_policy->floor_id : null @endphp`;
        var permit = `@php echo isset($booking_policy) ?$booking_policy->is_permitted : 1 @endphp`;
        {{--var slott = `@php echo old('slots') !=null? old('slots') : null @endphp`;--}}
        // console.log(slott);
        var KTFormControls = function () {
//             // Private functions
            var _initDemo1 = function () {
//                 $('#group_id').select2({placeholder: "Select a Group"});
                $('#kt_daterangepicker_1').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    format: 'd-m-Y',
                    cancelClass: 'btn-secondary',

                });
                $('.kt_timepicker_4').timepicker();

            }
            return {
                // public functions
                init: function () {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function () {
            $(document).on('click', '.print-button', function (e) {
                var editUrl = $(this).data('id');
                //   // location.href = editUrl;
                // var url=  $(this).next().attr('src');
                //  console.log(url);

                var contents ="<img src='" + editUrl + "' width='250px' height='250px' style='margin-left:100px'/>";
                var frame1 = document.createElement('iframe');
                frame1.name = "frame1";
                frame1.style.position = "absolute";
                frame1.style.top = "-1000000px";
                document.body.appendChild(frame1);
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
                frameDoc.document.open();
                frameDoc.document.write('<html><head><title>DIV Contents</title>');
                frameDoc.document.write('</head><body>');
                frameDoc.document.write(contents);
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    document.body.removeChild(frame1);
                }, 300);
                return false;


            });
            var m = $('.desks').length;
            locations();
            KTFormControls.init();

            if (permit == 1) {
                $('#DeskAdder').show();
            } else {
                $('#DeskAdder').hide();
                $('#slots-container').hide();
            }
            $(".locations").select2({
                placeholder: "No locations available",
            });
            $('.buildingId').change(function () {
                locations();
            });
            $(document).on('click', 'input[name=is_permitted]', function () {
                if ($(this).val() === '1') {
                    $('#DeskAdder').show();
                    $('#slots-container').show();
                } else {
                    $('#DeskAdder').hide();
                    $('#slots-container').hide();
                }
            });

            $(document).on('click', '.add_desk', function () {

                // $('#slots-container').empty();
                var dummy = $('#slots-box').clone();
                $(dummy).attr('id', '');
                $(dummy).addClass('desks');
                $(dummy).find('.desk').attr('name', 'desks[' + m + ']');
                $(dummy).show();
                $('#slots-container').append(dummy);
                m++;
            });
            $(document).on('click', '.trash-button', function () {
                $(this).parent().parent().parent().remove();
            })
        });

        function locations() {
            var building = $('.buildingId').val();
            if (building === "") {
                return false;
            }
            $.ajax({
                url: '/desk-access/floor-ajax/' + building,
                dataType: 'json',
                type: 'get',
                success: function (data) {
                    var location = data.data;
                    $('.locations').find('option').remove();
                    for (var i = 0; i < location.length; i++) {
                        var dummy = $('#options').clone();
                        if (i == 0) {
                            $(dummy).prop('selected', true);
                        }
                        if (slotLocation) {
                            if (slotLocation == location[i].id) {
                                $(dummy).prop('selected', true);
                            }
                        }

                        $(dummy).attr('id', '');
                        $(dummy).val(location[i].id);
                        $(dummy).text(location[i].floor_name);
                        $(dummy).show();
                        $('.locations').append(dummy);
                        $('.locations').select2();
                        $('#buildingId').select2();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('.locations').find('option').remove();
                    var dummy = $('#options').clone();
                    $(dummy).attr('id', '');
                    $(dummy).val('');
                    $(dummy).text('No floors available');
                    $(dummy).prop('selected', true);
                    $(dummy).show();
                    $('.locations').append(dummy);
                    $('.locations').select2();
                    $('#buildingId').select2();
                }
            });

        }

    </script>
    @if(isset($booking_policy) && $booking_policy->from_date!='')


        <script>
            $(document).ready(function () {
                $('#kt_daterangepicker_1').data('daterangepicker').setStartDate("{{date('m/d/Y',strtotime($booking_policy->from_date))}}");
                $('#kt_daterangepicker_1').data('daterangepicker').setEndDate("{{date('m/d/Y',strtotime($booking_policy->to_date))}}");
            });
        </script>
    @endif


@endsection
