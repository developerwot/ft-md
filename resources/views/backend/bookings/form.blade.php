@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($booking) ? 'Update Booking' : 'Add Booking' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('bookings.index') }}" class="text-muted">Bookings</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($booking) ? route('bookings.index').'/'.$booking->id.'/edit' : route('bookings.create') }} " class="text-muted">{!! isset($booking) ? 'Update Booking' : 'Add Booking' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($booking) ? 'Update Booking' : 'Add Booking' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($booking) ? route('bookings.update', $booking->id) : route('bookings.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            @if(isset($booking))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">Building </label>
                                        <select class="form-control select2" id="kt_select2_4" name="building_id" >
                                            <option value=""> Select Building</option>

                                            @if($buildings)
                                                @foreach($buildings as $building)
                                                    <option value="{{ $building->id }}" @if( (isset($booking) && $building->id == $booking->building_id) || $building->id == old('building_id') )  selected @endif>{{ $building->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">People </label>
                                        <select class="form-control select2" name="employee_id" id="kt_select2_3">
                                            <option value="">Select People</option>
                                            @if($employees)
                                                @foreach($employees as $emp)
                                                    <option value="{{ $emp->id }}"  @if( (isset($booking) && $emp->id == $booking->employee_id) || $emp->id == old('employee_id') )  selected @endif>{{ $emp->first_name . ' ' .$emp->last_name.' ( '.$emp->email.')' }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Booking Date </label>
                                        <input class="form-control" id="kt_datepicker" readonly="readonly" name="booking_date" placeholder="Select Date" type="text"  value="{{ (isset($booking)) ? date("m/d/Y",strtotime($booking->booking_date)) : old('booking_date') }}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Booking Time </label>
                                        <input class="form-control" id="kt_timepicker" readonly="readonly" name="booking_time" placeholder="Select Time" type="text" value="{{ (isset($booking)) ? date("h:i A",strtotime($booking->booking_time)) : old('booking_time') }}"/>
                                    </div>
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('bookings.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                $('#kt_datepicker').datepicker({
                    startDate: new Date(),
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary'
                });
                $('#kt_timepicker').timepicker({
                    minuteStep: 1,
                    showSeconds: true,
                    showMeridian: false,
                    snapToStep: true
                });

                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            building_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'Building is required'
                                    },
                                }
                            }
                            , employee_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'People is required'
                                    },
                                }
                            },
                            booking_date: {
                                validators: {
                                    notEmpty: {
                                        message: 'Booking Date is required'
                                    },
                                }
                            },
                            booking_time: {
                                validators: {
                                    notEmpty: {
                                        message: 'Booking Time is required'
                                    },
                                }
                            },
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });


    </script>
@endsection
