@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($building) ? 'Update Building' : 'Add Building' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('booking-access.index') }}" class="text-muted">Buildings</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($building) ? route('booking-access.index').'/'.$building->id.'/edit' : route('booking-access.create') }} " class="text-muted">{!! isset($building) ? 'Update Building' : 'Add Building' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($building) ? 'Update Building' : 'Add Building' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($building) ? route('booking-access.update', $building->id) : route('booking-access.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Building name </label>
                                        <input type="text" name="building_name" class="form-control" value="{!! isset($building) ? $building->building_name: old('building_name') !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Building limit </label>
                                        <input type="number" name="building_limit" class="form-control" value="{!! isset($building) ? $building->building_limit: old('building_limit') !!}" min="1">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Status </label>
                                        <select name="building_status" class="form-control">
                                            <option value="1" @if(isset($building) && $building->building_status==1) selected @endif selected >Active</option>
                                            <option value="0" @if(isset($building) && $building->building_status==0) selected @endif>Inactive</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="company_id" value="{{auth()->user()->company->id}}">
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('booking-access.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
        @if(isset($building))
            <div class="content flex-column-fluid" id="kt_content">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Location Management</h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{ route('location-access.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2">Add Location</a>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="user_datatable"></div>
                        <!--end: Datatable-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <div class="content flex-column-fluid" id="kt_content">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Floor Management</h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{ route('floor-access.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2">Add Floor</a>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="user_datatable2"></div>
                        <!--end: Datatable-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            @endif
    </div>
    @include('backend.snippets.delete')
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>
        $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('location-access.index')}}/delete-location/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('location-access.index')}}/' + $(this).data('id') + '/edit-location';
            location.href = editUrl;
        });
        $(document).on('click', '.delete-button-floor-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('floor-access.index')}}/delete-floor/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button-floor', function (e) {
            var editUrl = '{{route('floor-access.index')}}/' + $(this).data('id') + '/edit-floor';
            location.href = editUrl;
        });
        var buildingId=`@php echo isset($building) ?$building->id : null @endphp`;


        var DatatableDataLocalDemo = {
            init: function () {
                var e, a, i;
                atable = $("#user_datatable").KTDatatable({
                    data: {
                        saveState :false,
                        type: 'remote',
                        source: {
                            read: {

                                url:  '{{route('location-access.table2')}}',
                                method: 'POST',
                                data:{building_id:buildingId},
                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    console.log(raw.data.length);
                                    // $("#kt_subheader_total").html(raw.data.length + ' Total ');
                                    return dataSet;

                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: false,
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#kt_subheader_search_form"), key: 'generalSearch'},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        sortable: false,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },
                        {
                            field: "location_name",
                            title: "Location Name",
                            width: 200,
                            sortable: 1,
                            overflow: "visible",

                        },
                        {
                            field: "building_name",
                            title: "Building Name",
                            width: 200,
                            sortable: 1,
                            overflow: "visible",
                            template: function (e, a, i) {
                                return e.buildings.building_name;
                            }
                        },{
                            field: "location_limit",
                            title: "Location Limit",
                            width: 200,
                            sortable: 1,
                            overflow: "visible",

                        },
                        {
                            field: "location_status",
                            title: "Status",
                            width: 100,
                            sortable:true,
                            overflow: "visible",
                            template: function (e, a, i) {
                                if(e.location_status == 1){
                                    data = '<span class="label label-lg font-weight-bold label-light-success label-inline">Active</span>'

                                } else {
                                    data = '<span class="label label-lg font-weight-bold label-light-danger label-inline">Inactive</span>'
                                }
                                return data;
                            }
                        },
                        {
                            field: "Actions",
                            width: 120,
                            title: "Action",
                            sortable: !1,
                            overflow: "visible",
                            template: function (e, a, i) {

                                return ' <a href="javascript:;" data-id="' + e.id + '"'+ 'class=" edit-button btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>'+
                                    '<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>'+
                                    '<a href="javascript:;" data-id="' + e.id + '" class="delete-button-action btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon" title="Delete">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>'+
                                    '<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>';
                            }
                        }]
                });

            }
        };
        var DatatableDataLocalDemo2 = {
            init: function () {
                var e, a, i;
                atable = $("#user_datatable2").KTDatatable({
                    data: {
                        saveState :false,
                        type: 'remote',
                        source: {
                            read: {

                                url:  '{{route('floor-access.table2')}}',
                                method: 'POST',
                                data:{building_id:buildingId},
                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    console.log(raw.data.length);
                                    // $("#kt_subheader_total").html(raw.data.length + ' Total ');
                                    return dataSet;

                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: false,
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#kt_subheader_search_form"), key: 'generalSearch'},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        sortable: false,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },
                        {
                            field: "floor_name",
                            title: "Floor Name",
                            width: 200,
                            sortable: 1,
                            overflow: "visible",

                        },
                        {
                            field: "floor_name",
                            title: "Floor Name",
                            width: 200,
                            sortable: 1,
                            overflow: "visible",
                            template: function (e, a, i) {
                                return e.buildings.building_name;
                            }
                        },{
                            field: "floor_limit",
                            title: "Floor Limit",
                            width: 200,
                            sortable: 1,
                            overflow: "visible",

                        },
                        {
                            field: "floor_status",
                            title: "Status",
                            width: 100,
                            sortable:true,
                            overflow: "visible",
                            template: function (e, a, i) {
                                if(e.floor_status == 1){
                                    data = '<span class="label label-lg font-weight-bold label-light-success label-inline">Active</span>'

                                } else {
                                    data = '<span class="label label-lg font-weight-bold label-light-danger label-inline">Inactive</span>'
                                }
                                return data;
                            }
                        },
                        {
                            field: "Actions",
                            width: 120,
                            title: "Action",
                            sortable: !1,
                            overflow: "visible",
                            template: function (e, a, i) {

                                return ' <a href="javascript:;" data-id="' + e.id + '"'+ 'class=" edit-button-floor btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>'+
                                    '<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>'+
                                    '<a href="javascript:;" data-id="' + e.id + '" class="delete-button-floor-action btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon" title="Delete">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>'+
                                    '<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>';
                            }
                        }]
                });

            }
        };
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Name is required'
                                    },
                                }
                            }
                            , building_limit: {
                                validators: {
                                    notEmpty: {
                                        message: 'Building Limit is required'
                                    },
                                }
                            },
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
            DatatableDataLocalDemo.init();
            DatatableDataLocalDemo2.init();
        });
    </script>

@endsection
