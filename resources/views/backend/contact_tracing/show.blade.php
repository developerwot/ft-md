<div class="card card-custom">
    <!--begin::Header-->
    <div class="card-header h-auto">
        <div class="card-title">
            <h3 class="card-label">Basic Detail</h3>
        </div>
    </div>
    <!--end::Header-->
    <!--begin::Body-->
    <div class="card-body py-4">
        <div class="form-group row my-2">
            <label class="col-4 col-form-label">Name:</label>
            <div class="col-8">
                <span class="form-control-plaintext font-weight-bolder">{{ $employee->first_name }} {{ $employee->last_name }}</span>
            </div>
        </div>
        <div class="form-group row my-2">
            <label class="col-4 col-form-label">Email:</label>
            <div class="col-8">
                <span class="form-control-plaintext font-weight-bolder">{{ $employee->email }}</span>
            </div>
        </div>
        {{--
        <div class="form-group row my-2">
            <label class="col-4 col-form-label">Phone:</label>
            <div class="col-8">
                <span class="form-control-plaintext font-weight-bolder">+456 7890456</span>
            </div>
        </div>--}}
    </div>
</div>
<div class="card card-custom">
    <div class="card-header h-auto">
        <div class="card-title">
            <h3 class="card-label">Contacted Users</h3>
        </div>
    </div>
    <div class="card-body py-4">
        @if(!empty($contact_tracing_user))
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contact_tracing_user as $key=>$cuser)

                    @if (!empty($cuser->employee) && $cuser->employee->id != $employee->id)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>
                                {{ $cuser->employee->first_name }}
                            </td>
                            <td>
                                {{ $cuser->employee->last_name }}
                            </td>
                            <td>
                                {{ $cuser->employee->email }}
                            </td>
                            <td>
                                {{ date("d-M-Y",strtotime($cuser->contact_time)) }}
                            </td>
                        </tr>
                    @elseif( !empty($cuser->contact_employee) && $cuser->contact_employee->id != $employee->id)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>
                                {{ $cuser->contact_employee->first_name }}
                            </td>
                            <td>
                                {{ $cuser->contact_employee->last_name }}
                            </td>
                            <td>
                                {{ $cuser->contact_employee->email }}
                            </td>
                            <td>
                                {{ date("d-M-Y",strtotime($cuser->contact_time)) }}
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>