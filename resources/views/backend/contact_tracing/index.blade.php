@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Contact Tracing</h5>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Search Form-->
                    <div class="d-flex align-items-center" id="kt_subheader_search">
                        <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{--{{ $emp_temp->count() }}--}} Total</span>
                        <form class="ml-5" action="javascript:void(0)">
                            <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                                <input type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search..." />
                                <div class="input-group-append">
														<span class="input-group-text">
															<span class="svg-icon">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																		<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                                            <!--<i class="flaticon2-search-1 icon-sm"></i>-->
														</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end::Search Form-->
                    <!--begin::Group Actions-->

                    <!--end::Group Actions-->
                </div>
                <!--end::Details-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Button-->
                    <a href="#" class=""></a>
                    <!--end::Button-->
                    <!--begin::Button-->
                   {{-- @if(Auth::user()->hasRole('company'))
                        <a href="{{ route('emp_temp.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2">Add People Temperature</a>
                    @endif--}}
                    <!--end::Button-->
                    <!--begin::Dropdown-->
                    <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">

                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Contact Tracing Management
                        </h3>
                    </div>
                    <div class="card-toolbar">

                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    <div class="mb-12">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-xl-12">
                                <div class="row align-items-center">

                                    <div class="col-md-6 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">Date:</label>
                                            <div class="col-md-6 my-2 my-md-0">
                                                <input class="form-control" id="kt_datepicker" readonly="readonly" placeholder="Select date" type="text" />
                                            </div>
                                            <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                                <a href="javascript:void(0)" class="btn btn-light-primary px-6 font-weight-bold SearchFilter">Search</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--begin: Datatable-->
                    <div class="datatable datatable-bordered datatable-head-custom" id="user_datatable"></div>
                    <!--end: Datatable-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </div>
    <div id="contactModel" class="modal fade in">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">People Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body contactUserData" style="padding: 0px">
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <button type="button" class="btn btn-info cancel-btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @include('backend.snippets.delete')
@endsection
    @section('scripts')
    {{--<script src="{{ asset('theme/js/pages/custom/user/list-datatable.js?v=7.0.3') }}"></script>--}}
    <script>
        var  e,  i;
        $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('emp_temp.index')}}/delete/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('emp_temp.index')}}/' + $(this).data('id') + '/edit';
            location.href = editUrl;
        });
        $(document).on('click', '.show-contact-tracing', function (e) {
            console.log($(this).data('id'));
            var editUrl = '{{route('contact_tracing.show_contact')}}';
            $.ajax({
                url: editUrl,
                type: "post",
                data: {"employee_id": $(this).data('id'),"time":$(this).data('time')},
                success: function (res) {
                    if(res != '') {
                        $(".contactUserData").html(res);
                        $('#contactModel').modal();
                    }
                }
            })
        });
        $(document).on('click', '.show-other-contact-tracing', function (e) {
            console.log($(this).data('id'));
            var editUrl = '{{route('contact_tracing.show_other_contact')}}';
            $.ajax({
                url: editUrl,
                type: "post",
                data: {"contact_employee_id": $(this).data('id'),"time":$(this).data('time')},
                success: function (res) {
                    if(res != '') {
                        $(".contactUserData").html(res);
                        $('#contactModel').modal();
                    }
                }
            })
        });

     /*   var emp_temp = {{--{!! $emp_temp !!}--}};
        console.log('emp_temp', emp_temp);*/


        var DatatableDataLocalDemo = {
            init: function () {
                $('#kt_datepicker').datepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary',

                }).datepicker('setDate', new Date());
                 atable = $("#user_datatable").KTDatatable({
                     // 'processing': true,
                     // 'language': {
                     //     'loadingRecords': '&nbsp;',
                     //     'processing': '<div class="spinner"></div>'
                     // },
                    data: {
                        saveState :false,
                        type: 'remote',

                        source: {
                            read: {
                                method:'POST',
                                url: AjaxUrl + '/getContactTracing',
                                // sample custom headers
                                // headers: {'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    console.log(raw.data.length);
                                    $("#kt_subheader_total").html(raw.data.length + ' Total ');
                                    return dataSet;
                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: false,
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#kt_subheader_search_form"),key: 'generalSearch'},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        sortable: false,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },  {
                        field: "name",
                        title: "Name",
                        width: 100,
                        sortable: 1,
                        // textAlign: "center",
                    },   {
                        field: "email",
                        title: "Email",
                        width: 200,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },  {
                        field: "contact_count",
                        title: "Contacted People",
                        textAlign: "center",
                        width: 120,
                        sortable: 1,
                        overflow: "visible",

                    },  {
                        field: "contact_time_format",
                        title: "Contact At",
                        width: 150,
                        sortable: true,
                        overflow: "visible",

                    }, {
                        field: "Actions",
                        width: 60,
                        title: "Action",
                        sortable: !1,
                        overflow: "visible",
                        template: function (e, a, i) {
                            if(e.employee_id) {
                                var view = '<a href="javascript:;" data-id="'+e.employee_id+'" data-time="'+e.contact_date+'" class="show-contact-tracing btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="svg-icon">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"/>'+
                                    '<path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>';
                                return view;
                            }
                            else if(e.contact_employee_id != '')  {
                                var view = '<a href="javascript:;" data-id="'+e.contact_employee_id+'" data-time="'+e.contact_date+'" class="show-other-contact-tracing btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="svg-icon">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"/>'+
                                    '<path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>';
                                return view;
                            }

                           }
                        }
                    ]
                })
                     $('.SearchFilter').on('click', function() {

                         atable.setDataSourceParam('tracing_date',$('#kt_datepicker').val().toLowerCase());
                         atable.setDataSourceParam('search','1');
                         atable.search($('#kt_datepicker').val().toLowerCase(), 'tracing_date');
                     });


            }
            ,
            reload : function () {
                //console.log('reload');
                    //$('#user_datatable').KTDatatable('reload');
            }
        };
        jQuery(document).ready(function () {
            DatatableDataLocalDemo.init();

            //setInterval( DatatableDataLocalDemo.reload, 60000);
        });

    </script>
@endsection
