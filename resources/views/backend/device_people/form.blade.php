@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($device_people) ? 'Update Device People' : 'Add Device People' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('device_people.index') }}" class="text-muted">Device Peoples</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($device_people) ? route('device_people.index').'/'.$device_people->id.'/edit' : route('device_people.create') }} " class="text-muted">{!! isset($device_people) ? 'Update Device People' : 'Add Device People' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($device_people) ? 'Update Device People' : 'Add Device People' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($device_people) ? route('device_people.update', $device_people->id) : route('device_people.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            @if(isset($device_people))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Beacon Id</label>
                                        <select class="form-control select2" id="kt_select2_1" name="device_id" >
                                            <option value=""> Select Device</option>

                                            @if($devices)
                                                @foreach($devices as $device)
                                                    <option value="{{ $device->id }}" @if( (isset($device_people) && $device->id == $device_people->device_id) || $device->id == old('device_id') )  selected @endif>{{ $device->beaconId }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select People</label>
                                        <select class="form-control select2" id="kt_select2_3" name="employee_id" >
                                            <option value=""> Select People</option>

                                            @if($employees)
                                                @foreach($employees as $emp)
                                                    <option value="{{ $emp->id }}" @if( (isset($device_people) && $emp->id == $device_people->employee_id) || $emp->id == old('employee_id') )  selected @endif>{{ $emp->first_name.' '.$emp->last_name.' ('.$emp->email.')' }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Building</label>
                                        <select class="form-control select2" id="kt_select2_4" name="building_id" >
                                            <option value=""> Select Building</option>

                                            @if($buildings)
                                                @foreach($buildings as $building)
                                                    <option value="{{ $building->id }}" @if( (isset($device_people) && $building->id == $device_people->building_id) || $building->id == old('building_id') )  selected @endif>{{ $building->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('device_people.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            device_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'Device is required'
                                    },
                                }
                            }
                            , employee_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'People is required'
                                    },
                                }
                            },
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });


    </script>
@endsection
