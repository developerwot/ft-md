@extends('backend.layouts.master')
@section('content')
<style>
    #timerange_picker_div .table-condensed{display: none !important;}
</style>
<div class="main d-flex flex-column flex-row-fluid">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4" id="kt_subheader">
        <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($building_policy) ? 'Update Building Access Policy' : 'Add Building Access Policy' !!}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="{{ route('building_policy.index') }}" class="text-muted">Building Policies</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ isset($building_policy) ? route('building_policy.index').'/'.$building_policy->id.'/edit' : route('building_policy.create') }} " class="text-muted">{!! isset($building_policy) ? 'Update Building Access Policy' : 'Add Building Access Policy' !!}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Daterange-->
                <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                    <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                    <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                </a>
                <!--end::Daterange-->
                <!--begin::Dropdown-->

                <!--end::Dropdown-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <div class="content flex-column-fluid" id="kt_content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">{!! isset($building_policy) ? 'Update Building Access Policy' : 'Add Building Access Policy' !!} </h3>

                    </div>
                    <!--begin::Form-->
                    <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                          action="{{ isset($building_policy) ? route('building_policy.update', $building_policy->id) : route('building_policy.store')}}"
                          method="POST" enctype="multipart/form-data">
                        @csrf
                        @if(isset($building_policy))
                        @method('PUT')
                        @endif
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg-6 ">
                                    <label class="form-control-label">Building </label>
                                    <select class="form-control select2" id="kt_select2_4" name="building_id" >
                                        <option value=""> Select Building</option>

                                        @if($buildings)
                                        @foreach($buildings as $building)
                                        <option value="{{ $building->id }}" @if( (isset($building_policy) && $building->id == $building_policy->building_id) || $building->id == old('building_id') )  selected @endif>{{ $building->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6 ">
                                    <label class="form-control-label">Group </label>
                                    <select class="form-control select2" id="group_id" name="group_id" >
                                        <option value=""> Select Group</option>

                                        @if($groups)
                                        @foreach($groups as $group)
                                        <option value="{{ $group->id }}" @if( (isset($building_policy) && $group->id == $building_policy->group_id) || $group->id == old('group_id') )  selected @endif>{{ $group->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6 ">
                                    <label>Always / Date Range</label>
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input checked onclick="changeAlways()" type="radio" value="always" name="always_range">Always
                                            <span></span></label>
                                        <label class="radio">
                                            <input type="radio" @if( (isset($building_policy) && $building_policy->always_range=='date_range') || old('always_range')=='date_range' )  checked @endif onclick="changeAlways()" value="date_range" name="always_range">Date Range
                                            <span></span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row range_date_div" >
                                <div class="form-group col-lg-6 ">
                                    <label>Date Range</label>
                                    <input class="form-control" name="date_range" id="kt_daterangepicker_1" readonly="readonly" placeholder="Select Date" type="text"  />
                                </div>
                            </div>
                            <div class="row  range_date_div">
                                <div class="form-group col-lg-6 " id="timerange_picker_div">
                                    <label>Time Range</label>
                                    <input class="form-control" name="time_range" id="timerange_picker" readonly="readonly" placeholder="Select time" type="text" value="{{isset($building_policy) && $building_policy->from_hour!='' ? $building_policy->from_hour.' - '.$building_policy->to_hour : old('time_range')}}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6 ">
                                    <label>Permit / No Permit</label>
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input checked type="radio" name="permit" value="permit">Permit
                                            <span></span></label>
                                        <label class="radio">
                                            <input type="radio" @if( (isset($building_policy) && $building_policy->permit=='no permit') || old('permit')=='no permit' )  checked @endif  name="permit" value="no permit">No Permit
                                            <span></span></label>
                                    </div>
                                </div>
                            </div>
                            @if(0)
                            <div class="row">
                                <div class="form-group col-lg-6 mb-1">
                                    <label class="form-control-label">Policy </label>
                                    <textarea name="policy" class="form-control" rows="3" id="kt-ckeditor-1">
                                            {!! isset($building_policy) ? $building_policy->policy: old('policy') !!}
                                    </textarea>
                                </div>
                            </div>
                            @endif


                        </div>
                        <!--end::Code example-->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                            <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href ='{{ route('building_policy.index') }}'">Cancel</button>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Card-->
                <!--begin::Card-->

                <!--end::Card-->
            </div>

        </div>
    </div>
    <!--end::Content-->
</div>
@endsection

@section('scripts')

    <!--<script src="{{ asset('theme/plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.3') }}"></script>-->
<script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
<script>
                                var KTFormControls = function () {
                                    // Private functions
                                    var _initDemo1 = function () {
                                        $('#group_id').select2({placeholder: "Select a Group"});
                                        $('#kt_daterangepicker_1').daterangepicker({
                                            buttonClasses: ' btn',
                                            applyClass: 'btn-primary',
                                            format: 'd-m-Y',
                                            cancelClass: 'btn-secondary',
                                            
                                        });
                                        $('#timerange_picker').daterangepicker({
                                            buttonClasses: ' btn',
                                            applyClass: 'btn-primary',
                                            cancelClass: 'btn-secondary',
                                            timePicker: true,
                                            locale: {
                                                format: 'hh:mm A'
                                            },
                                            parentEl:'#timerange_picker_div'
                                        });
//                ClassicEditor
//                    .create( document.querySelector( '#kt-ckeditor-1' ),{
//                        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
//                        mediaEmbed: {
//                            removeProviders: [ 'dailymotion','spotify','vimeo','instagram', 'twitter', 'googleMaps', 'flickr', 'facebook','youtube' ]
//                        },
//                        heading: {
//                            options: [
//                                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
//                                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
//                                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
//                            ]
//                        }
//                    } )
//                    .then( editor => {
//                        console.log( editor );
//                    } )
//                    .catch( error => {
//                        console.error( error );
//                    } );

                                        FormValidation.formValidation(
                                                document.getElementById('companyForm'),
                                                {
                                                    fields: {
                                                        building_id: {
                                                            validators: {
                                                                notEmpty: {
                                                                    message: 'Building is required'
                                                                },
                                                            }
                                                        }
                                                        , group_id: {
                                                            validators: {
                                                                notEmpty: {
                                                                    message: 'Group is required'
                                                                },
                                                            }
                                                        },
                                                    },

                                                    plugins: {//Learn more: https://formvalidation.io/guide/plugins
                                                        trigger: new FormValidation.plugins.Trigger(),
                                                        // Bootstrap Framework Integration
                                                        bootstrap: new FormValidation.plugins.Bootstrap(),
                                                        // Validate fields when clicking the Submit button
                                                        submitButton: new FormValidation.plugins.SubmitButton(),
                                                        // Submit the form when all fields are valid
                                                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                                    }
                                                }
                                        );
                                    }
                                    return {
                                        // public functions
                                        init: function () {
                                            _initDemo1();

                                        }
                                    };
                                }();
                                function changeAlways(){
                                    if($('[name=always_range]:checked').val()=='always'){
                                        $('.range_date_div').hide();
                                        
                                    }else{
                                        $('.range_date_div').show();
                                    }
                                }
                                jQuery(document).ready(function () {
                                    KTFormControls.init();
                                    changeAlways();
                                });


</script>
@if(isset($building_policy) && $building_policy->from_date!='')


<script>
    $(document).ready(function(){
        $('#kt_daterangepicker_1').data('daterangepicker').setStartDate("{{date('m/d/Y',strtotime($building_policy->from_date))}}");
        $('#kt_daterangepicker_1').data('daterangepicker').setEndDate("{{date('m/d/Y',strtotime($building_policy->to_date))}}");
    });
</script>
@endif


@endsection
