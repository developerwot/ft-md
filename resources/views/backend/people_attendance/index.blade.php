@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">People Attendance</h5>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Search Form-->
                    <div class="d-flex align-items-center" id="kt_subheader_search">
                        <span class="text-dark-50 font-weight-bold" id="kt_subheader_total"> Total</span>
                        <form class="ml-5" action="javascript:void(0)">
                            <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                                <input type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search..." />
                                <div class="input-group-append">
														<span class="input-group-text">
															<span class="svg-icon">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																		<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                                            <!--<i class="flaticon2-search-1 icon-sm"></i>-->
														</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end::Search Form-->
                    <!--begin::Group Actions-->

                    <!--end::Group Actions-->
                </div>
                <!--end::Details-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Button-->
                    <a href="#" class=""></a>
                    <!--end::Button-->
                    <!--begin::Button-->
                    {{--<a href="{{ route('people_attendance.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2">Add People Attendance</a>--}}
                    <!--end::Button-->
                    <!--begin::Dropdown-->
                    <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">

                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">People Attendance Management
                        </h3>
                    </div>
                    <div class="card-toolbar">

                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">

                    <div class="mb-12">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-xl-12">
                                <div class="row align-items-center">
                                    {{--<div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                            <span>
																	<i class="flaticon2-search-1 text-muted"></i>
																</span>
                                        </div>
                                    </div>--}}
                                    <div class="col-md-6 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">Date:</label>
                                            <div class="col-md-6 my-2 my-md-0">
                                                <input class="form-control" id="kt_daterangepicker_1" readonly="readonly" placeholder="Select time" type="text" />
                                            </div>
                                            <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                                <a href="javascript:void(0)" class="btn btn-light-primary px-6 font-weight-bold SearchFilter1">Search</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">Custom:</label>
                                            <div class="col-md-4 my-2 my-md-0">
                                                <select class="form-control" id="month_query">
                                                    <option value=""> Month</option>
                                                    @if(!empty($months))
                                                        @foreach($months as $keym=>$month)
                                                            <option value="{{ $keym }}">{{$month}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                            </div>
                                            <div class="col-md-4 my-2 my-md-0">
                                                <select class="form-control" id="year_query">
                                                    <option value=""> Year</option>
                                                    @if(!empty($years))
                                                        @foreach($years as $keyy=>$year)
                                                            <option value="{{ $year }}">{{$year}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                            </div>
                                            <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                                <a href="javascript:void(0)" class="btn btn-light-primary px-6 font-weight-bold SearchFilter2">Search</a>
                                            </div>

                                        </div>

                                    </div>
                                    {{--<div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">

                                    </div>--}}
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--begin: Datatable-->
                    <div class="datatable datatable-bordered datatable-head-custom" id="emp_datatable"></div>
                    <!--end: Datatable-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->

    </div>
    <div id="logModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->

                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Attendance Logs</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                        <div class="main d-flex flex-column flex-row-fluid">
                            <div class="content flex-column-fluid" id="kt_content">
                                <!--begin::Card-->
                                <div class="card card-custom">
                                    <!--begin::Header-->
                                    {{--<div class="card-header flex-wrap border-0 pt-6 pb-0">--}}
                                        {{--<div class="card-title">--}}
                                            {{--<h3 class="card-label"></h3>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <!--end::Header-->
                                    <!--begin::Body-->
                                    <div class="card-body ">
                                        <table  id="table" class="table  table-bordered w-100">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Check-in</th>
                                                <th>Check-out</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody id="emptyBody">

                                            </tbody>
                                            <tr id="hiddenTable" style="display: none">
                                                <td class="Id"></td>
                                                <td class="checkIn"></td>
                                                <td class="checkOut"></td>
                                                <td class="date"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>


        </div>
    </div>




    <!-- Modal-->

    @include('backend.snippets.delete')
@endsection
    @section('scripts')
    {{--<script src="{{ asset('theme/js/pages/custom/user/list-datatable.js?v=7.0.3') }}"></script>--}}


    <script>

        $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('people_attendance.index')}}/delete/' + $(this).data('id'));
        });
        $(document).on('click', '.show-group-emp', function (e) {
            $('#exampleModal'+$(this).data('id')).modal();

        });

        $(document).on('click', '.edit-button', function (e) {
            var Id=$(this).data('id');
         var arr=   Id.split("/");
         // console.log(arr);
         $.ajax({
                url:"{{route('people_registry.ajax-attendance-log')}}",
                dataType:'json',
                type:'post',
                data:{employee_id:arr[0],date:arr[1]},
                success:function (data) {
                $('#emptyBody').empty();

                for(var i=0;i<data.length;i++){
                   var dummy= $('#hiddenTable').clone();
                   $(dummy).attr('id','');
                   $(dummy).find('.checkIn').html(data[i]['check_in']);
                   $(dummy).find('.checkOut').html(data[i]['check_out']);
                   $(dummy).find('.Id').html(i+1);
                   $(dummy).find('.date').html(data[i]['date']);
                   $(dummy).show();
                    $('#emptyBody').append(dummy);
                }

                $('#logModal').modal();
                }
            })
        });

      /*  var people_attendance = {!! $people_attendance ?? '' !!};
        console.log('people_attendance', people_attendance);*/


        var DatatableDataLocalDemoUser = {
            init: function () {

                $('#kt_daterangepicker_1').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    format:'d-m-Y',
                    cancelClass: 'btn-secondary'
                });
                var e, a, i;
               /* e = people_attendance,*/
                datatableser = $("#emp_datatable").KTDatatable({
                    data: {
                        saveState :false,
                        type: 'remote',
                        source: {
                            read: {

                                url:  AjaxUrl + '/people_attendance/get_attendance',
                                method: 'POST',

                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    console.log(raw.data.length);
                                    $("#kt_subheader_total").html(raw.data.length + ' Total ');
                                    return dataSet;

                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: false,
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {
                        input: $("#kt_subheader_search_form"),
                        key: 'generalSearch'
                    },
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        sortable: false,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    }, {
                        field: "first_name",
                        title: "People Name",
                        width: 150  ,
                        sortable: 1,
                        overflow: "visible",
                        template: function (e, a, i) {
                            if(e.employee) {
                                return e.first_name
                            }

                        }
                    }, {
                        field: "check_in",
                        title: "Check In",
                        width: 150,
                        sortable: 1,
                        overflow: "visible",

                    }, {
                        field: "check_out",
                        title: "Check Out",
                        width: 150,
                        sortable: 1,
                        overflow: "visible",

                    },  {
                        field: "Actions",
                        width: 60,
                        title: "Action",
                        sortable: !1,
                        overflow: "visible",
                        template: function (e, a, i) {

                            return ' <a href="javascript:;" data-id="' + e.employee.id+'/'+e.check_in + '"'+ 'class=" edit-button btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">'+
	                            '<span class="svg-icon svg-icon-md">'+
									'<i class="la la-eye"></i>'+
	                            '</span>'+
	                        '</a>'
	                        /*'<a href="javascript:;" data-id="' + e.id + '" class="delete-button-action btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon" title="Delete">'+
								'<span class="svg-icon svg-icon-md">'+
									'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
										'<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
											'<rect x="0" y="0" width="24" height="24"/>'+
											'<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>'+
											'<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>'+
										'</g>'+
									'</svg>'+
								'</span>'+
	                        '</a>'*/;
                        }
                    }]
                })

                    $('.SearchFilter1').on('click', function() {

                        console.log("in search1");
                        datatableser.setDataSourceParam('date_range',$('#kt_daterangepicker_1').val().toLowerCase());
                        datatableser.setDataSourceParam('search','1');
                        datatableser.search($('#kt_daterangepicker_1').val().toLowerCase(), 'date_range');
                    });

                    $('.SearchFilter2').on('click', function() {
                        console.log("in search2");
                        var month = $('#month_query').val().toLowerCase();
                        var year = $('#year_query').val().toLowerCase();
                        if(month !='' && year != '') {
                           // datatable.destroy()	;
                            monthYear = month+'-'+year;
                            //datatable.setDataSourceParam('2','SearchFilter');
                            datatableser.setDataSourceParam('search','2');
                            datatableser.setDataSourceParam('month_year',monthYear);
                            datatableser.search(monthYear, 'month_year');
                            //datatable.search(2, 'searchKey');
                        }

                    });

               /* $('#kt_daterangepicker_1').on('change', function() {

                });*/

            }
        };
        jQuery(document).ready(function () {
            DatatableDataLocalDemoUser.init();

        });
    </script>
@endsection
