@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($people_attendance) ? 'Update People Attendance' : 'Add People Attendance' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('people_attendance.index') }}" class="text-muted">People Attendance</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($people_attendance) ? route('people_attendance.index').'/'.$people_attendance->id.'/edit' : route('people_attendance.create') }} " class="text-muted">{!! isset($people_attendance) ? 'Update People Attendance' : 'Add People Attendance' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($people_attendance) ? 'Update People Attendance' : 'Add People Attendance' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($people_attendance) ? route('people_attendance.update', $people_attendance->id) : route('people_attendance.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            @if(isset($people_attendance))
                                @method('PUT')
                            @endif
                            <div class="card-body">

                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select People</label>
                                        <select class="form-control select2" id="kt_select2_3" name="employee_id" {{ isset($people_attendance) ? 'disabled':'' }}>
                                            <option value=""> Select People</option>

                                            @if($employees)
                                                @foreach($employees as $emp)
                                                    <option value="{{ $emp->id }}" @if( (isset($people_attendance) && $emp->id == $people_attendance->employee_id) || $emp->id == old('employee_id') )  selected @endif>{{ $emp->first_name.' '.$emp->last_name.' ('.$emp->email.')' }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Check In</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control " readonly="readonly" name="check_in" placeholder="Select Check In Date time" id="check_in_date_picker" value="{!! (isset($people_attendance)) ? date('Y/m/d H:i:s',strtotime($people_attendance->check_in)): old('check_in') !!}" />
                                            <div class="input-group-append">
															<span class="input-group-text">
																<i class="la la-calendar-check-o glyphicon-th"></i>
															</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Check Out</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control " readonly="readonly" name="check_out" placeholder="Select Out In Date time" id="check_out_date_picker"  value="{!! (isset($people_attendance) && !empty($people_attendance->check_out)) ? date('Y/m/d H:i:s',strtotime($people_attendance->check_out)): old('check_out') !!}"/>
                                            <div class="input-group-append">
															<span class="input-group-text">
																<i class="la la-calendar-check-o glyphicon-th"></i>
															</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('people_attendance.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
{{--
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js?v=7.0.3') }}"></script>
--}}
    <script>
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {

                $('#check_in_date_picker').datetimepicker({
                    todayHighlight: true,
                    autoclose: true,
                    pickerPosition: 'bottom-right',
                    format: 'yyyy/mm/dd hh:ii:ss'
                });
                $('#check_out_date_picker').datetimepicker({
                    todayHighlight: true,
                    autoclose: true,
                    pickerPosition: 'bottom-right',
                    format: 'yyyy/mm/dd hh:ii:ss'
                });
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            employee_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'People is required'
                                    },
                                }
                            },
                            check_in: {
                                validators: {
                                    notEmpty: {
                                        message: 'Check in Date is required'
                                    },
                                    date: {
                                        format: 'YYYY/MM/DD H:i:s',
                                        message: 'The value is not a valid date',
                                    }
                                }
                            },

                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });


    </script>
@endsection
