@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($desk) ? 'Update desk' : 'Add desk' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('desk-access.index') }}" class="text-muted">Desks</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($desk) ? route('desk-access.index').'/'.$desk->id.'/edit' : route('desk-access.create') }} " class="text-muted">{!! isset($desk) ? 'Update desk' : 'Add desk' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($desk) ? 'Update Desk' : 'Add Desk' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($desk) ? route('desk-access.update', $desk->id) : route('desk-access.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Building</label>
                                        <select name="building_id" class="form-control" id="buildingId">
                                            @foreach($buildings as $build)
                                                <option value= "{{$build->id}}"  {{  old('building_id') == $build->id ? 'selected' : '' }}   {!!  isset($desk) && $build->id==$desk->building_id ? 'selected="selected"' : '' !!}> {{$build->building_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Location</label>
                                        <select name="floor_id" class="form-control" id="locations">
                                            <option value="" style="display:none;" id="options"></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Desk name</label>
                                        <input type="text" class="form-control" name="desk_name" id="desk_name" value="{!! isset($desk) ? $desk->desk_name: old('desk_name') !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Status </label>
                                        <select name="desk_status" class="form-control">
                                            <option value="1" @if(isset($desk) && $desk->desk_status==1) selected @endif selected >Active</option>
                                            <option value="0" @if(isset($desk) && $desk->desk_status==0) selected @endif>Inactive</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="company_id" value="{{auth()->user()->company->id}}">
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('desk-access.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.3') }}"></script>
    <script>

        var slotLocation=`@php echo isset($desks) ?$desks->floor_access_id : null @endphp`;
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            desk_name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Desk name is required'
                                    },
                                }
                            }
                            , location_limit: {
                                validators: {
                                    notEmpty: {
                                        message: 'Building Limit is required'
                                    },
                                }
                            },
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
            $('#buildingId').select2();
            locations();
            $('#buildingId').change(function () {
                locations();
            })
        });
        function locations(){
            var building= $('#buildingId').val();
            $.ajax({
                url:'/desk-access/floor-ajax/'+building,
                dataType:'json',
                type:'get',
                success:function (data) {
                   var location=data.data;
                    $('#locations').find('option').not(':first').remove();
                   for(var i=0;i<location.length;i++){
                      var dummy=  $('#options').clone();
                       if(i==0){
                           $(dummy).prop('selected',true);
                       }
                      if(slotLocation){
                          if( slotLocation==location[i].id)
                          {
                              $(dummy).prop('selected',true);
                          }
                      }

                   $(dummy).attr('id','');
                   $(dummy).val(location[i].id);
                   $(dummy).text(location[i].floor_name);
                   $(dummy).show();
                   $('#locations').append(dummy);
                       $('#locations').select2();
                       $('#buildingId').select2();
                   }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('#locations').find('option').not(':first').remove();
                    var dummy=  $('#options').clone();
                    $(dummy).attr('id','');
                    $(dummy).val('');
                    $(dummy).text('No floors available');
                    $(dummy).prop('selected',true);
                    $(dummy).show();
                    $('#locations').append(dummy);
                    $('#locations').select2();
                    $('#buildingId').select2();
                }
            });
        }

    </script>
@endsection
