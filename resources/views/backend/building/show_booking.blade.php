<table class="table">
    @if(!empty($booking_lists))
        <thead>
        <tr>
            <th>Date</th>
            <th>Count</th>
            <th>Maximum Allowed</th>
        </tr>
        </thead>
        <tbody>
        @foreach($booking_lists as $key=>$booking)
            <tr>
                <td>{{ date("d-M-Y",strtotime($booking['booking_date'])) }}</td>
                <td>{{ $booking['booking_count'] }}</td>
                <td>{{ $building->building_limit }}</td>
            </tr>

        @endforeach
        </tbody>
    @endif

</table>