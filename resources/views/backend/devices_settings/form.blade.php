@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($device_settings) ? 'Update Device Setting' : 'Add Device Setting' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('device_settings.index') }}" class="text-muted">Device Settings</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{  route('device_settings.settings')  }} " class="text-muted">{!! isset($device_settings) ? 'Update Device Setting' : 'Add Device Setting' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($device_settings) ? 'Update Device Setting' : 'Add Device Setting' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="  {{ route('device_settings.update_settings') }}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                           {{-- @if(isset($device_settings))
                                @method('PUT')
                            @endif--}}
                            <div class="card-body">
                                  <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Camera Name</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->camera_name : old('camera_name') !!}" name="camera_name" placeholder="" />
                                    </div>
                                </div>
                                    <div class="row">
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">Building </label>
                                        <select class="form-control select2" id="kt_select2_4" name="building_id" >
                                            <option value=""> Select Building</option>

                                            @if($buildings)
                                                @foreach($buildings as $building)
                                                    <option value="{{ $building->id }}" @if( (isset($device_settings) && $building->id == $device_settings->building_id) || $building->id == old('building_id') )  selected @endif>{{ $building->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Host</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}" name="host" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Port</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}" name="port" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Video stream url (In Entry)</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}" name="video_stream_url" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Video stream url (Out Entry)</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->video_stream_url_out : old('video_stream_url_out') !!}" name="video_stream_url_out" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Face detect at seconds</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->face_detect_at_seconds : old('face_detect_at_seconds') !!}" name="face_detect_at_seconds" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Recognize at seconds</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}" name="recognize_at_seconds" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Temperature check</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}" name="temperature_check" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Temperature check end</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}" name="temperature_check_end" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Temperature threshold</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->temperature_threshold : old('temperature_threshold') !!}" name="temperature_threshold" placeholder="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Door open for green at</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->door_open_for_green_at : old('door_open_for_green_at') !!}" name="door_open_for_green_at" placeholder="" />
                                    </div>
                                </div>
                              
                               {{-- <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Door open for red at</label>
                                        <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->door_open_for_red_at : old('door_open_for_red_at') !!}" name="door_open_for_red_at" placeholder="" />
                                    </div>
                                </div>--}}
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('device_settings.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
        <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            host: {
                                validators: {
                                    notEmpty: {
                                        message: 'Host is required'
                                    },
                                }
                            }, port: {
                                validators: {
                                    notEmpty: {
                                        message: 'Port is required'
                                    },
                                }
                            }/*, video_stream_url: {
                                validators: {
                                    notEmpty: {
                                        message: 'Video Url is required'
                                    },
                                }
                            }*/,
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            };
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });


    </script>
@endsection
