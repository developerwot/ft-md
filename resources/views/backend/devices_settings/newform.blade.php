@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($selected) ? 'Update Gates Setting' : 'Add Gates Setting' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('device_settings.index') }}" class="text-muted">Gates Settings</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)"
                                   class="text-muted">{!! isset($selected) ? 'Update Gates Setting' : 'Add Gates Setting' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id=""
                       data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                          action="{{isset($selected) ? route('device_settings.update_settings',$location) : route('device_settings.store') }}"
                          method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <h3 class="card-title">{!! isset($selected) ? 'Update Gates Setting' : 'Add Gates Setting' !!} </h3>
                                <div class="card-toolbar">
                                    {{--<a href="javascript:void(0)"--}}
                                    {{--class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2 addDevice">Add--}}
                                    {{--Device</a>--}}
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    @if(isset($selected))
                                        <input type="hidden" value="{{$selected}}" name="selected">
                                    @endif
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">Building </label>
                                        <select class="form-control select2 building_id" id="kt_select2_4"
                                                name="building_id">
                                            <option value=""> Select Building</option>

                                            @if($buildings)
                                                @foreach($buildings as $building)
                                                    <option value="{{ $building->id }}"
                                                            @if( (isset($device_settings) && $building->id == $device_settings->building_id) || $building->id == old('building_id') )  selected @endif>{{ $building->building_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Location</label>
                                        <select name="location_id" class="form-control" id="locations">
                                            <option value="" style="display:none;" id="options"></option>
                                        </select>
                                    </div>
                                        @if(auth()->user()->company->people_link_status==1)
                                        <div class="form-group col-lg-6">
                                            <label class="form-control-label">Device Id</label>
                                            <input type="text" name="device_id" id="device_id" class="form-control" value="{{isset($deviceId) ? $deviceId : old('device_id')}}">
                                        </div>
                                        @endif
                                    {{--<div class="form-group col-lg-6">--}}
                                    {{--<label class="form-control-label">Type</label>--}}
                                    {{--<select class="form-control" id="" name="camera_type">--}}
                                    {{--<option value="0">Entrance</option>--}}
                                    {{--<option value="0">Exit</option>--}}
                                    {{--</select>--}}
                                    {{--</div>--}}
                                </div>

                                {{-- <div class="row">
                                     <div class="form-group col-lg-6">
                                         <label class="form-control-label">Door open for red at</label>
                                         <input type="text" class="form-control form-control-lg " value="{!! isset($device_settings) ? $device_settings->door_open_for_red_at : old('door_open_for_red_at') !!}" name="door_open_for_red_at" placeholder="" />
                                     </div>
                                 </div>--}}
                            </div>
                        </div>
                        @if(!empty(old('entrance')))
                            <div class="card_container devices-cards">
                                <div class="card card-custom gutter-b example example-compact count" id="device-card">
                                    <div class="card-body" id="body-container">
                                                <div class="card entrance-card card-custom gutter-b example example-compact">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Entrance</h3>
                                                        </div>
                                                    </div>
                                                    <div class="card-body" id="service-container">
                                                        <div class="row">
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Camera name</label>
                                                                <input type="text" name="entrance[camera_name]"
                                                                       class="form-control"
                                                                       value="{{old('entrance.camera_name')}}">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Device type</label>
                                                                <select class="form-control" onchange="deviceType(this)"
                                                                        name="entrance[device_type]">
                                                                    <option value="">Select device type</option>
                                                                    <option
                                                                        value="thermal" {{old('entrance.device_type')=='thermal' ? 'selected' : ''}}>
                                                                        Thermal camera
                                                                    </option>
                                                                    <option
                                                                        value="ip" {{old('entrance.device_type')=='ip' ? 'selected' : ''}}>
                                                                        IP camera
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        @if(old('entrance.device_type')=='thermal')
                                                            <div id="thermal" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.green_relay')}}"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.green_relay_command')}}"
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.green_relay_duration')}}"
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.red_relay')}}"
                                                                                       name="entrance[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.red_relay_command')}}"
                                                                                       name="entrance[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.red_relay_duration')}}"
                                                                                       name="entrance[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.buzzer_relay')}}"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.buzzer_relay_command')}}"
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.buzzer_relay_duration')}}"

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.temperature_threshold')}}"
                                                                                       name="entrance[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.host')}}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.port')}}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{old('entrance.video_stream_url')}}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.face_detect_seconds')}}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.recognize_at_seconds')}}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.temperature_check')}}"
                                                                                       name="entrance[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.temperature_check_end')}}"
                                                                                       name="entrance[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="entrance[face_detection]" {{old('entrance.face_detection')==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 @if(old('entrance.face_detection')!=1) style="display: none" @endif>
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="image" {{old('entrance.recognise_on')=='image' ? 'checked' : ''}} checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]" {{old('entrance.recognise_on')=='video' ? 'checked' : ''}}
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="entrance[temperature_detection]" {{old('entrance.temperature_detection')==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>




                                                        @elseif(old('entrance.device_type')=='ip')
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="entrance[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="entrance[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="entrance[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="entrance[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="entrance[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip"  class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.green_relay')}}"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.green_relay_command')}}"
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.green_relay_duration')}}"
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.buzzer_relay')}}"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.buzzer_relay_command')}}"
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.buzzer_relay_duration')}}"
                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.host')}}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.port')}}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{old('entrance.video_stream_url')}}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.face_detect_seconds')}}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('entrance.recognize_at_seconds')}}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox" {{old('entrance.face_detection')==1 ? 'checked' : ''}}
                                                                                        name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        @else
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="entrance[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="entrance[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="entrance[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="entrance[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="entrance[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="card exit-card card-custom gutter-b example example-compact">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Exit</h3>
                                                        </div>
                                                    </div>
                                                    <div class="card-body" id="service-container">
                                                        <div class="row">
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Camera name</label>
                                                                <input type="text" name="exit[camera_name]" value="{{old('exit.camera_name')}}"
                                                                       class="form-control">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Device type</label>
                                                                <select class="form-control" onchange="deviceType(this)"
                                                                        name="exit[device_type]">
                                                                    <option value="">Select device type</option>
                                                                    <option
                                                                        value="thermal" {{old('exit.device_type')=='thermal' ? 'selected' : ''}}>
                                                                        Thermal camera
                                                                    </option>
                                                                    <option
                                                                        value="ip" {{old('exit.device_type')=='ip' ? 'selected' : ''}}>
                                                                        IP camera
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        @if(old('exit.device_type')=='thermal')
                                                            <div id="thermal" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.green_relay')}}"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.green_relay_command')}}"
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.green_relay_duration')}}"
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.red_relay')}}"
                                                                                       name="exit[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.red_relay_command')}}"
                                                                                       name="exit[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.red_relay_duration')}}"
                                                                                       name="exit[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.buzzer_relay')}}"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.buzzer_relay_command')}}"
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.buzzer_relay_duration')}}"

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.temperature_threshold')}}"
                                                                                       name="exit[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.host')}}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.port')}}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{old('exit.video_stream_url')}}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.face_detect_seconds')}}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.recognize_at_seconds')}}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.temperature_check')}}"
                                                                                       name="exit[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.temperature_check_end')}}"
                                                                                       name="exit[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="exit[face_detection]" {{old('exit.face_detection')==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 @if(old('exit.face_detection')!=1) style="display: none" @endif>
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="image" {{old('exit.recognise_on')=='image' ? 'checked' : ''}} checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]" {{old('exit.recognise_on')=='video' ? 'checked' : ''}}
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="exit[temperature_detection]" {{old('exit.temperature_detection')==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        @elseif(old('exit.device_type')=='ip')
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="exit[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="exit[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="exit[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="exit[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="exit[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip"  class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.green_relay')}}"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.green_relay_command')}}"
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.green_relay_duration')}}"
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.buzzer_relay')}}"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.buzzer_relay_command')}}"
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.buzzer_relay_duration')}}"
                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.host')}}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.port')}}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{old('exit.video_stream_url')}}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.face_detect_seconds')}}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{old('exit.recognize_at_seconds')}}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox" {{old('exit.face_detection')==1 ? 'checked' : ''}}
                                                                                        name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @else
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="exit[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="exit[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="exit[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="exit[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="exit[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                    </div>
                                </div>
                            </div>
                        @else

                    @if(isset($deviceData))
                            <div class="card_container devices-cards">
                                <div class="card card-custom gutter-b example example-compact count" id="device-card">
                                    <div class="card-body" id="body-container">
                                        @foreach($deviceData as $key=>$data)
                                            @if($key=='entrance')
                                                <div
                                                    class="card entrance-card card-custom gutter-b example example-compact">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Entrance</h3>
                                                        </div>
                                                    </div>
                                                    <div class="card-body" id="service-container">
                                                        <div class="row">
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Camera name</label>
                                                                <input type="text" name="entrance[camera_name]"
                                                                       class="form-control"
                                                                       value="{{$data->camera_name}}">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Device type</label>
                                                                <select class="form-control" onchange="deviceType(this)"
                                                                        name="entrance[device_type]">
                                                                    <option value="">Select device type</option>
                                                                    <option
                                                                        value="thermal" {{$data->device_type=='thermal' ? 'selected' : ''}}>
                                                                        Thermal camera
                                                                    </option>
                                                                    <option
                                                                        value="ip" {{$data->device_type=='ip' ? 'selected' : ''}}>
                                                                        IP camera
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        @if($data->device_type=='thermal')
                                                            <div id="thermal" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay}}"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_command}}"
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_duration}}"
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->red_relay}}"
                                                                                       name="entrance[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->red_relay_command}}"
                                                                                       name="entrance[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->red_relay_duration}}"
                                                                                       name="entrance[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay}}"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_command}}"
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_duration}}"

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->temperature_threshold}}"
                                                                                       name="entrance[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->host}}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->port}}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{$data->video_stream_url}}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->face_detect_seconds}}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->recognize_at_seconds}}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->temperature_check}}"
                                                                                       name="entrance[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->temperature_check_end}}"
                                                                                       name="entrance[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="entrance[face_detection]" {{$data->face_detection==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 @if($data->face_detection!=1) style="display: none" @endif>
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="image" {{$data->recognise_on=='image' ? 'checked' : ''}} checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]" {{$data->recognise_on=='video' ? 'checked' : ''}}
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="entrance[temperature_detection]" {{$data->temperature_detection==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @elseif($data->device_type=='ip')
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="entrance[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="entrance[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="entrance[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="entrance[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="entrance[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay}}"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_command}}"
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_duration}}"
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay}}"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_command}}"
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_duration}}"
                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->host}}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->port}}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{$data->video_stream_url}}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->face_detect_seconds}}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->recognize_at_seconds}}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox" {{$data->face_detection==1 ? 'checked' : ''}}
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @else
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="entrance[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="entrance[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="entrance[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="entrance[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="entrance[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="entrance[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="entrance[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="entrance[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="entrance[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="entrance[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="entrance[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="entrance[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="entrance[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="entrance[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="entrance[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="entrance[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                            @if($key=='exit')
                                                <div class="card exit-card card-custom gutter-b example example-compact">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Exit</h3>
                                                        </div>
                                                    </div>
                                                    <div class="card-body" id="service-container">
                                                        <div class="row">
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Camera name</label>
                                                                <input type="text" name="exit[camera_name]" value="{{$data->camera_name}}"
                                                                       class="form-control">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="form-control-label">Device type</label>
                                                                <select class="form-control" onchange="deviceType(this)"
                                                                        name="exit[device_type]">
                                                                    <option value="">Select device type</option>
                                                                    <option
                                                                        value="thermal" {{$data->device_type=='thermal' ? 'selected' : ''}}>
                                                                        Thermal camera
                                                                    </option>
                                                                    <option
                                                                        value="ip" {{$data->device_type=='ip' ? 'selected' : ''}}>
                                                                        IP camera
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        @if($data->device_type=='thermal')
                                                            <div id="thermal" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay}}"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_command}}"
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_duration}}"
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->red_relay}}"
                                                                                       name="exit[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->red_relay_command}}"
                                                                                       name="exit[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->red_relay_duration}}"
                                                                                       name="exit[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay}}"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_command}}"
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_duration}}"

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->temperature_threshold}}"
                                                                                       name="exit[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->host}}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->port}}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{$data->video_stream_url}}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->face_detect_seconds}}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->recognize_at_seconds}}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->temperature_check}}"
                                                                                       name="exit[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->temperature_check_end}}"
                                                                                       name="exit[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="exit[face_detection]" {{$data->face_detection==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 @if($data->face_detection!=1) style="display: none" @endif>
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="image" {{$data->recognise_on=='image' ? 'checked' : ''}} checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]" {{$data->recognise_on=='video' ? 'checked' : ''}}
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="exit[temperature_detection]" {{$data->temperature_detection==1 ? 'checked' : ''}}><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @elseif($data->device_type=='ip')
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="exit[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="exit[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="exit[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input disabled type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="exit[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input disabled type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input disabled type="checkbox"
                                                                                                  name="exit[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip"  class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay}}"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_command}}"
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->green_relay_duration}}"
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay}}"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_command}}"
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->buzzer_relay_duration}}"
                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->host}}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->port}}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{{$data->video_stream_url}}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->face_detect_seconds}}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{{$data->recognize_at_seconds}}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox" {{$data->face_detection==1 ? 'checked' : ''}}
                                                                                        name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @else
                                                            <div id="thermal" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Red
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-red"
                                                                                       name="exit[red_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[red_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Threshold
                                                                                    Temperature</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="37.5"
                                                                                       name="exit[temperature_threshold]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                                       name="exit[temperature_check]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Temperature
                                                                                    check end</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                                       name="exit[temperature_check_end]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="thermal-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  class="FaceDetect"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12  hidden-radio"
                                                                                 style="display: none;">
                                                                                <div class="radio-inline">
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="image" checked>
                                                                                        <span></span>Detect from
                                                                                        image</label>
                                                                                    <label class="radio">
                                                                                        <input type="radio"
                                                                                               name="exit[recognise_on]"
                                                                                               value="video">
                                                                                        <span></span>Detect from
                                                                                        video</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Temperature
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="exit[temperature_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div id="ip" style="display: none;" class="row">
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">

                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Settings</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Green
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="intellyscan-green"
                                                                                       name="exit[green_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[green_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Buzzer
                                                                                    Relay </label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="Buzzer relay"
                                                                                       name="exit[buzzer_relay]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Command</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""
                                                                                       name="exit[buzzer_relay_command]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Duration</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value=""

                                                                                       name="exit[buzzer_relay_duration]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Host</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                                       name="exit[host]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label
                                                                                    class="form-control-label">Port</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                                       name="exit[port]"
                                                                                       placeholder=""/>
                                                                            </div>


                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Video
                                                                                    stream url</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg"
                                                                                       id="videoUrl"
                                                                                       value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                                       name="exit[video_stream_url]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Face
                                                                                    detect at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                                       name="exit[face_detect_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>

                                                                            <div class="form-group col-lg-12">
                                                                                <label class="form-control-label">Recognize
                                                                                    at seconds</label>
                                                                                <input type="text"
                                                                                       class="form-control form-control-lg "
                                                                                       value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                                       name="exit[recognize_at_seconds]"
                                                                                       placeholder=""/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    class="card card-custom gutter-b example example-compact col-lg-6">
                                                                    <div class="card-header">
                                                                        <div class="card-title">
                                                                            <h3 class="card-label">Services</h3>
                                                                        </div>

                                                                    </div>

                                                                    <div class="card-body ">
                                                                        <div class="ip-services row">
                                                                            <div class="col-lg-4">
                                                                                <label class="form-check-label mt-9">Face
                                                                                    Detection</label>
                                                                            </div>
                                                                            <div class="form-group col-lg-8">

                                                                                <!-- Material switch -->
                                                                                <br>
                                                                                <div class="switch">
                                                                                    <label><input type="checkbox"
                                                                                                  name="exit[face_detection]"><span
                                                                                            class="lever"></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card_container devices-cards">
                                <div class="card card-custom gutter-b example example-compact count" id="device-card">
                                    <div class="card-body" id="body-container">
                                        <div class="card entrance-card card-custom gutter-b example example-compact">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 class="card-label">Entrance</h3>
                                                </div>
                                            </div>
                                            <div class="card-body" id="service-container">
                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-control-label">Camera name</label>
                                                        <input type="text" name="entrance[camera_name]"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-control-label">Device type</label>
                                                        <select class="form-control" onchange="deviceType(this)"
                                                                name="entrance[device_type]">
                                                            <option value="">Select device type</option>
                                                            <option value="thermal">Thermal camera</option>
                                                            <option value="ip">IP camera</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="thermal" style="display: none;" class="row">
                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">

                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Settings</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="thermal-services row">
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Green
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="intellyscan-green"
                                                                           name="entrance[green_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[green_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[green_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Red Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="intellyscan-red"
                                                                           name="entrance[red_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[red_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[red_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Buzzer
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="Buzzer relay"
                                                                           name="entrance[buzzer_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[buzzer_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""

                                                                           name="entrance[buzzer_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Threshold
                                                                        Temperature</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="37.5"
                                                                           name="entrance[temperature_threshold]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Host</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                           name="entrance[host]" placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Port</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                           name="entrance[port]" placeholder=""/>
                                                                </div>


                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Video stream
                                                                        url</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg"
                                                                           id="videoUrl"
                                                                           value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                           name="entrance[video_stream_url]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Face detect at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                           name="entrance[face_detect_seconds]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Recognize at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                           name="entrance[recognize_at_seconds]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Temperature
                                                                        check</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                           name="entrance[temperature_check]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Temperature check
                                                                        end</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                           name="entrance[temperature_check_end]"
                                                                           placeholder=""/>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">
                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Services</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="thermal-services row">
                                                                <div class="col-lg-4">
                                                                    <label class="form-check-label mt-9">Face
                                                                        Detection</label>
                                                                </div>
                                                                <div class="form-group col-lg-8">

                                                                    <br>
                                                                    <div class="switch">
                                                                        <label><input type="checkbox" class="FaceDetect"
                                                                                      name="entrance[face_detection]"><span
                                                                                class="lever"></span></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12  hidden-radio"
                                                                     style="display: none;">
                                                                    <div class="radio-inline">
                                                                        <label class="radio">
                                                                            <input type="radio"
                                                                                   name="entrance[recognise_on]"
                                                                                   value="image" checked>
                                                                            <span></span>Detect from image</label>
                                                                        <label class="radio">
                                                                            <input type="radio"
                                                                                   name="entrance[recognise_on]"
                                                                                   value="video">
                                                                            <span></span>Detect from video</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label class="form-check-label mt-9">Temperature
                                                                        Detection</label>
                                                                </div>
                                                                <div class="form-group col-lg-8">

                                                                    <br>
                                                                    <div class="switch">
                                                                        <label><input type="checkbox"
                                                                                      name="entrance[temperature_detection]"><span
                                                                                class="lever"></span></label>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div id="ip" style="display: none;" class="row">
                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">

                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Settings</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="ip-services row">
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Green
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="intellyscan-green"
                                                                           name="entrance[green_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[green_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[green_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Buzzer
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="Buzzer relay"
                                                                           name="entrance[buzzer_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="entrance[buzzer_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""

                                                                           name="entrance[buzzer_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Host</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                           name="entrance[host]" placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Port</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                           name="entrance[port]" placeholder=""/>
                                                                </div>


                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Video stream
                                                                        url</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg"
                                                                           id="videoUrl"
                                                                           value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                           name="entrance[video_stream_url]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Face detect at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                           name="entrance[face_detect_seconds]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Recognize at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                           name="entrance[recognize_at_seconds]"
                                                                           placeholder=""/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">
                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Services</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="ip-services row">
                                                                <div class="col-lg-4">
                                                                    <label class="form-check-label mt-9">Face
                                                                        Detection</label>
                                                                </div>
                                                                <div class="form-group col-lg-8">

                                                                    <!-- Material switch -->
                                                                    <br>
                                                                    <div class="switch">
                                                                        <label><input type="checkbox"
                                                                                      name="entrance[face_detection]"><span
                                                                                class="lever"></span></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card exit-card card-custom gutter-b example example-compact">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 class="card-label">Exit</h3>
                                                </div>
                                            </div>
                                            <div class="card-body" id="service-container">
                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-control-label">Camera name</label>
                                                        <input type="text" name="exit[camera_name]"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="form-control-label">Device type</label>
                                                        <select class="form-control" onchange="deviceType(this)"
                                                                name="exit[device_type]">
                                                            <option value="">Select device type</option>
                                                            <option value="thermal">Thermal camera</option>
                                                            <option value="ip">IP camera</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="thermal" style="display: none;" class="row">
                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">

                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Settings</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="thermal-services row">
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Green
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="intellyscan-green"
                                                                           name="exit[green_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[green_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[green_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Red Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="intellyscan-red"
                                                                           name="exit[red_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[red_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[red_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Buzzer
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="Buzzer relay"
                                                                           name="exit[buzzer_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[buzzer_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""

                                                                           name="exit[buzzer_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Threshold
                                                                        Temperature</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="37.5"
                                                                           name="exit[temperature_threshold]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Host</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                           name="exit[host]" placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Port</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                           name="exit[port]" placeholder=""/>
                                                                </div>


                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Video stream
                                                                        url</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg"
                                                                           id="videoUrl"
                                                                           value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                           name="exit[video_stream_url]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Face detect at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                           name="exit[face_detect_seconds]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Recognize at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                           name="exit[recognize_at_seconds]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Temperature
                                                                        check</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"
                                                                           name="exit[temperature_check]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Temperature check
                                                                        end</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"
                                                                           name="exit[temperature_check_end]"
                                                                           placeholder=""/>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">
                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Services</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="thermal-services row">
                                                                <div class="col-lg-4">
                                                                    <label class="form-check-label mt-9">Face
                                                                        Detection</label>
                                                                </div>
                                                                <div class="form-group col-lg-8">

                                                                    <br>
                                                                    <div class="switch">
                                                                        <label><input type="checkbox" class="FaceDetect"
                                                                                      name="exit[face_detection]"><span
                                                                                class="lever"></span></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12  hidden-radio"
                                                                     style="display: none;">
                                                                    <div class="radio-inline">
                                                                        <label class="radio">
                                                                            <input type="radio"
                                                                                   name="exit[recognise_on]"
                                                                                   value="image" checked>
                                                                            <span></span>Detect from image</label>
                                                                        <label class="radio">
                                                                            <input type="radio"
                                                                                   name="exit[recognise_on]"
                                                                                   value="video">
                                                                            <span></span>Detect from video</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label class="form-check-label mt-9">Temperature
                                                                        Detection</label>
                                                                </div>
                                                                <div class="form-group col-lg-8">

                                                                    <br>
                                                                    <div class="switch">
                                                                        <label><input type="checkbox"
                                                                                      name="exit[temperature_detection]"><span
                                                                                class="lever"></span></label>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div id="ip" style="display: none;" class="row">
                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">

                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Settings</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="ip-services row">
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Green
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="intellyscan-green"
                                                                           name="exit[green_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[green_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[green_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Buzzer
                                                                        Relay </label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="Buzzer relay"
                                                                           name="exit[buzzer_relay]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Command</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""
                                                                           name="exit[buzzer_relay_command]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Duration</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value=""

                                                                           name="exit[buzzer_relay_duration]"
                                                                           placeholder=""/>
                                                                </div>
                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Host</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"
                                                                           name="exit[host]" placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Port</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"
                                                                           name="exit[port]" placeholder=""/>
                                                                </div>


                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Video stream
                                                                        url</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg"
                                                                           id="videoUrl"
                                                                           value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"
                                                                           name="exit[video_stream_url]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Face detect at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"
                                                                           name="exit[face_detect_seconds]"
                                                                           placeholder=""/>
                                                                </div>

                                                                <div class="form-group col-lg-12">
                                                                    <label class="form-control-label">Recognize at
                                                                        seconds</label>
                                                                    <input type="text"
                                                                           class="form-control form-control-lg "
                                                                           value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"
                                                                           name="exit[recognize_at_seconds]"
                                                                           placeholder=""/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div
                                                        class="card card-custom gutter-b example example-compact col-lg-6">
                                                        <div class="card-header">
                                                            <div class="card-title">
                                                                <h3 class="card-label">Services</h3>
                                                            </div>

                                                        </div>

                                                        <div class="card-body ">
                                                            <div class="ip-services row">
                                                                <div class="col-lg-4">
                                                                    <label class="form-check-label mt-9">Face
                                                                        Detection</label>
                                                                </div>
                                                                <div class="form-group col-lg-8">

                                                                    <!-- Material switch -->
                                                                    <br>
                                                                    <div class="switch">
                                                                        <label><input type="checkbox"
                                                                                      name="exit[face_detection]"><span
                                                                                class="lever"></span></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif


                        @endif
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                            <button type="button" class="btn btn-light-success font-weight-bold"
                                    onclick="return window.location.href='{{ route('device_settings.index') }}'">Cancel
                            </button>
                        </div>

                    </form>
                    <!--end::Form-->
                {{--<div style="display: none" id="service-card-thermal" class="row">--}}
                {{--<div class="card card-custom gutter-b example example-compact col-lg-6">--}}

                {{--<div class="card-header">--}}
                {{--<div class="card-title">--}}
                {{--<h3 class="card-label">Settings</h3>--}}
                {{--</div>--}}

                {{--</div>--}}

                {{--<div class="card-body ">--}}
                {{--<div class="thermal-services row">--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Green Relay </label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="intellyscan-green" name="green_relay"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Command</label>--}}
                {{--<input type="text" class="form-control form-control-lg " value=""--}}
                {{--name="entrance[green_relay_command]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Duration</label>--}}
                {{--<input type="text" class="form-control form-control-lg " value=""--}}
                {{--name="entrance[green_relay_duration]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Red Relay </label>--}}
                {{--<input type="text" class="form-control form-control-lg " value="intellyscan-red"--}}
                {{--name="entrance[red_relay]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Command</label>--}}
                {{--<input type="text" class="form-control form-control-lg " value=""--}}
                {{--name="entrance[red_relay_command]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Duration</label>--}}
                {{--<input type="text" class="form-control form-control-lg " value=""--}}
                {{--name="entrance[red_relay_duration]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Buzzer--}}
                {{--Relay </label>--}}
                {{--<input type="text"--}}
                {{--class="form-control form-control-lg "--}}
                {{--value="Buzzer relay"--}}
                {{--name="entrance[buzzer_relay]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Command</label>--}}
                {{--<input type="text"--}}
                {{--class="form-control form-control-lg "--}}
                {{--value=""--}}
                {{--name="entrance[buzzer_relay_command]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Duration</label>--}}
                {{--<input type="text"--}}
                {{--class="form-control form-control-lg "--}}
                {{--value=""--}}

                {{--name="entrance[buzzer_relay_duration]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Threshold Temperature</label>--}}
                {{--<input type="text" class="form-control form-control-lg " value="37.5"--}}
                {{--name="entrance[temperature_threshold]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Host</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"--}}
                {{--name="entrance[host]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Port</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"--}}
                {{--name="entrance[port]" placeholder=""/>--}}
                {{--</div>--}}


                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Video stream url</label>--}}
                {{--<input type="text" class="form-control form-control-lg" id="videoUrl"--}}
                {{--value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"--}}
                {{--name="entrance[video_stream_url]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Face detect at seconds</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"--}}
                {{--name="entrance[face_detect_seconds]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Recognize at seconds</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"--}}
                {{--name="entrance[recognize_at_seconds]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Temperature check</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->temperature_check : old('temperature_check') !!}"--}}
                {{--name="entrance[temperature_check]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Temperature check end</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->temperature_check_end : old('temperature_check_end') !!}"--}}
                {{--name="entrance[temperature_check_end]" placeholder=""/>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="card card-custom gutter-b example example-compact col-lg-6">--}}
                {{--<div class="card-header">--}}
                {{--<div class="card-title">--}}
                {{--<h3 class="card-label">Services</h3>--}}
                {{--</div>--}}

                {{--</div>--}}

                {{--<div class="card-body ">--}}
                {{--<div class="thermal-services row">--}}
                {{--<div class="col-lg-4">--}}
                {{--<label class="form-check-label mt-9">Face Detection</label>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-8">--}}

                {{--<br>--}}
                {{--<div class="switch">--}}
                {{--<label><input type="checkbox" class="FaceDetect" name="entrance[face_detection]"><span--}}
                {{--class="lever"></span></label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-12  hidden-radio" style="display: none;">--}}
                {{--<div class="radio-inline">--}}
                {{--<label class="radio">--}}
                {{--<input type="radio" name="entrance[recognise_on]" value="image" checked>--}}
                {{--<span></span>Detect from image</label>--}}
                {{--<label class="radio">--}}
                {{--<input type="radio" name="entrance[recognise_on]" value="video">--}}
                {{--<span></span>Detect from video</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-4">--}}
                {{--<label class="form-check-label mt-9">Temperature Detection</label>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-8">--}}

                {{--<br>--}}
                {{--<div class="switch">--}}
                {{--<label><input type="checkbox" name="entrance[temperature_detection]"><span--}}
                {{--class="lever"></span></label>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}

                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}
                {{--<div style="display: none" id="service-card-ip" class="row">--}}
                {{--<div class="card card-custom gutter-b example example-compact col-lg-6">--}}

                {{--<div class="card-header">--}}
                {{--<div class="card-title">--}}
                {{--<h3 class="card-label">Settings</h3>--}}
                {{--</div>--}}

                {{--</div>--}}

                {{--<div class="card-body ">--}}
                {{--<div class="ip-services row">--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Green Relay </label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="intellyscan-green" name="entrance[green_relay]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Command</label>--}}
                {{--<input type="text" class="form-control form-control-lg " value=""--}}
                {{--name="entrance[green_relay_command]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Duration</label>--}}
                {{--<input type="text" class="form-control form-control-lg " value=""--}}
                {{--name="entrance[green_relay_duration]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Buzzer--}}
                {{--Relay </label>--}}
                {{--<input type="text"--}}
                {{--class="form-control form-control-lg "--}}
                {{--value="Buzzer relay"--}}
                {{--name="entrance[buzzer_relay]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Command</label>--}}
                {{--<input type="text"--}}
                {{--class="form-control form-control-lg "--}}
                {{--value=""--}}
                {{--name="entrance[buzzer_relay_command]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Duration</label>--}}
                {{--<input type="text"--}}
                {{--class="form-control form-control-lg "--}}
                {{--value=""--}}

                {{--name="entrance[buzzer_relay_duration]"--}}
                {{--placeholder=""/>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Host</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->host : old('host') !!}"--}}
                {{--name="entrance[host]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Port</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->port : old('port') !!}"--}}
                {{--name="entrance[port]" placeholder=""/>--}}
                {{--</div>--}}


                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Video stream url</label>--}}
                {{--<input type="text" class="form-control form-control-lg" id="videoUrl"--}}
                {{--value="{!! isset($device_settings) ? $device_settings->video_stream_url : old('video_stream_url') !!}"--}}
                {{--name="entrance[video_stream_url]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Face detect at seconds</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->face_detect_seconds : old('face_detect_seconds') !!}"--}}
                {{--name="entrance[face_detect_seconds]" placeholder=""/>--}}
                {{--</div>--}}

                {{--<div class="form-group col-lg-12">--}}
                {{--<label class="form-control-label">Recognize at seconds</label>--}}
                {{--<input type="text" class="form-control form-control-lg "--}}
                {{--value="{!! isset($device_settings) ? $device_settings->recognize_at_seconds : old('recognize_at_seconds') !!}"--}}
                {{--name="entrance[recognize_at_seconds]" placeholder=""/>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--<div class="card card-custom gutter-b example example-compact col-lg-6">--}}
                {{--<div class="card-header">--}}
                {{--<div class="card-title">--}}
                {{--<h3 class="card-label">Services</h3>--}}
                {{--</div>--}}

                {{--</div>--}}

                {{--<div class="card-body ">--}}
                {{--<div class="ip-services row">--}}
                {{--<div class="col-lg-4">--}}
                {{--<label class="form-check-label mt-9">Face Detection</label>--}}
                {{--</div>--}}
                {{--<div class="form-group col-lg-8">--}}

                {{--<!-- Material switch -->--}}
                {{--<br>--}}
                {{--<div class="switch">--}}
                {{--<label><input type="checkbox" name="entrance[face_detection]"><span--}}
                {{--class="lever"></span></label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}


                <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>

        // var en=new Array();
        var slotLocation = `@php echo isset($location) ?$location : null @endphp`;
        var permit = `@php echo isset($selected) ?$selected: null @endphp`;
        var old = `@php echo old('location_id', null) ? old('location_id') : null  @endphp`;
      {{--var en = `@php echo old('entrance', null) ? old('entrance.camera_name') : null  @endphp`;--}}
          console.log(slotLocation);
        jQuery(document).ready(function () {
            if (permit) {
                $('.building_id').find('option[value="' + permit + '"]').prop('selected', true);
                $('.building_id').select2();
                locations(permit);
            }else{
                var building=$('.building_id').val();
                locations(building);
            }

            $(document).on('click', '.FaceDetect', function () {
                if ($(this).prop('checked') == true) {
                    $(this).parents(':eq(3)').find('div.hidden-radio').show()
                } else {
                    $(this).parents(':eq(3)').find('div.hidden-radio').hide();
                }
            });
            $('.addDevice').click(function () {
                // var count=$('.count').length;
                var dummy = $('#device-card').clone();
                $(dummy).attr('id', '');
                // $(dummy).find('#card-title').text(' '+count);
                $(dummy).show();
                $('.card_container').append(dummy);
            });

        });

        function deviceType(val) {
            if ($(val).val() == 'thermal') {
                $(val).parent().parent().parent().find('#ip').find('input[type=text]').attr('disabled', true);
                $(val).parent().parent().parent().find('#ip').find('input[type=checkbox]').prop('disable', true);
                $(val).parent().parent().parent().find('#ip').hide();
                $(val).parent().parent().parent().find('#thermal input[type=text]').attr('disabled', false);
                $(val).parent().parent().parent().find('#thermal input[type=checkbox]').prop('disabled', false);
                $(val).parent().parent().parent().find('#thermal input[type=radio]').prop('disabled', false);
                $(val).parent().parent().parent().find('#thermal').show();
            } else if ($(val).val() == 'ip') {
                $(val).parent().parent().parent().find('#thermal input[type=text]').attr('disabled', true);
                $(val).parent().parent().parent().find('#thermal input[type=checkbox]').prop('disabled', true);
                $(val).parent().parent().parent().find('#thermal').hide();
                $(val).parent().parent().parent().find('#ip').find('input[type=text]').attr('disabled', false);
                $(val).parent().parent().parent().find('#ip').find('input[type=checkbox]').prop('disabled', false);
                $(val).parent().parent().parent().find('#ip').find('input[type=radio]').prop('disabled', false);
                $(val).parent().parent().parent().find('#ip').show();
            }
        }

        // $(document).on('click', '.remove-button', function () {
        //     $(this).parent().parent().remove();
        // })
        $(document).on('change', '.building_id', function () {
            var building=$(this).val();
            locations(building);
        });

        function locations(building) {


            $.ajax({
                url: '/timeslot-access/location-ajax/' + building,
                dataType: 'json',
                type: 'get',
                success: function (data) {
                    var location = data.data;
                    // console.log(location[0].id);
                    $('#locations').find('option').not(':first').remove();
                    for (var i = 0; i < location.length; i++) {
                        var dummy = $('#options').clone();
                        if (i == 0) {
                            $(dummy).prop('selected', true);
                        }
                        if (slotLocation != null) {
                            if (slotLocation == location[i].id) {
                                console.log('oo');
                                $(dummy).prop('selected', true);
                            }
                        }
                        if(old){
                            console.log('old');
                            $(dummy).prop('selected', true);
                        }
                        $(dummy).attr('id', '');
                        $(dummy).val(location[i].id);
                        $(dummy).text(location[i].location_name);
                        $(dummy).show();
                        $('#locations').append(dummy);

                    }
                    $('#locations').select2();
                    $('.devices-cards').show();
                    // $('#location-title').text()
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('#locations').find('option').not(':first').remove();
                    var dummy = $('#options').clone();
                    $(dummy).attr('id', '');
                    $(dummy).val('');
                    $(dummy).text('No locations available');
                    $(dummy).prop('selected', true);
                    $(dummy).show();
                    $('#locations').append(dummy);
                    $('.devices-cards').hide();
                }
            });
        }



    </script>
@endsection



