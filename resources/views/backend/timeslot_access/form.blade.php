@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($timeslot) ? 'Update time-slot' : 'Add time-slot' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('timeslot-access.index') }}" class="text-muted">Time-slots</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($timeslot) ? route('timeslot-access.index').'/'.$timeslot->id.'/edit' : route('timeslot-access.create') }} " class="text-muted">{!! isset($timeslot) ? 'Update time-slot' : 'Add time-slot' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($timeslot) ? 'Update Time-slot' : 'Add Time-slot' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($timeslot) ? route('timeslot-access.update', $timeslot->id) : route('timeslot-access.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Building</label>
                                        <select name="building_access_id" class="form-control" id="buildingId">
                                            @foreach($buildings as $build)
                                                <option value= "{{$build->id}}"  {{  old('building_access_id') == $build->id ? 'selected' : '' }}   {!!  isset($timeslot) && $build->id==$timeslot->building_access_id ? 'selected="selected"' : '' !!}> {{$build->building_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Location</label>
                                        <select name="location_access_id" class="form-control" id="locations">
                                            <option value="" style="display:none;" id="options"></option>
                                        </select>
                                    </div>
                                </div>
                                {{--<div class="row">--}}
                                    {{--<div class="form-group col-lg-6">--}}
                                        {{--<label class="form-control-label">Date</label>--}}
                                        {{--<input type="date" name="date" class="form-control" value="{!! isset($timeslot) ? $timeslot->date: old('date') !!}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Start time </label>
                                        <input class="form-control" id="kt_timepicker_4" readonly="readonly" value= "{!! isset($timeslot) ? $timeslot->start_time: old('start_time') !!}" type="text" name="start_time">
{{--                                        <input type="text" name="start_time" class="form-control datetimepicker"  value="{!! isset($timeslot) ? $timeslot->start_time: old('start_time') !!}">--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">End time </label>
                                        <input class="form-control" id="kt_timepicker_4" readonly="readonly" value="{!! isset($timeslot) ? $timeslot->end_time: old('end_time') !!}" type="text" name="end_time">
                                        {{--<input type="text" name="end_time" class="form-control datetimepicker" value="{!! isset($timeslot) ? $timeslot->end_time: old('end_time') !!}">--}}
                                    </div>
                                </div>
                                {{--<div class="row">--}}
                                    {{--<div class="form-group col-lg-6">--}}
                                        {{--<label class="form-control-label">Limit</label>--}}
                                        {{--<input type="number" name="limit" class="form-control" value="{!! isset($timeslot) ? $timeslot->limit: old('limit') !!}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Status </label>
                                        <select name="status" class="form-control">
                                            <option value="1" @if(isset($timeslot) && $timeslot->status==1) selected @endif selected >Active</option>
                                            <option value="0" @if(isset($timeslot) && $timeslot->status==0) selected @endif>Inactive</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="company_id" value="{{auth()->user()->company->id}}">
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('timeslot-access.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.3') }}"></script>
    <script>

        var slotLocation=`@php echo isset($timeslot) ?$timeslot->location_access_id : null @endphp`;
        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            location_name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Name is required'
                                    },
                                }
                            }
                            , location_limit: {
                                validators: {
                                    notEmpty: {
                                        message: 'Building Limit is required'
                                    },
                                }
                            },
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
            locations();
            $('#buildingId').change(function () {
                locations();
            })
        });
        function locations(){
            var building= $('#buildingId').val();
            $.ajax({
                url:'/timeslot-access/location-ajax/'+building,
                dataType:'json',
                type:'get',
                success:function (data) {
                   var location=data.data;
                    $('#locations').find('option').not(':first').remove();
                   for(var i=0;i<location.length;i++){
                      var dummy=  $('#options').clone();
                       if(i==0){
                           $(dummy).prop('selected',true);
                       }
                      if(slotLocation!=null){
                          if( slotLocation==location[i].id)
                          {
                              $(dummy).prop('selected',true);
                          }
                      }

                   $(dummy).attr('id','');
                   $(dummy).val(location[i].id);
                   $(dummy).text(location[i].location_name);
                   $(dummy).show();
                   $('#locations').append(dummy);
                   }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('#locations').find('option').not(':first').remove();
                    var dummy=  $('#options').clone();
                    $(dummy).attr('id','');
                    $(dummy).val('');
                    $(dummy).text('No locations available');
                    $(dummy).prop('selected',true);
                    $(dummy).show();
                    $('#locations').append(dummy);
                }
            });
        }

    </script>
@endsection
