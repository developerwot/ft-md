@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($people_registry) ? 'Update People Registry' : 'Add People Registry' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('people_registry.index') }}" class="text-muted">People Registry</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($people_registry) ? route('people_registry.index').'/'.$people_registry->id.'/edit' : route('people_registry.create') }} " class="text-muted">{!! isset($people_registry) ? 'Update People Registry' : 'Add People Registry' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($people_registry) ? 'Update People Registry' : 'Add People Registry' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($people_registry) ? route('people_registry.update', $people_registry->id) : route('people_registry.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            @if(isset($people_registry))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select People</label>
                                        <select class="form-control select2" id="kt_select2_3" name="employee_id" >
                                            <option value=""> Select People</option>

                                            @if($employees)
                                                @foreach($employees as $emp)
                                                    <option value="{{ $emp->id }}" @if( (isset($people_registry) && $emp->id == $people_registry->employee_id) || $emp->id == old('employee_id') )  selected @endif>{{ $emp->first_name.' '.$emp->last_name.' ('.$emp->email.')' }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if(isset($people_registry) && isset($file_name))
                                    <div class="row">

                                        <div class="form-group col-lg-6">
                                            <label class="form-control-label">Your Video  </label>
                                            <div>
                                                <a href="javascript;;" class="btn btn-text-success btn-hover-light-success font-weight-bold mr-2"  data-toggle="modal" data-target="#exampleModal">View File</a>

                                            </div>
                                             <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">People Registry Video</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <i aria-hidden="true" class="ki ki-close"></i>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                        @if(isset($file_name) && !empty($file_name))
                                                            <video width="300" height="340" controls>
                                                                @php $extention = pathinfo($file_name,PATHINFO_EXTENSION);@endphp
                                                                @if($extention == '.mkv')
                                                                    <source src="{{ $file_name }}" type="video/mkv">
                                                                @else
                                                                    <source src="{{ $file_name }}"  type="video/mp4">
                                                                @endif
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        @else
                                                            <p>No Video</p>
                                                        @endif
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    {{--<div class="row">--}}
                                        {{--<div class="form-group col-lg-6">--}}
                                            {{--<label class="form-control-label">Status</label>--}}
                                            {{--<br><p class="text-success pl-4">{{ucfirst($training_status[$people_registry->training_status])}}</p>--}}
                                            {{--<select class="form-control select2" id="training_status" name="training_status" >--}}
                                                {{--<option value="">Select Status</option>--}}
                                                {{--@if($training_status)--}}
                                                    {{--@foreach($training_status as $key=>$status)--}}
                                                        {{--<option value="{{ $key }}" @if( (isset($people_registry) && $key == $people_registry->training_status) || $key == old('training_status') )  selected @endif>{{ $status }}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--@endif--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                @endif
                                {{--<div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Add Image / Video</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input " name="people_file" id="customFile" value="">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>

                                </div>--}}

                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('people_registry.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>
        var KTFormControls = function () {
            // Private functions
            $('#training_status').select2({
                placeholder: "Select a Status"
            });

            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            employee_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'People is required'
                                    },
                                }
                            },
                          /*  file_type: {
                                validators: {
                                    notEmpty: {
                                        message: 'Employee is required'
                                    },
                                }
                            },*/
                           /* people_file: {
                                notEmpty: {
                                    message: 'File is required'
                                },
                                file: {
                                    extension: 'jpeg,png,jpg,gif,svg,mp4,mkv',
                                    /!* type: 'application/vnd.ms-excel,text/comma-separated-values',*!/
                                    maxSize: 2097152,   // 2048 * 1024
                                    message: 'The selected file is not valid'
                                },
                            },*/

                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });


    </script>
@endsection
