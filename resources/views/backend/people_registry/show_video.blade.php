@if(isset($file_name) && !empty($file_name))
    <video width="300" height="340" controls>
        @php $extention = pathinfo($file_name,PATHINFO_EXTENSION);@endphp
        @if($extention == '.mkv')
            <source src="{{ $file_name }}" type="video/mkv">
        @else
            <source src="{{ $file_name }}"  type="video/mp4">
        @endif
        Your browser does not support the video tag.
    </video>
@else
    <p>No Video</p>
@endif