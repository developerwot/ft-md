@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">People Temperatures</h5>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Search Form-->
                    <div class="d-flex align-items-center" id="kt_subheader_search">
                        <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{--{{ $emp_temp->count() }}--}} Total</span>
                        <form class="ml-5" action="javascript:void(0)">
                            <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                                <input type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search..." />
                                <div class="input-group-append">
														<span class="input-group-text">
															<span class="svg-icon">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																		<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                                            <!--<i class="flaticon2-search-1 icon-sm"></i>-->
														</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end::Search Form-->
                    <!--begin::Group Actions-->

                    <!--end::Group Actions-->
                </div>
                <!--end::Details-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Button-->
                    <a href="#" class=""></a>
                    <!--end::Button-->
                    <!--begin::Button-->
                   {{-- @if(Auth::user()->hasRole('company'))
                        <a href="{{ route('emp_temp.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2">Add People Temperature</a>
                    @endif--}}
                    <!--end::Button-->
                    <!--begin::Dropdown-->
                    <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">

                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">People Temperatures Management
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2" data-toggle="modal" data-target="#myModal">Add Email</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    <div class="mb-12">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-xl-12">
                                <div class="row align-items-center">
                                    <div class="col-md-6 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">Date:</label>
                                            <div class="col-md-6 my-2 my-md-0">
                                                <input class="form-control" id="kt_datepicker" readonly="readonly" placeholder="Select date" type="text" />
                                            </div>
                                            <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                                <a href="javascript:void(0);" class="btn btn-light-primary px-6 font-weight-bold SearchFilter">Search</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin: Datatable-->
                    <div class="datatable datatable-bordered datatable-head-custom" id="user_datatable"></div>
                    <!--end: Datatable-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <form action="{{route('temp_report.email.update')}}" method="post">
                @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{isset($email) ? 'Update Email' : 'Add Email'}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="main d-flex flex-column flex-row-fluid">
                        <div class="content flex-column-fluid" id="kt_content">
                            <!--begin::Card-->
                            <div class="card card-custom">
                                <!--begin::Header-->
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title">
                                        <h3 class="card-label">Report will sent to this email</h3>
                                    </div>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body row">
                                    <div class="form-group col-sm-1 mt-4">
                                        <label for="">Email:</label>
                                    </div>
                                        <div class="form-group col-sm">
                                            <input type="email" name="email" class="form-control" value="{{isset($email) ? $email : ''}}">
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
            </form>

        </div>
    </div>


    @include('backend.snippets.delete')
@endsection
    @section('scripts')
    {{--<script src="{{ asset('theme/js/pages/custom/user/list-datatable.js?v=7.0.3') }}"></script>--}}
    <script>
        var  e, atable, i;
        $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('emp_temp.index')}}/delete/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('emp_temp.index')}}/' + $(this).data('id') + '/edit';
            location.href = editUrl;
        });

     /*   var emp_temp = {{--{!! $emp_temp !!}--}};
        console.log('emp_temp', emp_temp);*/


        var DatatableDataLocalDemo = {
            init: function () {
                $('#kt_datepicker').datepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary'
                }).datepicker('setDate', new Date());
                 atable = $("#user_datatable").KTDatatable({
                     // 'processing': true,
                     // 'language': {
                     //     'loadingRecords': '&nbsp;',
                     //     'processing': '<div class="spinner"></div>'
                     // },
                    data: {
                        saveState :false,
                        type: 'remote',
                        source: {
                            read: {
                                method:'POST',
                                url: AjaxUrl + '/getEmpTemp',
                                // sample custom headers
                                // headers: {'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    console.log(raw.data.length);
                                    $("#kt_subheader_total").html(raw.data.length + ' Total ');
                                    return dataSet;
                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: false,
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#kt_subheader_search_form"),key: 'generalSearch'},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 20,
                        sortable: false,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },  {
                        field: "name",
                        title: "Name",
                        width: 80,
                        sortable: 1,
                        // textAlign: "center",
                    },
                        // {
                    //     field: "email",
                    //     title: "Email",
                    //     width: 150,
                    //     sortable: 1,
                    //     overflow: "visible",
                    //     // textAlign: "center",
                    // },  {
                    //     field: "company",
                    //     title: "Company",
                    //     width: 100,
                    //     sortable: 1,
                    //     overflow: "visible",
                    //
                    // },  {
                    //     field: "temperature",
                    //     title: "Temp",
                    //     width: 80,
                    //     sortable: 1,
                    //     overflow: "visible",
                    //
                    // },
                        {
                        field: "status",
                        title: "Status",
                        width: 80,
                        sortable: true,
                        overflow: "visible",
                        template: function (e, a, i) {
                            if(parseFloat(e.temperature) < 37.5){
                               data = '<span class="label label-lg font-weight-bold label-light-success label-inline">Green</span>'

                            } else {
                                data = '<span class="label label-lg font-weight-bold label-light-danger label-inline">Red</span>'
                            }
                            return data;
                        }
                    }, {
                        field: "time",
                        title: "Last Updated Time",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",

                        // textAlign: "center",
                    }, {
                        field: "date",
                        title: "Date",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",

                        // textAlign: "center",
                    }/*, {
                        field: "Actions",
                        width: 80,
                        title: "Action",
                        sortable: !1,
                        overflow: "visible",
                        template: function (e, a, i) {
                            var edit = ' <a href="javascript:;" data-id="' + e.id + '"'+ 'class=" edit-button btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">'+
                                        '<span class="svg-icon svg-icon-md">'+
                                        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                        '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                        '<rect x="0" y="0" width="24" height="24"/>'+
                                        '<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>'+
                                        '<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>'+
                                        '</g>'+
                                        '</svg>'+
                                        '</span>'+
                                        '</a>';
                            var deleteB = '<a href="javascript:;" data-id="' + e.id + '" class="delete-button-action btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon" title="Delete">'+
                                        '<span class="svg-icon svg-icon-md">'+
                                        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                        '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                        '<rect x="0" y="0" width="24" height="24"/>'+
                                        '<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>'+
                                        '<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>'+
                                        '</g>'+
                                        '</svg>'+
                                        '</span>'+
                                        '</a>';


                            return '';


                        }
                    }*/
                    ]
                })
                 $('.SearchFilter').on('click', function() {

                     atable.setDataSourceParam('temp_date',$('#kt_datepicker').val().toLowerCase());
                     atable.setDataSourceParam('search','1');
                     atable.search($('#kt_datepicker').val().toLowerCase(), 'temp_date');
                 });

            }
            ,
            reload : function () {
                console.log('reload');
                    $('#user_datatable').KTDatatable('reload');
            }
        };
        jQuery(document).ready(function () {
            DatatableDataLocalDemo.init();

            setInterval( DatatableDataLocalDemo.reload, 60000);
        });

    </script>
@endsection
