@extends('backend.layouts.master')
@section('content')
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($location) ? 'Update Floor' : 'Add Floor' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('floor-access.index') }}" class="text-muted">Floors</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($location) ? route('floor-access.index').'/'.$location->id.'/edit' : route('floor-access.create') }} " class="text-muted">{!! isset($location) ? 'Update Floor' : 'Add Floor' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="" data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($location) ? 'Update Floor' : 'Add Floor' !!} </h3>

                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($location) ? route('floor-access.update', $location->id) : route('floor-access.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Select Building</label>
                                        <select name="building_access_id" class="form-control" id="">
                                            @foreach($buildings as $build)
                                            <option value= "{{$build->id}}"  {{  old('building_access_id') == $build->id ? 'selected' : '' }}   {!!  isset($location) && $build->id==$location->buildings['id'] ? 'selected="selected"' : '' !!}> {{$build->building_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Floor name </label>
                                        <input type="text" name="floor_name" class="form-control" value="{!! isset($location) ? $location->floor_name: old('floor_name') !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Floor limit </label>
                                        <input type="number" name="floor_limit" class="form-control" value="{!! isset($location) ? $location->floor_limit: old('floor_limit') !!}" min="1">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-control-label">Status </label>
                                        <select name="floor_status" class="form-control">
                                            <option value="1" @if(isset($location) && $location->floor_status==1) selected @endif selected >Active</option>
                                            <option value="0" @if(isset($location) && $location->floor_status==0) selected @endif>Inactive</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="company_id" value="{{auth()->user()->company->id}}">
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold" onclick="return window.location.href='{{ route('floor-access.index') }}'">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>

        </div>
      @if(isset($location))
            <div class="content flex-column-fluid" id="kt_content">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Desks Management</h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{ route('timeslot-access.create') }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2">Add Desk</a>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="user_datatable"></div>
                        <!--end: Datatable-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
          @endif
        <!--end::Content-->
    </div>
    @include('backend.snippets.delete')
@endsection

@section('scripts')
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script>
        var floorId=`@php echo isset($location) ?$location->id : null @endphp`;
        $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('timeslot-access.index')}}/delete-timeslot/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('timeslot-access.index')}}/' + $(this).data('id') + '/edit-timeslot';
            location.href = editUrl;
        });
        var DatatableDataLocalDemo = {
            init: function () {
                var e, a, i;
                atable = $("#user_datatable").KTDatatable({
                    data: {
                        saveState :false,
                        type: 'remote',
                        source: {
                            read: {

                                url:  '{{route('desk-access.table2')}}',
                                method: 'POST',
                                data:{floor_id:floorId},
                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    console.log(raw.data.length);
                                    // $("#kt_subheader_total").html(raw.data.length + ' Total ');
                                    return dataSet;

                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: false,
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#kt_subheader_search_form"), key: 'generalSearch'},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 15,
                        sortable: false,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },{
                        field: "building_name",
                        title: "Building Name",
                        width: 150,
                        sortable: 1,
                        overflow: "visible",
                        template: function (e, a, i) {
                            return e.building.building_name;
                        }
                    },
                        {
                            field: "location_name",
                            title: "Floor Name",
                            width: 150,
                            sortable: 1,
                            overflow: "visible",
                            template: function (e, a, i) {
                                return e.floor.floor_name;
                            }
                        },
                        {
                            field: "desk_name",
                            title: "Desk name",
                            width: 100,
                            sortable: 1,
                            overflow: "visible",

                        },
                        {
                            field: "status",
                            title: "Status",
                            width: 100,
                            sortable:true,
                            overflow: "visible",
                            template: function (e, a, i) {
                                if(e.desk_status == 1){
                                    data = '<span class="label label-lg font-weight-bold label-light-success label-inline">Active</span>'

                                } else {
                                    data = '<span class="label label-lg font-weight-bold label-light-danger label-inline">Inactive</span>'
                                }
                                return data;
                            }
                        },
                        {
                            field: "Actions",
                            width: 120,
                            title: "Action",
                            sortable: !1,
                            overflow: "visible",
                            template: function (e, a, i) {

                                // return    '<a href="javascript:;" data-id="'+e.id+'" class="show-booking btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2">'+
                                //     '<span class="svg-icon svg-icon-md">'+
                                //     '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="svg-icon">'+
                                //     '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                //     '<rect x="0" y="0" width="24" height="24"/>'+
                                //     '<path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"/>'+
                                //     '<path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"/>'+
                                //     '</g>'+
                                //     '</svg>'+
                                //     '</span>'+
                                //     '</a>'+
                                return ' <a href="javascript:;" data-id="' + e.id + '"'+ 'class=" edit-button btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>'+
                                    '<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>'+
                                    ' <a href="javascript:;" data-id="' + e.id + '"'+ 'class=" print-button btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Print QR code">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<i class="la la-print"></i>'+
                                    '</span>'+
                                    '</a>'+
                                    '<a href="javascript:;" data-id="' + e.id + '" class="delete-button-action btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon" title="Delete">'+
                                    '<span class="svg-icon svg-icon-md">'+
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">'+
                                    '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                    '<rect x="0" y="0" width="24" height="24"/>'+
                                    '<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>'+
                                    '<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>'+
                                    '</g>'+
                                    '</svg>'+
                                    '</span>'+
                                    '</a>';
                            }
                        }]
                });

            }
        };

        var KTFormControls = function () {
            // Private functions
            var _initDemo1 = function () {
                FormValidation.formValidation(
                    document.getElementById('companyForm'),
                    {
                        fields: {
                            location_name: {
                                validators: {
                                    notEmpty: {
                                        message: 'Name is required'
                                    },
                                }
                            }
                            , location_limit: {
                                validators: {
                                    notEmpty: {
                                        message: 'Building Limit is required'
                                    },
                                }
                            },
                        },

                        plugins: { //Learn more: https://formvalidation.io/guide/plugins
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            // Submit the form when all fields are valid
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    }
                );
            }
            return {
                // public functions
                init: function() {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
            DatatableDataLocalDemo.init();
        });


    </script>
@endsection
