@extends('backend.layouts.master')
@section('content')
    <style>
        #timerange_picker_div .table-condensed {
            display: none !important;
        }
    </style>
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4" id="kt_subheader">
            <div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5"> {!! isset($booking_policy) ? 'Update Booking Policy' : 'Add Booking Policy' !!}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="{{ route('booking-policy.index') }}" class="text-muted">Booking Policies</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ isset($booking_policy) ? route('building_policy.index').'/'.$booking_policy->id.'/edit' : route('building_policy.create') }} "
                                   class="text-muted">{!! isset($booking_policy) ? 'Update Booking Policy' : 'Add Booking Policy' !!}</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Daterange-->
                    <a href="javascript:void(0)" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id=""
                       data-toggle="tooltip" title="" data-placement="left">
                        <span class="opacity-60 font-weight-bold mr-2" id="">Today</span>
                        <span class="font-weight-bold" id="">{{ date('M d', strtotime(now())) }}</span>
                    </a>
                    <!--end::Daterange-->
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid" id="kt_content">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{!! isset($booking_policy) ? 'Update Booking Policy' : 'Add Booking Policy' !!} </h3>
                        </div>
                        <!--begin::Form-->
                        <form id="companyForm" class="form m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($booking_policy) ? route('booking-policy.update', $booking_policy->id) : route('booking-policy.store')}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body" >
                                <div class="row">
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">Building</label>
                                        <select class="form-control  buildingId" id="kt_select2_4" name="building_id">
                                            <option value=""> Select Building</option>
                                            @if($buildings)
                                                @foreach($buildings as $building)
                                                    <option value="{{ $building->id }}"
                                                            @if( (isset($booking_policy) && $building->id == $booking_policy->building_id) || $building->id == old('building_id') )  selected @endif>{{ $building->building_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6 ">
                                        <label class="form-control-label">Locations</label>
                                        <select class="form-control select2 locations" id="kt_select2_3"
                                                name="location_id[]" multiple>
                                        </select>
                                    </div>
                                </div>
                                <option value="" id="options" style="display: none">Select Locations</option>
                                <div class="row range_date_div">
                                    <div class="form-group col-lg-6 ">
                                        <label>Date Range</label>
                                        <input class="form-control" name="date_range" id="kt_daterangepicker_1"
                                               readonly="readonly" placeholder="Select Date" type="text"/>
                                    </div>
                                </div>
                                {{--<div class="row  range_date_div">--}}
                                {{--<div class="form-group col-lg-6 " id="timerange_picker_div">--}}
                                {{--<label>Time Range</label>--}}
                                {{--<input class="form-control" name="time_range" id="timerange_picker" readonly="readonly" placeholder="Select time" type="text" value="{{isset($booking_policy) && $booking_policy->from_hour!='' ? $booking_policy->from_hour.' - '.$booking_policy->to_hour : old('time_range')}}" />--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="row">
                                    <div class="form-group col-lg-3 ">
                                        <label>Permit / No Permit</label>
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input checked type="radio" name="is_permitted" value="1">Permit
                                                <span></span></label>
                                            <label class="radio">
                                                <input type="radio"
                                                       @if( (isset($booking_policy) && $booking_policy->is_permitted=='0') || old('is_permitted')=='0' )  checked
                                                       @endif  name="is_permitted" value="0">No Permit
                                                <span></span></label>
                                        </div>
                                    </div>

                                </div>
                                <div class="row" style="display: none" id="timeAdder">
                                    <div class="form-group col-lg-3">
                                        <label>start time</label>
                                        <input type="text" id="start" value="09:00:00"
                                               class="form-control mr-2 kt_timepicker_4">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label>End time</label>
                                        <div style="display: flex">
                                            <input type="text" id="end" value="16:00:00"
                                                   class="form-control mr-2 kt_timepicker_4">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 form-group ">
                                        <label>Time difference</label>
                                        <input type="number" class="form-control" placeholder="Enter time difference"
                                               id="minutes">
                                    </div>
                                    <div class="col-lg-2 mt-7 form-group ">
                                        <button type="button" class="btn btn-success font-weight-bold add_slots">Add
                                            Time-slots
                                        </button>
                                    </div>
                                </div>
                                <div class="row" id="slots-box" style="display: none">
                                    <div class="form-group col-lg-5">
                                        <label>start time</label>
                                        <input type="text" readonly="readonly"
                                               class="form-control mr-2  start">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>End time</label>
                                        <div style="display: flex">
                                            <input type="text" readonly="readonly"
                                                   class="form-control mr-2  end"><a
                                                href="javascript:void(0)" class="trash-button"><i class="fa fa-trash"
                                                                                                  style="font-size: 30px;color: red"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="slots-container">
                                @if(old('slots'))
                                    @foreach(old('slots') as $slot)
                                        <div class="row slots">
                                            <div class="form-group col-lg-5">
                                                <label>start time</label>
                                                <input type="text" name="slots[{{$loop->iteration-1}}][start]" readonly="readonly" class="form-control mr-2" value="{{$slot['start']}}">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label>End time</label>
                                                <div style="display: flex">
                                                    <input type="text" readonly="readonly" class="form-control mr-2 " name="slots[{{$loop->iteration-1}}][end]" value="{{$slot['end']}}" >
                                                    <a href="javascript:void(0)" class="trash-button"><i class="fa fa-trash" style="font-size: 30px;color: red"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @if(isset($booking_policy) && empty(old('slots')) )
                                    @if($booking_policy->is_permitted==1)
                                        @foreach($time_slots as $slot)
                                            <div class="row slots">
                                                <div class="form-group col-lg-5">
                                                    <label>start time</label>
                                                    <input type="text" name="slots[{{$loop->iteration-1}}][start]" class="form-control mr-2" readonly="readonly" value="{{$slot->start_time}}">
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>End time</label>
                                                    <div style="display: flex">
                                                        <input type="text"
                                                               class="form-control mr-2  {{($loop->last)? 'endTime' : ''}}"
                                                               name="slots[{{$loop->iteration-1}}][end]" value="{{$slot->end_time}}" readonly="readonly">
                                                        <a href="javascript:void(0)" class="trash-button"><i class="fa fa-trash" style="font-size: 30px;color: red"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                                </div>
                            </div>
                            <!--end::Code example-->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success font-weight-bold mr-2">Submit</button>
                                <button type="button" class="btn btn-light-success font-weight-bold"
                                        onclick="return window.location.href ='{{ route('booking-policy.index') }}'">
                                    Cancel
                                </button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>

            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')

    <!--<script src="{{ asset('theme/plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.3') }}"></script>-->
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/select2.js?v=7.0.3') }}"></script>
    <script src="{{ asset('theme/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.3') }}"></script>
    <script>
        var slotLocation = `@php echo isset($booking_policy) ?$booking_policy->location_id : null @endphp`;
        var permit = `@php echo isset($booking_policy) ?$booking_policy->is_permitted : 1 @endphp`;
        {{--var slott = `@php echo old('slots') !=null? old('slots') : null @endphp`;--}}
        // console.log(slott);
        var KTFormControls = function () {
//             // Private functions
            var _initDemo1 = function () {
//                 $('#group_id').select2({placeholder: "Select a Group"});
                $('#kt_daterangepicker_1').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    format: 'd-m-Y',
                    cancelClass: 'btn-secondary',

                });
                $('.kt_timepicker_4').timepicker();

            }
            return {
                // public functions
                init: function () {
                    _initDemo1();

                }
            };
        }();

        jQuery(document).ready(function () {
            locations();
            KTFormControls.init();

            if (permit == 1) {
                $('#timeAdder').show();
            } else {
                $('#timeAdder').hide();
            }
            $(".locations").select2({
                placeholder: "No locations available",
            });
            $('.buildingId').change(function () {
                locations();
            });
            $(document).on('click', 'input[name=is_permitted]', function () {
                if ($(this).val() === '1') {
                    $('#timeAdder').show();
                } else {
                    $('#timeAdder').hide();
                }
            });
            $(document).on('click', '.add_slots', function () {
                $('#slots-container').empty();
                var difference = $('#minutes').val();
                var start_time = getEndTime($('#start').val());
                var end_time = getEndTime($('#end').val());
                var interval = difference ? parseInt(difference) : 60;
                // calculate_time_slot( parseTime(start_time), parseTime(end_time), interval);
                var i;
                let m = 0;
                for (i = parseTime(start_time); i <= parseTime(end_time); i = i + interval) {
                    var formatted_start = convertHours(i);
                    var formatted_end = convertHours(i + interval);
                    if (i + interval <= parseTime(end_time)) {
                        var dummy = $('#slots-box').clone();
                        $(dummy).attr('id', '');
                        $(dummy).addClass('slots');
                        $(dummy).find('.start').attr('name', 'slots[' + m + '][start]').val(formatted_start);
                        $(dummy).find('.end').attr('name', 'slots[' + m + '][end]').val(formatted_end);
                        $(dummy).show();
                        $('#slots-container').append(dummy);
                        m++;
                    }
                    // $('.kt_timepicker_4').timepicker();
                }

                // $('.kt_timepicker_4').timepicker();


            });
            $(document).on('click', '.trash-button', function () {
                // var check = $(this).siblings().hasClass('endTime');
                // if (check) {
                //     var texts = $('.trash-button').length - 2;
                //     var previous = $('.trash-button').last().prevObject[texts].previousSibling;
                //     $(document).find(previous).addClass('endTime')
                //     // $(previous).addClass('endTime');
                // }
                $(this).parent().parent().parent().remove();
            })
        });

        function locations() {
            var building = $('.buildingId').val();
            if (building === "") {
                return false;
            }
            $.ajax({
                url: '/timeslot-access/location-ajax/' + building,
                dataType: 'json',
                type: 'get',
                success: function (data) {
                    var location = data.data;
                    $('.locations').find('option').remove();
                    for (var i = 0; i < location.length; i++) {
                        var dummy = $('#options').clone();

                        if (slotLocation != null) {
                            if (slotLocation == location[i].id) {
                                $(dummy).prop('selected', true);
                            }
                        } else if (i == 0) {
                            $(dummy).prop('selected', true);
                        }

                        $(dummy).attr('id', '');
                        $(dummy).val(location[i].id);
                        $(dummy).text(location[i].location_name);
                        $(dummy).show();
                        $('.locations').append(dummy);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('.locations').find('option').remove();
                }
            });
        }

        function getEndTime(time) {
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if (AMPM == "PM" && hours < 12) hours = hours + 12;
            if (AMPM == "AM" && hours == 12) hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) sHours = "0" + sHours;
            if (minutes < 10) sMinutes = "0" + sMinutes;
            return sHours + ":" + sMinutes + ":00"
        }

        function parseTime(s) {
            var c = s.split(':');
            return parseInt(c[0]) * 60 + parseInt(c[1]) + parseInt(c[2]);
        }

        function convertHours(mins) {
            var hour = Math.floor(mins / 60);
            var minutes = mins % 60;
            var sec = 00;
            var converted = pad(hour, 2) + ':' + pad(minutes, 2) + ':' + pad(sec, 2);
            return converted;
        }

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        // function calculate_time_slot(start_time, end_time, interval=60){
        //
        //     return time_slots;
        // }

        // function addMinutes(time, minsToAdd) {
        //     function D(J){ return (J<10? '0':'') + J;};
        //     var piece = time.split(':');
        //     var mins = piece[0]*60 + +piece[1] + +minsToAdd;
        //     return D(mins%(24*60)/60 | 0) + ':' + D(mins%60);
        // }


        // if(length===0){
        //
        //     var addedTime=addMinutes('9:00:00', time);
        //    var start='09:00:00';
        //    var end=addedTime;
        // }else{
        //     var Intime=$(document).find('.endTime').val();
        //     console.log(Intime);
        //     start=getEndTime(Intime);
        //     end=addMinutes(start, time);
        //     console.log(start,time);
        // }
        //  var dummy= $('#slots-box').clone();
        //  $(dummy).attr('id','');
        //  $(dummy).addClass('slots');
        //  $(dummy).find('#start').attr('name','slots[start]['+length+']').val(start);
        //  if(length===0){
        //      $(dummy).find('#end').addClass('endTime');
        //  }else{
        //      $(document).find('.endTime').removeClass('endTime');
        //      $(dummy).find('#end').addClass('endTime');
        //  }
        //   $(dummy).find('#end').attr('name','slots[end]['+length+']').val(end);
        //  $(dummy).show();
        //  $('#slots-container').append(dummy);
        //   $('.kt_timepicker_4').timepicker();

    </script>
    @if(isset($booking_policy) && $booking_policy->from_date!='')


        <script>
            $(document).ready(function () {
                $('#kt_daterangepicker_1').data('daterangepicker').setStartDate("{{date('m/d/Y',strtotime($booking_policy->from_date))}}");
                $('#kt_daterangepicker_1').data('daterangepicker').setEndDate("{{date('m/d/Y',strtotime($booking_policy->to_date))}}");
            });
        </script>
    @endif


@endsection
