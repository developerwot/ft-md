<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'forgot_password' => 'Forgotten Password ?',
    'reset_password' => 'Enter your email to reset your password:',
    'request_btn' => 'Request',
    'cancle_btn' => 'Cancel',
    'sign_in_admin' => 'Sign In To Admin',
    'remember_me' => ' Remember me',
    'sign_in' => 'Sign In',
    'profile' => 'My Profile',
    'your_profile' => 'Your Profile',
    'save_btn' => 'Save',
    'update_profile' => 'Update Profile',
    'change_password' => 'Change Password',
    'example_content' => 'The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.',
    'personal_detail' => 'Personal Details',
    'name' => 'Name:',
    'email' => 'Email:',
    'save_changes_btn' => 'Save changes',
    'old_password' => 'Old Password:',
    'new_password' => 'New Password:',
    'confirm_password' => 'Confirm Password:',
    'reset_password_blade' => 'Reset Password',
    'placeholder_email' => 'Email',
    'placeholder_password' => 'Password',
    'placeholder_old_password' => 'Enter old password',
    'placeholder_new_password' => 'Enter new password',
    'placeholder_confirm_password' => 'Confirm password here',

];
