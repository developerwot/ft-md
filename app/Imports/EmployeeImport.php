<?php

namespace App\Imports;

use App\Models\Employee;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Hash;


class EmployeeImport implements ToCollection,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $rows = 0;
    public function __construct(int $company_id)
    {
        $this->company_id = $company_id;
    }

    public function collection(Collection $rows)
    {

        if(!empty($rows)) {
            foreach ($rows as $row)
            {
                if(isset($row['email']) && !empty($row['email'])) {
                    $checkExisting = Employee::where('email',$row['email'])->first();
                    if(empty($checkExisting)) {

                        $newPassword = 'weboccult_123';
                        $user_input['password'] = Hash::make($newPassword);
                        $user_input['first_name'] = (isset($row['first_name'])) ? $row['first_name']: '';
                        $user_input['last_name'] = (isset($row['last_name'])) ? $row['last_name'] :"";
                        $user_input['email'] = $row['email'];
                        $user_input['status'] = (isset($row['status'])) ? $row['status'] : 0;

                        $user = User::create($user_input);

                        if($user) {

                            $roles = ['employee'];
                            $user->syncRoles($roles);

                            ++$this->rows;

                            $employee =  Employee::create([
                                'first_name'     => (isset($row['first_name'])) ? $row['first_name']: '',
                                'last_name'      => (isset($row['last_name'])) ? $row['last_name'] :"",
                                'email'          => $row['email'],
                                'status'         => (isset($row['status'])) ? $row['status'] : 0,
                                'company_id'    =>$this->company_id,
                                'user_id'    =>$user->id,

                            ]);
                        }

                    }
                }
                else {
                    return '';
                }

            }
        }
        else {
            return '';
        }


    }
    public function getRowCount(): int
    {
        return $this->rows;
    }
    /*public function headingRow(): int
    {
        return 2;
    }*/
}
