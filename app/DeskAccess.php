<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeskAccess extends Model
{
    use SoftDeletes;
    protected  $guarded=[];

    public function floor(){
        return  $this->belongsTo(FloorAccess::class,'floor_id');
    }
    public function building(){
        return $this->belongsTo(BuildingAccess::class,'building_id');
    }

}
