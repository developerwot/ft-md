<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationAccess extends Model
{
    protected  $guarded=[];
    use SoftDeletes;

public function slots(){
    return $this->belongsTo(TimeslotAccess::class,'slot_id');
}
}
