<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingInvitation extends Model
{
    protected $guarded=[];

    public function timeSlot(){
        return $this->belongsTo(TimeslotAccess::class,'slot_id');
    }

    public function desk(){
        return $this->belongsTo(DeskAccess::class,'slot_id');
    }
}
