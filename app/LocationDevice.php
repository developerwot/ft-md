<?php

namespace App;

use App\Models\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocationDevice extends Model
{
    protected $guarded=[];

    use SoftDeletes;

    public function building(){
        return $this->belongsTo(BuildingAccess::class,'building_id');
    }
    public function location(){
        return $this->belongsTo(LocationAccess::class,'location_id');
    }
    public function company(){
        return $this->belongsTo(Company::class,'company_id');
    }

}
