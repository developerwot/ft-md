<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingPolicy extends Model
{
    protected  $guarded=[];
    use SoftDeletes;

    protected $casts = [
        'slot_ids' => 'array',
    ];

    public function building(){
        return $this->belongsTo(BuildingAccess::class,'building_id');
    }
    public function location(){
        return $this->belongsTo(LocationAccess::class,'location_id');
    }
    public function slots(){
        return $this->belongsTo(TimeslotAccess::class,'slot_ids');
    }
}
