<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BbBackup extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $duration;
    public $templocation;
    public $message;

    public function __construct($duration,$templocation,$message)
    {
        $this->duration=$duration;
        $this->templocation=$templocation;
        $this->message=$message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject($this->message)
            ->from(env('MAIL_FROM_ADDRESS'))
            ->markdown('mails.db_backup')->with([
               'duration'=>$this->duration,
               'templocation'=>$this->templocation,
               'message'=>$this->message,
            ]);
    }
}
