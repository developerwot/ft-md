<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmployeeHighTempAlert extends Mailable
{
    use Queueable, SerializesModels;

    protected $username;
    protected $temp;
    protected $admin;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username,$temp)
    {
        $this->username = $username;
        $this->temp = $temp;
        //$this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->subject('People With High Temperature')
            ->markdown('mails.people_high_temp_notification')->with([
                'username' => $this->username,
                'temp' => $this->temp ,
                'admin' => 'Admin' ,

            ]);

    }
}
