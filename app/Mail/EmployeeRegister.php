<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmployeeRegister extends Mailable
{
    use Queueable, SerializesModels;
protected $email,$code,$link,$company;

    /**
     * Create a new message instance.
     *
     * @param $email
     * @param $code
     * @param $link
     * @param $company
     */
    public function __construct($email,$code,$link,$company)
    {
        $this->email = $email;
        $this->code = $code;
        $this->link = $link;
        $this->company = $company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

//    dd($this->company->email);

//            dd($this->company->business_name,$this->email,$this->code,$this->link);
            $this->subject('Registered on App ' . $this->company->business_name)
                ->from(env('MAIL_FROM_ADDRESS'))
                ->markdown('mails.employee_passsword')->with([
                    'email' => $this->email,
                    'password' => $this->code,
                    'guest_user_link' => $this->link,
                    'company' => $this->company,
                ]);


    }
}
