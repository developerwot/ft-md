<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PeopleRegistryTraining extends Mailable
{
    use Queueable, SerializesModels;
    protected $username;
    protected $temp;
    protected $admin;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username)
    {
        $this->username = $username;

        //$this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('People Training Status')
            ->markdown('mails.people_registry_training')->with([
                'username' => $this->username,
                'admin' => 'Admin' ,

            ]);
    }
}
