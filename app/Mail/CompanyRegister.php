<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CompanyRegister extends Mailable
{
    use Queueable, SerializesModels;

    protected $username,$newPassword;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username,$newPassword)
    {
        $this->username = $username;
        $this->newPassword = $newPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Register Successfully')
            ->markdown('mails.register_notification')->with([
                'username' => $this->username,
                'newPassword' => $this->newPassword,
            ]);

    }
}
