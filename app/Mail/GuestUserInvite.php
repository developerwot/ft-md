<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GuestUserInvite extends Mailable
{
    use Queueable, SerializesModels;
    protected $email,$code,$link;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$code,$link,$company)
    {
        $this->email = $email;
        $this->code = $code;
        $this->link = $link;
        $this->company = $company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Invitation to check-in from '.$this->company->business_name)
                ->from(env('MAIL_INVITE_ADDRESS'))
            ->markdown('mails.guest_user_notification')->with([
                'email' => $this->email,
                'verify_code' => $this->code,
                'guest_user_link' => $this->link,
                'company' => $this->company,
            ]);

    }
}
