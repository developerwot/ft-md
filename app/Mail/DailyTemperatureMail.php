<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class DailyTemperatureMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $file;
    public function __construct($file)
    {
     $this->file=$file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            $path =asset('public/storage/daily-reports/' .$this->file);
//        $path=public_path().'/storage/daily-reports/' .$this->file;
            return $this->subject('Daily Temperature Report')
            ->attach($path)
            ->from(env('MAIL_FROM_ADDRESS'))
            ->markdown('mails.dailytemperature')->with([
               'message'=>'Employee Temperature report of yesterday is attached with this Email'
            ]);
    }
}
