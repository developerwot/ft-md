<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteEmployeeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $link;
    public $reset;
    public $inviterName;
    public $inviterLastName;
    public $startTime;
    public $locationName;

    public function __construct($name, $link, $inviterName, $inviterLastName, $startTime, $locationName)
    {
        $this->name = $name;
        $this->link = $link;
        $this->inviterName = $inviterName;
        $this->inviterLastName = $inviterLastName;
        $this->startTime = $startTime;
        $this->locationName = $locationName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        dd($this->name,$this->link);
        $this->subject('Intelly-scan App invitation.')
            ->from(env('MAIL_FROM_ADDRESS'))
            ->markdown('mails.employee_invitation')->with([
                'name' => $this->name,
                'link' => $this->link,
                'inviterName'=>$this->inviterName,
                'inviterLastName'=>$this->inviterLastName,
                'startTime'=>$this->startTime,
                'locationName'=>$this->locationName,
            ]);
    }
}
