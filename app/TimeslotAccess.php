<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeslotAccess extends Model
{
    use SoftDeletes;
    protected $guarded=[];


    public function location(){
        return  $this->belongsTo(LocationAccess::class,'location_access_id');
    }
    public function building(){
        return $this->belongsTo(BuildingAccess::class,'building_access_id');
    }

    public function reservation(){
        return $this->hasMany(ReservationAccess::class);
    }
}
