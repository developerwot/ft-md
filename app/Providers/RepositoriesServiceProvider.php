<?php

namespace App\Providers;


use App\Repositories\Backend\ContactTrac\ContactTracContract;
use App\Repositories\Backend\Device\DeviceContract;
use App\Repositories\Backend\PeopleAttendance\PeopleAttendanceContract;
use App\Repositories\Backend\PeopleRegistry\FacialContract;
use App\Repositories\Backend\Group\GroupContract;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->apiFrontendRepositoryRegister();
        $this->backendRepositoryRegister();
    }

    public function apiFrontendRepositoryRegister()
    {

    }

    public function backendRepositoryRegister()
    {
        $this->app->bind(
            \App\Repositories\Backend\User\UserContract::class,
            \App\Repositories\Backend\User\UserRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Dashboard\DashboardContract::class,
            \App\Repositories\Backend\Dashboard\DashboardRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Profile\ProfileContracts::class,
            \App\Repositories\Backend\Profile\ProfileRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Role\RoleContract::class,
            \App\Repositories\Backend\Role\RoleRepository::class
        );
        $this->app->bind(
            \App\Repositories\Backend\Company\CompanyContract::class,
            \App\Repositories\Backend\Company\CompanyRepository::class
        );
        $this->app->bind(
            \App\Repositories\Backend\Employee\EmployeeContract::class,
            \App\Repositories\Backend\Employee\EmployeeRepository::class
        );
        $this->app->bind(
            \App\Repositories\Backend\EmpTemp\EmpTemperatureContract::class,
            \App\Repositories\Backend\EmpTemp\EmpTemperatureRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Group\GroupContract::class,
            \App\Repositories\Backend\Group\GroupRepository::class
        );
        $this->app->bind(
            \App\Repositories\Backend\PeopleRegistry\PeopleRegistryContract::class,
            \App\Repositories\Backend\PeopleRegistry\PeopleRegistryRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\PeopleAttendance\PeopleAttendanceContract::class,
            \App\Repositories\Backend\PeopleAttendance\PeopleAttendanceRepository::class
        );

        $this->app->bind(
            \App\Repositories\Backend\Device\DeviceContract::class,
            \App\Repositories\Backend\Device\DeviceRepository::class
        );
        $this->app->bind(
            \App\Repositories\Backend\DevicePeople\DevicePeopleContract::class,
            \App\Repositories\Backend\DevicePeople\DevicePeopleRepository::class
        );
        $this->app->bind(
            \App\Repositories\Backend\ContactTrac\ContactTracContract::class,
            \App\Repositories\Backend\ContactTrac\ContactTracRepository::class
        );

      $this->app->bind(
                \App\Repositories\Backend\Building\BuildingContract::class,
                \App\Repositories\Backend\Building\BuildingRepository::class
            );
      $this->app->bind(
                \App\Repositories\Backend\BuildingPolicy\BuildingPolicyContract::class,
                \App\Repositories\Backend\BuildingPolicy\BuildingPolicyRepository::class
            );
      $this->app->bind(
                \App\Repositories\Backend\Booking\BookingContract::class,
                \App\Repositories\Backend\Booking\BookingRepository::class
            );
      $this->app->bind(
                \App\Repositories\Backend\GuestUser\GuestUserContract::class,
                \App\Repositories\Backend\GuestUser\GuestUserRepository::class
            );
      $this->app->bind(
                \App\Repositories\Backend\GuestUserData\GuestUserDataContract::class,
                \App\Repositories\Backend\GuestUserData\GuestUserDataRepository::class
            );
      $this->app->bind(
        \App\Repositories\Backend\BuildingAccess\BuildingAccessContract::class,
        \App\Repositories\Backend\BuildingAccess\BuildingRepository::class
      );

    }
}
