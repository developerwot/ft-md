<?php

namespace App\Exports;

use App\Models\Employee;
use App\Models\EmployeeTemperature;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class DailyReportExport implements  WithHeadings, WithStrictNullComparison, WithEvents ,ShouldAutoSize ,FromCollection, WithMapping
{
    public $companyData;

    public function __construct($companyData)
    {
        $this->companyData = $companyData;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function headings(): array
    {
        return [
            'first_name',
            'last_name',
            'status',
        ];
    }

    public function collection()
    {
        return $this->companyData;
    }

    public function map($euser): array
    {
        if ($euser->temperrature < 37.5) {
            $status = 'safe';
        } else {
            $status = 'unsafe';
        }
        return [
            $euser->first_name,
            $euser->last_name,
            $status
        ];
    }
//    public function query()
//    {
////        Employee::query()->with('employee_temperature')->groupBy('company_id');
//        $articles = EmployeeTemperature::query()->join('employee', 'employee_temperature.employee_id', '=', 'employee.id')
//            ->select('employee.id', 'employee.first_name', 'employee.last_name','employee_temperature.temperature')
//            ->groupBy("employee.company_id")->get();
//        dd($articles);
//        return $articles;
//    }
//}
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // All headers - set font size to 14
                $cellRange = 'A1:W1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);

                // Apply array of styles to B2:G8 cell range
                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => 'FFFF0000'],
                        ]
                    ]
                ];
                $event->sheet->getDelegate()->getStyle('B2:G8')->applyFromArray($styleArray);

                // Set first row to height 20
                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(20);

                // Set A1:D4 range to wrap text in cells
                $event->sheet->getDelegate()->getStyle('A1:D4')
                    ->getAlignment()->setWrapText(true);
            },
        ];
    }
}
