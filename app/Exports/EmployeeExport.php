<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class EmployeeExport implements FromQuery , WithHeadings,WithMapping, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(int $company_id=0)
    {
        $this->company_id = $company_id;
    }
    public function headings(): array
    {
        return [
            'ID',
            'first_name',
            'last_name',
            'email',
            'status',
            'company_name',
            'company_email',

        ];
    }
    public function collection()
    {
        return Employee::all();
    }
    public function map($euser): array
    {
        return [
            $euser->id,
            $euser->first_name,
            $euser->last_name,
            $euser->email,
            $euser->status,
            $euser->company->business_name,
            $euser->company->email,

        ];
    }
    public function query()
    {
        if($this->company_id) {
            return Employee::query()->with('company')->where('company_id', $this->company_id);
        }
        else {
            return Employee::query()->with('company');
        }

    }
}
