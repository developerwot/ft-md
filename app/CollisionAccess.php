<?php

namespace App;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;

class CollisionAccess extends Model
{
    protected $fillable = [
        'from_user',
        'to_user',
        'contacted_at',
    ];

    protected $dates = [
        'contacted_at',
    ];
    public function fromUser(){
        return $this->belongsTo(Employee::class,'from_user','user_id');
    }
    public function toUser(){
        return $this->belongsTo(Employee::class,'to_user','user_id');
    }
}
