<?php

namespace App\Traits;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

trait FailedValidationJsonTrait{
    protected function failedValidation(Validator $validator)
    {
        $response = [];
        foreach ($validator->errors()->all() as $key => $value) {
            $response[] = $value;
        }
        $finalRes = [
            'status'  => 'error',
            'message' => $response,
            'code'    => 422
        ];
        throw new HttpResponseException(response()->json($finalRes, 422));
    }
}
