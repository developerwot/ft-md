<?php

namespace App;

use App\Notifications\ResetPasswordLink;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable implements CanResetPassword
{
    use \Illuminate\Auth\Passwords\CanResetPassword;
    use Notifiable;
    use LaratrustUserTrait;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','status','phone','logo','major_id','minor_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordLink($token));
    }
    public function company() {
        return $this->hasOne('App\Models\Company', 'user_id');
    }

    protected static function boot()
    {
        parent::boot();
//        static::creating(function ($model) {
//            $ids=checkUser();
//            $model->major_id =$ids['major'];
//            $model->minor_id =$ids['minor'];
//        });
    }
}
