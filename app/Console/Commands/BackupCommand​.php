<?php

namespace App\Console\Commands;

use App\Mail\BackupFailed;
use App\Mail\BbBackup;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupCommand​ extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     * Possible options for duration is : hourly | daily | weekly | monthly | yearly
     */
    protected $signature = 'backup:mysql {--duration=hourly}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'MySQL DB Backup with multiple interval.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $duration = $this->option('duration');
        if (empty($duration) || $duration == null) {
            $duration = 'hourly';
        }

        $this->takeBackupByDuration($duration);
    }

    public function takeBackupByDuration($duration)
    {
        set_time_limit(0);

        // define target file
        $tempLocation     = '/tmp/' .env('DB_DATABASE') . '_' .$duration.'_'. date("Y-m-d_H-i-s") . '.sql';
        $targetFilePath   = '/intellyscan-mysql-backup/' .$duration.'/' .env('DB_DATABASE') . '_' . date("Y-m-d_H-i-s") . '.sql';

//        $tempLocation =storage_path() . '\app\backup\\' .  "backup-".$duration.'-'. date('Y-m-d_H-i-s') . ".sql";
//        dd($tempLocation,$targetFilePath);
        $command = "mysqldump --user=" . env('DB_USERNAME') ." --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . "  > " . $tempLocation;
        $returnVar = NULL;
        $output  = NULL;
        exec($command, $output, $returnVar);

        try {

            if (file_exists($tempLocation))
            {
                $s3 = Storage::disk('s3');
                $s3->put($targetFilePath, file_get_contents($tempLocation), 'private');

//                $current_timestamp = time() - (72 * 3600);
//                $allFiles = $s3->allFiles(env('APP_ENV'));

//                foreach ($allFiles as $file)
//                {
//                    // delete the files older then 3 days..
//                    if ( $s3->lastModified($file) <= $current_timestamp )
//                    {
//                        $s3->delete($file);
//                        $this->info("File: {$file} deleted.");
//                    }
//                }
                @unlink($tempLocation);
//                $message='Database backup stored successfully';
//                Mail::to(env('ADMIN_BACKUP_ALERT_EMAIL'))->send(new BbBackup($duration,$targetFilePath,$message));
            }
            else {
                $message='Database backup has been failed';
                $reason='file does not exist at '.$tempLocation;
                Mail::to(env('ADMIN_BACKUP_ALERT_EMAIL'))->send(new BackupFailed($duration,$tempLocation,$message,$reason));
            }


        }
        catch (\Exception $e)
        {
            $reason=$e->getMessage();
            $message='Database backup has been failed';
            Mail::to(env('ADMIN_BACKUP_ALERT_EMAIL'))->send(new BackupFailed($duration,$tempLocation,$message,$reason));
            $this->info($e->getMessage());
        }
    }
}
