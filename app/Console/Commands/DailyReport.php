<?php

namespace App\Console\Commands;

use App\Mail\DailyTemperatureMail;
use App\Exports\DailyReportExport;
use App\Models\EmployeeTemperature;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class DailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Temperature reports of employees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $rr = \App\Models\EmployeeTemperature::join('employee', 'employee_temperature.employee_id', '=', 'employee.id')->whereDate('employee_temperature.temp_date',Carbon::now()->subDays(1))
            ->where('employee_temperature.deleted_at','=',null)
            ->select('employee.first_name', 'employee.last_name', 'employee_temperature.temperature', 'employee.company_id')
            ->get();
        if(collect($rr)->count()==0){
            $mailIds=Setting::get('report_email');
            \Maatwebsite\Excel\Facades\Excel::store(new \App\Exports\DailyReportExport($rr), 'daily-reports/daily_temperature.xlsx', 'public', \Maatwebsite\Excel\Excel::XLSX);
            foreach ($mailIds as $mailId){
                if($mailId->report_email){
                    Mail::to($mailId->report_email)->send(new DailyTemperatureMail('daily_temperature.xlsx'));
                }
            }
            @unlink(storage_path('app/public/daily-reports/daily_temperature.xlsx'));
        }else{
            $ji = collect($rr)->groupBy('company_id');
            foreach ($ji as $k => $h) {
                \Maatwebsite\Excel\Facades\Excel::store(new \App\Exports\DailyReportExport($h), 'daily-reports/daily_temperature'.$k.'.xlsx', 'public', \Maatwebsite\Excel\Excel::XLSX);
                $mail= Setting::where('company_id',$k)->value('report_email');
//      dd(asset('storage/daily-reports/daily_temperature'.$k.'.xlsx'));
//      dump($mail);

                if($mail!=null){
                    $file='daily_temperature'.$k.'.xlsx';
                    Mail::to($mail)->send(new DailyTemperatureMail($file));
                }
                @unlink(storage_path('app/public/daily-reports/daily_temperature'.$k.'.xlsx'));

            }
        }



    }
}
