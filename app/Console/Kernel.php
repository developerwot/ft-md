<?php

namespace App\Console;

use App\Console\Commands\BackupCommand​;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        BackupCommand​::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('backup:mysql --duration=hourly')->withoutOverlapping()->hourly();
        $schedule->command('backup:mysql --duration=daily')->withoutOverlapping()->daily();
        $schedule->command('backup:mysql --duration=weekly')->withoutOverlapping()->weekly();
        $schedule->command('backup:mysql --duration=monthly')->withoutOverlapping()->monthly();
        $schedule->command('backup:mysql --duration=yearly')->withoutOverlapping()->yearly();
        $schedule->command('daily:report')->dailyAt('05:00');
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
