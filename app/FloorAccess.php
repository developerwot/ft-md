<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FloorAccess extends Model
{
    use SoftDeletes;
    protected $guarded=[];

    public function buildings(){
        return  $this->belongsTo(BuildingAccess::class,'building_access_id');
    }
}
