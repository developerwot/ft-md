<?php

namespace App\Repositories\Backend\Device;

interface DeviceContract
{

    public function getAll();

    public function getAllDeviceByCompany($company_id,$search='');

    public function find($id);

    public function findByBeaconId($beaconId,$id='');

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
