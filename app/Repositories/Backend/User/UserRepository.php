<?php

namespace App\Repositories\Backend\User;

use App\EventParticipate;
use App\Imports\UsersImport;
use App\Role;
use App\User;
use App\UserGroup;
use App\UserSoftDelete;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class UserRepository implements UserContract
{

    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        $users_data = $this->model->orderBy('id', 'desc')->latest()->get();

        $users = $users_data->filter(function ($item){
           return $item->user_roles = $item->roles;
        });

        return $users;
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function store($input)
    {
        $input['password'] = '123456Aa';
        $input['password'] = bcrypt($input['password']);
        $user = $this->model->create($input);
        $user->syncRoles($input['roles']);
        return $user;
    }

    public function update($id, $input)
    {
        $user = $this->model->findOrFail($id);
        $user->syncRoles($input['roles']);
        return $user->update($input);
    }

    public function delete($id)
    {
        $user = $this->model->findOrFail($id);
        $user->destroy($id);
    }

}
