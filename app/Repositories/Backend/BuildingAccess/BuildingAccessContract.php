<?php

namespace App\Repositories\Backend\BuildingAccess;

interface BuildingAccessContract
{
    public function storeBuilding($data);

    public function updateBuilding($id,$data);

    public function deleteBuilding($id);

}
