<?php

namespace App\Repositories\Backend\BuildingAccess;
use App\BookingPolicy;
use App\BuildingAccess;
use App\DeskAccess;
use App\FloorAccess;
use App\LocationAccess;
use App\LocationDevice;
use App\TimeslotAccess;

class BuildingRepository implements BuildingAccessContract
{
    protected $model;
    protected $emp;

    public function __construct(BuildingAccess $model)
    {
        $this->model = $model;

    }

    public function storeBuilding($data)
    {
        $this->model->create($data);
    }

    public function updateBuilding($id, $data)
    {
     $this->model->where('id',$id)->update($data);
    }

    public function deleteBuilding($id)
    {
       $this->model->destroy($id);
        LocationAccess::where('building_access_id',$id)->delete();
        TimeslotAccess::where('building_access_id',$id)->delete();
        LocationDevice::where('building_id',$id)->delete();
        FloorAccess::where('building_access_id',$id)->delete();
        DeskAccess::where('building_id',$id)->delete();
        BookingPolicy::where('building_id',$id)->delete();
//        FloorAccess::where('building_access_id',$id)->delete();
    }
}
