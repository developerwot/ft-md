<?php

namespace App\Repositories\Backend\GuestUser;

use App\EventParticipate;
use App\Models\Employee;
use App\Models\GuestUser;
use Illuminate\Support\Facades\Input;


class GuestUserRepository implements GuestUserContract
{

    protected $model;
    protected $emp;


    public function __construct(GuestUser $model,Employee $emp)
    {
        $this->model = $model;
        $this->emp = $emp;
    }

    public function getAll($searchKey='')
    {
        return $this->model->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllGuestUsersByCompany($company_id,$date='',$search='')
        {
            $result =  $this->model->select('id','company_id','email_address','from_date','to_date')
                            ->where('company_id',$company_id);

            if( !empty($search) ) {
                $result->where('email_address', 'LIKE', "%$search%");

            }
            if(!empty($date)) {
                $result->whereRaw('"'.$date.'" between `from_date` and `to_date`');

            }


            return $result->orderBy('id', 'desc')->latest()->get();
        }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findByEmployee($employee_id)
    {
        return $this->model->where('employee_id',$employee_id)->first();
    }
    public function checkEmployee($employee_id,$company_id='')
    {
        return $this->emp->where('id',$employee_id)->where('company_id',$company_id)->first();
    }
    public function checkEmployeeByEmail($employee_email,$company_id='')
    {
        return $this->emp->where('email',$employee_email)->where('company_id',$company_id)->first();
    }
    public function checkEmail($email,$from_date,$to_date)
    {
        $from_date = date('Y-m-d',strtotime($from_date));
        $to_date = date('Y-m-d',strtotime($to_date));

        return $this->model->where('email_address',$email)
                            ->whereDate('from_date','=',$from_date)
                            ->whereDate('to_date','=',$to_date)

                            ->orWhere(function($query) use ($from_date,$to_date,$email) {
                               $query->whereBetween('from_date',[$from_date,$to_date]);
                                $query->where('email_address',$email);
                             })
                            ->orWhere(function($query) use ($from_date,$to_date,$email) {
                                $query->whereBetween('to_date',[$from_date,$to_date]);
                                $query->where('email_address',$email);
                             })

                            ->orWhere(function($query) use ($from_date,$to_date,$email) {
                                $query->whereDate('to_date','>=',$from_date);
                                $query->whereDate('from_date','<=',$from_date);
                                $query->where('email_address',$email);
                             })

                           /* ->orWhere(function($query) use ($from_date,$to_date,$email) {
                                $query->whereDate('to_date','<=',$to_date);
                                $query->whereDate('from_date','>=',$to_date);
                                $query->where('email_address',$email);
                             })*/

                            ->first();
    }
    public function findByCode($code,$current_date='')
    {
        $binds = array($code);
        return $this->model->whereRaw("BINARY `verify_code`  = ?",$binds)
                          /*  ->where("verify_code",$code)*/

                           /* ->where("is_verified",0)*/
                            /*->whereDate("from_date",'>=',$current_date)
                            ->whereDate("to_date",'<=',$current_date)*/
                            ->first();
    }
    public function findAllFieldByEmail($email)
    {
        $data = $this->getFields($email);
        $array = ["is_first_name","is_last_name","is_dob","is_address","is_city","is_postal_code","is_telephone_number","is_address_proof","is_identity_proof","is_company_name"];
        if(!empty($array)) {
            $fields = [];
            foreach ($array as $ar) {
                $fields[$ar] = collect($data)->where($ar,'=',1)->pluck($ar,$ar)->toArray();
            }
        }

        $result = array_filter($fields);
        return array_keys($result);

    }
    public function findByEmail($email)
    {
        return $this->model
                        ->select("email_address","company_id","from_date","to_date","is_completed","completed_at")
                        ->where('email_address',$email)->get();
    }
    public function getFields($email) {
        return $this->model
            ->select("is_first_name","is_last_name","is_dob","is_address","is_city","is_postal_code","is_telephone_number","is_address_proof","is_identity_proof","is_company_name")
            ->where('email_address',$email)
            ->get();
    }
    public function getEmpByCompanyAndStatus($company_id,$status)
    {
        $empIds = $this->getAllEmpByCompany($company_id,true);

        return $this->model->where('training_status',$status)
                ->whereIn('employee_id',$empIds)
                ->orderBy('id', 'desc')->latest()->pluck('employee_id');
    }
    public function getAllEmpByCompany($company_id,$isID = false,$search='')
    {

        if($isID) {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->pluck('id');
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }
        }
        else {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->get();
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->get();
            }

        }

    }
    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        $role->update($input);
        return $role;
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
