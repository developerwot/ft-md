<?php

namespace App\Repositories\Backend\GuestUser;

interface GuestUserContract
{

    public function getAll($searchKey='');

    public function getAllGuestUsersByCompany($company_id,$date='',$search='');

    public function find($id);

    public function findByEmployee($employee_id);

    public function checkEmployee($employee_id,$company_id='');
    
    public function checkEmployeeByEmail($employee_id,$company_id='');

    public function checkEmail($email,$from_date,$to_date);

    public function findByCode($code,$current_date='');

    public function findAllFieldByEmail($email);

    public function findByEmail($email);

    public function getAllEmpByCompany($company_id);

    public function getEmpByCompanyAndStatus($company_id,$status);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
