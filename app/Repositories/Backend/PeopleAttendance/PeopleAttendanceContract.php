<?php

namespace App\Repositories\Backend\PeopleAttendance;

interface PeopleAttendanceContract
{

    public function getAll();

    public function getAllAttendanceByCompany($company_id);

    public function getAllAttendanceByDate($company_id,$current_date,$search='');

    public function getAllAttendanceByDateRange($company_id,$start_date,$end_date,$searchKey='');

    public function getAllAttendanceByMonth($company_id,$month,$year,$searchKey='');

    public function find($id);

    public function findByDate($employee_id,$date);

    public function getAllEmpByCompany($company_id,$isID = false,$search='');

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
