<?php

namespace App\Repositories\Backend\PeopleAttendance;

use App\EventParticipate;
use App\Models\Employee;
use App\Models\PeopleAttendance;
use App\Repositories\Backend\PeopleAttendance\PeopleAttendanceContract;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class PeopleAttendanceRepository implements PeopleAttendanceContract
{

    protected $model;
    protected $emp;
    protected $group_emp;

    public function __construct(PeopleAttendance $model,Employee $emp)
    {
        $this->model = $model;
        $this->emp = $emp;
    }

    public function getAll()
    {
        return $this->model->with('group_employees.employee')->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllAttendanceByCompany($company_id)
    {
            $employee = $this->getAllEmpByCompany($company_id,true);
            return $this->model->with('employee')->whereIn('employee_id',$employee)->orderBy('id', 'desc')->latest()->get();
    }

    public function getAllAttendanceByDate($company_id,$current_date,$search='')
    {
            $employee = $this->getAllEmpByCompany($company_id,true,$search);
            return $this->model->with('employee')
                        ->whereIn('employee_id',$employee)
                        ->whereDate('date',$current_date)
                        ->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllAttendanceByDateRange($company_id,$start_date,$end_date,$searchKey='')
    {
            $employee = $this->getAllEmpByCompany($company_id,true,$searchKey);

            $attendance =  $this->model->with('employee')
                        ->whereIn('employee_id',$employee)
                        ->whereDate('date','>=',$start_date)
                        ->whereDate('date','<=',$end_date)
                        ->orderBy('id', 'desc')->latest()->get();

            return $attendance;
    }
    public function getAllAttendanceByMonth($company_id,$month,$year,$searchKey='')
    {
            $employee = $this->getAllEmpByCompany($company_id,true,$searchKey);
            return $this->model->with('employee')
                        ->whereIn('employee_id',$employee)
                        ->whereMonth('date',$month)
                        ->whereYear('date',$year)
                        ->orderBy('id', 'desc')->latest()->get();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findByDate($employee_id,$date)
    {
        return $this->model->where('employee_id',$employee_id)->whereDate('date',$date)->first();
    }
    public function getAllEmpByCompany($company_id,$isID = false,$search='')
    {
        if($isID) {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                               ->where(function($query) use ($search) {
                                   $query->where('first_name', 'LIKE', "%$search%");
                                   $query->orWhere(function ($query) use ($search) {
                                       $query->where('last_name', 'LIKE', "%$search%");
                                   });
                                   $query->orWhere(function ($query) use ($search) {
                                       $query->where('email', 'LIKE', "%$search%");
                                   });
                                 })
                                ->orderBy('id', 'desc')->latest()->pluck('id');
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }
        }
        else {
            return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->get();
        }

    }
    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
