<?php

namespace App\Repositories\Backend\PeopleRegistry;

interface PeopleRegistryContract
{

    public function getAll();

    public function getAllFaceRecoByCompany($company_id,$search='');

    public function find($id);

    public function findByEmployee($employee_id);

    public function getAllEmpByCompany($company_id);

    public function getEmpByCompanyAndStatus($company_id,$status);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
