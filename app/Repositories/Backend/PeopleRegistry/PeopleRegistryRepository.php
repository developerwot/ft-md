<?php

namespace App\Repositories\Backend\PeopleRegistry;

use App\EventParticipate;
use App\Models\Employee;
use App\Models\PeopleRegistry;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class PeopleRegistryRepository implements PeopleRegistryContract
{

    protected $model;
    protected $emp;
    protected $group_emp;

    public function __construct(PeopleRegistry $model,Employee $emp)
    {
        $this->model = $model;
        $this->emp = $emp;
    }

    public function getAll()
    {
        return $this->model->with('group_employees.employee')->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllFaceRecoByCompany($company_id,$search='')
        {
            $employee = $this->getAllEmpByCompany($company_id,true,$search);
            return $this->model->with('employee')->whereIn('employee_id',$employee)->orderBy('id', 'desc')->latest()->get();
        }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findByEmployee($employee_id)
    {
        return $this->model->where('employee_id',$employee_id)->first();
    }
    public function getEmpByCompanyAndStatus($company_id,$status)
    {
        $empIds = $this->getAllEmpByCompany($company_id,true);

        return $this->model->where('training_status',$status)
                ->whereIn('employee_id',$empIds)
                ->orderBy('id', 'desc')->latest()->pluck('employee_id');
    }
    public function getAllEmpByCompany($company_id,$isID = false,$search='')
    {

        if($isID) {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->pluck('id');
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }
        }
        else {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->get();
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->get();
            }

        }

    }
    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
