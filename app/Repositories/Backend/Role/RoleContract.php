<?php

namespace App\Repositories\Backend\Role;

interface RoleContract
{

    public function getAll();

    public function find($id);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
