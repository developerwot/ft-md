<?php

namespace App\Repositories\Backend\Group;

use App\EventParticipate;
use App\Models\Employee;
use App\Models\Group;
use App\Models\GroupEmployee;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class GroupRepository implements GroupContract
{

    protected $model;
    protected $emp;
    protected $group_emp;

    public function __construct(Group $model,Employee $emp,GroupEmployee $group_emp)
    {
        $this->model = $model;
        $this->emp = $emp;
        $this->group_emp = $group_emp;
    }

    public function getAll()
    {
        return $this->model->with('group_employees.employee')->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllGroupByCompany($company_id,$search='')
        {

            if(!empty($search)) {
                return $this->model->with('group_employees.employee')
                    ->where('name', 'LIKE', "%$search%")
                    ->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
            }
            else {
                return $this->model->with('group_employees.employee')
                    ->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
            }

        }

    public function find($id)
    {
        return $this->model->with('group_employees')->findOrFail($id);
    }
    public function getAllEmpByCompany($company_id,$isID = false)
    {
        if($isID) {
            return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
        }
        else {
            return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->get();
        }

    }
    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }
    public function store_group_emp($input)
    {
        $role = $this->group_emp->create($input);
        return $role;
    }
    public function delete_group_emp($group_id)
    {
        $role = $this->group_emp->where('group_id',$group_id)->delete();
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
