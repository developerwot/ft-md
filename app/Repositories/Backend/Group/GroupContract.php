<?php

namespace App\Repositories\Backend\Group;

interface GroupContract
{

    public function getAll();

    public function getAllGroupByCompany($company_id,$searchKey='');

    public function find($id);

    public function getAllEmpByCompany($company_id);

    public function store($input);

    public function store_group_emp($input);

    public function delete_group_emp($group_id);

    public function update($id,$input);

    public function delete($id);

}
