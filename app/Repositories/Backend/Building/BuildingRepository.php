<?php

namespace App\Repositories\Backend\Building;

use App\EventParticipate;
use App\Models\Building;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class BuildingRepository implements BuildingContract
{

    protected $model;
    protected $emp;

    public function __construct(Building $model)
    {
        $this->model = $model;

    }

    public function getAll()
    {
        return $this->model->with('group_employees.employee')->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllByCompany($company_id,$search='')
    {
        if(!empty($search)) {
            return $this->model->where('company_id',$company_id)
                ->where('name', 'LIKE', "%$search%")->orderBy('id', 'desc')->latest()->get();
        }
        else {
            return $this->model->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
        }

    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findByBeaconId($beaconId,$id='')
    {
        if(!empty($id)) {
            return $this->model->where('beaconId',$beaconId)->where('id','!=',$id)->first();
        }
        else {
            return $this->model->where('beaconId',$beaconId)->first();
        }

    }

    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
