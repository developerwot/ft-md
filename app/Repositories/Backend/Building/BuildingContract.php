<?php

namespace App\Repositories\Backend\Building;

interface BuildingContract
{

    public function getAll();

    public function getAllByCompany($company_id,$searchKey='');

    public function find($id);

    public function findByBeaconId($beaconId,$id='');

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
