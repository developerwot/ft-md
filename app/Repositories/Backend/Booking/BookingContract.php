<?php

namespace App\Repositories\Backend\Booking;

interface BookingContract
{


   public function getAllBuildingByCompany($company_id,$ids);

   public function getAllEmployeeByCompany($company_id,$ids);

   public function getBookingsByDate($building_id,$date,$emp_id='',$time='',$id='');

   public function getAllByCompany($company_id,$searchKey='');

    public function find($id);

    public function findBuildingById($building_id,$id='');

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
