<?php

namespace App\Repositories\Backend\Booking;

use App\EventParticipate;
use App\Models\Booking;
use App\Models\Building;
use App\Models\BuildingPolicy;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Log;

class BookingRepository implements BookingContract
{

    protected $model;
    protected $building;
    protected $emp;

    public function __construct(Booking $model,Building $building,Employee $emp)
    {
        $this->model = $model;
        $this->building = $building;
        $this->emp = $emp;

    }
    public function getAllBuildingByCompany($company_id,$ids=false,$search='')
    {
        if($ids) {
            if(!empty($search)) {
                return $this->building->where('company_id',$company_id)
                        ->where('name', 'LIKE', "%$search%")
                        ->orderBy('id', 'desc')->latest()->get()->pluck('id');
            }
            else {
                return $this->building->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get()->pluck('id');
            }

        }
        else {
            return $this->building->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
        }

    }
    public function getAllEmployeeByCompany($company_id,$ids=false,$search='')
    {

        if($ids) {

            if(!empty($search)) {
                return $this->emp->where('company_id', $company_id)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->get()->pluck('id');
            }
            else {
                return $this->emp->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get()->pluck('id');
            }

        }
        else {

            return $this->emp->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
        }

    }
    public function getBookingsByDate($building_id,$date,$emp_id='',$time='',$id='')
    {
        DB::enableQueryLog();

        if(!empty($emp_id) && !empty($time)) {

            $data =  $this->model->where('building_id',$building_id)
                ->whereDate('booking_date',"=",$date)
                /*->whereTime('booking_time','>=',$time)
                ->whereTime('booking_time','<=',$time)*/
                ->where('employee_id','=',$emp_id);

            if(!empty($id)) {
                $data->where('id','!=',$id);
            }

            $queryRawSql = vsprintf($data->toSql(), $data->getBindings());

            Log::debug('[SQL EXEC]', [
                    "raw sql"  => $queryRawSql,
                ]
            );
            return $data->get();
        }
        else {
            if(!empty($id)) {
                $data= $this->model->where('building_id',$building_id)->whereDate('booking_date',"=",$date);
                $data->where('id','!=',$id);
            }
            else {
                $data = $this->model->where('building_id',$building_id)->whereDate('booking_date',"=",$date);
            }

            return $data->get();
        }

    }


    public function getAllByCompany($company_id,$search='')
    {

        $building_ids = $this->getAllBuildingByCompany($company_id,true,$search);

        if(!empty($search)) {
            $employee_ids = $this->getAllEmployeeByCompany($company_id,true,$search);

            if(!empty($employee_ids)) {
                return $this->model->with('building')
                    ->where(function($query) use ($employee_ids,$building_ids) {
                        $query->orWhere(function ($query) use ($employee_ids) {
                            $query->whereIn('employee_id', $employee_ids);
                        });
                        $query->orWhere(function ($query) use ($building_ids) {
                            $query->whereIn('building_id', $building_ids);
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->get();
            }
        }
        else {
            return $this->model->with('building')->whereIn('building_id',$building_ids)
                ->orderBy('id', 'desc')->latest()->get();
        }

    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findBuildingById($building_id,$id='')
    {
        return $this->building->where('id',$building_id)->first();
    }

    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
