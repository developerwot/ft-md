<?php

namespace App\Repositories\Backend\Company;

use App\EventParticipate;
use App\Models\Company;
use App\Repositories\Backend\Company\CompanyContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class CompanyRepository implements CompanyContract
{

    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        $company_data = $this->model->orderBy('id', 'desc')->latest()->get();
        return $company_data;
    }

    public function find($id)
    {
        return $this->model->with('employees','groups')->findOrFail($id);
    }
    public function findEmail($email)
    {
        return $this->model->where("email",$email)->first();
    }
    public function findByStatus($id,$status)
    {
        return $this->model->where('status',$status)->where('id',$id)->first();
    }

    public function store($input)
    {

        $user = $this->model->create($input);

        return $user;
    }

    public function update($id, $input)
    {
        $user = $this->model->findOrFail($id);
      try {
          $user->update($input);
      }catch(\Exception $e){
          dd($e);
      }

    }

    public function delete($id) //Soft Delete
    {
        $user = $this->model->findOrFail($id);
        $user->destroy($id);
    }
    public function cancel($id) //Hard Delete
    {
        $user = $this->model->findOrFail($id);
        $user->forceDelete();
    }

}
