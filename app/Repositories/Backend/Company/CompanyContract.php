<?php

namespace App\Repositories\Backend\Company;

interface CompanyContract
{

    public function getAll();

    public function find($id);

    public function findEmail($email);

    public function findByStatus($id,$status);

    public function store($input);

    public function update($id,$input);

    public function delete($id); //Soft Delete

    public function cancel($id); //Hard Delete

}
