<?php

namespace App\Repositories\Backend\BuildingPolicy;

use App\EventParticipate;
use App\Models\Building;
use App\Models\BuildingPolicy;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class BuildingPolicyRepository implements BuildingPolicyContract
{

    protected $model;
    protected $building;

    public function __construct(BuildingPolicy $model,Building $building)
    {
        $this->model = $model;
        $this->building = $building;

    }
    public function getAllBuildingByCompany($company_id,$ids=false)
    {
        if($ids) {
            return $this->building->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get()->pluck('id');
        }
        else {
            return $this->building->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
        }

    }

    public function getAllByCompany($company_id)
    {
        $building_ids = $this->getAllBuildingByCompany($company_id,true);
        return $this->model->with('building')->whereIn('building_id',$building_ids)
                            ->orderBy('id', 'desc')->latest()->get();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findByBeaconId($beaconId,$id='')
    {
        if(!empty($id)) {
            return $this->model->where('beaconId',$beaconId)->where('id','!=',$id)->first();
        }
        else {
            return $this->model->where('beaconId',$beaconId)->first();
        }

    }

    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
