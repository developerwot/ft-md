<?php

namespace App\Repositories\Backend\BuildingPolicy;

interface BuildingPolicyContract
{


   public function getAllBuildingByCompany($company_id,$ids);

   public function getAllByCompany($company_id);

    public function find($id);

    public function findByBeaconId($beaconId,$id='');

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
