<?php

namespace App\Repositories\Backend\EmpTemp;

use App\EventParticipate;
use App\Models\Company;
use App\Models\Employee;
use App\Models\EmployeeTemperature;
use App\Models\EmployeeTemperatureLog;
use App\Repositories\Backend\EmpTemp\EmpTemperatureContract;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class EmpTemperatureRepository implements EmpTemperatureContract
{

    protected $model;
    protected $emp;
    protected $log;

    public function __construct(EmployeeTemperature $model,Employee $emp,EmployeeTemperatureLog $log)
    {
        $this->model = $model;
        $this->emp = $emp;
        $this->log = $log;
    }

    public function getAll($date='',$search='')
    {

        if(!empty($date)) {
            if(!empty($search)) {

                $companyIds = Company::where('business_name', 'LIKE', "%$search%")->where('status',1)->pluck('id')->toArray();
                $empIDs = $this->getAllEmp($search,$companyIds);
                return $this->model->whereDate("temp_date","=",$date)
                    ->whereIn('employee_id',$empIDs)
                    ->orWhere(function($query) use ($search) {
                        $query->where('temperature', 'LIKE', "%$search%");
                    })
                    ->orderBy('id', 'desc')->latest()->get();
            }
            else {
                return $this->model->whereDate("temp_date","=",$date)
                    ->orderBy('id', 'desc')->latest()->get();
            }
        }
        else {
            if(!empty($search)) {
                $empIDs = $this->getAllEmp($search);
                return $this->model->whereIn('employee_id',$empIDs)->orderBy('id', 'desc')->latest()->get();
            }
            else {
                return $this->model->orderBy('id', 'desc')->latest()->get();
            }
        }

    }
    public function getAllEmp($search='',$companyIds='')
    {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('status',1)
                    ->where(function($query) use ($search,$companyIds) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($companyIds) {
                            $query->where('company_id', $companyIds);
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->pluck('id');
            }
            else {
                return $this->emp->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }



    }
    public function getAllEmpByCompany($company_id,$isID = false,$search='')
    {
        if($isID) {
            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->pluck('id');
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }

        }
        else {
            return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->get();
        }

    }
    public function getAllEmpTempByCompany($company_id,$date='',$search='')
    {
        $empIDs = $this->getAllEmpByCompany($company_id,true,$search);
        if(!empty($date)) {
            return $this->model->whereDate("temp_date","=",$date)
                            ->whereIn('employee_id',$empIDs)->orderBy('id', 'desc')->latest()->get();
        }
        else {
            return $this->model->whereIn('employee_id',$empIDs)->orderBy('id', 'desc')->latest()->get();
        }

    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findByEmployee($employee_id,$date)
    {
        return $this->model->where('employee_id',$employee_id)->where('is_guest','<>',1)
                            ->whereDate('temp_date','=',$date)->first();

    }
    public function findByGuest($employee_id,$date)
    {
        return $this->model->where('employee_id',$employee_id)->where('is_guest',1)
                            ->whereDate('temp_date','=',$date)->first();

    }

    public function store($input)
    {
        $role = $this->model->create($input);
        $this->storeLog($input);
        return $role;
    }
    public function storeLog($input)
    {
        $role = $this->log->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        $this->storeLog($input);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
