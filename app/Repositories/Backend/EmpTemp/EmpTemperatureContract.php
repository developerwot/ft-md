<?php

namespace App\Repositories\Backend\EmpTemp;

interface EmpTemperatureContract
{

    public function getAll($date='',$search='');

    public function getAllEmp($search);

    public function getAllEmpByCompany($company_id);

    public function getAllEmpTempByCompany($company_id,$date='',$search='');

    public function find($id);

    public function findByEmployee($employee_id,$date);
    
     public function findByGuest($employee_id,$date);

    public function store($input);

    public function storeLog($input);

    public function update($id,$input);

    public function delete($id);

}
