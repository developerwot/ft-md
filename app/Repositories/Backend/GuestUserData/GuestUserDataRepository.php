<?php

namespace App\Repositories\Backend\GuestUserData;

use App\EventParticipate;
use App\Models\Employee;
use App\Models\GuestUser;
use App\Models\GuestUserData;
use Illuminate\Support\Facades\Input;


class GuestUserDataRepository implements GuestUserDataContract
{

    protected $model;
    protected $guser;

    public function __construct(GuestUserData $model,GuestUser $guser)
    {
        $this->model = $model;
        $this->guser = $guser;
    }

    public function getAll()
    {
        return $this->model->with('group_employees.employee')->orderBy('id', 'desc')->latest()->get();
    }
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function checkGuestUser($guest_user_id)
    {
        return $this->guser->where('id',$guest_user_id)->first();
    }

    public function findByGuestUser($guser_id)
    {
        return $this->model->where('guest_user_id',$guser_id)->first();
    }
    public function store($input)
    {
        $role = $this->model->create($input);
        
        return $role;
    }
    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        $role->update($input);
        return $role;
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }
}