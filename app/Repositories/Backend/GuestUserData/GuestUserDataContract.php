<?php

namespace App\Repositories\Backend\GuestUserData;

interface GuestUserDataContract
{

    public function getAll();

    public function find($id);

    public function findByGuestUser($guser_id);

    public function checkGuestUser($guest_user_id);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
