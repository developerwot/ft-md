<?php

namespace App\Repositories\Backend\DevicePeople;

use App\EventParticipate;
use App\Models\Building;
use App\Models\Device;
use App\Models\DevicePeople;
use App\Models\Employee;
use Illuminate\Support\Facades\Input;


class DevicePeopleRepository implements DevicePeopleContract
{

    protected $model;
    protected $emp;
    protected $device;

    public function __construct(DevicePeople $model,Device $device,Employee $emp)
    {
        $this->model = $model;
        $this->device = $device;
        $this->emp = $emp;
    }

    public function getAll()
    {
        return $this->model->with('group_employees.employee')->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllDevicePeopleByCompany($company_id,$search='')
    {
        if(!empty($search)) {
            $employee = $this->getAllEmpByCompany($company_id,true,$search);
            $device_ids = $this->findByBeaconIdSearch($company_id,$search);
            $bulding_ids = $this->searchByBuilding($company_id,$search);

            return $this->model->with('employee','building')
                            ->with('device')
                            ->where(function($query) use ($employee,$device_ids,$bulding_ids) {
                                $query->orWhere(function ($query) use ($employee) {
                                    $query->whereIn('employee_id', $employee);
                                });
                                $query->orWhere(function ($query) use ($device_ids) {
                                    $query->whereIn('device_id', $device_ids);
                                });
                                $query->orWhere(function ($query) use ($bulding_ids) {
                                    $query->whereIn('building_id', $bulding_ids);
                                });
                            })
                            ->orderBy('id', 'desc')->latest()->get();
        }
        else {
            $employee = $this->getAllEmpByCompany($company_id,true);
            return $this->model->with('employee','building')
                ->with('device')
                ->whereIn('employee_id',$employee)
                ->orderBy('id', 'desc')->latest()->get();
        }


    }
    public function getAllEmpByCompany($company_id,$isID = false,$search='')
    {
        if($isID) {
            if(!empty($search)) {
                return $this->emp->where('company_id',$company_id)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }

        }
        else {
            return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->get();
        }

    }
    public function getAllDeviceByCompany($company_id)
    {
        return $this->device->where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllBuildingByCompany($company_id)
    {
        return Building::where('company_id',$company_id)->orderBy('id', 'desc')->latest()->get();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function findByBeaconIdSearch($company_id,$search)
    {
            return $this->device->where('company_id',$company_id)->where('beaconId', 'LIKE', "%$search%")->pluck('id');
    }
    public function searchByBuilding($company_id,$search)
    {
            return Building::where('company_id',$company_id)->where('name', 'LIKE', "%$search%")->pluck('id');
    }

    public function findByEmployeeId($employee_id,$id='')
    {
        if(!empty($id)) {
            return $this->model->where('employee_id',$employee_id)->where('id','!=',$id)->first();
        }
        else {
            return $this->model->where('employee_id',$employee_id)->first();
        }
    }

    public function findByDeviceId($device_id,$id='')
    {
        if(!empty($id)) {
            return $this->model->where('device_id',$device_id)->where('id','!=',$id)->first();
        }
        else {
            return $this->model->where('device_id',$device_id)->first();
        }
    }

    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
