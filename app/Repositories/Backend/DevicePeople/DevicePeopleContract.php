<?php

namespace App\Repositories\Backend\DevicePeople;

interface DevicePeopleContract
{

    public function getAll();

    public function getAllDevicePeopleByCompany($company_id,$search='');

    public function getAllDeviceByCompany($company_id);

    public function getAllBuildingByCompany($company_id);

    public function find($id);

    public function findByBeaconIdSearch($company_id,$search);

    public function searchByBuilding($company_id,$search);

    public function findByEmployeeId($employee_id,$id='');

    public function findByDeviceId($device_id,$id='');

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
