<?php

namespace App\Repositories\Backend\ContactTrac;

use App\CollisionAccess;
use App\EventParticipate;
use App\Models\ContactTracing;
use App\Models\ContactTracingLog;
use App\Models\Device;
use App\Models\Employee;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class ContactTracRepository implements ContactTracContract
{

    protected $model;
    protected $log;
    protected $emp;

    public function __construct(ContactTracing $model,Employee $emp,ContactTracingLog $log)
    {
        $this->model = $model;
        $this->emp = $emp;
        $this->log = $log;
    }

    public function getAll()
    {
        return $this->model->orderBy('id', 'desc')->latest()->get();
    }
    public function getAllEmpByCompany($company_id,$isID = false,$search='')
    {

        if($isID) {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->pluck('id');
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->pluck('id');
            }
        }
        else {

            if(!empty($search) && $search !='') {
                return  $this->emp->where('company_id',$company_id)->where('status',1)
                    ->where(function($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%$search%");
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('last_name', 'LIKE', "%$search%");
                        });
                        $query->orWhere(function ($query) use ($search) {
                            $query->where('email', 'LIKE', "%$search%");
                        });
                    })
                    ->orderBy('id', 'desc')->latest()->get();
            }
            else {
                return $this->emp->where('company_id',$company_id)->where('status',1)->orderBy('id', 'desc')->latest()->get();
            }

        }

    }
    public function getContactTracingByEmployee($employee_id,$start_date,$end_date) {
      $data=CollisionAccess::where('from_user',$employee_id)
            ->whereDate('contacted_at',">=",$start_date)
            ->whereDate('contacted_at',"<=",$end_date)->get();
      $allCount=collect($data)->count();
        $count=collect($data)->groupBy('to_user')->count();
//      if($user==238){
//          dd($count);
//      }

//            $this->model->select('employee_id')
//            ->where('employee_id',$employee_id)
//            ->whereDate('contact_date',">=",$start_date)
//            ->whereDate('contact_date',"<=",$end_date)->count();
        $items=[
            'count'=>$count,
            'allCount'=>$allCount
        ];
        return $items;
    }
    public function getContactTracingByEmployeeContactPeople($employee_id,$start_date,$end_date) {
        $data =  $this->model->select('contact_employee_id')
            ->where('contact_employee_id',$employee_id)
            ->whereDate('contact_date',">=",$start_date)
            ->whereDate('contact_date',"<=",$end_date)->count();

        return $data;
    }
    public function getAllTracingByCompany($company_id,$current_date,$search='')
    {
        $empIDs = $this->getAllEmpByCompany($company_id,true,$search);
        DB::enableQueryLog();
        $data =  $this->model->select(DB::raw('COUNT(employee_id) as contact_count') ,'employee_id','contact_date')
            ->with('employee')
            ->whereIn('employee_id',$empIDs)
            ->whereDate('contact_date',"=",$current_date)
            ->groupBy(['employee_id','contact_date']);

        $queryRawSql = vsprintf($data->toSql(), $data->getBindings());

        Log::debug('[SQL EXEC]', [
                "raw sql"  => $queryRawSql,
            ]
        );


        return $data->get();
    }
    public function findByBeaconID($beaconId)
    {
        $term = strtolower($beaconId);
        $device = Device::with('device_people.employee')->whereRaw('lower(beaconId) like (?)',["%{$term}%"])->first();
        if(!empty($device)) {
            $device_people = $device->device_people()->first();
            if(!empty($device_people)) {
                $people = $device_people->employee()->where('status',1)->first();
                return $people;
            }

        }
    }
    public function getAllTracingByCompanyContactPeople($company_id,$current_date,$search='')
    {
        $empIDs = $this->getAllEmpByCompany($company_id,true,$search);
        return $this->model->select(DB::raw('COUNT(contact_employee_id) as contact_count') ,'contact_employee_id','contact_date')
            ->with('contact_employee')
            ->whereIn('contact_employee_id',$empIDs)
            ->whereDate('contact_date',"=",$current_date)
            ->groupBy(['contact_employee_id','contact_date'])
            ->get();
    }
    public function getAllTracingByEmployee($emp_id,$time)
    {

        return $this->model
            ->with('contact_employee')
            ->whereIn('employee_id',[$emp_id])
            ->whereDate('contact_date','=',$time)
            ->get();
    }
    public function getAllTracingByEmployeeContact($emp_id,$time)
    {
        return $this->model
            ->with('employee')
            ->whereIn('contact_employee_id',[$emp_id])
            ->where('contact_date',$time)
            ->get();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function checkTracingByDate($emp1,$emp2,$date)
    {
        $check =  $this->model
            ->whereDate('contact_time',$date)
            ->where('employee_id',$emp1)
            ->where('contact_employee_id',$emp2)
            ->first();
        if(!empty($check)) {
            return $check;
        }
        else {
            $check =  $this->model
                ->whereDate('contact_time',$date)
                ->where('employee_id',$emp2)
                ->where('contact_employee_id',$emp1)
                ->first();
            return $check;
        }
    }
    public function findByEmployeeID($emp_id)
    {
        return $this->emp->where('status',1)->find($emp_id);
    }

    public function store($input)
    {
        $role = $this->model->create($input);
        $this->store_logs($input);
        return $role;
    }

    public function store_logs($input)
    {
        $role =  $this->log->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
