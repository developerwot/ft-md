<?php

namespace App\Repositories\Backend\ContactTrac;

interface ContactTracContract
{

    public function getAll();

    public function getAllEmpByCompany($company_id,$isID = false,$search='');

    public function getContactTracingByEmployee($employee_id,$start_date,$end_date);

    public function getContactTracingByEmployeeContactPeople($employee_id,$start_date,$end_date);

    public function getAllTracingByCompany($company_id,$current_date,$searchKey='');

    public function getAllTracingByCompanyContactPeople($company_id,$current_date,$searchKey='');

    public function getAllTracingByEmployee($employee_id,$time);

    public function getAllTracingByEmployeeContact($company_id,$time);

    public function find($id);

    public function findByBeaconID($beaconId);

    public function checkTracingByDate($emp1,$emp2,$date);

    public function store($input);

    public function store_logs($input);

    public function update($id,$input);

    public function delete($id);

}
