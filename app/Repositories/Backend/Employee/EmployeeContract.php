<?php

namespace App\Repositories\Backend\Employee;

interface EmployeeContract
{

    public function getAll($search='');

    public function getAllByCompany($company_id,$search='');

    public function getAllActiveByCompany($company_id,$status);

    public function find($id);

    public function findWith($id);

    public function findByEmail($email);

    public function store($input);

    public function update($id,$input);

    public function delete($id);
    public function cancel($id);

}
