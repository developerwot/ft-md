<?php

namespace App\Repositories\Backend\Employee;

use App\EventParticipate;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeRepository implements EmployeeContract
{

    protected $model;

    public function __construct(Employee $model)
    {

        $this->model = $model;
    }

    public function getAll($search='')
    {
        if(!empty($search)) {
            $companyIds = Company::where('business_name', 'LIKE', "%$search%")->where('status',1)->pluck('id')->toArray();

            return $this->model->with('company')
                ->where(function($query) use ($search,$companyIds) {
                    $query->where('first_name', 'LIKE', "%$search%");
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('last_name', 'LIKE', "%$search%");
                    });
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('email', 'LIKE', "%$search%");
                    });
                    $query->orWhere(function ($query) use ($companyIds) {
                        $query->where('company_id', $companyIds);
                    });
                })
                ->orderBy('id', 'desc')->latest()->get();
        }
        else {
            return $this->model->with('company')->orderBy('id', 'desc')->latest()->get();
        }

    }
    public function getAllByCompany($company_id,$search='')
    {
        if(!empty($search)) {
            return $this->model->with('company')
                ->where('company_id',$company_id)
                ->where(function($query) use ($search) {
                    $query->where('first_name', 'LIKE', "%$search%");
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('last_name', 'LIKE', "%$search%");
                    });
                    $query->orWhere(function ($query) use ($search) {
                        $query->where('email', 'LIKE', "%$search%");
                    });
                })
                ->orderBy('id', 'desc')->latest()->get();
        }
        else {
            return $this->model->with('company')
                ->where('company_id',$company_id)
                ->orderBy('id', 'desc')->latest()->get();
        }

    }

    public function getAllActiveByCompany($company_id,$status)
    {
        return $this->model->where('company_id',$company_id)
                            ->where('status',$status)
                            ->orderBy('id', 'desc')->latest()->get();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findWith($id)
    {
        return $this->model->with('employee_temperature','people_registry','people_attendance','employee_temperature_log')->findOrFail($id);
    }
    public function findByEmail($email)
    {
        return $this->model->where('email',$email)->first();
    }

    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }
    public function cancel($id)
    {
        $user = $this->model->findOrFail($id);
        $user->forceDelete();
    }

}
