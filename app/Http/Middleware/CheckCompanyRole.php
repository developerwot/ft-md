<?php

namespace App\Http\Middleware;
use Auth;
use Redirect;
use Closure;

class CheckCompanyRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasRole('company')) {
            return Redirect::to('/dashboard')->with('failure', 'Permission denied to access module');
        }
        return $next($request);
    }
}
