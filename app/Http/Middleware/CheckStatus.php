<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::guard($guard)->check() && Auth::user()->status != 1){

            Auth::logout();

            $request->session()->flash('error', 'Your Account is not active,Contact technical support');

            return redirect('login')->with('error', 'Your Account is not active,Contact technical support');

        }

        return $next($request);
    }
}
