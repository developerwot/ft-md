<?php
namespace App\Http\Middleware;

use Auth;
use Closure;
use Redirect;

class CheckAdminRole
{
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasRole('admin')) {
            return Redirect::to('/dashboard')->with('failure', 'Permission denied to access module');
        }

        return $next($request);
    }
}
