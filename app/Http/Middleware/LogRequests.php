<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class LogRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $request->start = microtime(true);
        return $next($request);
    }
    public function terminate($request, $response)
    {
        $request->end = microtime(true);
//commented by dipesh
//        $this->log($request,$response);
    }

    protected function log($request,$response)
    {
        $apiArray = ['emp','emp_temp'];

        if(Str::contains($request->fullUrl() ,'emp'))
        {

        }
        else if(Str::contains($request->fullUrl() ,'emp_temp'))
        {

        }
        else if(Str::contains($request->fullUrl() ,'emp_get_data'))
        {

        }
        else if(Str::contains($request->fullUrl() ,'people_registry'))
        {

        }
        else if(Str::contains($request->fullUrl() ,'company'))
        {

        }
        else if(Str::contains($request->fullUrl() ,'guest_user'))
        {

        }
        else if(Str::contains($request->fullUrl() ,'guest_user_detail'))
        {

        }

        else {

            $duration = $request->end - $request->start;
            $url = $request->fullUrl();
            $method = $request->getMethod();
            $ip = $request->getClientIp();

            $log = "{$ip}: {$method}@{$url} - {$duration}ms \n".
                "Request : {".$request->getContent()."} \n".
                "Response : {$response->getContent()} \n";

            Log::channel('api')->info($log);
        }

       // Log::error('Log Middleware called');

    }
}
