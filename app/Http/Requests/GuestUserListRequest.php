<?php

namespace App\Http\Requests;

use App\Traits\FailedValidationJsonTrait;
use Illuminate\Foundation\Http\FormRequest;

class GuestUserListRequest extends FormRequest
{
    use FailedValidationJsonTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*$user = request()->user();
        $id = $user->id;*/
        return [
            'api_key' => 'required',
            'company_id' => 'required',

         ];

    }
    public function messages()
    {
        return [
           /* 'email' => 'Guest User id is required',*/

        ];
    }
}
