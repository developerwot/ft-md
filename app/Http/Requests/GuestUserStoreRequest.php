<?php

namespace App\Http\Requests;

use App\Traits\FailedValidationJsonTrait;
use Illuminate\Foundation\Http\FormRequest;

class GuestUserStoreRequest extends FormRequest
{
    use FailedValidationJsonTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*$user = request()->user();
        $id = $user->id;*/
        return [
            'is_first_name' => 'required|numeric|lte:1|min:0',
            'is_last_name' => 'required|numeric|lte:1|min:0',
            'is_dob' => 'required|numeric|lte:1|min:0',
            'is_address' => 'required|numeric|lte:1|min:0',
            'is_city' => 'required|numeric|lte:1|min:0',
            'is_postal_code' => 'required|numeric|lte:1|min:0',
            'is_telephone_number' => 'required|numeric|lte:1|min:0',
            'is_identity_proof' => 'required|numeric|lte:1|min:0',
            'is_address_proof' => 'required|numeric|lte:1|min:0',
            'is_company_name' => 'required|numeric|lte:1|min:0',
            'from_date' => 'required|date',
            'to_date' => 'required|date|after_or_equal:from_date',
            'email_address' => 'required|email',
            'receiver_employee_id' => 'required|numeric',

        ];
    }
    public function messages()
    {
        return [
            'is_first_name.required' => 'Is Firstname is required',
            'is_first_name.numeric' => 'Is Firstname must be number',
            'is_first_name.lte' => 'Is Firstname must be 0 or 1',
            'is_first_name.min' => 'Is Firstname must be 0 or 1',

            'is_last_name.required' => 'is_last_name is required',
            'is_last_name.numeric' => 'is_last_name must be number',
            'is_last_name.lte' => 'is_last_name must be 0 or 1',
            'is_last_name.min' => 'is_last_name must be 0 or 1',

            'is_dob.required' => 'is_dob is required',
            'is_dob.numeric' => 'is_dob must be number',
            'is_dob.lte' => 'is_dob must be 0 or 1',
            'v.min' => 'is_dob must be 0 or 1',

            'is_address.required' => 'is_address is required',
            'is_address.numeric' => 'is_address must be number',
            'is_address.lte' => 'is_address must be 0 or 1',
            'is_address.min' => 'is_address must be 0 or 1',

            'is_city.required' => 'is_city is required',
            'is_city.numeric' => 'is_city must be number',
            'is_city.lte' => 'is_city must be 0 or 1',
            'is_city.min' => 'is_city must be 0 or 1',

            'is_postal_code.required' => 'is_postal_code is required',
            'is_postal_code.numeric' => 'is_postal_code must be number',
            'is_postal_code.lte' => 'is_postal_code must be 0 or 1',
            'is_postal_code.min' => 'is_postal_code must be 0 or 1',

            'is_telephone_number.required' => 'is_telephone_number is required',
            'is_telephone_number.numeric' => 'is_telephone_number must be number',
            'is_telephone_number.lte' => 'is_telephone_number must be 0 or 1',
            'is_telephone_number.min' => 'is_telephone_number must be 0 or 1',

            'is_identity_proof.required' => 'is_identity_proof is required',
            'is_identity_proof.numeric' => 'is_identity_proof must be number',
            'is_identity_proof.lte' => 'is_identity_proof must be 0 or 1',
            'is_identity_proof.min' => 'is_identity_proof must be 0 or 1',

            'is_address_proof.required' => 'is_address_proof is required',
            'is_address_proof.numeric' => 'is_address_proof must be number',
            'is_address_proof.lte' => 'is_address_proof must be 0 or 1',
            'is_address_proof.min' => 'is_address_proof must be 0 or 1',

            'is_company_name.required' => 'is_company_name is required',
            'is_company_name.numeric' => 'is_company_name must be number',
            'is_company_name.lte' => 'is_company_name must be 0 or 1',
            'is_company_name.min' => 'is_company_name must be 0 or 1',

            'policy.required' => 'Building Policy Content is required',

            'from_date.required' => 'from_date is required',
            'from_date.date' => 'from_date must be date',

            'to_date.required' => 'to_date is required',
            'to_date.date' => 'to_date must be date',

            'email_address.required' => 'email_address is required',
            'email_address.email' => 'email_address must be valid email',
        ];
    }
}
