<?php

namespace App\Http\Requests;

use App\Traits\FailedValidationJsonTrait;
use Illuminate\Foundation\Http\FormRequest;

class GuestUserVerfyRequest extends FormRequest
{
    use FailedValidationJsonTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*$user = request()->user();
        $id = $user->id;*/
        return [
            /*'email' => 'required|email',*/
            'verify_code' => 'required',
         ];
    }
    public function messages()
    {
        return [
           /* 'email.required' => 'Email is required',*/
            'verify_code.required' => 'Verify code is required',
            /*'email.email' => 'Email must be valid email',*/
        ];
    }
}
