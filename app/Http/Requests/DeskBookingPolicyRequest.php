<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeskBookingPolicyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_id'=>'required',
            'floor_id'=>'required|array|min:1',
            'floor_id.*'=>'required',
        ];
    }
    public function messages()
    {
        return [
          'building_id.required'=>'Building name is Required',
          'floor_id.required'=>'floor name is Required',
            'floor_id.*.required'=>'floor name is Required'
        ];
    }
}
