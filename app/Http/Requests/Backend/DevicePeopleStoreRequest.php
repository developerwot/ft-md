<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class DevicePeopleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'device_id' => 'required',
            'employee_id' => 'required'

        ];
    }
    public function messages()
    {

        return [
            'device_id.required' => 'Device is required',
            'employee_id.unique' => 'Employee is required',
        ];
    }
}
