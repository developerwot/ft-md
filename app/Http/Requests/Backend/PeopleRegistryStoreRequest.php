<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class PeopleRegistryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
         /*   'people_file' => 'required|mimes:jpeg,png,jpg,gif,svg,mp4,mov,mkv',*/
            'employee_id' => 'required',
        ];
    }
    public function messages()
    {

        return [
            'people_file.required' => 'Video or image is required',
            'people_file.mimes' => 'Video or image is invalid',
            'employee_id.required' => 'People is required',
        ];
    }
}
