<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        //$id = $this->route('user');

        return [
            'name' => 'required',
            'last_name' => 'required',
            /*'phone' => 'required',*/
            'address' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'business_name' => 'required',
            'country' => 'required',
            'vat' => 'required',
            'status' => 'required',
            'employee_limit' => 'required',
            'customer_id' => 'required_with:people_link`,on',
        ];
    }
}
