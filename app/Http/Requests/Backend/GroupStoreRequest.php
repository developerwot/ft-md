<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class GroupStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required',
            'employee_id' => 'required|min:1',
        ];
    }
    public function messages()
    {

        return [
            'name.required' => 'Group Name is required',
            'employee_id.required' => 'People is required',
            'employee_id.min' => 'Select atlest 1 People for group',
        ];
    }
}
