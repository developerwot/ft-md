<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LocationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_access_id'=>'required',
            'location_name'=>Rule::unique('location_accesses')->where(function ($query) {return $query->where('id',$this->id)->where('building_access_id', $this->building_access_id)->where('deleted_at','=',null);}),
            'location_limit'=>'required',
            'location_status'=>'required'
        ];
    }
}
