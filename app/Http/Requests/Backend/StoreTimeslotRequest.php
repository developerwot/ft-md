<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class StoreTimeslotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_access_id'=>'required',
            'location_access_id'=>'required',
            'start_time'=>'required',
            'end_time'=>'required',
            'status'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'building_access_id.required'=>'Building name is necessary',
            'location_access_id.required'=>'Location can not be empty',
        ];
    }
}
