<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class BuildingPolicyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_id' => 'required',
            'group_id' => 'required',

        ];
    }
    
    public function messages()
    {

        return [
            'building_id.required' => 'Building is required',
            'group_id.required' => 'Group is required',
            'policy.required' => 'Building Policy Content is required',
        ];
    }
}
