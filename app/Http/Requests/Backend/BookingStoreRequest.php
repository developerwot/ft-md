<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class BookingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_id' => 'required',
            'employee_id' => 'required',
            'booking_date' => 'required|date|after:yesterday',
            'booking_time' => 'required',

        ];
    }
    public function messages()
    {

        return [
            'building_id.required' => 'Building is required',
            'employee_id.required' => 'People is required',
            'booking_date.required' => 'Booking Date is required',
            'booking_date.date' => 'Booking Date is not Valid',
            'booking_date.after' => 'Booking Date must be future date',
            'booking_time.required' => 'Booking Time is required',
        ];
    }
}
