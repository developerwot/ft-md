<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeskStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_id'=>'required',
            'floor_id'=>'required',
            'desk_name'=>Rule::unique('desk_accesses')->where(function ($query) {return $query->where('floor_id', $this->floor_id)->where('deleted_at','=',null);}),
            'desk_status'=>'required',
        ];
    }
}
