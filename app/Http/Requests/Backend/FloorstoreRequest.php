<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class FloorstoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_access_id'=>'required',
            'floor_name'=>'required | unique:location_accesses,location_name,'.$this->building_access_id,
            'floor_limit'=>'required',
            'floor_status'=>'required'
        ];
    }
}
