<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class BuildingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'building_limit' => 'required|numeric|min:0|not_in:0',

        ];
    }
    public function messages()
    {

        return [
            'name.required' => 'Building Name is required',
            'building_limit.required' => 'Building Limit is required',
            'building_limit.not_in' => 'Building Limit must be greater than zero',
        ];
    }
}
