<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class DeviceSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'building_id'=>'required',
            'location_id'=>'required',
            'device_id'=>'required_if:'.auth()->user()->company->people_link_status.',1'


        ];
    }
    public function messages()
    {

        return [
            'building_id.required'=>'Please select building first',
            'location_id.required'=>'Please select location',
        ];
    }
}
