<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class PeopleAttendanceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'check_in' => 'required|date_format:"Y/m/d H:i:s"',
            'check_out' => 'nullable|date_format:"Y/m/d H:i:s',
        ];
    }
    public function messages()
    {

        return [
            'check_in.required' => 'Check In Date Required',
            'check_out.required' => 'Check Out Date Required',
            'check_in.date_format' => 'Check In Date is Invalid',
            'check_out.date_format' => 'Check Out Date is Invalid',

        ];
    }
}
