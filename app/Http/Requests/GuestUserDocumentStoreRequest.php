<?php

namespace App\Http\Requests;

use App\Traits\FailedValidationJsonTrait;
use Illuminate\Foundation\Http\FormRequest;

class GuestUserDocumentStoreRequest extends FormRequest
{
    use FailedValidationJsonTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*$user = request()->user();
        $id = $user->id;*/
        return [
            'api_key' => 'required',
            'guest_user_id' => 'required|numeric|min:1',
            'address_proof' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'identity_proof' => 'nullable|mimes:jpeg,png,jpg,gif,svg',

         ];

    }
    public function messages()
    {
        return [
            'guest_user_id' => 'Guest User id is required',

        ];
    }
}
