<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PeopleAttendanceStoreRequest;
use App\Http\Requests\Backend\PeopleAttendanceUpdateRequest;
use App\Http\Requests\Backend\PeopleRegistryStoreRequest;
use App\Models\Company;
use App\Models\Employee;
use App\PeopleAttendanceLog;
use App\Repositories\Backend\PeopleAttendance\PeopleAttendanceContract;
use App\Repositories\Backend\PeopleRegistry\FacialContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use React\EventLoop\Factory;

class PeopleApiAttendanceController extends Controller
{

    protected $repository;

    public function __construct(PeopleAttendanceContract $repository)
    {
        $this->repository = $repository;
    }

    public function store(Request $request)
    {

        $input = json_decode($request->getContent(), true);

        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {


                if (isset($data['employee_id']) && $data['employee_id'] != '') {

                    $isemployee = Employee::with('company')->where('id', $data['employee_id'])->where('status', 1)->first();

                    if (empty($isemployee)) {
                        return response()->json([
                            "status" => 0,
                            "message" => "People Not Found"
                        ], 404);
                    }
                    $log = PeopleAttendanceLog::create([
                        'employee_id' => $data['employee_id'],
                        'check_in' => isset($data['check_in']) && !empty($data['check_in']) ? $data['check_in'] : null,
                        'check_out' => isset($data['check_out']) && !empty($data['check_out']) ? $data['check_out'] : null,
                        'date' => !empty($data['check_in']) ? date('Y-m-d',strtotime( $data['check_in'])) : (!empty($data['check_out']) ? date('Y-m-d',strtotime($data['check_out'])) : null)
                    ]);

                    if ($isemployee->company['people_link_status'] == 1) {
                        $customerID = $isemployee->company['customer_id'];
                        $uuid = Str::uuid()->toString();
                        $url = 'wss://ws.tuono.org/' . $customerID . '_' . $uuid;
//                        $url = 'wss://ws.tuono.org/PL-FRAMIS_43b6cba6-0d3a-11eb-adc1-0242ac120002';
                        $target = $data['device_id'];
                        $payload = 'DNA' . str_pad($data['employee_id'], 7, '0', STR_PAD_LEFT);
                        $loop = Factory::create();
                        \Ratchet\Client\connect($url)->then(function ($conn) use ($target, $payload, $customerID, $log) {
                            $conn->on('message', function ($msg) use ($conn, $log) {
                                $log->update(['socket_response' => $msg]);
                                $conn->close();
                            });
                            $arr = [
                                "target" => $target,
                                "payload" => $payload,
                                "impianto" => $customerID
                            ];
                            $base = base64_encode(json_encode($arr));
                            $conn->send($base);
                        }, function ($e) use ($log) {
                            $log->update(['socket_response' => 404]);
                        });
                        $loop->run();
                    }


                    if (isset($data['check_in']) && $data['check_in'] != '') {

                        //Validate date using validator format
                        if (!$this->verifyDate($data['check_in'])) {
                            return response()->json([
                                "status" => 0,
                                "message" => "Check in Date is invalid"
                            ], 404);
                        }
                        //Validate Check in Date in Database
                        $data['date'] = date('Y-m-d', strtotime($data['check_in']));
                        $data['check_in'] = date("Y-m-d H:i:s", strtotime($data['check_in']));
                        if ($data['date'] != Carbon::today()->format('Y-m-d')) {
                            return response()->json([
                                "status" => 0,
                                "message" => "Check in Date is invalid"
                            ], 404);
                        }
                    } else {
                        unset($data['check_in']);
                    }

                    if (isset($data['check_out']) && !empty($data['check_out'])) {

                        //Validate date using validator format
                        if (!$this->verifyDate($data['check_out'])) {
                            return response()->json([
                                "status" => 0,
                                "message" => "Check Out Date is invalid"
                            ], 404);
                        }
                        $data['check_out'] = date("Y-m-d H:i:s", strtotime($data['check_out']));

                        $data['date'] = date('Y-m-d', strtotime($data['check_out']));
                        $check_out_date = date("Y-m-d", strtotime($data['check_out']));
                        if ($check_out_date != Carbon::today()->format('Y-m-d')) {
                            return response()->json([
                                "status" => 0,
                                "message" => "Check out Date is invalid"
                            ], 404);
                        }
//                        $data['date']=date('Y-m-d',strtotime($data['date']));
//                        if (!empty($data['date'])) {
//                            if ($data['date'] != $check_out_date) {
//                                return response()->json([
//                                            "status" => 0,
//                                            "message" => "Check Out Date is Invalid , must be same as check in date"
//                                                ], 404);
//                            } else if(strtotime($data['check_out']) < strtotime($data['check_in'])) {
//
//                                return response()->json([
//                                            "status" => 0,
//                                            "message" => "Check Out Time is Invalid , must be greater than check in date"
//                                                ], 404);
//                            }
//
//                        }
//                        else {

                        //Always go in this
                        $checkExisting = $this->repository->findByDate($data['employee_id'], $check_out_date);
                        if (empty($checkExisting)) {
                            return response()->json([
                                "status" => 0,
                                "message" => "No Check in entry found for date"
                            ], 404);
                        } else if (strtotime($data['check_out']) < strtotime($checkExisting->check_in)) {
                            return response()->json([
                                "status" => 0,
                                "message" => "Check Out Time is Invalid , must be greater than check in time"
                            ], 404);
                        }
//                        }


                    } else {
                        unset($data['check_out']);
                    }


                    /*  $data['date'] = $data['date']; */
                    $data['employee_id'] = $data['employee_id'];

                    $checkExisting = $this->repository->findByDate($data['employee_id'], $data['date']);
                    if (!empty($checkExisting)) {
                        /* return response()->json([
                          "status"=>0,
                          "message" => "Check in Date Entry already available for this People"
                          ], 404); */

                        //Update existing data
                        /* if(empty($data['check_in'])) { */
//                        if (isset($data['check_in']) && $data['check_in'] != '') {
//                            return response()->json([
//                                        "status" => 0,
//                                        "message" => "User Checke in already"
//                                            ], 404);
//                        }
                        unset($data['check_in']);
                        $update = $this->repository->update($checkExisting->id, $data);
                        return response()->json([
                            "status" => 1,
                            "message" => " People Attendance updated Successfully "
                        ], 200);
                        /* }
                          else {
                          return response()->json([
                          "status"=>0,
                          "message" => "Check in Date Entry already available for this People"
                          ], 404);
                          } */
                    } else {
                        //Create New Attendance
                        $attendance = $this->repository->store($data);
                        if ($attendance) {
                            return response()->json([
                                "status" => 1,
                                "message" => " People Attendance added Successfully "
                            ], 200);
                        }
                    }


                    /*  else {
                      return response()->json([
                      "status"=>0,
                      "message" => "Check In Date Time is required"
                      ], 404);
                      } */
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Employee ID is required"
                    ], 404);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found!!"
                ], 404);
            }
        }
    }

    public function get_attendance(Request $request)
    {

        try {

            $inputs = $request->only('query', 'search');

            if (!empty($inputs['query'])) {
                if (isset($inputs['query']['date_range']) && !empty($inputs['query']['date_range']) && $inputs['search'] == 1) {

                    $dates = explode('-', $inputs['query']['date_range']);
                    if (!empty($dates)) {
                        $start_date = date("Y-m-d", strtotime(trim($dates[0])));
                        $end_date = date('Y-m-d', strtotime(trim($dates[1])));
                    }
                } elseif (isset($inputs['query']['month_year']) && !empty($inputs['query']['month_year']) && $inputs['search'] == 2) {
                    $m_year = explode('-', $inputs['query']['month_year']);
                    if (!empty($m_year)) {
                        $month = trim($m_year[0]);
                        $year = trim($m_year[1]);
                    }
                }


                if (isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }
            }

            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
            if (!empty($start_date) && !empty($end_date)) {

                if (!empty($searchKey)) {
                    $people_attendance_data = $this->repository->getAllAttendanceByDateRange($getUserCompany->id, $start_date, $end_date, $searchKey);
                } else {
                    $people_attendance_data = $this->repository->getAllAttendanceByDateRange($getUserCompany->id, $start_date, $end_date);
                }
            } else if (!empty($month) && !empty($year)) {
                if (!empty($searchKey)) {
                    $people_attendance_data = $this->repository->getAllAttendanceByMonth($getUserCompany->id, $month, $year, $searchKey);
                } else {
                    $people_attendance_data = $this->repository->getAllAttendanceByMonth($getUserCompany->id, $month, $year);
                }
            } else {
                $current_date = date("Y-m-d");
                if (!empty($searchKey)) {
                    $people_attendance_data = $this->repository->getAllAttendanceByDate($getUserCompany->id, $current_date, $searchKey);
                } else {
                    $people_attendance_data = $this->repository->getAllAttendanceByDate($getUserCompany->id, $current_date);
                }
            }

            $people_attendance = $people_attendance_data->filter(function ($item) {
                $item->check_in = date("M d, Y h:i A", strtotime($item->check_in));
                if (!empty($item->check_out)) {
                    $item->check_out = date("M d, Y h:i A", strtotime($item->check_out));
                }

                return $item;
            });

            return response()->json(['status' => 'success', 'data' => $people_attendance, 'code' => 200], 200);
        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage());
            return '';
        }
    }

    public function dstore(PeopleAttendanceStoreRequest $request)
    {

        try {
            $input = $request->only('employee_id', 'check_in', 'check_out');
            $check_in_date = date("Y-m-d", strtotime($input['check_in']));
            $input['date'] = date("Y-m-d", strtotime($input['check_in']));

            $checkExisting = $this->repository->findByDate($input['employee_id'], $input['date']);
            if (!empty($checkExisting)) {
                return redirect()->back()->withInput()->withErrors(['Check in Date Entry already available for this People']);
            }
            if (!empty($input['check_out'])) {
                $check_out_date = date("Y-m-d", strtotime($input['check_out']));
                if ($check_in_date != $check_out_date) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Date is Invalid , must be same as check in date']);;
                } else if (strtotime($input['check_out']) < strtotime($input['check_in'])) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Time is Invalid , must be greater than as check in date']);;
                }
                $input['check_out'] = date("Y-m-d H:i:s", strtotime($input['check_out']));
            }


            $input['check_in'] = date("Y-m-d H:i:s", strtotime($input['check_in']));

            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
            if (!empty($getUserCompany)) {
                $group = $this->repository->store($input);
                return redirect()->route('people_attendance.index')->with('success', 'New People Attendance added successfully');
            }
        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('people_attendance.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $people_attendance = $this->repository->find($id);
            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
            if (!empty($getUserCompany)) {
                $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
                return view('backend.people_attendance.form', compact('employees', 'people_attendance'));
            } else {
                return redirect()->route('dashboard')->with('failure', 'Company Not Found');
            }
        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage());
            return redirect()->route('people_attendance.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(PeopleAttendanceUpdateRequest $request, $id)
    {

        try {
            $input = $request->only('check_in', 'check_out');
            $check_in_date = date("Y-m-d", strtotime($input['check_in']));
            $people_attendance = $this->repository->find($id);
            if ($people_attendance->date != $check_in_date) {
                return redirect()->back()->withInput()->withErrors(["Invalid Check in date"]);
            }

            if (!empty($input['check_out'])) {
                $check_out_date = date("Y-m-d", strtotime($input['check_out']));
                if ($check_in_date != $check_out_date) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Date is Invalid , must be same as check in date']);;
                } else if (strtotime($input['check_out']) < strtotime($input['check_in'])) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Time is Invalid , must be greater than as check in date']);;
                }
                $input['check_out'] = date("Y-m-d H:i:s", strtotime($input['check_out']));
            }

            $input['date'] = date("Y-m-d", strtotime($input['check_in']));
            $input['check_in'] = date("Y-m-d H:i:s", strtotime($input['check_in']));

            $this->repository->update($id, $input);

            return redirect()->route('people_attendance.index')->with('success', 'People Attendance updated successfully');
        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('people_attendance.index')->with('Something went wrong');
        }
    }

    /* public function destroy($id)
      {
      try {
      $people_attendance = $this->repository->find($id);
      if($people_attendance) {

      $this->repository->delete($id);
      }
      return redirect()->route('people_attendance.index')->with('success', 'People Attendance deleted successfully');

      } catch (\Exception $e) {
      Log::error('People Attendance management error:' . $e->getMessage());
      return redirect()->route('people_attendance.index')->with('failure', 'Something went wrong');
      }
      } */
}
