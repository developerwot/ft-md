<?php

namespace App\Http\Controllers;

use App\BookingPolicy;
use App\BuildingAccess;
use App\DeskAccess;
use App\DeskBookingPolicy;
use App\FloorAccess;
use App\LocationAccess;
use App\Models\Company;
use App\ReservationAccess;
use App\TimeslotAccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingApiController extends Controller
{
    public function getBuildings(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
                    if (!empty($checkCompany)) {
                        $buildings = BuildingAccess::where([['company_id', '=', $data['company_id']], ['building_status', '=', 1]])->get(['id', 'building_name', 'building_limit']);
                        return response()->json(['success' => true, 'status' => 1, 'message' => 'buildings fetched', 'buildings' => $buildings]);
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function getLocations(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
                    if (!empty($checkCompany)) {
                        if (isset($data['building_id']) && !empty($data['building_id'])) {
                            $checkCompany = BuildingAccess::where([['id', '=', $data['building_id']], ['building_status', '=', 1]])->first();
                            if (!empty($checkCompany)) {
                                $locations = LocationAccess::where([['building_access_id', '=', $data['building_id']], ['location_status', '=', 1]])->get(['id', 'location_name', 'location_limit']);
                                return response()->json(['success' => true, 'status' => 1, 'message' => 'Locations fetched', 'locations' => $locations]);
                            } else {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "Building Not Found"
                                ], 422);
                            }
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Building Id is required"
                            ], 422);
                        }
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function getTimeSlots(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
                    if (!empty($checkCompany)) {
                        if (isset($data['location_id']) && !empty($data['location_id'])) {
                            $checkCompany = LocationAccess::where([['id', '=', $data['location_id']], ['location_status', '=', 1]])->first();
                            if (!empty($checkCompany)) {
//                                dd(Carbon::parse($data['date']));
                                $exist = BookingPolicy::where([['location_id', $data['location_id']], ['company_id', $data['company_id']], ['from_date', '<=', $data['date']], ['to_date', '>=', $data['date']]])->get();
                                if ($exist->count() > 0) {
                                    if ($exist[0]->is_permitted == 0) {
                                        return response()->json([
                                            "status" => 0,
                                            "message" => "Booking is not permitted for this date"
                                        ], 422);
                                    } else {
                                        if ($data['date'] == Carbon::today()) {
                                            $timeslots = TimeslotAccess::whereIn('id', $exist[0]->slot_ids)->whereTime('end_time', '>=', Carbon::now())->get(['id', 'start_time', 'end_time']);
                                        } else {
                                            $timeslots = TimeslotAccess::whereIn('id', $exist[0]->slot_ids)->get(['id', 'start_time', 'end_time']);
                                        }

                                    }
                                } else {
                                    if (Carbon::parse($data['date']) == Carbon::today()) {
                                        $timeslots = TimeslotAccess::whereTime('end_time', '>=', Carbon::now())->where([['company_id', '=', $data['company_id']], ['location_access_id', '=', $data['location_id']], ['status', '=', 1], ['is_policy_added', '=', null]])->get(['id', 'start_time', 'end_time']);
                                    } else if ($data['date'] > Carbon::today()) {

                                        $timeslots = TimeslotAccess::where([['company_id', '=', $data['company_id']], ['location_access_id', '=', $data['location_id']], ['status', '=', 1], ['is_policy_added', '=', null]])->get(['id', 'start_time', 'end_time']);
                                    } else {
                                        return response()->json([
                                            "status" => 0,
                                            "message" => "Invalid date"
                                        ], 422);
                                    }
                                }


                                $limit = $checkCompany->location_limit;
                                foreach ($timeslots as $key => $val) {
                                    $count = ReservationAccess::whereDate('booked_at', $data['date'])->where('slot_id', $val->id)->where('type','location')->count();
                                    $checkIfBooked = ReservationAccess::whereDate('booked_at', $data['date'])->where([['slot_id', '=', $val->id], ['employee_id', '=', $input['employee_id']],['type','=','location']])->first();
                                    $timeslots[$key]['total_limit'] = $limit;
                                    $timeslots[$key]['book_count'] = $count;
                                    if ($checkIfBooked) {
                                        $timeslots[$key]['booking_id'] = $checkIfBooked->id;
                                    } else {
                                        $timeslots[$key]['booking_id'] = null;
                                    }

                                }
                                if ($timeslots->count() == 0) {
                                    return response()->json([
                                        "status" => 0,
                                        "message" => "No time slots available"
                                    ], 422);
                                }
                                return response()->json(['success' => true, 'status' => 1, 'message' => 'Time slots fetched', 'time-slots' => $timeslots]);
                            } else {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "Location Not Found"
                                ], 422);
                            }
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Location Id is required"
                            ], 422);
                        }

                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not found! "
                        ], 422);
                    }

                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function bookTimeSlot(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                $time = TimeslotAccess::with('location')->where('id', $input['slot_id'])->first();
                $limit = $time->location['location_limit'];
                $start = Carbon::parse($time['start_time'])->addMinutes(1);
                $end = $time['end_time'];
                $reserve = DB::table('timeslot_accesses')
                    ->where('reservation_accesses.deleted_at', '=', null)
                    ->join('reservation_accesses', 'reservation_accesses.slot_id', '=', 'timeslot_accesses.id')
                    ->where('employee_id', $input['employee_id'])
                    ->where(function ($q) use ($start, $end) {
                       $q->where('timeslot_accesses.start_time','=',$start)->where('timeslot_accesses.end_time','=',$end);
                       $q->orWhere(function ($q) use ($start, $end) {
                           $q->whereBetween('timeslot_accesses.start_time', [$start, $end])
                               ->orWhereBetween('timeslot_accesses.end_time', [$start, $end]);
                       });
                        })

                    ->select('reservation_accesses.id', 'timeslot_accesses.start_time','reservation_accesses.employee_id', 'timeslot_accesses.end_time', 'reservation_accesses.booked_at')->get();
               $array= collect($reserve)
                    ->where('booked_at', '=', $data['date'])->toArray();
                if (collect($array)->count()>0) {
                    return response()->json([
                        "status" => 0,
                        "message" => "You have already booked same time slot at another location.!",
                        "data"=>$array,
                    ], 422);
                } else {
                    $total = ReservationAccess::where('type', 'location')->whereDate('booked_at', $data['date'])->where('slot_id', '=', $input['slot_id'])->count();
                    if ($total == $limit) {
                        return response()->json([
                            "status" => 0,
                            "message" => "Location limit reached"
                        ], 422);
                    } else {
                        ReservationAccess::create([
                            'slot_id' => $input['slot_id'],
                            'type' => 'location',
                            'employee_id' => $input['employee_id'],
                            'booked_at' => $data['date']
                        ]);
                        return response()->json([
                            'success' => true,
                            'status' => 1,
                            'message' => 'Slot booked successfully!',
                        ], 200);
                    }
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function bookings(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                $reserve = ReservationAccess::whereDate('booked_at', '>=', Carbon::today())->where('employee_id', $input['employee_id'])->get(['id','booked_at','type','slot_id']);
                if (collect($reserve)->count() == 0) {
                    return response()->json([
                        "status" => 0,
                        "message" => "No bookings found!"
                    ], 422);
                }
                $response=[];
                foreach ($reserve as $key=>$val){
                    if($val->type=='desk'){
                      $floorName=  DeskAccess::with('floor')->where('id',$val->slot_id)->first();
                        $response[$key]['floor_name']=$floorName->floor['floor_name'];
                        $response[$key]['desk_name']=$floorName->desk_name;
                         $response[$key]['id']=$val->id;
                        $response[$key]['booked_at']=$val->booked_at;
                        $response[$key]['type']=$val->type;
                    }else{
                      $location=  TimeslotAccess::with('location','building')->where('id',$val->slot_id)->first();
                        $response[$key]['building_name']=$location->building['building_name'];
                        $response[$key]['location_name']=$location->location['location_name'];
                        $response[$key]['id']=$val->id;
                        $response[$key]['start_time']=$location->start_time;
                        $response[$key]['end_time']=$location->end_time;
                        $response[$key]['booked_at']=$val->booked_at;
                        $response[$key]['type']=$val->type;
                    }
                }
                return response()->json(['success' => true, 'status' => 1, 'message' => 'users bookings fetched', 'booking_data' => $response], 200);
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function cancel(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['booking_id']) && !empty($data['booking_id'])) {
                    $cancel = ReservationAccess::where('id', $input['booking_id'])->delete();
                    if ($cancel) {
                        return response()->json([
                            'success' => true,
                            'status' => 1,
                            'message' => 'booking cancelled!',
                        ], 200);
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Booking not found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Booking id not found"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function getFloors(Request $request)
    {

        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
                    if (!empty($checkCompany)) {
                        if (isset($data['building_id']) && !empty($data['building_id'])) {
                            $checkBuilding = BuildingAccess::where([['id', '=', $data['building_id']], ['building_status', '=', 1]])->first();
                            if (!empty($checkBuilding)) {
                                $locations = FloorAccess::where([['building_access_id', '=', $data['building_id']], ['floor_status', '=', 1]])->get(['id', 'floor_name', 'floor_limit']);
                                return response()->json(['success' => true, 'status' => 1, 'message' => 'Floors fetched', 'floors' => $locations]);
                            } else {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "Building Not Found"
                                ], 422);
                            }
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Building Id is required"
                            ], 422);
                        }
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function getDesks(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
                    if (!empty($checkCompany)) {
                        if (isset($data['floor_id']) && !empty($data['floor_id'])) {
                            $checkFloor = FloorAccess::where([['id', '=', $data['floor_id']], ['floor_status', '=', 1]])->first();
                            if (!empty($checkFloor)) {
                              $exist=  DeskBookingPolicy::where([['company_id','=',$data['company_id']],['floor_id','=',$data['floor_id']],['from_date', '<=', $data['date']], ['to_date', '>=', $data['date']]])->get();
                                if ($exist->count() > 0) {
                                    if ($exist[0]->is_permitted == 0) {
                                        return response()->json([
                                            "status" => 0,
                                            "message" => "Booking is not permitted for this date"
                                        ], 422);
                                    } else {
                                        $desks = DeskAccess::whereIn('id', $exist[0]->desk_ids)->get(['id', 'desk_name']);
                                        foreach ($desks as $key => $desk) {
                                            $reserved = ReservationAccess::where('employee_id', $data['employee_id'])->where('slot_id', $desk->id)->where('booked_at', $data['date'])->where('type', 'desk')->first('id');
                                            if ($reserved) {
                                                $desks[$key]['booking_id'] = $reserved->id;
                                            } else {
                                                $desks[$key]['booking_id'] = null;
                                            }
                                        }
                                    }

                                    }
                               else{
                                   $desks = DeskAccess::where([['floor_id', '=', $data['floor_id']], ['desk_status', '=', 1],['is_policy_added','=',0]])->get(['id', 'desk_name']);
                                   foreach ($desks as $key=>$desk){
                                       $reserved= ReservationAccess::where('employee_id',$data['employee_id'])->where('slot_id',$desk->id)->where('booked_at',$data['date'])->where('type','desk')->first('id');
                                       if ($reserved) {
                                           $desks[$key]['booking_id'] = $reserved->id;
                                       } else {
                                           $desks[$key]['booking_id'] = null;
                                       }
                                   }
                               }

                                return response()->json(['success' => true, 'status' => 1, 'message' => 'Desks fetched', 'Desks' => $desks]);
                            } else {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "Floor Not Found"
                                ], 422);
                            }
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Floor Id is required"
                            ], 422);
                        }
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function bookDesk(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                $IsDeskExist = DeskAccess::where('id', $data['desk_id'])->where('desk_status', 1)->first();
                if ($IsDeskExist) {
                    $reservedDesk = ReservationAccess::where([['employee_id', $data['employee_id']], ['type', 'desk']])->whereDate('Booked_at', $data['date'])->first();
                    if ($reservedDesk) {
                        return response()->json([
                            "status" => 0,
                            "message" => "You have already booked a desk same day.!"
                        ], 422);
                    }
                    $reservedByanother = ReservationAccess::where([['type', 'desk'], ['slot_id', $data['desk_id']]])->whereDate('Booked_at', $data['date'])->first();
                    if ($reservedByanother) {
                        return response()->json([
                            'success' => true,
                            'status' => 1,
                            'message' => 'Desk is already Booked!',
                        ], 200);
                    }
                    ReservationAccess::create([
                        'slot_id' => $input['desk_id'],
                        'employee_id' => $input['employee_id'],
                        'booked_at' => $data['date'],
                        'type' => 'desk'
                    ]);
                    return response()->json([
                        'success' => true,
                        'status' => 1,
                        'message' => 'Desk booked successfully!',
                    ], 200);
                } else {
                    return response()->json(["status" => 0,
                        "message" => "Desk not found"], 422);
                }
            } else {
                return response()->json(["status" => 0,
                    "message" => "Api key not found"], 422);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Not Valid data"
        ], 422);
    }

    public function scanQrCode(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (strpos($data['qr_string'], '_L-o-c-a-t-i-o-n_scan_qr_') !== false) {
                    $locations = LocationAccess::where([['location_QR', $data['qr_string']], ['location_status', '=', 1]])->first();
                    if ($locations) {
                        $fetched = [
                            'location_id' => $locations->id,
                            'location_name'=>$locations->location_name,
                            'type' => 'location'
                        ];
                        return response()->json(['success' => true, 'status' => 1, 'message' => 'data fetched', 'data' => $fetched]);

//                        $checkCompany = $locations;
//                        if (!empty($checkCompany)) {
////                                dd(Carbon::parse($data['date']));
//                            $exist = BookingPolicy::where([['location_id', $locations->id], ['company_id', $data['company_id']], ['from_date', '<=', $data['date']], ['to_date', '>=', $data['date']]])->get();
//                            if ($exist->count() > 0) {
//                                if ($exist[0]->is_permitted == 0) {
//                                    return response()->json([
//                                        "status" => 0,
//                                        "message" => "Booking is not permitted for this date"
//                                    ], 422);
//                                } else {
//                                    if ($data['date'] == Carbon::today()) {
//                                        $timeslots = TimeslotAccess::whereIn('id', $exist[0]->slot_ids)->where('end_time', '>=', date('H:i:s'))->get(['id', 'start_time', 'end_time']);
//                                    } else {
//                                        $timeslots = TimeslotAccess::whereIn('id', $exist[0]->slot_ids)->get(['id', 'start_time', 'end_time']);
//                                    }
//
//                                }
//                            } else {
//                                if (Carbon::parse($data['date']) == Carbon::today()) {
//                                    $timeslots = TimeslotAccess::where([['company_id', '=', $data['company_id']], ['end_time', '>=', date('H:i:s')], ['location_access_id', '=', $locations->id], ['status', '=', 1], ['is_policy_added', '=', null]])->get(['id', 'start_time', 'end_time']);
//                                } else if ($data['date'] > Carbon::today()) {
//
//                                    $timeslots = TimeslotAccess::where([['company_id', '=', $data['company_id']], ['location_access_id', '=', $locations->id], ['status', '=', 1], ['is_policy_added', '=', null]])->get(['id', 'start_time', 'end_time']);
//                                } else {
//                                    return response()->json([
//                                        "status" => 0,
//                                        "message" => "Invalid date"
//                                    ], 422);
//                                }
//                            }
//
//
//                            $limit = $checkCompany->location_limit;
//                            foreach ($timeslots as $key => $val) {
//                                $count = ReservationAccess::whereDate('booked_at', $data['date'])->where('slot_id', $val->id)->count();
//                                $checkIfBooked = ReservationAccess::whereDate('booked_at', $data['date'])->where([['slot_id', '=', $val->id], ['employee_id', '=', $input['employee_id']]])->first();
//                                $timeslots[$key]['total_limit'] = $limit;
//                                $timeslots[$key]['book_count'] = $count;
//                                if ($checkIfBooked) {
//                                    $timeslots[$key]['booking_id'] = $checkIfBooked->id;
//                                } else {
//                                    $timeslots[$key]['booking_id'] = null;
//                                }
//
//                            }
//                            if ($timeslots->count() == 0) {
//                                return response()->json([
//                                    "status" => 0,
//                                    "message" => "No time slots available"
//                                ], 422);
//                            }
//                            return response()->json(['success' => true, 'status' => 1, 'message' => 'Time slots fetched', 'time-slots' => $timeslots]);
//                        }
//                        else {
//                            return response()->json([
//                                "status" => 0,
//                                "message" => "Location Not Found"
//                            ], 422);
//                        }

                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Location not found"
                        ], 422);
                    }
                } elseif (strpos($data['qr_string'], '_D-e-s-k_scan_qr_') !== false) {
                    $desks = DeskAccess::with('floor','building')->where([['desk_QR', $data['qr_string']], ['desk_status', '=', 1]])->first();
                    if ($desks) {
                        $fetched = [
                            'building_name'=>$desks->building['building_name'],
                            'floor_name'=>$desks->floor['floor_name'],
                            'desk_id' => $desks->id,
                            'desk_name' => $desks->desk_name,
                            'type' => 'desk'
                        ];
                        return response()->json(['success' => true, 'status' => 1, 'message' => 'data fetched', 'data' => $fetched]);
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Desk not found"
                        ], 422);
                    }
                }
                return response()->json([
                    "status" => 0,
                    "message" => "Invalid QR code!"
                ], 422);

            } else {
                return response()->json(["status" => 0,
                    "message" => "Api key not found"], 422);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Not Valid data"
        ], 422);


    }

}



