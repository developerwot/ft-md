<?php

namespace App\Http\Controllers;

use App\LocationDevice;
use App\Models\Company;
use App\Models\Setting;
use App\Repositories\Backend\Company\CompanyContract;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyApiController extends Controller
{
    protected $repository;
    use SendsPasswordResetEmails;

    public function __construct(CompanyContract $repository)
    {
        $this->repository = $repository;
    }

    public function getDeviceSettings(Request $request)
    {

        try {
            $data = json_decode($request->getContent(), true);
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = $this->repository->findByStatus($data['company_id'], 1);
                    if (!empty($checkCompany)) {
                        $settings = Setting::select('host', 'port', 'video_stream_url', 'video_stream_url_out', 'face_detect_at_seconds', 'recognize_at_seconds', 'temperature_check', 'temperature_check_end', 'temperature_threshold', 'door_open_for_green_at', 'door_open_for_red_at')
                            ->where('company_id', $checkCompany->id)->first();

                        if (!empty($settings)) {
                            return response()->json(['success' => true, 'device_settings' => $settings->toArray()], 200);

                        } else {
                            return response()->json(['success' => true, 'device_settings' => ''], 200);
                        }

                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }


        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());

        }
    }

    public function login(Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true);
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['email']) && !empty($data['email'])) {
                    if (isset($data['password']) && !empty($data['password'])) {
                        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                            $checkCompany = $this->repository->findEmail($data['email']);
                            if (!empty($checkCompany)) {

                                $credentials = ["email" => $data['email'], "password" => $data['password']];

                                if (Auth::attempt($credentials)) {
                                    $user = auth()->user();
                                    if ($user->status == 1) {

                                        $checkCompany->phone = $user->phone;
                                        $countries = $this->getCountries();
                                        if ($checkCompany->country != '') {
                                            $checkCompany->country = $countries[$checkCompany->country];
                                        }

                                        if ($user->logo != '' && file_exists(public_path('company-logo/' . $user->logo))) {
                                            $checkCompany->logo = asset('company-logo/' . $user->logo);
                                        } else {
                                            $checkCompany->logo = asset('theme/media/users/blank.png');
                                        }

                                        return response()->json(['success' => true, 'company' => $checkCompany->toArray()], 200);
                                    } else {
                                        return response()->json([
                                            "status" => 0,
                                            "message" => "Your Account is not active"
                                        ], 422);
                                    }
                                } else {
                                    return response()->json([
                                        "status" => 0,
                                        "message" => "Invalid Credential"
                                    ], 422);
                                }
                            } else {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "Invalid Credential"
                                ], 422);
                            }
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Email is invalid"
                            ], 422);
                        }
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Password is required"
                        ], 422);
                    }

                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Email is required"
                    ], 422);
                }

            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());

        }
    }

    public function getLocationDeviceSetting(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if(!isset($data['company_id'])){
                    return response()->json([
                        "status" => 0,
                        "message" => "Company not found"
                    ], 422);

                }
                if(!isset($data['building_id'])){
                    return response()->json([
                        "status" => 0,
                        "message" => "building not found"
                    ], 422);

                }
                if(!isset($data['location_id'])){
                    return response()->json([
                        "status" => 0,
                        "message" => "location not found"
                    ], 422);

                }
                $devices = LocationDevice::with('location')->where([['company_id', '=', $data['company_id']], ['building_id', '=', $data['building_id']], ['location_id', '=', $data['location_id']]])->get();



                $response = [
                    'in' => [
                        'services' => [],
                        'relays' => [],
                    ],
                    'out' => [
                        'services' => [],
                        'relays' => [],
                    ],
                ];
//                dd($devices);
                $people_link_status=Company::where('id',$data['company_id'])->value('people_link_status');
                foreach ($devices as $d) {
//                    if($people_link_status==1){
//                       if($d->device_id==null){
//                           return response()->json([
//                               "status" => 0,
//                               "message" => " please enter deviceId in ". $d->location['location_name']." gate.",
//                           ], 422);
//                       }
//                    }
                    if ($d->device_location == 'entrance') {
                        $response['in']['camera_name'] = $d->camera_name !=null ? strtolower($d->camera_name) : null ;
                        $response['in']['device_type'] = $d->device_type !=null ? strtolower($d->device_type) : null ;
                        $response['in']['device_location'] = $d->device_location !=null ? strtolower($d->device_location) : null ;
                        $response['in']['host'] = $d->host !=null ? strtolower($d->host) : null ;
                        $response['in']['port'] = $d->port !=null ? strtolower($d->port) : null ;
                        $response['in']['video_stream_url'] = $d->video_stream_url !=null ? strtolower($d->video_stream_url) : null ;
                        $response['in']['temperature_threshold'] =(float)$d->temperature_threshold;
                        $response['in']['services']['face_detection'] = $d->face_detection==1? true :false;
                        $response['in']['services']['temperature_detection'] = $d->temperature_detection==1? true :false;
                        $response['in']['face_detect_seconds'] = $d->face_detect_seconds !=null ? (int)$d->face_detect_seconds :null;
                        $response['in']['recognize_at_seconds'] = $d->recognize_at_seconds !=null ? (int)$d->recognize_at_seconds :null;
                        $response['in']['temperature_check'] = $d->temperature_check !=null ? (int)$d->temperature_check :null;
                        $response['in']['temperature_check_end'] = $d->temperature_check_end !=null ? (int)$d->temperature_check_end :null;
                        $response['in']['relays']['green']['relay'] = $d->green_relay;
                        $response['in']['relays']['green']['command'] = $d->green_relay_command!=null ? strtolower($d->green_relay_command) : null;
                        $response['in']['relays']['green']['durations'] = $d->green_relay_duration !=null ? (int)$d->green_relay_duration :null;
                        $response['in']['relays']['red']['relay'] = $d->red_relay;
                        $response['in']['relays']['red']['command'] = $d->red_relay_command!=null ? strtolower($d->red_relay_command) : null ;
                        $response['in']['relays']['red']['durations'] = $d->red_relay_duration !=null ? (int)$d->red_relay_duration :null;
                        $response['in']['relays']['buzzer']['relay'] = $d->buzzer_relay;
                        $response['in']['relays']['buzzer']['command'] = $d->buzzer_relay_command!=null ? strtolower($d->buzzer_relay_command) : null;
                        $response['in']['relays']['buzzer']['durations'] = $d->buzzer_relay_duration !=null ? (int)$d->buzzer_relay_duration :null;
                        if($d->device_type=='thermal'){
                            $response['in']['recognise_on'] = $d->recognise_on !=null ? strtolower($d->recognise_on) : 'image' ;
                        }
                        if($people_link_status==1){
                            $response['in']['device_id']=$d->device_id;
                        }

                    } else {
                        $response['out']['camera_name'] = $d->camera_name !=null ? strtolower($d->camera_name) : null ;
                        $response['out']['device_type'] = $d->device_type !=null ? strtolower($d->device_type) : null ;
                        $response['out']['device_location'] = $d->device_location !=null ? strtolower($d->device_location) : null ;
                        $response['out']['host'] = $d->host !=null ? strtolower($d->host) : null ;
                        $response['out']['port'] = $d->port !=null ? strtolower($d->port) : null ;
                        $response['out']['video_stream_url'] = $d->video_stream_url !=null ? strtolower($d->video_stream_url) : null ;
                        $response['out']['temperature_threshold'] = (float)$d->temperature_threshold;
                        $response['out']['services']['face_detection'] = $d->face_detection==1? true :false;
                        $response['out']['services']['face_detection'] = $d->face_detection==1? true :false;
                        $response['out']['services']['temperature_detection'] = $d->temperature_detection==1? true :false;
                        $response['out']['face_detect_seconds'] = $d->face_detect_seconds !=null ? (int)$d->face_detect_seconds :null;
                        $response['out']['recognize_at_seconds'] = $d->recognize_at_seconds !=null ? (int)$d->recognize_at_seconds :null;
                        $response['out']['temperature_check'] = $d->temperature_check !=null ? (int)$d->temperature_check :null;
                        $response['out']['temperature_check_end'] = $d->temperature_check_end !=null ? (int)$d->temperature_check_end :null;
                        $response['out']['relays']['green']['relay'] = $d->green_relay;
                        $response['out']['relays']['green']['command'] = $d->green_relay_command !=null ? strtolower($d->green_relay_command) : null;
                        $response['out']['relays']['green']['durations'] = $d->green_relay_duration !=null ? (int)$d->green_relay_duration :null;
                        $response['out']['relays']['red']['relay'] = $d->red_relay;
                        $response['out']['relays']['red']['command'] = $d->red_relay_command!=null ? strtolower($d->red_relay_command) : null;
                        $response['out']['relays']['red']['durations'] = $d->red_relay_duration !=null ? (int)$d->red_relay_duration :null;
                        $response['out']['relays']['buzzer']['relay'] = $d->buzzer_relay;
                        $response['out']['relays']['buzzer']['command'] = $d->buzzer_relay_command!=null ? strtolower($d->buzzer_relay_command) : null;
                        $response['out']['relays']['buzzer']['durations'] = $d->buzzer_relay_duration !=null ? (int)$d->buzzer_relay_duration :null;
                        if($d->device_type=='thermal'){
                            $response['out']['recognise_on'] = $d->recognise_on !=null ? strtolower($d->recognise_on) : 'image' ;
                        }
                        if($people_link_status==1){
                            $response['out']['device_id']=$d->device_id;
                        }
                    }


                }
//              dd($response);
                return response()->json([
                    "status" => 1,
                    "message" => "Device settings fetched",
                    "data" => $response,
                ], 200);
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }
}
