<?php

namespace App\Http\Controllers;

use App\BookingInvitation;
use App\Mail\InviteEmployeeMail;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Setting;
use App\ReservationAccess;
use App\TimeslotAccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class BookingInvitationApiController extends Controller
{
    public function locationInvitation(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                $k = 0;
                $response = [];
                $time = TimeslotAccess::with('location')->where('id', $input['slot_id'])->first();
                $start = Carbon::parse($time['start_time'])->addMinutes(1);
                $end = $time['end_time'];
                $appLink=Setting::where('company_id',$data['company_id'])->value('guest_user_link');
                $invitedBy=Employee::where('id',$data['employee_id'])->first(['first_name','last_name']);
                foreach ($data['invited_to'] as $employee) {
                    $invited = BookingInvitation::where([['invited_to', '=', $employee], ['company_id', '=', $data['company_id']], ['slot_id', '=', $data['slot_id']], ['type', '=', 'location'], ['status', '!=', 'rejected']])->whereDate('invited_at', $data['date'])->first();
                    if ($invited) {
                        $emp = Employee::where('id', $employee)->first(['first_name', 'last_name', 'email']);
                        $response[$k]['status'] = 'failed';
                        $response[$k]['reason'] = 'employee already been Invited to this time slot!';
                        $response[$k]['first_name'] = $emp->first_name;
                        $response[$k]['last_name'] = $emp->last_name;
                        $response[$k]['email'] = $emp->email;
                    } else {
                        $reserve = DB::table('timeslot_accesses')
                            ->where('reservation_accesses.deleted_at', '=', null)
                            ->join('reservation_accesses', 'reservation_accesses.slot_id', '=', 'timeslot_accesses.id')
                            ->where('employee_id', $employee)
                            ->where(function ($q) use ($start, $end) {
                                $q->where('timeslot_accesses.start_time', '=', $start)->where('timeslot_accesses.end_time', '=', $end);
                                $q->orWhere(function ($q) use ($start, $end) {
                                    $q->whereBetween('timeslot_accesses.start_time', [$start, $end])
                                        ->orWhereBetween('timeslot_accesses.end_time', [$start, $end]);
                                });
                            })
                            ->select('reservation_accesses.id', 'timeslot_accesses.start_time', 'reservation_accesses.employee_id', 'timeslot_accesses.end_time', 'reservation_accesses.booked_at')->get();
                        $array = collect($reserve)->where('booked_at', '=', $data['date'])->toArray();
                        if (collect($array)->count() > 0) {
                            $emp = Employee::where('id', $employee)->first(['first_name', 'last_name', 'email']);
                            $response[$k]['status'] = 'failed';
                            $response[$k]['reason'] = 'Employee already have booking at this time slot';
                            $response[$k]['first_name'] = $emp->first_name;
                            $response[$k]['last_name'] = $emp->last_name;
                            $response[$k]['email'] = $emp->email;
                        } else {
//                            $total = ReservationAccess::where('type', 'location')->whereDate('booked_at', $data['date'])->where('slot_id', '=', $input['slot_id'])->count();
//                            if ($total == $limit) {
//                                $emp = Employee::where('id', $employee)->first(['first_name', 'last_name', 'email']);
//                                $response[$k]['status'] = 'failed';
//                                $response[$k]['reason'] = 'location limit reached';
//                                $response[$k]['first_name'] = $emp->first_name;
//                                $response[$k]['last_name'] = $emp->last_name;
//                                $response[$k]['email'] = $emp->email;
//                            } else {
                            $emp = Employee::where('id', $employee)->first(['first_name', 'last_name', 'email']);
                            $response[$k]['status'] = 'success';
                            $response[$k]['reason'] = 'Invitation sent successfully!';
                            $response[$k]['first_name'] = $emp->first_name;
                            $response[$k]['last_name'] = $emp->last_name;
                            $response[$k]['email'] = $emp->email;
                            BookingInvitation::create([
                                'slot_id' => $input['slot_id'],
                                'type' => 'location',
                                'invited_to' => $employee,
                                'employee_id' => $data['employee_id'],
                                'invited_at' => $data['date'],
                                'company_id' => $data['company_id'],
                                'status' => 'pending'
                            ]);
                            Mail::to($emp->email)->send(new InviteEmployeeMail($emp->first_name,$appLink,$invitedBy->first_name,$invitedBy->last_name,$time->start_time,$time->location['location_name']));

                        }

                    }
                    $k++;
                }
                return response()->json([
                    'success' => true,
                    'status' => 1,
                    'message' => 'Invitations have been sent successfully!',
                    'data' => $response
                ], 200);
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function acceptInvitation(Request $request){
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                $time = TimeslotAccess::with('location')->where('id', $input['slot_id'])->first();
                $limit = $time->location['location_limit'];
                $start = Carbon::parse($time['start_time'])->addMinutes(1);
                $end = $time['end_time'];
                $reserve = DB::table('timeslot_accesses')
                    ->where('reservation_accesses.deleted_at', '=', null)
                    ->join('reservation_accesses', 'reservation_accesses.slot_id', '=', 'timeslot_accesses.id')
                    ->where('employee_id', $input['employee_id'])
                    ->where(function ($q) use ($start, $end) {
                        $q->where('timeslot_accesses.start_time','=',$start)->where('timeslot_accesses.end_time','=',$end);
                        $q->orWhere(function ($q) use ($start, $end) {
                            $q->whereBetween('timeslot_accesses.start_time', [$start, $end])
                                ->orWhereBetween('timeslot_accesses.end_time', [$start, $end]);
                        });
                    })

                    ->select('reservation_accesses.id', 'timeslot_accesses.start_time','reservation_accesses.employee_id', 'timeslot_accesses.end_time', 'reservation_accesses.booked_at')->get();
                $array= collect($reserve)
                    ->where('booked_at', '=', $data['date'])->toArray();
                if (collect($array)->count()>0) {
                    return response()->json([
                        "status" => 0,
                        "message" => "You have already booked same time slot at another location.!",
                        "data"=>$array,
                    ], 422);
                } else {
                    $total = ReservationAccess::where('type', 'location')->whereDate('booked_at', $data['date'])->where('slot_id', '=', $input['slot_id'])->count();
                    if ($total == $limit) {
                        return response()->json([
                            "status" => 0,
                            "message" => "Location limit reached"
                        ], 422);
                    } else {
                        ReservationAccess::create([
                            'slot_id' => $input['slot_id'],
                            'type' => 'location',
                            'employee_id' => $input['employee_id'],
                            'booked_at' => $data['date']
                        ]);
                        BookingInvitation::where([['invited_to','=',$data['employee_id']],['slot_id','=',$data['slot_id']],['invited_at','=',$data['date']]])->update(['status'=>'accepted']);
                        return response()->json([
                            'success' => true,
                            'status' => 1,
                            'message' => 'Slot booked successfully!',
                        ], 200);

                    }
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function getInvitation(Request $request){
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
                    if (!empty($checkCompany)) {
                        if (isset($data['employee_id']) && !empty($data['employee_id'])) {
                            $checkEmployee = Employee::where('id', $data['employee_id'])->first();
                            if (!empty($checkEmployee)) {
                                $locations = BookingInvitation::where([['invited_to', '=', $data['employee_id']], ['booking_invitations.company_id', '=', $data['company_id']]])
                                    ->whereDate('invited_at','>=',Carbon::today()->format('Y-m-d'))
                                    ->join('timeslot_accesses','booking_invitations.slot_id','=','timeslot_accesses.id')
                                    ->join('location_accesses','timeslot_accesses.location_access_id','=','location_accesses.id')
                                    ->join('employee','booking_invitations.employee_id','=','employee.id')
                                    ->select('booking_invitations.id','timeslot_accesses.start_time','timeslot_accesses.end_time','location_accesses.location_name','employee.first_name','booking_invitations.slot_id','booking_invitations.status','booking_invitations.invited_at')
                                    ->get();
                                return response()->json(['success' => true, 'status' => 1, 'message' => 'invitations fetched', 'invitations' => $locations]);
                            } else {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "Employee Not Found"
                                ], 422);
                            }
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Employee id is required"
                            ], 422);
                        }
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function rejectInvitations(Request $request){
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {


                try{
                    BookingInvitation::where('id',$data['invitation_id'])->update(['status'=>'rejected']);
                    return response()->json([
                        "status" => 1,
                        "message" => "invitation rejected successfully!"
                    ], 200);
                }catch (\Exception $e){
                    return response()->json([
                        "status" => 0,
                        "message" => "invitation not found"
                    ], 422);
                }
//                if (isset($data['company_id']) && !empty($data['company_id'])) {
//                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
//                    if (!empty($checkCompany)) {
//                        if (isset($data['employee_id']) && !empty($data['employee_id'])) {
//                            $checkEmployee = Employee::where('id', $data['employee_id'])->first();
//                            if (!empty($checkEmployee)) {
//                                BookingInvitation::where([['invited_to','=',$data['employee_id']],['slot_id','=',$data['slot_id']],['invited_at','=',$data['date']]])->update(['status'=>'rejected']);
//                            } else {
//                                return response()->json([
//                                    "status" => 0,
//                                    "message" => "Employee Not Found"
//                                ], 422);
//                            }
//                        } else {
//                            return response()->json([
//                                "status" => 0,
//                                "message" => "Employee id is required"
//                            ], 422);
//                        }
//                    } else {
//                        return response()->json([
//                            "status" => 0,
//                            "message" => "Company Not Found"
//                        ], 422);
//                    }
//                } else {
//                    return response()->json([
//                        "status" => 0,
//                        "message" => "Company Id is required"
//                    ], 422);
//                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function deleteInvitation(Request $request){
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                try{
                    BookingInvitation::where('id',$data['invitation_id'])->delete();
                    return response()->json([
                        "status" => 1,
                        "message" => "invitation deleted"
                    ], 200);
                }catch (\Exception $e){
                    return response()->json([
                        "status" => 0,
                        "message" => "invitation not found"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }
}
