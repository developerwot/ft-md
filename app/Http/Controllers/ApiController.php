<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApiController extends Controller
{
   public function index(Request $request ,Response $response) {

       $url = $request->fullUrl();
       $method = $request->getMethod();
       $ip = $request->getClientIp();

       $log = "{$ip}: {$method}@{$url} -  \n".
           "Request :". $request->getContent()." \n".
           "Response : {$response->getContent()} \n";

       Log::channel('api')->info($log);
       Log::error('Log called');
   }
}
