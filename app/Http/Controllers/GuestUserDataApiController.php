<?php

namespace App\Http\Controllers;

use App\Http\Requests\GuestUserDataStoreRequest;
use App\Http\Requests\GuestUserStoreRequest;
use App\Http\Requests\GuestUserVerfyRequest;
use App\Mail\GuestUserInvite;
use App\Models\Company;
use App\Models\GuestUser;
use App\Models\Setting;
use App\Repositories\Backend\GuestUser\GuestUserContract;
use App\Repositories\Backend\GuestUserData\GuestUserDataContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mail;
use Illuminate\Support\Facades\File;

class GuestUserDataApiController extends Controller
{
    protected $repository;
    protected $company_id;

    public function __construct(GuestUserDataContract $repository)
    {

        $this->repository = $repository;
    }

    public function store(GuestUserDataStoreRequest $request)
    {
       // return response()->json(['name' => 'test']);


        $address_proof_file = $request->file('address_proof');
        $identity_proof_file = $request->file('identity_proof');

        $input = $request->only('api_key','guest_user_id','first_name','last_name','dob','address','city',
                                'postal_code','telephone_number','company_name');
            if(!empty($input) ) {
                $data = $input;
                if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                    if(isset($data['guest_user_id']) && $data['guest_user_id'] !='') {

                        $checkGuestUser = $this->repository->checkGuestUser($data['guest_user_id']);

                        if(!empty($checkGuestUser)) {

                            /*Upload file*/

                            $checkExistingUserdata = $this->repository->findByGuestUser($data['guest_user_id']);
                            if(!empty($address_proof_file)) {
                                $destinationPath = 'guest_user_proof/';
                                $filePath_address= time() .'-'. $address_proof_file->getClientOriginalName();
                                $address_proof_file->move($destinationPath, $filePath_address);
                            }
                            if(!empty($identity_proof_file)) {
                                $destinationPath = 'guest_user_proof/';
                                $filePath_identity= time() .'-'. $identity_proof_file->getClientOriginalName();
                                $identity_proof_file->move($destinationPath, $filePath_identity);

                                /*if(!empty($checkExistingUserdata)) {

                                }*/
                            }

                            if(isset($data['dob']) && !empty($data['dob'])) {
                                $data['dob'] =  date("Y-m-d",strtotime($data['dob']));
                            }
                            $data['company_id'] = $checkGuestUser->company_id;;


                            if(!empty($filePath_address) ) {
                                if(!empty($checkExistingUserdata) && $checkExistingUserdata->address_proof !='' && $checkExistingUserdata->address_proof != null) {
                                    File::delete(public_path('guest_user_proof/'. $checkExistingUserdata->address_proof));
                                }
                                $data['address_proof'] =  $filePath_address;
                            }
                            if(!empty($filePath_identity)) {
                                if(!empty($checkExistingUserdata) && $checkExistingUserdata->identity_proof !='' && $checkExistingUserdata->identity_proof != null) {
                                    File::delete(public_path('guest_user_proof/'. $checkExistingUserdata->identity_proof));
                                }
                                $data['identity_proof'] =  $filePath_identity;
                            }

                            if($checkExistingUserdata) {
                                $guser = $this->repository->update($checkExistingUserdata->id,$data);
                                $guser_data  = $this->repository->find($guser->id);

                                if(!empty($guser_data->address_proof)) {
                                    $guser_data->address_proof = asset('guest_user_proof/'.$guser->address_proof) ;
                                }
                                if(!empty($guser_data->identity_proof)) {
                                    $guser_data->identity_proof = asset('guest_user_proof/'.$guser->identity_proof) ;
                                }
                                return response()->json([
                                    "status"=>1,
                                    "message" => "Guest User data updated successfully",
                                    "data"=>$guser_data->toArray()
                                ], 200);
                            }
                            else {
                                $guser = $this->repository->store($data);
                                $guser_data  = $this->repository->find($guser->id);
                                if(!empty($guser_data->address_proof)) {
                                    $guser_data->address_proof = asset('guest_user_proof/'.$guser_data->address_proof) ;
                                }
                                if(!empty($guser->identity_proof)) {
                                    $guser_data->identity_proof = asset('guest_user_proof/'.$guser_data->identity_proof) ;
                                }
                                return response()->json([
                                    "status"=>1,
                                    "message" => "Guest User data added successfully",
                                    "data"=>$guser_data->toArray()
                                ], 200);
                            }
                        }
                        else {
                            return response()->json([
                                "status"=>0,
                                "message" => "Guest User not Found"
                            ], 422);
                        }
                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Guest user id is required"
                        ], 422);
                    }
                }
                else {
                    return response()->json([
                        "status"=>0,
                        "message" => "Api key not found"
                    ], 422);
                }
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Not Valid Data"
                ],422);
            }
    }
    public function verify(GuestUserVerfyRequest $request)
    {
        // return response()->json(['name' => 'test']);

        //$input = $request->only('temperature', 'employee_id','api_key');
        //echo '<pre>';
        $input = json_decode($request->getContent(), true);

        if(!empty($input) ) {
            $data = $input;
            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                if(isset($data['email']) && $data['email'] !='') {
                    $current_date = date("Y-m-d");
                   $verify = $this->repository->findByEmail($data['email'],$data['verify_code'],$current_date);
                    if(!empty($verify)) {
                        $updated_guest = $this->repository->update($verify->id,['verify_at'=>$current_date]);
                        return response()->json(['success' => true, "status"=>1,'data'=>$updated_guest->toArray()], 200);
                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Invalid verify code or email"
                        ], 422);
                    }

                }
                else {
                    return response()->json([
                        "status"=>0,
                        "message" => "Email is required"
                    ], 422);
                }
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 422);
            }
        }
        else {
            return response()->json([
                "status"=>0,
                "message" => "Not Valid Data"
            ],422);
        }
    }
    public function checkBoolen($value) {
        if(isset($value) && $value !='' && $value != null && in_array($value,[0,1])) {
            return true;
        }
        else {
            return false;
        }
    }
}
