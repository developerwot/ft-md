<?php

namespace App\Http\Controllers;
use App\Models\Employee;
use App\Mail\PeopleRegistryTraining;
use App\Models\GuestUser;
use App\Models\GuestUserRegistry;
use App\Repositories\Backend\PeopleRegistry\PeopleRegistryContract;
use Illuminate\Http\Request;
use App\Models\Setting;
use Mail;
use Illuminate\Support\Facades\Log;
class PeopleRegistryApiController extends Controller
{
    protected $repository;

    public function __construct(PeopleRegistryContract $repository)
    {
        $this->repository = $repository;
    }

    public function update(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if(!empty($input)) {
            $data = $input;

            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {


                if(isset($data['employee_id']) && $data['employee_id'] !='') {

                    if(isset($data['is_guest_user']) && !empty($data['is_guest_user']) && $data['is_guest_user'] == 1) {
                        /*Process for guest user*/
                        $checkGuestUser = GuestUser::find($data['employee_id']);
                        if( !empty($checkGuestUser) ) {
                            $checkGuestUserRegistry = GuestUserRegistry::where("guest_user_id",$data['employee_id'])->first();
                            if(empty($checkGuestUserRegistry)) {
                                //Create new registry
                                if(isset($data['status']) && $data['status'] !='') {

                                    //Validate date using validator format
                                    if (!is_numeric($data['status']) || !in_array($data['status'], [0,1, 2, 3])) {
                                        return response()->json([
                                            "status" => 0,
                                            "message" => "Status is invalid"
                                        ], 404);
                                    }
                                }
                                $createGData['guest_user_id'] = $data['employee_id'];
                                if(isset($data['status']) && $data['status'] !='') {
                                    $createGData['training_status'] = $data['status'];
                                }
                                if(isset($data['people_file']) && $data['people_file'] !='') {
                                    $createGData['people_file'] = $data['people_file'];
                                }
                                $peopleGR = GuestUserRegistry::create($createGData);
                                return response()->json([
                                    "status"=>1,
                                    "message" => "Guest User Registry status stored successfully"
                                ], 200);
                            }
                            else {
                                //Update Existing data
                                $updateGData =[];
                                if(isset($data['status']) && $data['status'] !='') {
                                    $updateGData['training_status'] = $data['status'];
                                }

                                if(isset($data['people_file']) && $data['people_file'] !='' &&  $data['people_file'] != null) {
                                    $updateGData['people_file'] = $data['people_file'];
                                }

                                $update = $checkGuestUserRegistry->update($updateGData);
                                return response()->json([
                                    "status"=>1,
                                    "message" => "Guest User Registry status updated successfully"
                                ], 200);
                            }
                        }
                        else {
                            return response()->json([
                                "status"=>0,
                                "message" => "Guest User Not Found"
                            ], 404);
                        }
                    }
                    else {

                        $isemployee = Employee::where('id',$data['employee_id'])->where('status',1)->first();
                        if(empty($isemployee)) {
                            return response()->json([
                                "status"=>0,
                                "message" => "People Not Found"
                            ], 404);
                        }
                        if(isset($data['status']) && $data['status'] !='') {

                            //Validate date using validator format
                            if (!is_numeric($data['status']) || !in_array($data['status'], [0,1, 2, 3])) {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "Status is invalid"
                                ], 404);
                            }
                        }

                        $people_registry = $this->repository->findByEmployee($isemployee->id);

                        if( empty($people_registry) ) {

                            //Create People Registry
                            if(isset($data['status']) && $data['status'] !='') {
                                $createData['training_status'] = $data['status'];
                            }

                            $createData['employee_id'] = $data['employee_id'];
                            if(isset($data['people_file']) && $data['people_file'] !='') {
                                $createData['people_file'] = $data['people_file'];
                            }
                            else {
                                $createData['people_file']  = '';
                            }
                            $peopleR = $this->repository->store($createData);

                            if(isset($data['status']) && $data['status'] == 3 ) {
                                //Send Admin Mail
                                $settings = Setting::where('company_id',$isemployee->company_id)->first();
                                if( !empty($settings) ) {
                                    if( !empty($settings->hr_email) ) {
                                        Mail::to($settings->hr_email)->send(new PeopleRegistryTraining($isemployee->first_name.' '.$isemployee->last_name));
                                    }
                                }
                            }
                            return response()->json([
                                "status"=>1,
                                "message" => "People Registry status store successfully"
                            ], 200);

                        }
                        else {
                            $updateData =[];
                            if(isset($data['status']) && $data['status'] !='') {
                                $updateData['training_status'] = $data['status'];
                            }

                            if(isset($data['people_file']) && $data['people_file'] !='') {
                                $updateData['people_file'] = $data['people_file'];
                            }

                            $update = $this->repository->update($people_registry->id,$updateData);

                            if(isset($data['status']) && $data['status'] == 3 ) {
                                //Send Admin Mail
                                $settings = Setting::where('company_id',$isemployee->company_id)->first();
                                if( !empty($settings) ) {
                                    if( !empty($settings->hr_email) ) {
                                        Mail::to($settings->hr_email)->send(new PeopleRegistryTraining($isemployee->first_name.' '.$isemployee->last_name));
                                    }
                                }
                            }
                            return response()->json([
                                "status"=>1,
                                "message" => "People Registry status updated successfully"
                            ], 200);
                        }
                    }

                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Employee ID is required"
                        ], 404);
                    }

            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 404);
            }
        }
    }

    public function checkPeopleRegistry(Request $request)
    {

        try{
            $input = json_decode($request->getContent(), true);

            if(!empty($input)) {
                $data = $input;

                if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {



                    if(isset($data['employee_id']) && $data['employee_id'] !='') {

                        if(isset($data['is_guest_user']) && !empty($data['is_guest_user']) && $data['is_guest_user'] == 1) {
                            /*Process for guest user*/
                            $checkGuestUser = GuestUser::find($data['employee_id']);
                            if (!empty($checkGuestUser)) {
                                $people_registry_guest = GuestUserRegistry::where("guest_user_id",$data['employee_id'])->first();
                                if( empty($people_registry_guest) ) {
                                    return response()->json([
                                        "status"=>1,
                                        "success"=>true,
                                    ], 200);
                                }
                                else {
                                    if($people_registry_guest->training_status == 1) {

                                        return response()->json([
                                            "status"=>0,
                                            "training_status"=>$people_registry_guest->training_status,
                                            "message" => "Guest User Registry Video Uploaded"
                                        ], 404);
                                    }
                                    else if($people_registry_guest->training_status == 2) {

                                        return response()->json([
                                            "status"=>0,
                                            "training_status"=>$people_registry_guest->training_status,
                                            "message" => "Guest User Registry Training in Progress"
                                        ], 404);
                                    }
                                    else if($people_registry_guest->training_status == 3) {

                                        return response()->json([
                                            "status"=>0,
                                            "training_status"=>$people_registry_guest->training_status,
                                            "message" => "Guest User Registry Training is Done"
                                        ], 404);
                                    }
                                    else if($people_registry_guest->training_status == 0) {

                                        return response()->json([
                                            "status"=>1,
                                            "success"=>true,
                                        ], 200);
                                    }
                                }

                            }
                            else {
                                return response()->json([
                                    "status"=>0,
                                    "message" => "Guest User Not Found"
                                ], 404);
                            }
                        }
                        else {
                            $isemployee = Employee::where('id',$data['employee_id'])->where('status',1)->first();
                            if(empty($isemployee)) {
                                return response()->json([
                                    "status"=>0,
                                    "message" => "People Not Found"
                                ], 404);
                            }

                            $people_registry = $this->repository->findByEmployee($isemployee->id);
                            if( empty($people_registry) ) {
                                return response()->json([
                                    "status"=>1,
                                    "success"=>true,
                                ], 200);
                            }
                            else {
                                if($people_registry->training_status == 1) {

                                    return response()->json([
                                        "status"=>0,
                                        "training_status"=>$people_registry->training_status,
                                        "message" => "People Registry Video Uploaded"
                                    ], 404);
                                }
                                else if($people_registry->training_status == 2) {

                                    return response()->json([
                                        "status"=>0,
                                        "training_status"=>$people_registry->training_status,
                                        "message" => "People Registry Training in Progress"
                                    ], 404);
                                }
                                else if($people_registry->training_status == 3) {

                                    return response()->json([
                                        "status"=>0,
                                        "training_status"=>$people_registry->training_status,
                                        "message" => "People Registry Training is Done"
                                    ], 404);
                                }
                                else if($people_registry->training_status == 0) {

                                    return response()->json([
                                        "status"=>1,
                                        "success"=>true,
                                    ], 200);
                                }
                            }
                        }
                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Employee ID is required"
                        ], 404);
                    }

                }
                else {
                    return response()->json([
                        "status"=>0,
                        "message" => "Api key not found"
                    ], 404);
                }
            }
        }
       catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());

        }

    }

    public function getTrainedIds(Request $request) {
        $input = json_decode($request->getContent(), true);
        if(!empty($input)) {
            $data = $input;
            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                if(isset($data['company_id']) && !empty($data['company_id'])) {
                    $employees = $this->repository->getEmpByCompanyAndStatus($data['company_id'],2);
                    return response()->json(['success' => true,'employeeId'=>$employees->toArray()], 200);
                }
                else {
                    return response()->json([
                        "status"=>0,
                        "message" => "Company Id is required"
                    ], 422);
                }

            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 404);
            }
        }
    }
}
