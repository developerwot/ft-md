<?php

namespace App\Http\Controllers;

use App\Http\Requests\Backend\EmpTemperatureStoreRequest;
use App\Http\Controllers\Controller;
use App\Mail\EmployeeRegister;
use App\Models\Company;
use App\Models\Employee;
use App\Models\EmployeeTemperature;
use App\Models\EmployeeTemperatureLog;
use App\Models\PeopleAttendance;
use App\Models\Setting;
use App\Repositories\Backend\Employee\EmployeeContract;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\EmpTemp\EmpTemperatureContract;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class EmployeeApiController extends Controller
{
    protected $repository;
    protected $company_id;

    public function __construct(EmployeeContract $repository)
    {
        $this->repository = $repository;
    }

    public function login(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;
//            dd($data);
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (Auth::attempt(array('email' => $data['email'], 'password' => $data['password']))) {
                    $exist = Employee::where('email', $data['email'])->first();
                    if (!$exist) {
                        return response()->json([
                            "status" => 0,
                            "message" => "Employee not found please check email & password"
                        ], 422);
                    }
                    $user = Employee::with('company', 'user')->where('email', $data['email'])->first();
//                  dd($user);
                    $Jdata = [
                        'user_data' => [
                            'user_id' => $user['id'],
                            'first_name' => $user['first_name'],
                            'last_name' => $user['last_name'],
                            'email' => $user['email'],
                            'status' => $user['status'],
                            'minor_id' => $user->user['minor_id'],
                            'major_id' => $user->user['major_id']

                        ],
                        'company_data' => [
                            'company_id' => $user->company['id'],
                            'name' => $user->company['name'],
                            'last_name' => $user->company['last_name'],
                            'email' => $user->company['email'],
                            'business_name' => $user->company['business_name'],
                            'address' => $user->company['address'],
                            'city' => $user->company['city'],
                            'postal_code' => $user->company['postal_code'],
                            'country' => $user->company['country'],
                            'vat' => $user->company['vat'],
                            'status' => $user->company['status'],
                        ]
                    ];
                    if ($data['type'] == 'intelly-track') {
                        if ($user->company['is_tracking_enabled'] == 1) {
                            return response()->json([
                                'success' => true,
                                "status" => 1,
                                "message" => "Login successfully!",
                                "user_data" => $Jdata['user_data'],
                                "company_data" => $Jdata['company_data'],
                            ], 200);
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Tracking service is not available"
                            ], 422);
                        }
                    }
                    if ($data['type'] == 'intelly-access') {
                        if ($user->company['is_booking_enabled'] == 1) {

                            return response()->json([
                                'success' => true,
                                "status" => 1,
                                "message" => "Login Successfully!",
                                "user_data" => $Jdata['user_data'],
                                "company_data" => $Jdata['company_data'],
                            ], 200);
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Booking service is not available"
                            ], 422);
                        }
                    }


                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Invalid Credentials"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function get_data(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                if (isset($data['company_id']) && !empty($data['company_id'])) {
                    $checkCompany = Company::where('status', 1)->where('id', $data['company_id'])->first();
                    if (!empty($checkCompany)) {
                        $employees = $this->repository->getAllActiveByCompany($checkCompany->id, 1);
                        return response()->json(['success' => true, "status" => 1, 'count' => $employees->count(), 'employee' => $employees->toArray()], 200);
                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }

            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function data_store(Request $request)
    {

        $input = json_decode($request->getContent(), true);
        if (!empty($input) && isset($input[0])) {
            $data = $input[0];

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                if (isset($data['company_email']) && $data['company_email'] != '') {
                    $getCompany = Company::where('email', $data['company_email'])->where('status', 1)->first();
                    if (empty($getCompany)) {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Email is required"
                    ], 422);
                }
                if (!empty($data['employee'])) {
                    $count = 0;
                    foreach ($data['employee'] as $emp) {

                        //Insert Employee Data
                        if (isset($emp['email']) && $emp['email'] != '') {
                            $checkExisting = $this->repository->findByEmail($emp['email']);
                            if (empty($checkExisting)) {

                                $newPassword = 'weboccult_123';
                                $user_input['password'] = Hash::make($newPassword);
                                $user_input['first_name'] = (isset($emp['first_name'])) ? $emp['first_name'] : ' ';
                                $user_input['last_name'] = (isset($emp['last_name'])) ? $emp['last_name'] : ' ';
                                $user_input['email'] = $emp['email'];
                                $user_input['status'] = (isset($emp['status'])) ? $emp['status'] : 0;
                                $user = User::create($user_input);

                                if ($user) {

                                    $roles = ['employee'];
                                    $user->syncRoles($roles);
                                    $empData['first_name'] = (isset($emp['first_name'])) ? $emp['first_name'] : ' ';
                                    $empData['last_name'] = (isset($emp['last_name'])) ? $emp['last_name'] : ' ';
                                    $empData['email'] = $emp['email'];
                                    $empData['user_id'] = $user->id;
                                    $empData['company_id'] = $getCompany->id;
                                    $empData['status'] = (isset($emp['status'])) ? $emp['status'] : 0;
                                    $this->repository->store($empData);
                                    $count++;
                                }
                            }
                        }

                    }
                    if ($count > 0) {
                        return response()->json([
                            "status" => 1,
                            "message" => $count . " People added Successfully "
                        ], 200);
                    } else {
                        return response()->json([
                            "status" => 1,
                            "message" => "Not People added , Please check data "
                        ], 200);
                    }

                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Invalid Data"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        }


    }

    public function store(Request $request)
    {

        $input = json_decode($request->getContent(), true);

        if (!empty($input) && isset($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {


                if (isset($data['company_id']) && $data['company_id'] != '') {
                    $getCompany = Company::where('id', $data['company_id'])->where('status', 1)->first();
                    if (empty($getCompany)) {
                        return response()->json([
                            "status" => 0,
                            "message" => "Company Not Found"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Id is required"
                    ], 422);
                }

                if (!empty($data['email'])) {
                    $count = 0;
                    //Insert Employee Data
                    if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                        $checkExisting = $this->repository->findByEmail($data['email']);
                        if (empty($checkExisting)) {
                            if (isset($data['password'])) {
                                if (strlen($data['password']) < 6) {
                                    return response()->json(['status' => 0, 'message' => 'password should contain at least 6 characters'], 400);
                                } elseif ($data['password'] != $data['password_confirmation']) {
                                    return response()->json(['status' => 0, 'message' => 'password confirm password do not match'], 400);
                                } else {
                                    $newPassword = $data['password'];
                                    $user_input['password'] = Hash::make($data['password']);
                                }

                            } else {
                                $newPassword = 'intellyaccess@123';
                                $user_input['password'] = Hash::make($newPassword);
                            }
                            $user_input['first_name'] = (isset($data['first_name'])) ? $data['first_name'] : ' ';
                            $user_input['last_name'] = (isset($data['last_name'])) ? $data['last_name'] : ' ';
                            $user_input['email'] = $data['email'];
                            $user_input['status'] = (isset($data['status'])) ? $data['status'] : 1;
//                                        $digits=checkUser();
//                                        $user_input['minor_id']=$digits['minor'];
//                                        $user_input['major_id']=$digits['major'];
                            $user = User::create($user_input);

                            if ($user) {
                                $roles = ['employee'];
                                $user->syncRoles($roles);
                                $empData['first_name'] = (isset($data['first_name'])) ? $data['first_name'] : ' ';
                                $empData['last_name'] = (isset($data['last_name'])) ? $data['last_name'] : ' ';
                                $empData['email'] = $data['email'];
                                $empData['user_id'] = $user->id;
                                $empData['company_id'] = $getCompany->id;
                                $empData['status'] = (isset($data['status'])) ? $data['status'] : 1;
                                $emp = $this->repository->store($empData);
                                $link = Setting::where('company_id', $getCompany->id)->value('guest_user_link');
                                $getUserCompany = Company::where('id', $getCompany->id)->first();
//                                            \Illuminate\Support\Facades\Mail::to($empData['email'])->send(new EmployeeRegister($empData['email'],$newPassword,$link,$getUserCompany));
                                return response()->json([
                                    'success' => true,
                                    "status" => 1,
                                    "message" => " People added Successfully",
                                    "data" => $emp->toArray()
                                ], 200);
                            }
                        } else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Already registered People"
                            ], 422);
                        }


                    } else {
                        return response()->json([
                            "status" => 0,
                            "message" => "Invalid Email"
                        ], 422);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Invalid Data"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        }


    }


    public function profile(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                $employee = Employee::with('company', 'user')->where('id', $data['employee_id'])->first();
//                dd($employee);
                if (!empty($employee->company)) {
                    $emp_data = [
                        'user_id' => $employee['id'],
                        'first_name' => $employee['first_name'],
                        'last_name' => $employee['last_name'],
                        'email' => $employee['email'],
                        'status' => $employee['status'],
                        'minor_id' => $employee->user['minor_id'],
                        'major_id' => $employee->user['major_id']
                    ];
                    return response()->json([
                        'success' => true,
                        'status' => 1,
                        'message' => 'Employee profile fetched',
                        'employee_data' => $emp_data
                    ], 200);
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Not Found"
                    ], 422);
                }


            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function profileUpdate(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;
            if (!isset($data['first_name']) || $data['first_name'] == null) {
                return response()->json([
                    "status" => 0,
                    "message" => "First name is required"
                ], 422);
            }
            if (!isset($data['last_name']) || $data['last_name'] == null) {
                return response()->json([
                    "status" => 0,
                    "message" => "Last name is required"
                ], 422);
            }
            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                if (isset($data['password'])) {
                    if ($data['password'] != $data['password_confirmation']) {
                        return response()->json(['status' => 0, 'message' => 'password confirm password do not match'], 400);
                    } elseif (strlen($data['password']) < 6) {
                        return response()->json(['status' => 0, 'message' => 'password should contain at least 6 characters'], 400);
                    }
                    try {
                        $employee = Employee::with('user')->where('id', $data['employee_id'])->first();
                    } catch (\Exception $e) {
                        return response()->json(['status' => 0, 'message' => 'employee does not exist']);
                    }
                    $data['password'] = Hash::make($data['password']);
                    unset($data['email']);
                    unset($data['api_key']);
                    unset($data['employee_id']);
                    unset($data['password_confirmation']);
                    unset($data['major_id']);
                    unset($data['minor_id']);
//                    dd($data);
                    try {
                        $employee->user->update(['first_name' => $data['first_name'], 'last_name' => $data['last_name'], 'password' => $data['password']]);
                        unset($data['password']);
                        $employee->update(['first_name' => $data['first_name'], 'last_name' => $data['last_name']]);
                    } catch (\Exception $e) {
                        return response()->json(['status' => 0, 'message' => 'something went wrong'], 400);
                    }
                    $employee_data = [
                        'user_id' => $employee['id'],
                        'first_name' => $employee['first_name'],
                        'last_name' => $employee['last_name'],
                        'email' => $employee['email'],
                        'status' => $employee['status'],
                        'minor_id' => $employee->user['minor_id'],
                        'major_id' => $employee->user['major_id']
                    ];
                    return response()->json([
                        'success' => true,
                        'status' => 1,
                        'messaged' => 'Profile updated',
                        'employee_data' => $employee_data
                    ], 200);
                } else {

                    try {
                        $employee = Employee::with('user')->where('id', $data['employee_id'])->first();
                    } catch (\Exception $e) {
                        return response()->json(['status' => 0, 'message' => 'employee does not exist']);
                    }
                    unset($data['email']);
                    unset($data['api_key']);
                    unset($data['employee_id']);
                    unset($data['password_confirmation']);
                    unset($data['major_id']);
                    unset($data['minor_id']);
                    try {
                        $employee->user->update(['first_name' => $data['first_name'], 'last_name' => $data['last_name']]);
                        $employee->update(['first_name' => $data['first_name'], 'last_name' => $data['last_name']]);
                    } catch (\Exception $e) {
                        return response()->json(['status' => 0, 'message' => 'something went wrong'], 400);
                    }

                    $employee_data = [
                        'user_id' => $employee['id'],
                        'first_name' => $employee['first_name'],
                        'last_name' => $employee['last_name'],
                        'email' => $employee['email'],
                        'status' => $employee['status'],
                        'minor_id' => $employee->user['minor_id'],
                        'major_id' => $employee->user['major_id']
                    ];
                    return response()->json([
                        'success' => true,
                        'status' => 1,
                        'messaged' => 'Profile updated',
                        'employee_data' => $employee_data
                    ], 200);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function passwordUpdate(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                if (isset($data['password'])) {
                    if (strlen($data['password']) < 6) {
                        return response()->json(['status' => 0, 'message' => 'password should contain at least 6 characters'], 422);
                    } elseif ($data['password'] != $data['password_confirmation']) {
                        return response()->json(['status' => 0, 'message' => 'password confirm password do not match'], 422);
                    } else {
                        $password = Hash::make($data['password']);
                        $employee = Employee::with('user')->where('id', $data['employee_id'])->first();
                        if (empty($employee)) {
                            return response()->json([
                                "status" => 0,
                                "message" => "Employee not found!"
                            ], 422);
                        }
                        $employee->user->update(['password' => $password]);
                        return response()->json([
                            'success' => true,
                            'status' => 1,
                            'message' => 'Password changed successfully',
                        ], 200);
                    }
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Password not found!"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function employeeTemperature(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                try {
                    $employee = Employee::findOrFail($data['employee_id']);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => 1,
                        "message" => "Employee not found!"
                    ], 200);
                }

                if ($data['date'] != null) {
                    $checks = PeopleAttendance::where('employee_id', $data['employee_id'])->whereDate('date', $data['date'])->latest()->first(['check_in', 'check_out', 'date']);
                    $temp = EmployeeTemperatureLog::where('employee_id', $data['employee_id'])->whereDate('temp_date', Carbon::parse($data['date']))->orderBy('created_at', 'asc')->first();
                    return response()->json([
                        'status' => 1,
                        'success' => true,
                        'message' => 'employee attendance & average temperature !',
                        'attendance' => $checks,
                        'averageTemperature' => round($temp->temperature, 2)
                    ]);
                } else {
                    return response()->json([
                        "status" => 0,
                        "message" => "Date not found"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function monthWiseTemperature(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                try {
                    $employee = Employee::findOrFail($data['employee_id']);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => 1,
                        "message" => "Employee not found!"
                    ], 200);
                }

                if ((!isset($data['month'])||!isset($data['year'])) ||($data['year'] == null || $data['month'] == null)) {
                    $checks = PeopleAttendance::where('employee_id', $data['employee_id'])->whereYear('date', '=', Carbon::now()->month)->whereMonth('date', '=', Carbon::now()->year)->pluck('date');
                }else{
                    $checks = PeopleAttendance::where('employee_id', $data['employee_id'])->whereYear('date', '=', $data['year'])->whereMonth('date', '=', $data['month'])->pluck('date');
                }
//dd($checks);
                if (!$checks || $checks->count()==0) {
                    return response()->json([
                        "status" => 0,
                        "message" => "No date available!"
                    ], 422);
                }
                return response()->json([
                    'status' => 1,
                    'success' => true,
                    'message' => 'Available dates fetched!',
                    'Dates' => $checks,
                ],200);


            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json(["status" => 0,
                "message" => "Not Valid data"], 422);
        }
    }


}
