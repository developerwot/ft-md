<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mail;
use Illuminate\Container\Container;
use Illuminate\Mail\Markdown;

class CronController extends Controller {

    public function endGuestUser() {

        $ids = \App\Models\GuestUser::where('to_date', date('Y-m-d'))->get()->pluck('id');
        if (!empty($ids)) {
            $response = \Http::asForm()->post('http://3.125.234.142:5000/api/remove_guest_user', [
                'guest_user_ids' => $ids,
            ]);
        } else {
            $response = \Http::asForm()->post('http://3.125.234.142:5000/api/remove_guest_user', [
                'guest_user_ids' => [],
            ]);
        }
    }

    public function testMail(Request $request) {

        if ($request->email == '') {
            return response()->json(['status' => 'error', 'data' => ['message' => 'Please Provide EMail'], 'code' => 400], 400);
        }
        if ($request->body == '') {
            return response()->json(['status' => 'error', 'data' => ['message' => 'Please Provide EMail Content'], 'code' => 400], 400);
        }

        try {
            $markdown = Container::getInstance()->make(Markdown::class);

            $html = $markdown->render('mails.test_mail', [
                'introLines' => [$request->body]
            ]);


            $mail = Mail::send(['html' => $html], array(
                            ), function($message) use ( $request) {
                        $message->to($request->email, env('APP_NAME'))
                                ->subject('Welcome Mail');
                    });

            return response()->json(['status' => 'error', 'data' => ['message' => 'Mail sent successfully.'], 'code' => 200], 200);
        } catch (Exception $ex) {
            return response()->json(['status' => 'error', 'data' => ['message' => $ex->getMessage()], 'code' => 400], 400);
        }
    }

}
