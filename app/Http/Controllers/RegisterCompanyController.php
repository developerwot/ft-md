<?php

namespace App\Http\Controllers;

use App\Http\Requests\Backend\CompanyStoreRequest;
use App\Http\Requests\Backend\CompanyUpdateRequest;
use App\Http\Controllers\Controller;
use App\Mail\CompanyRegister;
use App\Models\Company;
use App\Repositories\Backend\Company\CompanyContract;
use App\User;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\CompanyRegisterRequest;
use Mail;
class RegisterCompanyController extends Controller
{
    protected $repository;

    public function __construct(CompanyContract $repository)
    {
        $this->repository = $repository;
    }

    public function register() {
        return view('register_company');
    }
    public function registerCreate(CompanyRegisterRequest $request) {
        $data = $request->all();

        $findEmail = Company::where('email',$data['email'])->first();
        if(empty($findEmail)) {

            return redirect()->back()->withInput()->with('failure', 'Company Not allowed to register');;
        }

        $file = $request->file('logo');

        if($file) {
            $destinationPath = 'company-logo/';
            $originalFile = $file->getClientOriginalName();
            $filename= rand(1,100).$originalFile;
            $file->move($destinationPath, $filename);
        }
        $newPassword = $this->randomPassword();

        if(!empty($request->file('logo'))) {
            $data['logo'] = $filename;
        }
        else {
            $data['logo'] ='';
        }
        $user =  User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'status' => '1',
            'logo'=>$data['logo'],
            'password' => Hash::make($newPassword),
        ]);
        if($user) {
            $roles = ['company'];
            $user->syncRoles($roles);

            //Send User Mail
            Mail::to($data['email'])->send(new CompanyRegister($data['first_name'],$newPassword));

            if(!empty($user)) {
                $companyData =[
                    'name'=>$data['first_name'],
                    'last_name'=>$data['last_name'],
                    'email'=>$data['email'],
                    'business_name'=>$data['business_name'],
                    'address'=>$data['address'],
                    'city'=>$data['city'],
                    'postal_code'=>$data['postal_code'],
                    'country'=>$data['country'],
                    'vat'=>$data['vat'],
                    'status'=>1,
                    'user_id'=>$user->id
                ];

                $company = Company::find($findEmail->id)->update($companyData);
            }
        }

        return redirect('login')->with('success','Your company has been registered successfully. Please check your email');
    }

}
