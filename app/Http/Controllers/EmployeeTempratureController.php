<?php

namespace App\Http\Controllers;

use App\Http\Requests\Backend\EmpTemperatureStoreRequest;
use App\Http\Controllers\Controller;
use App\Mail\EmployeeHighTempAlert;
use App\Models\Company;
use App\Models\Employee;
use App\Models\EmployeeTemperature;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\EmpTemp\EmpTemperatureContract;
use Illuminate\Support\Facades\Log;
use Mail;

class EmployeeTempratureController extends Controller
{
    protected $repository;
    protected $company_id;

    public function __construct(EmpTemperatureContract $repository)
    {

        $this->repository = $repository;
    }

    public function index()
    {
        try {

            if (Auth::user()->hasRole('admin')) {
                $emp_temp_data =  $this->repository->getAll();
            }
            else {
                $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
                $this->company_id  = $getUserCompany->id;

                $emp_temp_data = $this->repository->getAllEmpTempByCompany($this->company_id);
            }

            $emp_temp = $emp_temp_data->filter(function ($item){
                        $item->first_name = $item->employee->first_name;
                        $item->last_name = $item->employee->last_name;
                        $item->date = date('d-M-Y',strtotime($item->created_at));
                return  $item->email = $item->employee->email;
            });
            return view('backend.emp_temp.index', compact('emp_temp'));

        } catch (\Exception $e) {
            Log::error('Empployee Temp management error:' . $e->getMessage());
            return redirect()->route('emp_temp.index')->with('failure', 'Something went wrong');
        }
    }


    public function store(Request $request)
    {
       // return response()->json(['name' => 'test']);

            //$input = $request->only('temperature', 'employee_id','api_key');
            //echo '<pre>';
            $input = json_decode($request->getContent(), true);
            if(!empty($input) ) {
                $data = $input;
                if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                    if(isset($data['employee_id']) && $data['employee_id'] !='') {
                        $isemployee = Employee::where('id',$data['employee_id'])->where('status',1)->first();
                        if(empty($isemployee)) {
                            return response()->json([
                                "status"=>0,
                                "message" => "People Not Found"
                            ], 404);
                        }


                        if(isset($data['temperature']) &&  $data['temperature'] !='') {
                            if(!is_numeric($data['temperature'])) {
                                    return response()->json([
                                        "status"=>0,
                                        "message" => "Temperature must be number"
                                    ], 404);
                             }
                             if(isset($data['temp_time']) && $data['temp_time'] !=''){

                                 $dateTime = strtotime($data['temp_time']);

                                 if(!$dateTime) {
                                     return response()->json([
                                         "status"=>0,
                                         "message" => "Temperature Time must be valid in datetime format"
                                     ], 404);
                                 }
                                 $is_guest=0;
                                 if(isset($data['is_guest_user']) && $data['is_guest_user']==1){
                                     $is_guest=1;
                                 }
                                 
                                 $data['temp_date'] = date('Y-m-d H:i:s',$dateTime);
                                 $temp_date = date('Y-m-d',$dateTime);
                                 if($is_guest){
                                 $getEmp = $this->repository->findByGuest($data['employee_id'],$temp_date) ;
                                 }else{
                                 $getEmp = $this->repository->findByEmployee($data['employee_id'],$temp_date) ;
                                 }

                                 //Send Admin Mail
                                 if($data['temperature'] >= 37.5) {
                                     $settings = Setting::where('company_id',$isemployee->company_id)->first();
                                     if( !empty($settings) ) {
                                        if( !empty($settings->hr_email) ) {
                                            Mail::to($settings->hr_email)->send(new EmployeeHighTempAlert($isemployee->first_name.' '.$isemployee->last_name,number_format($data['temperature'],2) + 0));
                                        }
                                     }
                                 }

                                 if(!empty($getEmp)) {
                                     $this->repository->update($getEmp->id,$data);
                                     return response()->json(['success' => true, "status"=>1,'message'=>'People Temperature updated successfully'], 202);
                                 }
                                 else {
                                     $emp_temp_data['is_guest'] = $is_guest;
                                     $emp_temp_data['employee_id'] = $data['employee_id'];
                                     $emp_temp_data['temperature'] = $data['temperature'];
                                     $emp_temp_data['temp_date'] = $data['temp_date'];
                                     $this->repository->store($emp_temp_data);
                                     return response()->json(['success' => true, "status"=>1,'message'=>'People Temperature store successfully'], 202);
                                 }
                             }
                             else {
                                 return response()->json([
                                     "status"=>0,
                                     "message" => "Temperature Time is required"
                                 ], 404);
                             }


                        }
                        else {
                            return response()->json([
                                "status"=>0,
                                "message" => " Temperature is required"
                            ], 404);
                        }
                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "People ID required"
                        ], 404);
                    }
                }
                else {
                    return response()->json([
                        "status"=>0,
                        "message" => "Api key not found"
                    ], 404);
                }
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Not Valid Data"
                ]);
            }


    }
}
