<?php

namespace App\Http\Controllers;

use App\Http\Requests\Backend\EmpTemperatureStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Employee;
use App\Repositories\Backend\ContactTrac\ContactTracContract;
use App\Repositories\Backend\ContactTrac\ContactTracRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DateTime;
class ContactTracingApiController extends Controller
{
    protected $repository;
    protected $company_id;

    public function __construct(ContactTracContract $repository)
    {

        $this->repository = $repository;
    }


    public function store(Request $request)
    {
       // return response()->json(['name' => 'test']);

            //$input = $request->only('temperature', 'employee_id','api_key');
            //echo '<pre>';
            $input = json_decode($request->getContent(), true);

            if(!empty($input)) {

                $data = $input;

                    if(isset($data['beaconId']) && $data['beaconId'] !='') {

                        $checkEmployee = $this->repository->findByBeaconID($data['beaconId']);
                        if(empty($checkEmployee)) {
                            return response()->json([
                                "status"=>0,
                                "message" => "People Not Found"
                            ], 200);
                        }
                        if(isset($data['time']) && $data['time'] != '') {

                            if($this->isTimestamp($data['time'])) {
                                if(isset($data['beacons']) &&  !empty($data['beacons'])) {

                                    $count = 0;
                                    foreach ($data['beacons'] as $device) {
                                        $contactTrac =[];
                                        $checkDeviceEmp = $this->repository->findByBeaconID($device);

                                        if(!empty($checkDeviceEmp)) {
                                            if($checkEmployee->id == $checkDeviceEmp->id) {
                                                continue;
                                            }
                                            $epoch = $data['time']/1000;
                                            $data['time_new'] =  Carbon::createFromTimestamp($epoch);

                                            $contactTrac['contact_time'] = date("Y-m-d H:i:s",strtotime($data['time_new']));
                                            $contactTrac['contact_date'] = date("Y-m-d",strtotime($data['time_new']));
                                            $contactDate = date("Y-m-d",strtotime($data['time_new']));
                                            /*if( $contactDate > date('Y-m-d') ) {
                                                return response()->json([
                                                    "status"=>0,
                                                    "message" => "Time is invalid"
                                                ], 200);
                                            }
                                            if( $contactDate < date('2019-m-d') ) {
                                                return response()->json([
                                                    "status"=>0,
                                                    "message" => "Time is invalid"
                                                ], 200);
                                            }*/
                                            $checkExisting = $this->repository->checkTracingByDate($checkEmployee->id,$checkDeviceEmp->id,$contactDate);
                                            if(!empty($checkExisting)) {
                                                $count++;
                                                echo $checkExisting->id;
                                                $this->repository->update($checkExisting->id,$contactTrac);
                                                if($checkExisting->employee_id == $checkEmployee->id) {
                                                    $contactTrac['employee_id'] = $checkEmployee->id;
                                                    $contactTrac['contact_employee_id'] = $checkDeviceEmp->id;
                                                    $this->repository->store_logs($contactTrac);
                                                }
                                                else {
                                                    $contactTrac['employee_id'] = $checkDeviceEmp->id;
                                                    $contactTrac['contact_employee_id'] = $checkEmployee->id;
                                                    $this->repository->store_logs($contactTrac);
                                                }


                                            }
                                            else {
                                                $count++;
                                                //Insert Device Data
                                                $contactTrac['employee_id'] = $checkEmployee->id;
                                                $contactTrac['contact_employee_id'] = $checkDeviceEmp->id;
                                                $this->repository->store($contactTrac);
                                            }
                                        }
                                    }
                                    if($count > 0) {
                                        return response()->json(['success' => true, "status"=>1,'message'=>$count.' Contact Tracing data added successfully'], 200);
                                    }
                                    else {
                                        return response()->json([
                                            "status"=>0,
                                            "message" => "Beacons People not found"
                                        ], 200);
                                    }
                                }
                                else {
                                    return response()->json([
                                        "status"=>0,
                                        "message" => "Beacons device required"
                                    ], 200);
                                }
                            }
                            else {
                                return response()->json([
                                    "status"=>0,
                                    "message" => "Time is invalid"
                                ], 200);
                            }

                        }
                        else {
                            return response()->json([
                                "status"=>0,
                                "message" => "Time is required"
                            ], 200);
                        }

                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "BeaconId is required"
                        ], 200);
                    }

            }
            else
            {
                return response()->json([
                    "status"=>0,
                    "message" => "Data is not valid"
                ], 200);
            }


    }

    public function edit($id)
    {
        try {
            $emp_temp = $this->repository->find($id);
            return view('backend.emp_temp.form', compact('emp_temp'));

        } catch (\Exception $e) {
            Log::error('Empployee Temp management error:' . $e->getMessage());
            return redirect()->route('emp_temp.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(EmpTemperatureStoreRequest $request, $id)
    {

        try {
            $input = $request->only('temperature', 'employee_id');
            $this->repository->update($id, $input);
            return redirect()->route('emp_temp.index')->with('success', 'Empployee Temp updated successfully');

        } catch (\Exception $e) {
            Log::error('Empployee Temp management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('emp_temp.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return redirect()->route('emp_temp.index')->with('success', 'Empployee Temp deleted successfully');

        } catch (\Exception $e) {
            Log::error('Empployee Temp management error:' . $e->getMessage());
            return redirect()->route('emp_temp.index')->with('failure', 'Something went wrong');
        }
    }
}
