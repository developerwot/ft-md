<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Backend\UserProfileRequest;
use App\Models\Company;
use App\Models\Setting;
use App\Repositories\Backend\Profile\ProfileContracts;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
class ProfileController extends Controller
{
    protected $repository;

    public function __construct(ProfileContracts $repository)
    {
        $this->repository = $repository;
    }

    public function editProfile()
    {
        try {
            $user = $this->repository->edit();
            $userCompany = Company::where('user_id',$user->id)->first();
            return view('backend.auth.profile', compact('user','userCompany'));
        } catch (\Exception $e) {
            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }
    }
    public function showProfile()
    {
        try {
            $user = $this->repository->edit();
            $userCompany = Company::where('user_id',$user->id)->first();
           // dd($user);
            return view('backend.auth.company_profile', compact('user','userCompany'));
        } catch (\Exception $e) {
            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }
    }

    public function updateProfile(UserProfileRequest $request)
    {

        try {

            $input = $request->only('first_name', 'last_name','phone','business_name','profile_avatar','profile_avatar_remove');

//            $checkExist = User::where('email',$input['email'])->where('id','!=',auth()->user()->id)->first();
//            if(!empty($checkExist)) {
//                return redirect()->back()->withInput()->with('failure', 'Email already Exists');
//            }

            $file = $request->file('profile_avatar');
            if($file) {
                $destinationPath = 'company-logo/';
                $originalFile = $file->getClientOriginalName();
                $filename= rand(1,100).$originalFile;
                $file->move($destinationPath, $filename);
            }
            $user = $this->repository->edit(Auth::user()->id);

            if(!empty($request->file('profile_avatar'))) {
                $input['logo'] = $filename;

                if(!empty($user->logo)) {

                    File::delete(public_path('company-logo/'. $user->logo));

                }
            }
            if($input['profile_avatar_remove'] == "1" ){
                if(!empty($user->logo)) {

                    File::delete(public_path('company-logo/'. $user->logo));

                }
                $input['logo'] = '';
            }

            $userupdate = $this->repository->update($input);

            if(Auth::user()->hasRole(['company'])) {

                $userCompany = Company::where('user_id',auth()->id())->first();

                $companyData['name'] = $input['first_name'];
                $companyData['last_name'] = $input['last_name'];
//                $companyData['email'] = $input['email'];
                $companyData['business_name'] = $input['business_name'];

                $company = Company::find($userCompany->id)->update($companyData);

            }
            return redirect()->route('edit.profile')->with('success', 'Profile updated successfully');

        } catch (\Exception $e) {

            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }

    }
    public function changePassword()
    {
        $user = $this->repository->edit();
        return view('backend.auth.change_password', compact('user'));
    }
    public function updatePassword(Request $request)
    {
        try {

            $validatedData = Validator::make($request->all(),[
                'new_password' => 'min:8|required_with:confirm_password',
            ]);
            if ($validatedData->fails()) {
                return redirect()->back()
                    ->withErrors($validatedData)
                    ->withInput();
            }
            $input =  $input = $request->only('new_password');

            $data['password'] =$input['new_password'];
            $user = $this->repository->update($data);
            return redirect()->route('view.profile')->with('success', 'Password updated successfully');
        }
        catch (\Exception $e) {
            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }
    }
    public function editAddress() {
        $user = $this->repository->edit();
        $userCompany = Company::where('user_id',$user->id)->first();
        return view('backend.auth.change_address', compact('user','userCompany'));
    }
    public function updateAddress(Request $request)
    {

        try {
            $input = $request->only('address', 'city','postal_code','country','vat');

            if(Auth::user()->hasRole(['company'])) {

                $userCompany = Company::where('user_id',auth()->id())->first();
                $company = Company::find($userCompany->id)->update($input);
                return redirect()->route('view.profile')->with('success', 'Address updated successfully');
            }


        } catch (\Exception $e) {

            Log::error('Profile error:'.$e->getMessage());
            return redirect()->back()->with('failure', 'Something went wrong');
        }

    }
    public function settings() {
        $user = $this->repository->edit();
        $userCompany = Company::where('user_id',$user->id)->first();
        $settings = Setting::where('company_id',$userCompany->id)->first();
        return view('backend.auth.settings', compact('user','userCompany','settings'));
    }
    public function updateSettings(Request $request) {
        $input = $request->only(
            'hr_email',
                 'low',
                 'medium',
                 'high',
                 'low_color',
                 'medium_color',
                 'high_color',
                'guest_user_link'
                );

        $userCompany = Company::where('user_id',auth()->id())->first();
        if(!empty($input['hr_email'])) {

            $settings = Setting::where('company_id',$userCompany->id)->first();
            if(!empty($settings)) {
                Setting::find($settings->id)->update($input);
            }
            else {
                $input['company_id'] = $userCompany->id;
                $setting = Setting::create($input);
            }
            return redirect()->route('view.profile')->with('success', 'Setting updated successfully');
        }
    }
}
