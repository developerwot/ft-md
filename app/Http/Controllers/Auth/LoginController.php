<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest','checkstatus'])->except('logout');
    }

    public function showLoginForm()
    {
        return view('backend.auth.login');
    }
   /* public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            dd(Auth::user());
            return redirect()->intended('dashboard');
        }
    }*/
    public function login(Request $request)
    {
        $this->validateLogin($request);
        $credentials = $request->only('email', 'password');


        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

      /*  if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }*/
        if (!$status = Auth::attempt($credentials)) {
            return redirect()->route('login')->with('failure','Invalid Credential');
        }
        else {
            $user = auth()->user();
        }
        if(auth()->user()->hasRole('employee')){
            Auth::logout();
            return redirect()->route('login')->with('failure','Not authorised! try login using app');
        }

        if ($user->status == 0) {
            Auth::logout();
            return redirect()->route('login')->with('failure','Your Account is not active,Contact technical support');
        }



        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('/login');
    }
}
