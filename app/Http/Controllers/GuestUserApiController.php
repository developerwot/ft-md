<?php

namespace App\Http\Controllers;

use App\Http\Requests\GuestUserInvitationRequest;
use App\Http\Requests\GuestUserListRequest;
use App\Http\Requests\GuestUserStoreRequest;
use App\Http\Requests\GuestUserVerfyRequest;
use App\Http\Requests\GuestUserDocumentStoreRequest;
use App\Mail\GuestUserInvite;
use App\Models\Company;
use App\Models\GuestUserDocuments;
use App\Models\Setting;
use App\Repositories\Backend\GuestUser\GuestUserContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Mail;

class GuestUserApiController extends Controller
{
    protected $repository;
    protected $company_id;

    public function __construct(GuestUserContract $repository)
    {

        $this->repository = $repository;
    }

    public function store(GuestUserStoreRequest $request)
    {
       // return response()->json(['name' => 'test']);

            //$input = $request->only('temperature', 'employee_id','api_key');
            //echo '<pre>';
            $input = json_decode($request->getContent(), true);

            if(!empty($input) ) {
                $data = $input;
                if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                    if(isset($data['company_id']) && $data['company_id'] !='') {
                        $getCompany = Company::where('id', $data['company_id'])->where('status',1)->first();
                        if(empty($getCompany)) {
                            return response()->json([
                                "status"=>0,
                                "message" => "Company Not Found"
                            ], 422);
                        }

                            $checkEmployee = $this->repository->checkEmployee($data['receiver_employee_id'],$data['company_id']);
                            if(empty($checkEmployee)) {
                                return response()->json([
                                    "status"=>0,
                                    "message" => "Receiver Employee Not Found"
                                ], 422);
                            }
                            
                            $checkIsCurrentEmployee = $this->repository->checkEmployeeByEmail($data['email_address'],$data['company_id']);
                            if(!empty($checkIsCurrentEmployee)) {
                                return response()->json([
                                    "status"=>0,
                                    "message" => "Employee already exists"
                                ], 422);
                            }

                            $start_date  = date('Y-m-d',strtotime($data['from_date']));

                            if($start_date < date("Y-m-d")) {
                                return response()->json([
                                    "status"=>0,
                                    "message" => "From date is invalid"
                                ], 422);
                            }


                            $checkEmailUnique = $this->repository->checkEmail($data['email_address'],$data['from_date'],$data['to_date']);

                            if(empty($checkEmailUnique)) {
                                $guestUserData['company_id'] = $getCompany->id;
                                $guestUserData['receiver_employee_id'] = $data['receiver_employee_id'];
                                $guestUserData['email_address'] = $data['email_address'];
                                $guestUserData['is_first_name'] = $data['is_first_name'];
                                $guestUserData['is_last_name'] = $data['is_last_name'];
                                $guestUserData['is_dob'] = $data['is_dob'];
                                $guestUserData['is_address'] = $data['is_address'];
                                $guestUserData['is_city'] = $data['is_city'];
                                $guestUserData['is_postal_code'] = $data['is_postal_code'];
                                $guestUserData['is_telephone_number'] = $data['is_telephone_number'];
                                $guestUserData['is_identity_proof'] = $data['is_identity_proof'];
                                $guestUserData['is_address_proof'] = $data['is_address_proof'];
                                $guestUserData['is_company_name'] = $data['is_company_name'];
                                if(isset($data['instructions_upon_arrival']) && $data['instructions_upon_arrival'] != '' && $data['instructions_upon_arrival'] != null ) {
                                    $guestUserData['instructions_upon_arrival'] = $data['instructions_upon_arrival'];
                                }
                                else {
                                    $guestUserData['instructions_upon_arrival'] ='';
                                }

                                $guestUserData['from_date'] = (isset($data['from_date'])) ? date("Y-m-d",strtotime($data['from_date'])) :'';
                                $guestUserData['to_date'] = (isset($data['to_date'])) ? date("Y-m-d",strtotime($data['to_date'])) :'';
                                $guestUserData['verify_code'] = $this->randomPassword(5);
                                 $this->repository->store($guestUserData);

                                //Sent mail for code
                                //Send Admin Mail

                                $settings = Setting::where('company_id',$data['company_id'])->first();
                                if( !empty($settings) ) {
//                                    if( !empty($settings->guest_user_link) ) {
                                        $guest_user_link=$settings->guest_user_link?$settings->guest_user_link:env('GUEST_USER_LINK');
                                        $company = Company::where('id', $data['company_id'])->with(['settings'])->where('status',1)->first();
                                        Mail::to($data['email_address'])->send(new GuestUserInvite($data['email_address'],$guestUserData['verify_code'],$guest_user_link,$company));
//                                    }
                                }


                                return response()->json([
                                    "status"=>1,
                                    "message" => "Guest User Added Successfully"
                                ], 200);

                            }
                            else {
                                return response()->json([
                                    "status"=>0,
                                    "message" => "Email address already exists for Date "
                                ], 422);
                            }

                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Company ID required"
                        ], 422);
                    }
                }
                else {
                    return response()->json([
                        "status"=>0,
                        "message" => "Api key not found"
                    ], 422);
                }
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Not Valid Data"
                ],422);
            }
    }
    public function verify(GuestUserVerfyRequest $request)
    {
        $input = json_decode($request->getContent(), true);

        if(!empty($input) ) {
            $data = $input;
            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                   $current_date = date("Y-m-d");
                   $verify = $this->repository->findByCode($data['verify_code']);
                    if(!empty($verify)) {

                        if($verify->is_completed == 1 ) {
                            return response()->json([
                                "status"=>0,
                                "message" => "Otp is already used"
                            ], 422);
                        }
                        else {
                            return response()->json(['success' => true, "status"=>1,'data'=>$verify->toArray()], 200);
                        }
                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Invalid verify code"
                        ], 422);
                    }
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 422);
            }
        }
        else {
            return response()->json([
                "status"=>0,
                "message" => "Not Valid Data"
            ],422);
        }
    }

    public function existing_data(Request $request)
    {
        $input = json_decode($request->getContent(), true);

        if(!empty($input) ) {
            $data = $input;
            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                    if(isset($data['email_address']) && !empty($data['email_address']) && $data['email_address'] != null) {
                        $checkGuestUser = $this->repository->findAllFieldByEmail($data['email_address']);
                        if(!empty($checkGuestUser)) {
                            return response()->json(['success' => true, "status"=>1,'data'=>$checkGuestUser], 200);
                        }
                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Email address is required"
                        ], 422);
                    }
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 422);
            }
        }
        else {
            return response()->json([
                "status"=>0,
                "message" => "Not Valid Data"
            ],422);
        }
    }

    public function complete(GuestUserVerfyRequest $request)
    {
        $input = json_decode($request->getContent(), true);

        if(!empty($input) ) {
            $data = $input;
            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                   $current_date = date("Y-m-d");
                   $verify = $this->repository->findByCode($data['verify_code']);
                    if(!empty($verify) ) {
                        $updateArray['is_completed'] = 1;
                        $updateArray['completed_at'] = date("Y-m-d H:i:s");
                        $verifyUpdate = $this->repository->update($verify->id,$updateArray);
                        return response()->json(['success' => true, "status"=>1,'data'=>$verifyUpdate->toArray()], 200);
                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Invalid verify code"
                        ], 422);
                    }


            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 422);
            }
        }
        else {
            return response()->json([
                "status"=>0,
                "message" => "Not Valid Data"
            ],422);
        }
    }

    public function document_upload(GuestUserDocumentStoreRequest $request)
    {
        try {
            $input = $request->only('guest_user_id','api_key');
            if(!empty($input) ) {
                $data = $input;
                if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {

                    if(isset($data['guest_user_id']) && $data['guest_user_id'] !='') {

                        $checkGuestUser = $this->repository->find($data['guest_user_id']);

                        if($checkGuestUser) {

                            //Process uploaded image
                            $address_proof_file = $request->file('address_proof');
                            $identity_proof_file = $request->file('identity_proof');


                            if(!empty($address_proof_file)) {
                                $address_filename = time() .'-'. $address_proof_file->getClientOriginalName();
                                $filePath1 = 'guest_user_documents/'.$checkGuestUser->id.'/' . $address_filename;
                                Storage::disk('s3')->put($filePath1, file_get_contents($address_proof_file),"public");
                            }
                            if(!empty($identity_proof_file)) {
                                $identity_filename = time() .'-'. $identity_proof_file->getClientOriginalName();
                                $filePath2 = 'guest_user_documents/'.$checkGuestUser->id.'/'  . $identity_filename;
                                Storage::disk('s3')->put($filePath2, file_get_contents($identity_proof_file),"public");
                            }

                            $guest_user_data = [];
                            $guest_user_data['guest_user_id'] = $checkGuestUser->id;
                            if( !empty($address_filename) ) {
                                $guest_user_data['address_proof'] = $address_filename;
                            }
                            if( !empty($identity_filename) ) {
                                $guest_user_data['identity_proof'] = $identity_filename;
                            }
                            //dd($guest_user_data);


                            if( !empty($address_filename) || !empty($identity_filename) ) {

                                $checkExistingDocuments = GuestUserDocuments::where("guest_user_id",$checkGuestUser->id)->first();
                                if(!empty($checkExistingDocuments)) {
                                    $update = $checkExistingDocuments->update($guest_user_data);
                                    $guestUserDoc = GuestUserDocuments::find($checkExistingDocuments->id);
                                    $message ='Guest User document uploaded successfully';
                                }
                                else {
                                    $guestUserDoc =  GuestUserDocuments::create($guest_user_data);
                                    $message ='Guest User document uploaded successfully';
                                }

                                if(!empty($guestUserDoc)) {
                                    $guestUserDoc = GuestUserDocuments::find($guestUserDoc->id);

                                    $s3url = 'https://'.env('AWS_BUCKET').'.s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' ;
                                    $files = Storage::disk('s3')->files('guest_user_documents/'.$checkGuestUser->id.'/');
                                    if(!empty($files)) {
                                        foreach ($files as $file) {
                                            if(!empty($guestUserDoc->address_proof)) {
                                                if(str_contains($file,$guestUserDoc->address_proof)) {
                                                    $guestUserDoc->address_proof = $s3url.$file;
                                                }

                                            }
                                            if(!empty($guestUserDoc->identity_proof)) {
                                                if(str_contains($file,$guestUserDoc->identity_proof)) {
                                                    $guestUserDoc->identity_proof = $s3url.$file;
                                                }

                                            }

                                        }
                                    }

                                    return response()->json([
                                        "status" => 1,
                                        "success" => true,
                                        "message"=>$message,
                                        "data"=>$guestUserDoc->toArray()
                                    ], 200);
                                }
                            }
                            else {
                                return response()->json([
                                    "status" => 0,
                                    "message" => "No documents found"
                                ], 422);
                            }


                        }
                        else {
                            return response()->json([
                                "status" => 0,
                                "message" => "Guest User Not Found"
                            ], 422);
                        }

                    }
                    else {
                        return response()->json([
                            "status"=>0,
                            "message" => "Guest user id is required"
                        ], 422);
                    }

                }
                else {
                    return response()->json([
                        "status"=>0,
                        "message" => "Api key not found"
                    ], 422);
                }
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Not Valid Data"
                ],422);
            }
        }
          catch (\Exception $e) {
            Log::error('Guest User document upload error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
              return response()->json([
                  "status" => 0,
                  "message" => "Guest User Not Found"
              ], 422);
            }

    }
    public function checkBoolen($value) {
        if(isset($value) && $value !='' && $value != null && in_array($value,[0,1])) {
            return true;
        }
        else {
            return false;
        }
    }
    public function invitation(GuestUserInvitationRequest $request) {
        $input = json_decode($request->getContent(), true);

        if(!empty($input) ) {
            $data = $input;
            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                $guest_user_invitations_data = $this->repository->findByEmail($data['email_address']);
                $guest_user_invitations = $guest_user_invitations_data->filter(function ($item){

                    if($item->company) {
                        $item->company_name = $item->company->business_name;
                    }
                    unset($item->company);
                    $item->completed_at = date('d-M-Y H:i A',strtotime($item->completed_at));
                    $item->from_date = date('d-M-Y',strtotime($item->from_date));
                    $item->to_date = date('d-M-Y',strtotime($item->to_date));
                    return  $item;

                    return $item;

                });
                return response()->json(['success' => true, "status"=>1,'data'=>$guest_user_invitations->toArray()], 200);
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 422);
            }
        }
        else {
            return response()->json([
                "status"=>0,
                "message" => "Not Valid Data"
            ],422);
        }

    }

    public function guest_users_lists(GuestUserListRequest $request) {
        $input = json_decode($request->getContent(), true);

        if(!empty($input) ) {
            $data = $input;
            if(isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                $current_date = date("Y-m-d");
                //$current_date = date("Y-m-d",strtotime(date("2020-09-15")));
                $guest_user_lists = $this->repository->getAllGuestUsersByCompany($data['company_id'],$current_date);
                return response()->json(['success' => true, "status"=>1,'data'=>$guest_user_lists->toArray()], 200);
            }
            else {
                return response()->json([
                    "status"=>0,
                    "message" => "Api key not found"
                ], 422);
            }
        }
        else {
            return response()->json([
                "status"=>0,
                "message" => "Not Valid Data"
            ],422);
        }
    }
}
