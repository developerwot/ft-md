<?php

namespace App\Http\Controllers;

use App\Mail\InviteEmployeeMail;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmployeeInviteApiController extends Controller
{
    public function getEmployees(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
                try {
                    $compnay = Company::findOrFail($data['company_id']);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => 0,
                        "message" => "Company Not Found"
                    ], 422);
                }
                try {
                    $compnay = Employee::findOrFail($data['employee_id']);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => 0,
                        "message" => "Employee Not Found"
                    ], 422);
                }
               $employees= Employee::where('company_id',$data['company_id'])->where('id','!=',$data['employee_id'])->get(['id','first_name','last_name','email']);
                    return response()->json([
                        'success' => true,
                        'status' => 1,
                        'message' => 'Employees fetched',
                        'employees' => $employees
                    ], 200);
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }

    public function sendInvitations(Request $request){
        $input = json_decode($request->getContent(), true);
        if (!empty($input)) {
            $data = $input;

            if (isset($data['api_key']) && $data['api_key'] == env('REST_API_KEY')) {
               $appLink= Setting::where('company_id',$data['company_id'])->value('guest_user_link');
//               dd($data['employee_ids']);
                foreach ($data['employee_ids'] as $id){
                  $detail=  Employee::where('id',$id)->where('company_id',$data['company_id'])->first(['email','first_name']);
                  Mail::to($detail->email)->send(new InviteEmployeeMail($detail->first_name,$appLink));
                }
                return response()->json([
                    'success' => true,
                    'status' => 1,
                    'message' => 'Invitation sent to Employees',
                ], 200);
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Api key not found"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Not Valid data"
            ], 422);
        }
    }
}
