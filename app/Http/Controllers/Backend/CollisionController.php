<?php

namespace App\Http\Controllers\Backend;

use App\CollisionAccess;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CollisionController extends Controller
{
    public function index(){
        return view('backend.collision.index');
    }

    public function tableAjax(Request $request)
    {
        //        $collisions=  CollisionAccess::with('fromUser')->where('from_user','171')->get();
        if (!isset($request->date)) {
            $collisions = CollisionAccess::
            //            ->get();
            with('fromUser', 'toUser')->whereHas('fromUser', function ($q) {
                $q->where('deleted_at', '=', null);
            })->whereHas('toUser', function ($q) {
                $q->where('deleted_at', '=', null);
            })->groupBy('from_user')->get();
        }
        else {
            $collisions = CollisionAccess::with('fromUser', 'toUser')
                ->whereDate('contacted_at', Carbon::parse($request->date)->format('Y-m-d'))
                ->with('fromUser', 'toUser')
                ->whereHas('fromUser', function ($q) {
                    $q->where('deleted_at', '=', null);
                })
                ->whereHas('toUser', function ($q) {
                    $q->where('deleted_at', '=', null);
                })
                ->orderBy('id', 'desc')
                ->groupBy('from_user')
                ->get();
        }
        //       dd(collect($collisions)->toArray());
        //        with(array('fromUser'=>function($query){
        //            $query->select('id','first_name');
        //        }))->with(array('toUser'=>function($query){
        //            $query->select('id','first_name');
        //        }))
        $colllect = collect($collisions)->where('fromUser.company_id', auth()->user()->company->id)->values();
        //       dd($colllect);
        //dd($colllect);
        return response()->json([
            'status' => 1,
            'data'   => $colllect
        ]);
    }

    public function details($id,$date){
//dd($date);
//       dd($data);
return view('backend.collision.view',compact('id','date'));
    }
    public function viewTable(Request $request){
//        dd($request->all());
        $data= CollisionAccess::with('toUser')->where('from_user',$request->id)->whereDate('contacted_at',Carbon::parse($request->date))->get();
//        dd(collect($data)->where());
        return response()->json(['status'=>1,'data'=>$data]);
    }
}
