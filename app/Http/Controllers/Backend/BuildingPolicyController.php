<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BuildingPolicyStoreRequest;
use App\Http\Requests\Backend\DevicePeopleStoreRequest;
use App\Models\Company;
use App\Repositories\Backend\BuildingPolicy\BuildingPolicyContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BuildingPolicyController extends Controller {

    protected $repository;

    public function __construct(BuildingPolicyContract $repository) {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index() {
        try {

            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
//            $building_policy_data = $this->repository->getAllByCompany($getUserCompany->id);
            $building_policy_data = \App\Models\BuildingPolicy::selectRaw('building_access_polices.*')->with(['group'])
                        ->leftJoin('buildings', 'buildings.id', '=', 'building_access_polices.building_id')
                    ->where('buildings.company_id', $getUserCompany->id)->get();

            $building_policy = $building_policy_data->filter(function ($item) {
                $item->policy_content = substr($item->policy, 0, 100);
                if (strlen($item->policy) > 100) {
                    $item->policy_content = $item->policy_content . '...';
                }
                if ($item->always_range=='date_range') {
                    $item->always_range='Date Range';
                }
                if ($item->group) {
                    $item->group_name=$item->group->name;
                }else{
                    $item->group_name='';
                }
                if ($item->building) {
                    $item->building_name = $item->building->name;
                } else {
                    $item->building_name = '';
                }
                return $item;
            });
            return view('backend.building_policy.index', compact('building_policy'));
        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }

    public function create() {
        $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
        if (!empty($getUserCompany)) {

            $buildings = $this->repository->getAllBuildingByCompany($getUserCompany->id);
            $groups = \App\Models\Group::where('company_id', $getUserCompany->id)->get();
            return view('backend.building_policy.form', compact('buildings', 'groups'));
        } else {
            return redirect()->route('dashboard')->with('failure', 'Company Not Found');
        }
    }

    public function store(BuildingPolicyStoreRequest $request) {

        try {
            $input = $request->only('policy', 'building_id', 'group_id', 'permit', 'always_range');
            if ($input['always_range'] != 'always') {
                $rules['date_range'] = ['required'];
                $rules['time_range'] = ['required'];


                $validator = Validator::make($request->all(), $rules);

                if ($validator->fails()) {
                    return redirect()->route('building_policy.create')
                                    ->withErrors($validator)
                                    ->withInput();
                }

                $drange = explode('-', $request->date_range);
                $trange = explode('-', $request->time_range);
                $input['from_date'] = date('Y-m-d', strtotime(trim($drange[0])));
                $input['to_date'] = date('Y-m-d', strtotime(trim($drange[1])));
                $input['from_hour'] = trim($trange[0]);
                $input['to_hour'] = trim($trange[1]);
            }
            $device = $this->repository->store($input);
            return redirect()->route('building_policy.index')->with('success', 'New Building Policy added successfully');
        } catch (\Exception $e) {
            Log::error('Building Policy management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('building_policy.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id) {
        try {
            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
            $building_policy = $this->repository->find($id);
            $buildings = $this->repository->getAllBuildingByCompany($getUserCompany->id);
            $groups = \App\Models\Group::where('company_id', $getUserCompany->id)->get();
            return view('backend.building_policy.form', compact('building_policy', 'buildings', 'groups'));
        } catch (\Exception $e) {
            Log::error('Building Policy management error:' . $e->getMessage());
            return redirect()->route('building_policy.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(BuildingPolicyStoreRequest $request, $id) {

        try {
            $input = $request->only('policy', 'building_id', 'group_id', 'permit', 'always_range');
            if ($input['always_range'] != 'always') {
                $rules['date_range'] = ['required'];
                $rules['time_range'] = ['required'];


                $validator = Validator::make($request->all(), $rules);

                if ($validator->fails()) {
                    return redirect()->route('building_policy.edit', $id)
                                    ->withErrors($validator)
                                    ->withInput();
                }

                $drange = explode('-', $request->date_range);
                $trange = explode('-', $request->time_range);
                $input['from_date'] = date('Y-m-d', strtotime(trim($drange[0])));
                $input['to_date'] = date('Y-m-d', strtotime(trim($drange[1])));
                $input['from_hour'] = trim($trange[0]);
                $input['to_hour'] = trim($trange[1]);
            }
            $building_policy = $this->repository->find($id);

            $this->repository->update($id, $input);

            return redirect()->route('building_policy.index')->with('success', 'Building Policy updated successfully');
        } catch (\Exception $e) {
            Log::error('Building Policy management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('building_policy.index')->with('Something went wrong');
        }
    }

    public function destroy($id) {
        try {
            $building_policy = $this->repository->find($id);
            if ($building_policy) {
                $this->repository->delete($id);
            }
            return redirect()->route('building_policy.index')->with('success', 'Building Policy deleted successfully');
        } catch (\Exception $e) {
            Log::error('Building Policy management error:' . $e->getMessage());
            return redirect()->route('building_policy.index')->with('failure', 'Something went wrong');
        }
    }

}
