<?php

namespace App\Http\Controllers\Backend;

use App\Exports\EmployeeExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\EmployeeUpdateRequest;
use App\Imports\EmployeeImport;
use App\Mail\CompanyRegister;
use App\Mail\EmployeeBlock;
use App\Mail\EmployeeRegister;
use App\Mail\GuestUserInvite;
use App\Models\Booking;
use App\Models\Company;
use App\Models\ContactTracing;
use App\Models\DevicePeople;
use App\Models\Employee;
use App\Models\EmployeeTemperature;
use App\Models\EmployeeTemperatureLog;
use App\Models\GroupEmployee;
use App\Models\PeopleAttendance;
use App\Models\PeopleRegistry;
use App\Models\Setting;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Backend\Employee\EmployeeContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;

class EmployeeController extends Controller {

    protected $repository;

    public function __construct(EmployeeContract $repository) {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index() {
        try {
            return view('backend.employee.index');
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }

    public function get_ajax_data(Request $request) {
        try {
            $inputs = $request->only('query');
            $search = '';
            if (!empty($inputs['query'])) {
                if (isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $search = $inputs['query']['generalSearch'];
                }
            }
            if (Auth::user()->hasRole('admin')) {
                $employees_data = \App\Models\Employee::with(['company', 'groups'])
                                ->where(function($query) use ($search) {
                                    $query->where('first_name', 'LIKE', "%$search%");
                                    $query->orWhere(function ($query) use ($search) {
                                        $query->where('last_name', 'LIKE', "%$search%");
                                    });
                                    $query->orWhere(function ($query) use ($search) {
                                        $query->where('email', 'LIKE', "%$search%");
                                    });
                                })
                                ->orderBy('id', 'desc')->latest()->get();
//                if (!empty($searchKey)) {
//                    $employees_data = $this->repository->getAll($searchKey);
//                } else {
//                    $employees_data = $this->repository->getAll();
//                }
            } else if (Auth::user()->hasRole('company')) {
                
                $getUserCompany = Company::where('user_id', Auth::user()->id)->first();

                $employees_data = \App\Models\Employee::with(['company', 'groups'])
                                ->where('company_id', $getUserCompany->id)
                                ->where(function($query) use ($search) {
                                    $query->where('first_name', 'LIKE', "%$search%");
                                    $query->orWhere(function ($query) use ($search) {
                                        $query->where('last_name', 'LIKE', "%$search%");
                                    });
                                    $query->orWhere(function ($query) use ($search) {
                                        $query->where('email', 'LIKE', "%$search%");
                                    });
                                })
                                ->orderBy('id', 'desc')->latest()->get();
                                
//                if (!empty($searchKey)) {
//                    $employees_data = $this->repository->getAllByCompany($getUserCompany->id, $searchKey);
//                } else {
//                    $employees_data = $this->repository->getAllByCompany($getUserCompany->id);
//                }
            }
            $employees = [];
            if (!empty($employees_data)) {
                $employees = $employees_data->filter(function ($item) {
                    $item->emp_id = $item->id;
                    $item->group = implode(",", $item->groups->pluck('name')->toArray());
                    return $item->business_name = $item->company->business_name;
                });
            }
            /*  $perpeage =10;
              $page = 1; */
            $meta = [];
            /* $meta = [ "page"=> $page,
              "pages"=> 35,
              "perpage"=> $perpeage,
              "total"=> $employees->count(),
              ]; */

            return response()->json(['status' => 'success', 'data' => $employees, 'code' => 200], 200);
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return '';
        }
    }

    public function create() {
        $getUserCompany = Company::where('user_id', Auth::user()->id)->first();

        $data['groups'] = \App\Models\Group::where('company_id', $getUserCompany->id)->get();
        $Employees=Employee::where('company_id',auth()->user()->company->id)->get()->count();
        $data['Employees']=$Employees;

        return view('backend.employee.form', $data);
    }

    public function store(EmployeeUpdateRequest $request) {
        $request->validate([
            'password' => !empty($request->password) ? 'confirmed|min:6' :'',
        ]);

        try {
           $existing_employee= Employee::where('company_id',auth()->user()->company->id)->get()->count();
         if(auth()->user()->company->employee_limit!=null){
             if($existing_employee>=auth()->user()->company->employee_limit){
                 return redirect()->back()->withInput()->with('failure','Employees Limit reached');
             }
         }
            $input = $request->only('first_name', 'last_name', 'email', 'status','password');
            $existedUser = User::where('email', $input['email'])->first();

            if (empty($existedUser)) {
                $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
//                dd($getUserCompany);
                if (!empty($getUserCompany)) {
                    $user_input = $input;
            if($user_input['password']==null){
                $newPassword = 'intellyaccess@123';
                $input['password']=$newPassword;
                $user_input['password'] = Hash::make($newPassword);
            }
//            $digits=checkUser();
//            $user_input['minor_id']=$digits['minor'];
//            $user_input['major_id']=$digits['major'];
                    $user_input['password']=Hash::make($user_input['password']);
                    $user = User::create($user_input);

                   $link= Setting::where('company_id',$getUserCompany->id)->value('guest_user_link');
                    if (!empty($user)) {
                        $roles = ['employee'];
                        $user->syncRoles($roles);
                        $input['company_id'] = $getUserCompany->id;
                        $input['user_id'] = $user->id;
                        $employee = $this->repository->store($input);
if(isset($request->group_id)) {
    foreach ($request->group_id as $g) {
        GroupEmployee::create([
            'employee_id' => $employee->id,
            'group_id' => $g,
        ]);
    }
}

                        \Illuminate\Support\Facades\Mail::to($input['email'])->send(new EmployeeRegister($input['email'],$input['password'],$link,$getUserCompany));
                        return redirect()->route('employee.index')->with('success', 'New People added successfully');
                    }
                }
            } else {
                return redirect()->back()->withInput()->with('failure', 'Already registered User');
            }
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('employee.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id) {
        try {

//            $employee = $this->repository->find($id);
            $employee = \App\Models\Employee::with(['groups'])->where('id', $id)->first();
            if ($employee->groups->pluck('id')->count() > 0) {
                $employee->groups2 = $employee->groups->pluck('id')->toArray();
            } else {
                $employee->groups2 = [];
            }
            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
            $groups = \App\Models\Group::where('company_id', $getUserCompany->id)->get();
            return view('backend.employee.form', compact('employee', 'groups'));
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('employee.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(EmployeeUpdateRequest $request, $id) {
        $request->validate([
            'password' => !empty($request->password) ? 'confirmed|min:6' :'',
        ]);
        try {
            $input = $request->only('first_name', 'last_name', 'email', 'status','password');

            $employee = $this->repository->find($id);
            $existedUser = User::where('email', $input['email'])->where("id", '!=', $employee->user_id)->first();
            if (empty($existedUser)) {

                $user = User::find($employee->user_id);
                if ($user) {
                    if($input['password']!=null){
                        $pass=$input['password'];
                        $input['password']=Hash::make($input['password']);
                        $user->update($input);
                        $link=Setting::where('company_id',$employee->company_id)->value('guest_user_link');
                        $getUserCompany=  Company::where('id',$employee->company_id)->first();
                        \Illuminate\Support\Facades\Mail::to($input['email'])->send(new EmployeeRegister($input['email'],$pass,$link,$getUserCompany));
                    }else{
                        unset($input['password']);
                        $user->update($input);
                    }
                }
                unset($input['password']);
                $this->repository->update($id, $input);
                GroupEmployee::where('employee_id', $id)->delete();
                if(isset($request->group_id)){
                    foreach ($request->group_id as $g) {
                        GroupEmployee::create([
                            'employee_id' => $id,
                            'group_id' => $g,
                        ]);
                    }
                }

                if ($input['status'] == 0) {
                    //Send User Mail
                    Mail::to($employee->email)->send(new EmployeeBlock($employee->first_name . ' ' . $employee->last_name));
                }
                return redirect()->route('employee.index')->with('success', 'People updated successfully');
            } else {

                return redirect()->back()->withInput()->with('failure', 'Already registered User');
            }
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('employee.index')->with('Something went wrong');
        }
    }

    public function destroy($id) {
        try {
            $emp = $this->repository->find($id);
            if ($emp) {
                $userid = User::find($emp->user_id);
                if ($userid) {
                    $client = new Client();
                    $res = $client->request('POST', 'http://3.125.234.142:5000/api/remove_user', [
                            "user_ids"=>$userid,
                             "company_id"=>$emp->company_id

                    ]);
                    $status=$res->getStatusCode();
                    if($status!=200){
                        return redirect()->route('employee.index')->with('failure', 'something went wrong');
                    }
                    User::destroy($emp->user_id);
                }
                $this->repository->delete($id);
                return redirect()->route('employee.index')->with('success', 'People deleted successfully');
            } else {
                return redirect()->route('employee.index')->with('failure', 'No People found');
            }
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('employee.index')->with('failure', 'Something went wrong');
        }
    }

    public function block($id) {
        try {
            $emp = $this->repository->find($id);
            $user = User::find($emp->user_id);
            if ($emp) {
                if ($emp->status == 1) {
                    $emp->update(['status' => 0]);
                    if ($user) {
                        $user->update(['status' => 0]);
                    }
                    //Send User Mail
                    Mail::to($emp->email)->send(new EmployeeBlock($emp->first_name . ' ' . $emp->last_name));
                } else {
                    $emp->update(['status' => 1]);
                    if ($user) {
                        $user->update(['status' => 1]);
                    }
                }

                return redirect()->route('employee.index')->with('success', 'People Status Changed successfully');
            } else {
                return redirect()->route('employee.index')->with('failure', 'No Company Found');
            }
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('employee.index')->with('failure', 'Something went wrong');
        }
    }

    public function cancel($id) {
        try {
            $emp = $this->repository->findWith($id);
            if ($emp) {

                //Delete People Temp
                $employees_temp = EmployeeTemperature::withTrashed()->where('employee_id', $emp->id)->get();
                if (!empty($employees_temp)) {
                    $emp->employee_temperature()->forceDelete();
                }
                //Delete People Temp Log
                $employees_temp_log = EmployeeTemperatureLog::withTrashed()->where('employee_id', $emp->id)->get();
                if (!empty($employees_temp_log)) {
                    $emp->employee_temperature_log()->forceDelete();
                }
                //Delete People registry - Only in force Delete
                $employees_registry = PeopleRegistry::withTrashed()->where('employee_id', $emp->id)->get();
                if (!empty($employees_registry)) {
                    foreach ($employees_registry as $ep) {
                        //Delete People registry Files
                        /* if(!empty($ep->people_file)) {
                          File::delete(public_path('people-registry/'. $ep->people_file));
                          } */
                    }
                    $emp->people_registry()->forceDelete();
                }

                //Delete Emp from Group
                $group_emp = GroupEmployee::where('employee_id', $emp->id)->get();
                if (!empty($group_emp)) {
                    $emp->group_emp()->forceDelete();
                }

                //Delete Emp Attendance
                $attendance = PeopleAttendance::where('employee_id', $emp->id)->get();
                if (!empty($attendance)) {
                    $emp->people_attendance()->delete();
                }

                ////Delete Emp from Device People
                $device = DevicePeople::withTrashed()->where('employee_id', $emp->id)->get();
                if (!empty($device)) {
                    $emp->device_people()->forceDelete();
                }

                ////Delete Emp from Contact Tracing Data
                $contact_tracing = ContactTracing::where('employee_id', $emp->id)->get();
                if (!empty($contact_tracing)) {
                    $emp->contact_tracing()->delete();
                }
                $contact_tracing_2 = ContactTracing::where('contact_employee_id', $emp->id)->get();
                if (!empty($contact_tracing_2)) {
                    $emp->emp_contact_tracing()->delete();
                }

                $booking = Booking::withTrashed()->where('employee_id', $emp->id)->get();
                if (!empty($booking)) {
                    $emp->booking()->forceDelete();
                }

                //Delete People User
                $user = User::withTrashed()->where('id', $emp->user_id)->first();
                if ($user) {
                    if (!empty($user->logo)) {

                        File::delete(public_path('company-logo/' . $user->logo));
                    }
                    $user->forceDelete();
                }

                $this->repository->cancel($id);

                return redirect()->route('employee.index')->with('success', 'People Cancelled successfully');
            } else {
                return redirect()->route('employee.index')->with('failure', 'No People Found');
            }
        } catch (\Exception $e) {
            Log::error('People management error:' . $e->getMessage());
            return redirect()->route('employee.index')->with('failure', 'Something went wrong');
        }
    }

    public function export_data() {
        if (Auth::user()->hasRole('admin')) {
            $employees_data = $this->repository->getAll();
            return Excel::download(new EmployeeExport(), date('Ymd') . '-people.csv');
        } else if (Auth::user()->hasRole('company')) {
            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
            $employees_data = $this->repository->getAllByCompany($getUserCompany->id);
            return Excel::download(new EmployeeExport($getUserCompany->id), date('Ymd') . '-people.csv');
        }
    }

    public static function import_data() {
        //$this->middleware('company_role');
        return view('backend.employee.import_form');
    }

    public static function import_data_store(Request $request) {
        try {
            $data = $request->file('file');
            $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
            $import = new EmployeeImport($getUserCompany->id);
            Excel::import($import, $request->file('file'));
            if ($import->getRowCount() > 0) {
                return redirect()->route('employee.index')->with('success', $import->getRowCount() . ' People added successfully');
            } else {
                return redirect()->route('employee.index')->with('failure', ' Not People added , Please check your data');
            }
        } catch (\Exception $e) {
            Log::error('People import error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('employee.index')->with('failure', 'Failed to import data , please check your data and try again');
        }
    }

}
