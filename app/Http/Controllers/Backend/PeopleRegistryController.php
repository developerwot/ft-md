<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\GroupStoreRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PeopleRegistryStoreRequest;
use App\Mail\PeopleRegistryTraining;
use App\Models\Employee;
use App\Models\Company;
use App\Models\Setting;
use App\Repositories\Backend\PeopleRegistry\FacialContract;
use App\Repositories\Backend\PeopleRegistry\PeopleRegistryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\Group\GroupContract;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Mail;
class PeopleRegistryController extends Controller
{
    protected $repository;

    public function __construct(PeopleRegistryContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {
            /*$training_status = ["1"=>"Video Uploaded","2"=>"Training in Progress","3"=>"Training is Done","0"=>""];
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            $people_registry_data = $this->repository->getAllFaceRecoByCompany($getUserCompany->id);
            $people_registry = $people_registry_data->filter(function ($item) use($training_status){
                $item->first_name = $item->employee->first_name.' '.$item->employee->last_name;
                $item->email = $item->employee->email;
                $item->status = $training_status[$item->training_status];
                return $item;
            });*/

            return view('backend.people_registry.index');

        } catch (\Exception $e) {
            Log::error('People Registry management error:' . $e->getMessage());
            return redirect()->route('people_registry.index')->with('failure', 'Something went wrong');
        }
    }
    public function get_ajax_data(Request $request) {
        try {

            $inputs = $request->only('query');

            if(!empty($inputs['query'])) {
                if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }
            }

            $training_status = ["1"=>"Video Uploaded","2"=>"Training in Progress","3"=>"Training is Done","0"=>""];
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if(!empty($searchKey)) {
                $people_registry_data = $this->repository->getAllFaceRecoByCompany($getUserCompany->id,$searchKey);
            }
            else {
                $people_registry_data = $this->repository->getAllFaceRecoByCompany($getUserCompany->id);
            }

            $people_registry = $people_registry_data->filter(function ($item) use($training_status){
                $item->first_name = $item->employee->first_name.' '.$item->employee->last_name;
                $item->email = $item->employee->email;
                $item->status = $training_status[$item->training_status];
                return $item;
            });

            return response()->json(['status'=>'success','data'=>$people_registry,'code'=>200],200);

        } catch (\Exception $e) {
            Log::error('People Registry management error:' . $e->getMessage());
            return '';
        }
    }

    public function create()
    {
        $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
        if(!empty($getUserCompany)) {
            $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
            return view('backend.people_registry.form',compact('employees'));
        }
        else {
            return redirect()->route('dashboard')->with('failure', 'Company Not Found');
        }

    }

    public function store(PeopleRegistryStoreRequest $request)
    {
        $image_mime_type = ['image/gif','image/jpeg','image/png','image/jpg','image/svg'];
        $video_mime_type = ['video/mp4','video/x-matroska','video/quicktime'];
        try {
            $input = $request->only('employee_id', 'people_file');
            $file = $request->file('people_file');

            if($file) {

                if(in_array($file->getMimeType(),$image_mime_type)) {
                    $input['file_type'] = 'Image';

                }
                else if (in_array($file->getMimeType(),$video_mime_type)) {
                    $input['file_type'] = 'Video';
                }
                //Stroe in amazon s3 bucket
                //$filename = time() .'-'. $file->getClientOriginalName();
                //$filePath = 'people_registry/' . $filename;

                $url = 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';
                $images = [];
                /*$files = Storage::disk('s3')->files('people_registry');
                dd($files);*/
                //Storage::disk('s3')->put($filePath, file_get_contents($file));

                //Store in Folder
                $destinationPath = 'people-registry/';
                $filePath= time() .'-'. $file->getClientOriginalName();
                $file->move($destinationPath, $filePath);
            }

            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if( !empty($getUserCompany) ) {
                /*if(!empty($file)) {
                    $input['people_file'] =$filePath;
                }
                else {
                    $input['people_file'] = '';
                }*/
                $checkExists = $this->repository->findByEmployee($input['employee_id']);
                if( !empty($checkExists) ) {
                    return redirect()->back()->withInput()->withErrors([ 'People Registry already Exists']);
                }

                $group = $this->repository->store($input);
                return redirect()->route('people_registry.index')->with('success', 'New People Registry added successfully');
            }



        } catch (\Exception $e) {
            Log::error('People Registry management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('people_registry.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $file_name = '';
            $training_status = ["1"=>"Video Uploaded","2"=>"Training in Progress","3"=>"Training is Done"];
            $people_registry = $this->repository->find($id);
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            $empID = $people_registry->employee_id;
            /*$empID =33;*/

//            if(!empty($people_registry->people_file)) {
//                $url = 'https://'.env('AWS_BUCKET').'.s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/';
//                $files = Storage::disk('s3')->files($getUserCompany->id.'/videos/'.$empID);
//
//
//                if( !empty($files) ) {
//                    foreach ($files as $file) {
//                        if(str_contains($file,$people_registry->people_file)) {
//                            $file_name = $url.$file;
//                        }
//
//                    }
//
//                }
//            }


            if(!empty($getUserCompany)) {
                $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
                return view('backend.people_registry.form',compact('employees','people_registry','file_name','training_status'));
            }
            else {
                return redirect()->route('dashboard')->with('failure', 'Company Not Found');
            }

        } catch (\Exception $e) {
            Log::error('People Registry management error:' . $e->getMessage());
            return redirect()->route('people_registry.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(PeopleRegistryStoreRequest $request, $id)
    {

        $image_mime_type = ['image/gif','image/jpeg','image/png','image/jpg','image/svg'];
        $video_mime_type = ['video/mp4','video/x-matroska','video/quicktime'];

        try {
            $input = $request->only('employee_id', 'people_file','training_status');
            $people_registry = $this->repository->find($id);
            $isemployee = Employee::where('id',$people_registry->employee_id)->where('status',1)->first();
            $file = $request->file('people_file');
            /*if($file) {
                if(in_array($file->getMimeType(),$image_mime_type)) {
                    $input['file_type'] = 'Image';

                }
                else if (in_array($file->getMimeType(),$video_mime_type)) {
                    $input['file_type'] = 'Video';
                }
                //Stroe in amazon s3 bucket
                //$filename = time() .'-'. $file->getClientOriginalName();


                //Store in Folder
                $destinationPath = 'people-registry/';
                $originalFile = $file->getClientOriginalName();
                $filePath= time() .'-'. $file->getClientOriginalName();
                $file->move($destinationPath, $filePath);

                if(!empty($people_registry->people_file)) {
                    File::delete(public_path('people-registry/'. $people_registry->people_file));
                }
            }*/

           /* if(!empty($file)) {
                $input['people_file'] = $filePath;
            }
            else {
                $input['people_file'] = '';
            }*/
           // $input['people_file'] = '';
            if(empty($input['training_status']) ) {
                $input['training_status']  = 0;
            }
            if($input['training_status'] == 3) {
                //Send Admin Mail

                $settings = Setting::where('company_id',$isemployee->company_id)->first();
                if( !empty($settings) ) {
                    if( !empty($settings->hr_email) ) {
                        Mail::to($settings->hr_email)->send(new PeopleRegistryTraining($isemployee->first_name.' '.$isemployee->last_name));
                    }
                }
            }
            $this->repository->update($id, $input);

            return redirect()->route('people_registry.index')->with('success', 'People Registry updated successfully');

        } catch (\Exception $e) {
            Log::error('People Registry management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('people_registry.index')->with('Something went wrong');
        }
    }

    public function show_video(Request $request) {

        $data = $request->only('people_registry_id');
        $file_name = '';

        $people_registry = $this->repository->find($data['people_registry_id']);
        $empID = $people_registry->employee_id;
        $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
      /*  $empID =33;*/
        if(!empty($people_registry->people_file)) {
            $url = 'https://'.env('AWS_BUCKET').'.s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/';
            $files = Storage::disk('s3')->files($getUserCompany->id.'/videos/'.$empID);

            if( !empty($files) ) {
                foreach ($files as $file) {
                    if(str_contains($file,$people_registry->people_file)) {
                        $file_name = $url.$file;
                    }

                }
            }
        }

        return view('backend.people_registry.show_video',compact('file_name'));
    }
    public function destroy($id)
    {
        try {
            $people_registry = $this->repository->find($id);
            if($people_registry) {
                /* if(!empty($people_registry->people_file)) {
                    File::delete(public_path('people-registry/'. $people_registry->people_file));
                    //Delete s3 File if needed
                    //Storage::disk('s3')->delete('people_registry/' . $people_registry->people_file);
                }*/


                $this->repository->delete($id);
            }
            return redirect()->route('people_registry.index')->with('success', 'People Registry deleted successfully');

        } catch (\Exception $e) {
            Log::error('People Registry management error:' . $e->getMessage());
            return redirect()->route('people_registry.index')->with('failure', 'Something went wrong');
        }
    }
}
