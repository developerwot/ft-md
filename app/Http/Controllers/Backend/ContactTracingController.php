<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\EmpTemperatureStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Employee;
use App\Repositories\Backend\ContactTrac\ContactTracContract;
use App\Repositories\Backend\ContactTrac\ContactTracRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use DateTime;
class ContactTracingController extends Controller
{
    protected $repository;
    protected $company_id;

    public function __construct(ContactTracContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }
    public function index() {

       /* $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
        $contact_tracing_data = $this->repository->getAllTracingByCompany($getUserCompany->id);
        $contact_tracing_other_data = $this->repository->getAllTracingByCompanyContactPeople($getUserCompany->id);
        $contact_other_emp_ids = collect($contact_tracing_other_data)->pluck('contact_count','contact_employee_id')->toArray();
        $contact_emp_ids = collect($contact_tracing_data)->pluck('employee_id')->toArray();

        $contact_tracing = $contact_tracing_data->filter(function ($item) use ($contact_other_emp_ids){
            if(in_array($item->employee_id,array_keys($contact_other_emp_ids))) {
                $item->contact_count = ($item->contact_count + $contact_other_emp_ids[$item->employee_id]);
            }
            return $item;
        });
        $contact_tracing_other = $contact_tracing_other_data->whereNotIn('contact_employee_id',$contact_emp_ids);*/

        return view('backend.contact_tracing.index');
    }
    public function getLists(Request $request) {
        try {
                $inputs = $request->only('tracing_date','search','query');

                if( !empty($inputs['tracing_date']) && !empty($inputs['search']) && $inputs['search'] == 1 ) {
                    $tracing_date = date('Y-m-d',strtotime($inputs['tracing_date']));
                }
                if(!empty($inputs['query'])) {
                    if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                        $searchKey = $inputs['query']['generalSearch'];
                    }
                }

            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if(!empty($tracing_date)) {
                $current_date = $tracing_date;
            }
            else {
                $current_date = date("Y-m-d");
            }
            if(!empty($searchKey)) {
                $contact_tracing_data = $this->repository->getAllTracingByCompany($getUserCompany->id,$current_date,$searchKey);
                $contact_tracing_other_data = $this->repository->getAllTracingByCompanyContactPeople($getUserCompany->id,$current_date,$searchKey);
            }
            else {
                $contact_tracing_data = $this->repository->getAllTracingByCompany($getUserCompany->id,$current_date);
                $contact_tracing_other_data = $this->repository->getAllTracingByCompanyContactPeople($getUserCompany->id,$current_date);
            }

            $contact_other_emp_ids = collect($contact_tracing_other_data)->pluck('contact_count','contact_employee_id')->toArray();
            $contact_emp_ids = collect($contact_tracing_data)->pluck('employee_id')->toArray();

            $contact_tracing = $contact_tracing_data->filter(function ($item) use ($contact_other_emp_ids){
                if(in_array($item->employee_id,array_keys($contact_other_emp_ids))) {
                    $item->contact_count = ($item->contact_count + $contact_other_emp_ids[$item->employee_id]);
                }
                return $item;
            });

            $contact_tracing_other = $contact_tracing_other_data->whereNotIn('contact_employee_id',$contact_emp_ids);

            $result = $contact_tracing->toBase()->merge($contact_tracing_other->toBase());

            $contact_tracing = $result->filter(function ($item){
                if($item->employee) {
                    $item->name = $item->employee->first_name.' '.$item->employee->last_name;
                    $item->last_name = $item->employee->last_name;
                    $item->contact_time_format = date('d-M-Y',strtotime($item->contact_date));
                    //return  $item->email = $item->employee->email.' '.$item->employee->id;
                    return  $item->email = $item->employee->email;

                }
                if($item->contact_employee) {
                    $item->name = $item->contact_employee->first_name.' '.$item->contact_employee->last_name;
                    $item->last_name = $item->contact_employee->last_name;
                    $item->contact_time_format = date('d-M-Y',strtotime($item->contact_date));
                    return  $item->email = $item->contact_employee->email;
                    //return  $item->email = $item->contact_employee->email.' '.$item->contact_employee->id;

                }
                return $item;

            });

            return response()->json(['status'=>'success','data'=>$contact_tracing,'code'=>200],200);
        }
        catch (\Exception $e) {
            Log::error('Contact Tracing management error:' . $e->getMessage());
            return '';
        }

    }

    public function show(Request $request)
    {
        try {
            $data = $request->only('employee_id','time');

            if(!empty($data['employee_id']) && !empty($data['time'])) {
                $employee = $this->repository->findByEmployeeID($data['employee_id']);
                $contact_tracing_user1 = $this->repository->getAllTracingByEmployee($data['employee_id'],$data['time']);
                $contact_tracing_user2 = $this->repository->getAllTracingByEmployeeContact($data['employee_id'],$data['time']);
                $contact_tracing_user = $contact_tracing_user1->toBase()->merge($contact_tracing_user2->toBase());

                $main = 1;
                return view('backend.contact_tracing.show',compact('employee','contact_tracing_user','main'));
            }


        } catch (\Exception $e) {
            Log::error('Contact Tracing management error:' . $e->getMessage());
            return '';
        }
    }
    public function show_other(Request $request)
    {
        try {
            $data = $request->only('contact_employee_id','time');
            if(!empty($data['contact_employee_id'])) {
                $employee = $this->repository->findByEmployeeID($data['contact_employee_id']);
                $contact_tracing_user = $this->repository->getAllTracingByEmployeeContact($data['contact_employee_id'],$data['time']);
                $main = 2;

                return view('backend.contact_tracing.show',compact('employee','contact_tracing_user','main'));
            }

        } catch (\Exception $e) {
            Log::error('Contact Tracing management error:' . $e->getMessage());
            return '';
        }
    }

    public function update(EmpTemperatureStoreRequest $request, $id)
    {

        try {
            $input = $request->only('temperature', 'employee_id');
            $this->repository->update($id, $input);
            return redirect()->route('emp_temp.index')->with('success', 'Empployee Temp updated successfully');

        } catch (\Exception $e) {
            Log::error('Empployee Temp management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('emp_temp.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return redirect()->route('emp_temp.index')->with('success', 'Empployee Temp deleted successfully');

        } catch (\Exception $e) {
            Log::error('Empployee Temp management error:' . $e->getMessage());
            return redirect()->route('emp_temp.index')->with('failure', 'Something went wrong');
        }
    }
}
