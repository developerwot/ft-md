<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CompanyStoreRequest;
use App\Http\Requests\Backend\CompanyUpdateRequest;
use App\Http\Controllers\Controller;
use App\Mail\CompanyBlock;
use App\Models\Building;
use App\Models\BuildingPolicy;
use App\Models\Device;
use App\Models\DevicePeople;
use App\Models\Employee;
use App\Models\Group;
use App\Models\PeopleAttendance;
use App\Models\PeopleRegistry;
use App\Repositories\Backend\Company\CompanyContract;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Mail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;



class CompanyController extends Controller
{
    protected $repository;
    use SendsPasswordResetEmails;

    public function __construct(CompanyContract $repository)
    {
        $this->middleware('admin_role');
        $this->repository = $repository;
    }

    public function index()
    {

        try {
            $company = $this->repository->getAll();

            return view('backend.company.index', compact('company'));

        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());
            return redirect()->route('company.index')->with('failure', 'Something went wrong');
        }
    }

    public function create()
    {
        return view('backend.company.form');
    }

    public function store(CompanyStoreRequest $request)
    {
        try {
            $input = $request->only( 'email');
            $existedUser = User::where('email',$input['email'])->first();
            if(empty($existedUser)) {
                $input['name'] = '';
                $input['business_name'] = '';
                $input['address'] = '';
                $input['city'] = '';
                $input['postal_code'] = '';
                $input['country'] = '';
                $input['vat'] = '';
                $input['user_id'] = 0;
                $input['status'] = 0;
                $input['gate_limit']=0;
                $input['company_limit']=0;
                $input['people_link_status']=0;
                $company = $this->repository->store($input);
                return redirect()->route('company.index')->with('success', 'New Company added successfully');
            }
            else {
                return redirect()->back()->with('failure', 'Already registered User');
            }
        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('company.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $company = $this->repository->find($id);
            $user = User::find($company->user_id);
//dd($company);
            return view('backend.company.form', compact('company','user'));

        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());
            return redirect()->route('company.index')->with('failure', 'Something went wrong');
        }
    }
    public function show($id)
    {
        try {
            $company = $this->repository->find($id);
            $roles = Role::all();
            return view('backend.company.form', compact('company','roles'));

        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());
            return redirect()->route('company.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(CompanyUpdateRequest $request, $id)
    {
//dd($request->all());
        try {
            $input = $request->only('name', 'last_name','address','city','postal_code','business_name','country','vat','status','gate_limit','employee_limit');
            if(isset($request->is_tracking_enabled)){
                $input['is_tracking_enabled']=1;
            }else{
                $input['is_tracking_enabled']=0;
            }
            if(isset($request->is_booking_enabled)){
                $input['is_booking_enabled']=1;
            }else{
                $input['is_booking_enabled']=0;
            }
            if(isset($request->people_link)){
                $input['people_link_status']=1;
                $input['customer_id']=$request->customer_id;
            }else{
                $input['people_link_status']=0;
            }
//dd($input);
            $this->repository->update($id, $input);

            $company = $this->repository->find($id);
            if(!empty($company->user_id)) {
                $user = User::find($company->user_id);
                $user->first_name = $input['name'];
                $user->last_name = $input['last_name'];
                $user->phone = $request->phone;
                $user->status = $input['status'];
                $user->save();
            }
            if( $input['status'] == 0 ) {
                //Send User Mail
                Mail::to($company->email)->send(new CompanyBlock($company->name.' '.$company->last_name));
            }
            return redirect()->route('company.index')->with('success', 'Company updated successfully');

        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('company.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $emp = $this->repository->find($id);
            if($emp) {
                $user = User::find($emp->user_id);
                /*if(!empty($user->logo)) {

                    File::delete(public_path('company-logo/'. $user->logo));

                }*/
                $user = User::find($emp->user_id);
                if($user) {
                    User::destroy($emp->user_id);
                }

                $this->repository->delete($id);
                return redirect()->route('company.index')->with('success', 'Company deleted successfully');
            }
            else {
                return redirect()->route('company.index')->with('failure', 'No Company Found');
            }


        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());
            return redirect()->route('company.index')->with('failure', 'Something went wrong');
        }
    }
    public function cancel($id)
    {
        try {
            $company = $this->repository->find($id);
            if($company) {

                //Delete Company Employee
                $employees = Employee::withTrashed()->where('company_id',$company->id)->get();
                if( !empty($employees) ) {
                    //Delete Employee Temp
                    foreach ( $employees as $emp ) {
                        $emp_user = User::withTrashed()->find($emp->user_id);
                        if(!empty($emp_user)) {
                            $emp_user->forceDelete();
                        }

                        $emp->employee_temperature()->forceDelete();
                        $emp->employee_temperature_log()->forceDelete();
                        //Delete Employee People registry
                        //$employees_registry = PeopleRegistry::withTrashed()->where('employee_id',$emp->id)->get();
                        $employees_registry = PeopleRegistry::where('employee_id',$emp->id)->get();
                        if( !empty($employees_registry) ) {
                            foreach ($employees_registry as $ep) {
                                //Delete People registry Files
                               /* if(!empty($ep->people_file)) {
                                    File::delete(public_path('people-registry/'. $ep->people_file));
                                }*/
                            }
                            $emp->people_registry()->delete();
                        }
                        /*//Delete Emp Attendance*/

                        $attendance = PeopleAttendance::where('employee_id',$emp->id)->get();
                        if( !empty($attendance) ) {
                            $emp->people_attendance()->delete();
                        }
                    }
                    $company->employees()->forceDelete();

                }

                //Delete Company Group
                $groups = Group::withTrashed()->with('group_employees')->where('company_id',$company->id)->get();
                if( !empty($groups) ) {
                    foreach ( $groups as $grp ) {
                        $grp->group_employees()->forceDelete();
                    }
                    $company->groups()->forceDelete();
                }



                //Delete Company Device
                $devices = Device::withTrashed()->where('company_id',$company->id)->get();
                if(!empty($devices)) {
                    foreach ($devices as $device) {
                        $device_people = DevicePeople::withTrashed()->where('device_id',$device->id)->get();
                        if(!empty($device_people)) {
                            $device->device_people()->forceDelete();
                        }
                    }
                    $company->devices()->forceDelete();

                }

                //Delete Company Building
                $buildings = Building::withTrashed()->where('company_id',$company->id)->get();
                if(!empty($buildings)) {
                    foreach ($buildings as $building) {
                        $building_policy = BuildingPolicy::withTrashed()->where('building_id',$building->id)->get();
                        if(!empty($building_policy)) {
                            $building->building_policy()->forceDelete();
                        }
                    }
                    $company->buildings()->forceDelete();
                }

                //Delete Company User
                $user = User::withTrashed()->where('id' , $company->user_id)->first();
                if($user)
                {
                    if(!empty($user->logo)) {

                        File::delete(public_path('company-logo/'. $user->logo));

                    }
                    $user->forceDelete();
                }

                $this->repository->cancel($id);

                return redirect()->route('company.index')->with('success', 'Company Cancelled successfully');
            }
            else {
                return redirect()->route('company.index')->with('failure', 'No Company Found');
            }


        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());
            return redirect()->route('company.index')->with('failure', 'Something went wrong');
        }
    }
    public function block($id)
    {
        try {
            $cmp = $this->repository->find($id);
            $user = User::find($cmp->user_id);
            if($cmp) {
                if($cmp->status == 1) {
                    $cmp->update(['status'=>0]);
                    if($user) {
                        $user->update(['status'=>0]);
                    }
                    //Send User Mail
                    Mail::to($cmp->email)->send(new CompanyBlock($cmp->name.' '.$cmp->last_name));

                }
                else {
                    $cmp->update(['status'=>1]);
                    if($user) {
                        $user->update(['status'=>1]);
                    }
                }

                return redirect()->route('company.index')->with('success', 'Company Status Changed successfully');
            }
            else {
                return redirect()->route('company.index')->with('failure', 'No Company Found');
            }


        } catch (\Exception $e) {
            Log::error('Company management error:' . $e->getMessage());
            return redirect()->route('company.index')->with('failure', 'Something went wrong');
        }
    }

    public function resetPassword(Request $request) {

        $data = $request->only('id','email');
        $company = $this->repository->find($request->only('id'));

        if($company) {

            $response = $this->broker()->sendResetLink(
                $this->credentials($request)
            );

            $response == Password::RESET_LINK_SENT
                ? $this->sendResetLinkResponse($request, $response)
                : $this->sendResetLinkFailedResponse($request, $response);
          if($response == 'passwords.sent') {
              return response()->json(['status' => 'success','message'=>'Password reset link sent successfully, please check your mail'], 200);
          }
        }
    }

}
