<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DevicePeopleStoreRequest;
use App\Models\Company;
use App\Repositories\Backend\DevicePeople\DevicePeopleContract;
use App\Repositories\Backend\DevicePeople\DevicePeopleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class DevicePeopleController extends Controller
{
    protected $repository;

    public function __construct(DevicePeopleContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {

            /* $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
             $device_people_data = $this->repository->getAllDevicePeopleByCompany($getUserCompany->id);

            $device_people = $device_people_data->filter(function ($item){
                $item->first_name = $item->employee->first_name;
                $item->last_name = $item->employee->last_name;
                $item->email = $item->employee->email;
                $item->beaconId = $item->device->beaconId;
                if($item->building) {
                    $item->building_name = $item->building->name;
                }
                else {
                    $item->building_name = '';
                }
                return $item;
            });*/
             return view('backend.device_people.index');

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }
    public function get_ajax_data(Request $request)
    {
        try {
            $inputs = $request->only('query');
            if(!empty($inputs['query'])) {
                if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }
            }
             $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if(!empty($searchKey)) {
                $device_people_data = $this->repository->getAllDevicePeopleByCompany($getUserCompany->id,$searchKey);
            }
            else {
                $device_people_data = $this->repository->getAllDevicePeopleByCompany($getUserCompany->id);
            }

            $device_people = $device_people_data->filter(function ($item){
                $item->first_name = $item->employee->first_name;
                $item->last_name = $item->employee->last_name;
                $item->email = $item->employee->email;
                $item->beaconId = $item->device->beaconId;
                if($item->building) {
                    $item->building_name = $item->building->name;
                }
                else {
                    $item->building_name = '';
                }
                return $item;
            });
            return response()->json(['status'=>'success','data'=>$device_people,'code'=>200],200);

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return '';
        }
    }

    public function create()
    {
        $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
        if(!empty($getUserCompany)) {
            $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
            $devices = $this->repository->getAllDeviceByCompany($getUserCompany->id);
            $buildings = $this->repository->getAllBuildingByCompany($getUserCompany->id);
            return view('backend.device_people.form',compact('employees','devices','buildings'));
        }
        else {
            return redirect()->route('dashboard')->with('failure', 'Company Not Found');
        }

    }

    public function store(DevicePeopleStoreRequest $request)
    {
        try {
            $input = $request->only('device_id','employee_id','building_id');
            $checkExistsEmployee = $this->repository->findByEmployeeId($input['employee_id']);
            if(!empty($checkExistsEmployee)) {
                return redirect()->back()->withInput()->withErrors([ 'People already assign to device']);
            }
            $checkExistsDevice = $this->repository->findByDeviceId($input['device_id']);
            if(!empty($checkExistsDevice)) {
                return redirect()->back()->withInput()->withErrors(['Device already to assign to People']);
            }

            $device = $this->repository->store($input);
            return redirect()->route('device_people.index')->with('success', 'New Device People added successfully');


        } catch (\Exception $e) {
            Log::error('Device People management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('device_people.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            $device_people = $this->repository->find($id);
            $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
            $devices = $this->repository->getAllDeviceByCompany($getUserCompany->id);
            $buildings = $this->repository->getAllBuildingByCompany($getUserCompany->id);
            return view('backend.device_people.form',compact('device_people','employees','devices','buildings'));

        } catch (\Exception $e) {
            Log::error('Device People management error:' . $e->getMessage());
            return redirect()->route('device_people.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(DevicePeopleStoreRequest $request, $id)
    {

        try {
            $input = $request->only('device_id','employee_id','building_id');
            $checkExistsEmployee = $this->repository->findByEmployeeId($input['employee_id'],$id);
            if(!empty($checkExistsEmployee)) {
                return redirect()->back()->with('failure', 'People already assign to device,');
            }
            $checkExistsDevice = $this->repository->findByDeviceId($input['device_id'],$id);
            if(!empty($checkExistsDevice)) {
                return redirect()->back()->with('failure', 'Device already to assign to People');
            }
            $device_people = $this->repository->find($id);

            $this->repository->update($id, $input);

            return redirect()->route('device_people.index')->with('success', 'Device People updated successfully');

        } catch (\Exception $e) {
            Log::error('Device People management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('device_people.index')->with('Something went wrong');
        }
    }

  /*  public function destroy($id)
    {
        try {
            $device_people = $this->repository->find($id);
            if($device_people) {
                $this->repository->delete($id);
            }
            return redirect()->route('device_people.index')->with('success', 'Device People deleted successfully');

        } catch (\Exception $e) {
            Log::error('Device People management error:' . $e->getMessage());
            return redirect()->route('device_people.index')->with('failure', 'Something went wrong');
        }
    }*/
}