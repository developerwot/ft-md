<?php

namespace App\Http\Controllers\Backend;

use App\BuildingAccess;
use App\Http\Requests\Backend\DeviceSettingRequest;
use App\LocationDevice;
use App\Models\Company;
use App\Models\Setting;
use App\Repositories\Backend\Profile\ProfileContracts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DeviceSettingsController extends Controller
{
    protected $repository;

    public function __construct(ProfileContracts $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        $device_settings_data = LocationDevice::with('building', 'location')->where('company_id', auth()->user()->company->id)->get();
        $device_settings = collect($device_settings_data)->groupBy('location_id');
        $data = [];
        $o = 0;
        if (collect($device_settings)->count() > 0) {
            foreach ($device_settings as $k => $v) {
                $count = collect($v)->count();
                $data[$o]['location_name'] = $v[0]->location->location_name;
                $data[$o]['count'] = $count;
                $data[$o]['building_name'] = $v[0]->building->building_name;
                $data[$o]['building_id'] = $v[0]->location->id;
                $data[$o]['type'] = $v[0]->device_location;
                $data[$o]['id'] = $v[0]->id;
                $o++;
            }
        }
        $device_settings = collect($data)->values();
        return view('backend.devices_settings.index', compact('device_settings'));
    }

    public function create()
    {
        $buildings = BuildingAccess::where('company_id', auth()->user()->company->id)->get();
        return view('backend.devices_settings.newform', compact('buildings'));
    }

    public function newSettings($id)
    {
        $data = LocationDevice::where('id', $id)->first(['building_id', 'location_id','device_id']);
        $selected = $data->building_id;
        $location = $data->location_id;
        $deviceId = $data->device_id;
        $deviceData = LocationDevice::where('location_id', $location)->groupBy('device_location')->get();
        $deviceData = collect($deviceData)->keyBy('device_location');
        $buildings = BuildingAccess::where('company_id', auth()->user()->company->id)->get();
//        dd($deviceData);
        return view('backend.devices_settings.newform', compact('buildings', 'selected', 'location', 'deviceData','deviceId'));
    }

    public function store(DeviceSettingRequest $request)
    {
        if ($request->entrance['camera_name'] != null && $request->entrance['device_type'] != null) {
            $this->validate($request, [
                "entrance.*" => 'required',
            ],
                [
                    'entrance.*.required' => 'all fields are necessary for camera ' . $request->entrance['camera_name'],
                ]
            );
        }
        if ($request->exit['camera_name'] != null && $request->exit['device_type'] != null) {
            $this->validate($request, [
                "exit.*" => 'required',
            ],
                [
                    'exit.*.required' => 'all fields are necessary for camera ' . $request->exit['camera_name'],
                ]
            );
        }
        if(auth()->user()->company->people_link_status==1){
            $this->validate($request, [
                "device_id" => 'required',
            ],
                [
                    'device_id.required' => 'Device Id is required',
                ]
            );
        }

//        dd($request->all());
//        $locationId=$request->location_id;
        $locationCount = LocationDevice::where('location_id', $request->location_id)->get()->count();
//        dd()
        if ($locationCount != 0) {
            return redirect()->back()->withInput()->with('failure', 'Device for this location already added');
        }
        if (auth()->user()->company->gate_limit != null) {
            $self = LocationDevice::where('company_id',auth()->user()->company->id)->get()->count();
            $self > 0 ? $locationCount = $self / 2 : $locationCount = 0;

            if ($locationCount >= auth()->user()->company->gate_limit) {
                $message = 'Company gate limit is reached!';
                return redirect()->back()->withInput()->with('failure', $message);
            }
        }
//        }

        try {
//           $ids = collect($entrance)->pluck('location_id')->toArray();
            $entrance = $request->entrance;
            $exit = $request->exit;

            if (isset($entrance['temperature_detection']) && $entrance['temperature_detection'] == 'on') {
                $entrance['temperature_detection'] = 1;
            }
            if (isset($exit['face_detection']) && $exit['face_detection'] == 'on') {
                $exit['face_detection'] = 1;
            }
            if (isset($exit['temperature_detection']) && $exit['temperature_detection'] == 'on') {
                $exit['temperature_detection'] = 1;
            }
            if (isset($entrance['face_detection']) && $entrance['face_detection'] == 'on') {
                $entrance['face_detection'] = 1;
            }

            $exit['building_id'] = $request->building_id;
            $entrance['building_id'] = $request->building_id;
            $exit['device_id'] = $request->device_id;
            $entrance['device_id'] = $request->device_id;
            $exit['location_id'] = $request->location_id;
            $entrance['location_id'] = $request->location_id;
            $exit['device_location'] = 'exit';
            $entrance['device_location'] = 'entrance';
            $entrance['company_id'] = auth()->user()->company->id;
            $exit['company_id'] = auth()->user()->company->id;
            LocationDevice::create($entrance);
            LocationDevice::create($exit);
            return redirect(route('device_settings.index'))->with('success', 'Device settings Added');
        } catch (\Exception $e) {
            return redirect(route('device_settings.index'))->with('failure', 'Something went wrong ');
        }
    }

    public function updateSettings(Request $request, $id)
    {
        if(auth()->user()->company->people_link_status==1){
            $this->validate($request, [
                "device_id" => 'required',
            ],
                [
                    'device_id.required' => 'Device Id is required',
                ]
            );
        }
        try {
            $counts = LocationDevice::where('location_id', $request->location_id)->where('location_id', '!=', $id)->get()->count();
            if ($counts != 0) {
                return redirect()->back()->withInput()->with('failure', 'Device already added for this location');
            }
//            if (auth()->user()->company->gate_limit != null) {
//                $self = LocationDevice::where('company_id', auth()->user()->company->id)->get()->count();
//                $self > 0 ? $locationCount = $self / 2 : $locationCount = 0;
//
//                if ($locationCount >= auth()->user()->company->gate_limit) {
//                    $message = 'Company gate limit is reached!';
//                    return redirect()->back()->withInput()->with('failure', $message);
//                }
//            }
//        }


//           $ids = collect($entrance)->pluck('location_id')->toArray();
            $entrance = $request->entrance;
            $exit = $request->exit;

            if (isset($entrance['temperature_detection']) && $entrance['temperature_detection'] == 'on') {
                $entrance['temperature_detection'] = 1;
            } else {
                $entrance['temperature_detection'] = null;
            }
            if (isset($exit['face_detection']) && $exit['face_detection'] == 'on') {
                $exit['face_detection'] = 1;
            } else {
                $exit['face_detection'] = null;
            }
            if (isset($exit['temperature_detection']) && $exit['temperature_detection'] == 'on') {
                $exit['temperature_detection'] = 1;
            } else {
                $exit['temperature_detection'] = null;
            }
            if (isset($entrance['face_detection']) && $entrance['face_detection'] == 'on') {
                $entrance['face_detection'] = 1;
            } else {
                $entrance['face_detection'] = null;
            }

            $exit['building_id'] = $request->building_id;
            $entrance['building_id'] = $request->building_id;
            $exit['device_id'] = $request->device_id;
            $entrance['device_id'] = $request->device_id;
            $exit['location_id'] = $request->location_id;
            $entrance['location_id'] = $request->location_id;
            $exit['device_location'] = 'exit';
            $entrance['device_location'] = 'entrance';
            $entrance['company_id'] = auth()->user()->company->id;
            $exit['company_id'] = auth()->user()->company->id;
            LocationDevice::where('location_id', $id)->where('device_location', 'entrance')->update($entrance);
            LocationDevice::where('location_id', $id)->where('device_location', 'exit')->update($exit);
            return redirect(route('device_settings.index'))->with('success', 'Device settings updated');
        } catch (\Exception $e) {
            return redirect(route('device_settings.index'))->with('failure', 'Something went wrong ');
        }


    }

    public function settingsAjax(Request $request)
    {
        $buildings = LocationDevice::with('location')->where('building_id', $request->building_id)->get();

        if ($buildings->count() > 0) {
//        if($buildings->locationDevice->count()<$buildings->location->count()){
//           $ids=collect($buildings->locationDevice)->pluck('location_id')->toArray();
//           $loc_ids=collect($buildings->location)->pluck('id')->toArray();
//            dd(array_merge(array_diff($ids, $loc_ids), array_diff($loc_ids, $ids)));
//        }
//        else{
            $build = collect($buildings)->groupBy('location_id');
//            dd($build);
            return response()->json(['status' => 1, 'builds' => $build]);
//        }
        } else {
            $buildings = BuildingAccess::with('location')->where('id', $request->building_id)->first();
            return response()->json(['status' => 1, 'locations' => $buildings]);
        }


    }

    public function deleteBuilding($id)
    {
        try {
            LocationDevice::where('location_id', $id)->delete();
            return redirect(route('device_settings.index'))->with('success', 'Device settings Deleted');
        } catch (\Exception $e) {
            return redirect(route('device_settings.index'))->with('failure', 'Something went wrong ');
        }
    }
}
