<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ReservationAccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReservationController extends Controller
{
    public function index()
    {
        return view('backend.reservation.index');
    }

    public function ajaxTable(Request $request)
    {
        $start = $request->start;
        $end = $request->end;
        $searchKey = '';
        $inputs = $request->only('temp_date', 'search', 'query');
        if (!empty($inputs['temp_date']) && !empty($inputs['search']) && $inputs['search'] == 1) {
            $temp_date = date('Y-m-d', strtotime($inputs['temp_date']));
        }
        if (!empty($inputs['query'])) {
            if (isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                $searchKey = $inputs['query']['generalSearch'];
            }
        }
//        if (!empty($temp_date)) {
        $reserve = DB::table('reservation_accesses')
            ->whereBetween('reservation_accesses.booked_at', [$start, $end])
            ->where('reservation_accesses.deleted_at', '=', null)
            ->where('reservation_accesses.type', 'location')
            ->join('employee', 'reservation_accesses.employee_id', '=', 'employee.id')
            ->join('timeslot_accesses', 'reservation_accesses.slot_id', '=', 'timeslot_accesses.id')
            ->join('location_accesses', 'location_accesses.id', '=', 'timeslot_accesses.location_access_id')
            ->join('building_accesses', 'building_accesses.id', '=', 'location_accesses.building_access_id')
            ->where('employee.company_id', '=', auth()->user()->company->id)
            ->select('reservation_accesses.id', 'reservation_accesses.booked_at', 'employee.first_name', 'employee.email', 'building_accesses.building_name', 'location_accesses.location_name', 'timeslot_accesses.start_time', 'timeslot_accesses.end_time')
            ->get();

        $deskReserve = DB::table('reservation_accesses')
            ->whereBetween('reservation_accesses.booked_at', [$start, $end])
            ->where('reservation_accesses.deleted_at', '=', null)
            ->where('reservation_accesses.type', 'desk')
            ->join('employee', 'reservation_accesses.employee_id', '=', 'employee.id')
            ->join('desk_accesses', 'reservation_accesses.slot_id', '=', 'desk_accesses.id')
            ->join('floor_accesses', 'floor_accesses.id', '=', 'desk_accesses.floor_id')
            ->join('building_accesses', 'building_accesses.id', '=', 'floor_accesses.building_access_id')
            ->where('employee.company_id', '=', auth()->user()->company->id)
            ->select('reservation_accesses.id', 'reservation_accesses.booked_at', 'employee.first_name',
                'employee.email', 'building_accesses.building_name', 'floor_accesses.floor_name', 'desk_accesses.desk_name')
            ->get();
        $data = [];
        $key = 0;
        foreach ($reserve as $val) {
            $data[$key]['id'] = $val->id;
            $data[$key]['title'] = $val->building_name;
            $data[$key]['color'] = 'yellow';
            $data[$key]['type'] = 'location';
            $data[$key]['start'] = $val->booked_at . 'T' . $val->start_time;
            $data[$key]['end'] = $val->booked_at . 'T' . $val->end_time;
            $key++;
        }
        foreach ($deskReserve as $desk) {
            $data[$key]['id'] = $desk->id;
            $data[$key]['title'] = $desk->desk_name;
            $data[$key]['type'] = 'desk';
            $data[$key]['color'] = '#640972';
            $data[$key]['start'] = $desk->booked_at;
            $data[$key]['allDay '] = true;
            $key++;
        }

//dd($data);
        return response()->json($data);
    }

    public function details($id, $type)
    {
        if ($type == 'desk') {
            $details = ReservationAccess::where('reservation_accesses.id', $id)
                ->where('reservation_accesses.deleted_at', '=', null)
                ->where('reservation_accesses.type', 'desk')
                ->join('employee', 'reservation_accesses.employee_id', '=', 'employee.id')
                ->join('desk_accesses', 'reservation_accesses.slot_id', '=', 'desk_accesses.id')
                ->join('floor_accesses', 'floor_accesses.id', '=', 'desk_accesses.floor_id')
                ->join('building_accesses', 'building_accesses.id', '=', 'floor_accesses.building_access_id')
                ->select('reservation_accesses.id', 'reservation_accesses.booked_at', 'employee.first_name', 'employee.email',
                    'building_accesses.building_name', 'floor_accesses.floor_name', 'reservation_accesses.type', 'desk_accesses.desk_name')->first();
        } else {
            $details = ReservationAccess::where('reservation_accesses.id', $id)
                ->where('reservation_accesses.deleted_at', '=', null)
                ->where('reservation_accesses.type', 'location')
                ->join('employee', 'reservation_accesses.employee_id', '=', 'employee.id')
                ->join('timeslot_accesses', 'reservation_accesses.slot_id', '=', 'timeslot_accesses.id')
                ->join('location_accesses', 'location_accesses.id', '=', 'timeslot_accesses.location_access_id')
                ->join('building_accesses', 'building_accesses.id', '=', 'location_accesses.building_access_id')
                ->select('reservation_accesses.id', 'reservation_accesses.booked_at', 'reservation_accesses.type', 'employee.first_name', 'employee.email', 'building_accesses.building_name', 'location_accesses.location_name', 'timeslot_accesses.start_time', 'timeslot_accesses.end_time')->first();
        }

        return response()->json($details);
    }
}

