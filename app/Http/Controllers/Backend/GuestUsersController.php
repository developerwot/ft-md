<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\EmpTemperatureStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\EmployeeTemperature;
use App\Repositories\Backend\GuestUser\GuestUserContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class GuestUsersController extends Controller
{
    protected $repository;
    protected $company_id;

    public function __construct(GuestUserContract $repository)
    {

        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {

            /*if (Auth::user()->hasRole('admin')) {
                $guest_users_data =  $this->repository->getAll();
            }
            else {
                $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
                $this->company_id  = $getUserCompany->id;

                $guest_users_data = $this->repository->getAllEmpTempByCompany($this->company_id);
            }

            $guest_users = $guest_users_data->filter(function ($item){
                if($item->employee) {
                    $item->name = $item->employee->first_name.' '.$item->employee->last_name;
                    $item->last_name = $item->employee->last_name;
                    $item->date = date('d-M-Y',strtotime($item->temp_date));
                    $item->time = date('h:i A',strtotime($item->temp_date));

                    $company = Company::where('id',$item->employee->company_id)->first();
                    if(!empty($company)) {
                        $item->company = $company->business_name;
                    }
                    $item->status ='';
                    $item->temperature = number_format($item->temperature,2) + 0;
                    return  $item->email = $item->employee->email;
                }
                return $item;

            });*/
            return view('backend.guest_users.index');

        } catch (\Exception $e) {
            Log::error('Guest Users management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }

    public function getLists(Request $request) {
        try {

                $searchKey = '';
                $inputs = $request->only('search','query');
                if( !empty($inputs['temp_date']) && !empty($inputs['search']) && $inputs['search'] == 1 ) {
                    $temp_date = date('Y-m-d',strtotime($inputs['temp_date']));
                }
                if(!empty($inputs['query'])) {
                    if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                        $searchKey = $inputs['query']['generalSearch'];
                    }
                }

                if(!empty($temp_date)) {
                    $current_date = $temp_date;
                }
                else {
                    $current_date = date("Y-m-d");
                }

                $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
                $this->company_id  = $getUserCompany->id;
                if(!empty($searchKey)) {
                    $guest_users_data = $this->repository->getAllGuestUsersByCompany($this->company_id,'',$searchKey);
                }
                else {
                    $guest_users_data = $this->repository->getAllGuestUsersByCompany($this->company_id,'');
                }


                $guest_users = $guest_users_data->filter(function ($item){

                    $item->from_date = date('d-M-Y',strtotime($item->from_date));
                    $item->to_date = date('d-M-Y',strtotime($item->to_date));
                    return  $item;

                return $item;

            });
            return response()->json(['status'=>'success','data'=>$guest_users,'code'=>200],200);

        } catch (\Exception $e) {
            Log::error('Guest Users management error:' . $e->getMessage());
            return '';
        }
    }
    public function show_video(Request $request) {

        $data = $request->only('guest_user_id');
        $file_name = '';

        $guest_user = $this->repository->find($data['guest_user_id']);

        if($guest_user->guest_registry && $guest_user->guest_registry->people_file !='' && $guest_user->guest_registry->people_file != null) {

            $url = 'https://'.env('AWS_BUCKET').'.s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/';
            $files = Storage::disk('s3')->files($guest_user->guest_user_company_id.'/videos/'.$guest_user->id);
            if( !empty($files) ) {
                foreach ($files as $file) {
                    if(str_contains($file,$guest_user->guest_registry->people_file)) {
                        $file_name = $url.$file;
                    }

                }

            }
        }



        return view('backend.people_registry.show_video',compact('file_name'));
    }
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            EmployeeTemperature::where('is_guest',1)->where('employee_id',$id)->delete();
            return redirect()->route('bookings.index')->with('success', 'Guest Users deleted successfully');

        } catch (\Exception $e) {
            Log::error('Guest Users management error:' . $e->getMessage());
            return redirect()->route('guest_users.index')->with('failure', 'Something went wrong');
        }
    }
}
