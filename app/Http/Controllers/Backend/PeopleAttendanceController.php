<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PeopleAttendanceStoreRequest;
use App\Http\Requests\Backend\PeopleAttendanceUpdateRequest;
use App\Http\Requests\Backend\PeopleRegistryStoreRequest;
use App\Models\Company;
use App\PeopleAttendanceLog;
use App\Repositories\Backend\PeopleAttendance\PeopleAttendanceContract;
use App\Repositories\Backend\PeopleRegistry\FacialContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use React\EventLoop\Factory;

class PeopleAttendanceController extends Controller
{
    protected $repository;

    public function __construct(PeopleAttendanceContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {
            $months = $this->getMonths();
            $year_start = 2012;
            $years = range($year_start,date('Y'));

             /*$getUserCompany = Company::where('user_id',Auth::user()->id)->first();
             $people_attendance_data = $this->repository->getAllAttendanceByCompany($getUserCompany->id);
             $current_date = date("Y-m-d");
             $people_attendance_data = $this->repository->getAllAttendanceByDate($getUserCompany->id,$current_date);
             $people_attendance = $people_attendance_data->filter(function ($item){
                    $item->check_in = date("M d, Y h:i A",strtotime($item->check_in));
                    if(!empty($item->check_out)) {
                        $item->check_out = date("M d, Y h:i A",strtotime($item->check_out));
                    }

                    return $item;
             });*/

             return view('backend.people_attendance.index',compact('months','years'));

        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage());
            return redirect()->route('people_attendance.index')->with('failure', 'Something went wrong');
        }
    }

    public function get_attendance(Request $request) {

        try {

            $inputs = $request->only('query','search','date_range','month_year');

            if(!empty($inputs['query'])) {


                    if( isset($inputs['date_range']) && !empty($inputs['date_range']) && $inputs['search'] ==1 ) {

                       
                        $dates = explode('-',$inputs['date_range']);
                        if(!empty($dates)) {
                            $start_date = date("Y-m-d",strtotime(trim($dates[0])));
                            $end_date = date('Y-m-d',strtotime(trim($dates[1])));

                        }
                    }

                    elseif(isset($inputs['month_year']) && !empty($inputs['month_year']) && $inputs['search'] ==2) {


                            $m_year = explode('-',$inputs['month_year']);
                            if(!empty($m_year)) {
                                $month = trim($m_year[0]);
                                $year = trim($m_year[1]);
                            }
                        }


                if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }

            }

            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if( !empty($start_date) && !empty($end_date) ) {

                if(!empty($searchKey)) {
                    $people_attendance_data = $this->repository->getAllAttendanceByDateRange($getUserCompany->id,$start_date,$end_date,$searchKey);
                }
                else {
                    $people_attendance_data = $this->repository->getAllAttendanceByDateRange($getUserCompany->id,$start_date,$end_date);
                }


            }
            else if(!empty($month) && !empty($year)) {

                if(!empty($searchKey)) {
                    $people_attendance_data = $this->repository->getAllAttendanceByMonth($getUserCompany->id,$month,$year,$searchKey);
                }
                else {
                    $people_attendance_data = $this->repository->getAllAttendanceByMonth($getUserCompany->id,$month,$year);
                }

            }
            else {
                $current_date = date("Y-m-d");
                if(!empty($searchKey)) {
                    $people_attendance_data = $this->repository->getAllAttendanceByDate($getUserCompany->id,$current_date,$searchKey);
                }
                else {
                    $people_attendance_data = $this->repository->getAllAttendanceByDate($getUserCompany->id,$current_date);
                }


            }

            $people_attendance = $people_attendance_data->filter(function ($item){
                $item->check_in = date("M d, Y h:i A",strtotime($item->check_in));
                $item->first_name = $item->employee->first_name." ".$item->employee->last_name;
                if(!empty($item->check_out)) {
                    $item->check_out = date("M d, Y h:i A",strtotime($item->check_out));
                }
                else {
                    $item->check_out='';
                }

                return $item;
            });

            return response()->json(['status'=>'success','data'=>$people_attendance,'code'=>200],200);

        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage());
            return '';
        }
    }
    public function create()
    {
        $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
        if(!empty($getUserCompany)) {
            $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
            return view('backend.people_attendance.form',compact('employees'));
        }
        else {
            return redirect()->route('dashboard')->with('failure', 'Company Not Found');
        }
    }

    public function store(PeopleAttendanceStoreRequest $request)
    {

        try {
            $input = $request->only('employee_id', 'check_in','check_out');
            PeopleAttendanceLog::create($input);
            $check_in_date = date("Y-m-d",strtotime($input['check_in']));
            $input['date'] = date("Y-m-d",strtotime($input['check_in']));

            $checkExisting = $this->repository->findByDate($input['employee_id'], $input['date']);
            if(!empty($checkExisting)) {
                return redirect()->back()->withInput()->withErrors(['Check in Date Entry already available for this People']);
            }
            if(!empty($input['check_out'])) {
                $check_out_date = date("Y-m-d",strtotime($input['check_out']));
                if($check_in_date != $check_out_date) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Date is Invalid , must be same as check in date']);;
                }
                else if (strtotime($input['check_out'])  < strtotime($input['check_in']) ) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Time is Invalid , must be greater than as check in date']);;
                }
                $input['check_out'] = date("Y-m-d H:i:s",strtotime($input['check_out']));
            }



            $input['check_in'] = date("Y-m-d H:i:s",strtotime($input['check_in']));

            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if( !empty($getUserCompany) ) {
                $group = $this->repository->store($input);

                return redirect()->route('people_attendance.index')->with('success', 'New People Attendance added successfully');
            }
        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('people_attendance.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $people_attendance = $this->repository->find($id);
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if(!empty($getUserCompany)) {
                $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
                return view('backend.people_attendance.form',compact('employees','people_attendance'));
            }
            else {
                return redirect()->route('dashboard')->with('failure', 'Company Not Found');
            }

        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage());
            return redirect()->route('people_attendance.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(PeopleAttendanceUpdateRequest $request, $id)
    {

        try {
            $input = $request->only( 'check_in','check_out');
            PeopleAttendanceLog::create([
                'employee_id'=>$id,
                'check_in'=>isset($request->check_in) ? $request->check_in : null,
                'check_out'=> isset($request->check_out) ? $request->check_out : null,
                'date'=>Carbon::today()->format('Y-m-d')
            ]);
            $check_in_date = date("Y-m-d",strtotime($input['check_in']));
            $people_attendance = $this->repository->find($id);
            if($people_attendance->date != $check_in_date) {
                return redirect()->back()->withInput()->withErrors(["Invalid Check in date"]);
            }

            if(!empty($input['check_out'])) {
                $check_out_date = date("Y-m-d",strtotime($input['check_out']));
                if($check_in_date != $check_out_date) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Date is Invalid , must be same as check in date']);;
                }
                else if (strtotime($input['check_out'])  < strtotime($input['check_in']) ) {
                    return redirect()->back()->withInput()->withErrors(['Check Out Time is Invalid , must be greater than as check in date']);;
                }
                $input['check_out'] = date("Y-m-d H:i:s",strtotime($input['check_out']));
            }

            $input['date'] = date("Y-m-d",strtotime($input['check_in']));
            $input['check_in'] = date("Y-m-d H:i:s",strtotime($input['check_in']));

            $this->repository->update($id, $input);

            return redirect()->route('people_attendance.index')->with('success', 'People Attendance updated successfully');

        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('people_attendance.index')->with('Something went wrong');
        }
    }

    public function ajaxAttendanceLog(Request $request){
    $date=Carbon::parse($request->date)->format('Y-m-d');
   $logs= PeopleAttendanceLog::where('employee_id',$request->employee_id)->whereDate('date',$date)->get();

        $logData = $logs->filter(function ($item) {
            if ($item->check_in!=null) {
                $item->check_in = date("h:i A", strtotime($item->check_in));
            } else {
                $item->check_in = '-';
            }
//            $item->check_in = date("h:i A", strtotime($item->check_in));
//            $item->first_name = $item->employee->first_name . " " . $item->employee->last_name;
            $item->date=Carbon::parse($item->date)->format('Y-m-d');
            if ($item->check_out!=null) {
                $item->check_out = date("h:i A", strtotime($item->check_out));
            } else {
                $item->check_out = '-';
            }
            return $item;
        });
    return response()->json($logData);
    }

    /*public function destroy($id)
    {
        try {
            $people_attendance = $this->repository->find($id);
            if($people_attendance) {

                $this->repository->delete($id);
            }
            return redirect()->route('people_attendance.index')->with('success', 'People Attendance deleted successfully');

        } catch (\Exception $e) {
            Log::error('People Attendance management error:' . $e->getMessage());
            return redirect()->route('people_attendance.index')->with('failure', 'Something went wrong');
        }
    }*/
}
