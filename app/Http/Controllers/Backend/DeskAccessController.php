<?php

namespace App\Http\Controllers\Backend;

use App\BuildingAccess;
use App\DeskAccess;
use App\FloorAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DeskStoreRequest;
use App\Http\Requests\Backend\DeskUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DeskAccessController extends Controller
{
    public function index()
    {
        return view('backend.desk.index');
    }

    public function tableAjax()
    {
        $locations = DeskAccess::with('building', 'floor')->where('company_id', auth()->user()->company->id)->where('is_policy_added','!=',1)->get();
        return response()->json(['status' => 1, 'message' => 'table fetched', 'data' => $locations], 200);
    }

    public function create()
    {
        $buildings = BuildingAccess::where([['company_id', '=', auth()->user()->company->id], ['building_status', '=', 1]])->get();
        return view('backend.desk.form', compact('buildings'));
    }

    public function floorAjax($id)
    {
        $locations = FloorAccess::where([['building_access_id', '=', $id], ['floor_status', '=', 1]])->get();
        if (count($locations->toArray()) > 0) {
            return response()->json(['status' => 1, 'message' => 'locations fetched', 'data' => $locations], 200);
        } else {
            return response()->json(['status' => 0, 'message' => 'locations failed'], 422);
        }
    }

    public function store(DeskStoreRequest $request)
    {
        try {
            $inputs = $request->except('_token');
            $floor_limit = FloorAccess::where('id', $inputs['floor_id'])->value('floor_limit');
            if ($floor_limit == 0) {
                return redirect()->back()->with('Floor limit is 0');
            }
            $existed = DeskAccess::where('floor_id', $inputs['floor_id'])->get();
            if (collect($existed)->count() > 0) {
                if (collect($existed)->count() >= $floor_limit) {
                    return redirect()->back()->withInput()->with('failure', 'Floor limit reached');
                }
            }
            $QR_string = 'intelly-scan_D-e-s-k_scan_qr_' .str_replace(' ', '',  $inputs['desk_name']) . '_' . $inputs['floor_id'] . '_' . $inputs['building_id'];
            $inputs['desk_QR'] = $QR_string;
            QrCode::size(400)
                ->format('png')
                ->merge('http://3.125.234.142:8080/images/site_logo.png', 0.3, true)
                ->errorCorrection('H')
                ->generate($QR_string, storage_path('app/public/QR_codes/'.$QR_string.'.png'));
            DeskAccess::create($inputs);
            return redirect(route('desk-access.index'))->with('success', 'Desk added!');

        } catch (\Exception $e) {
            dd($e);
            return redirect(route('desk-access.index'))->with('failure', 'Something went wrong');
        }


    }

    public function edit($id)
    {
        $buildings = BuildingAccess::where([['company_id', '=', auth()->user()->company->id], ['building_status', '=', 1]])->get();
        $desk = DeskAccess::where('id', $id)->first();
        return view('backend.desk.form', compact('buildings', 'desk'));
    }

    public function update(DeskUpdateRequest $request, $id)
    {
        try {
            $existed = DeskAccess::where([['desk_name', $request->desk_name], ['floor_id', $request->floor_id], ['id', '!=', $id]])->first();
            if ($existed != null) {
                return redirect()->back()->withInput()->with('failure', 'Desk name is already taken');
            }
            $inputs = $request->except('_token');
                        DeskAccess::where('id', $id)->update($inputs);
                        return redirect(route('desk-access.index'))->with('success', 'Desk updated!');
        } catch (\Exception $e) {
            return redirect(route('location-access.index'))->with('failure', 'Something went wrong');
        }

    }

    public function deleteDesk($id)
    {
        try {
          $desk=  DeskAccess::where('id', $id)->first();
          @unlink(storage_path('app\images/'.$desk->desk_QR.'.png'));
          $desk->delete();
          return redirect(route('desk-access.index'))->with('success', 'Desk deleted!');
        } catch (\Exception $e) {
            return redirect(route('desk-access.index'))->with('failure', 'something went wrong');
        }
    }

    public function tableAjax2(Request $request)
    {
        $locations = DeskAccess::with('building', 'floor')->where([['company_id', auth()->user()->company->id],['floor_id',$request->floor_id]])->get();
        return response()->json(['status' => 1, 'message' => 'table fetched', 'data' => $locations], 200);
    }
}
