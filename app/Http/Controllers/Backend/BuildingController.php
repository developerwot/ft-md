<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\BuildingStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Company;
use App\Repositories\Backend\Building\BuildingContract;
use App\Repositories\Backend\PeopleRegistry\FacialContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BuildingController extends Controller
{
    protected $repository;

    public function __construct(BuildingContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {
             /*$getUserCompany = Company::where('user_id',Auth::user()->id)->first();
             $building = $this->repository->getAllByCompany($getUserCompany->id);*/

            return view('backend.building.index');

        } catch (\Exception $e) {
            Log::error('Building management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }
    public function get_ajax_data(Request $request)
    {
        try {
             $inputs = $request->only('query');
             if(!empty($inputs['query'])) {
                if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }
             }

             $getUserCompany = Company::where('user_id',Auth::user()->id)->first();

            if(!empty($searchKey)) {
                $building = $this->repository->getAllByCompany($getUserCompany->id,$searchKey);
            }
            else {
                $building = $this->repository->getAllByCompany($getUserCompany->id);
            }


            return response()->json(['status'=>'success','data'=>$building,'code'=>200],200);

        } catch (\Exception $e) {
            Log::error('Building management error:' . $e->getMessage());
            return '';
        }
    }

    public function create()
    {
            return view('backend.building.form');
    }

    public function store(BuildingStoreRequest $request)
    {

        try {
            $input = $request->only('name','building_limit');
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();


            if( !empty($getUserCompany) ) {

                $input['company_id'] = $getUserCompany->id;
                $device = $this->repository->store($input);
                return redirect()->route('building.index')->with('success', 'New Building added successfully');
            }

        } catch (\Exception $e) {
            Log::error('Building management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('building.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $building = $this->repository->find($id);

            return view('backend.building.form',compact('building'));

        } catch (\Exception $e) {
            Log::error('Building management error:' . $e->getMessage());
            return redirect()->route('building.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(BuildingStoreRequest $request, $id)
    {
        try {
            $input = $request->only('name','building_limit');
            $building = $this->repository->find($id);

            $this->repository->update($id, $input);

            return redirect()->route('building.index')->with('success', 'Building updated successfully');

        } catch (\Exception $e) {
            Log::error('Building management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('building.index')->with('Something went wrong');
        }
    }
    public function show_booking(Request $request)
    {
        $data = $request->only('building_id');
        $current_date = date("Y-m-d");
        $start_date = $current_date;
        $end_date = date('Y-m-d',strtotime($current_date."+ 15 days"));

        $building = $this->repository->find($data['building_id']);
        $dates =[];
        $period = new \DatePeriod(new \DateTime($start_date), new \DateInterval('P1D'), new \DateTime($start_date. '+15 day'));
        foreach ($period as $date) {
            $dates[] = $date->format("Y-m-d");
        }

        $booking_lists = [];
        foreach($dates as $key=>$date){
            $sales= Booking::select('booking_date','building_id')
                        ->where("building_id",$building->id)
                        ->whereDate('booking_date','=',$date)->count();
            if($sales){
                $booking_lists[] = ['booking_date'=>$date,"booking_count"=>$sales];
            }else {
                $booking_lists[] = ['booking_date'=>$date,"booking_count"=>0];
            }

        }

        return view('backend.building.show_booking',compact('building','booking_lists'));

    }

    public function destroy($id)
    {
        try {
            $building = $this->repository->find($id);
            if($building) {
                $this->repository->delete($id);
            }
            return redirect()->route('building.index')->with('success', 'Building deleted successfully');

        } catch (\Exception $e) {
            Log::error('Building management error:' . $e->getMessage());
            return redirect()->route('building.index')->with('failure', 'Something went wrong');
        }
    }
}