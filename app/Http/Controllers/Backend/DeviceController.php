<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\DeviceStoreRequest;
use App\Http\Requests\Backend\DeviceUpdateRequest;
use App\Http\Requests\Backend\GroupStoreRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PeopleRegistryStoreRequest;
use App\Models\Employee;
use App\Models\Company;
use App\Repositories\Backend\Device\DeviceContract;
use App\Repositories\Backend\PeopleRegistry\FacialContract;
use App\Repositories\Backend\PeopleRegistry\PeopleRegistryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\Group\GroupContract;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DeviceController extends Controller
{
    protected $repository;

    public function __construct(DeviceContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {
/*
             $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
             $devices = $this->repository->getAllDeviceByCompany($getUserCompany->id);*/

            return view('backend.devices.index');

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return redirect()->route('devices.index')->with('failure', 'Something went wrong');
        }
    }
    public function get_ajax_data(Request $request)
    {
        try {
            $inputs = $request->only('query');
            if(!empty($inputs['query'])) {
                if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }
            }
             $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if(!empty($searchKey)) {
                $devices = $this->repository->getAllDeviceByCompany($getUserCompany->id,$searchKey);
            }
            else {
                $devices = $this->repository->getAllDeviceByCompany($getUserCompany->id);
            }


            return response()->json(['status'=>'success','data'=>$devices,'code'=>200],200);

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return '';
        }
    }

    public function create()
    {
            return view('backend.devices.form');
    }

    public function store(DeviceUpdateRequest $request)
    {

        try {
            $input = $request->only('beaconId');
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            $deviceExists = $this->repository->findByBeaconId($input['beaconId']);
            if($deviceExists) {
                return redirect()->back()->withInput()->withErrors(['Already Exists BeaconId']);
            }
            if( !empty($getUserCompany) ) {

                $input['company_id'] = $getUserCompany->id;
                $device = $this->repository->store($input);
                return redirect()->route('devices.index')->with('success', 'New Device added successfully');
            }

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('devices.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $device = $this->repository->find($id);

            return view('backend.devices.form',compact('device'));

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return redirect()->route('devices.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(DeviceUpdateRequest $request, $id)
    {
        try {
            $input = $request->only('beaconId');
            $deviceExists = $this->repository->findByBeaconId($input['beaconId'],$id);
            if($deviceExists) {
                return redirect()->back()->withInput()->withErrors(['Already Exists BeaconId']);
            }
            $devices = $this->repository->find($id);

            $this->repository->update($id, $input);

            return redirect()->route('devices.index')->with('success', 'Device updated successfully');

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('devices.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $devices = $this->repository->find($id);
            if($devices) {
                $this->repository->delete($id);
            }
            return redirect()->route('devices.index')->with('success', 'Device deleted successfully');

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return redirect()->route('devices.index')->with('failure', 'Something went wrong');
        }
    }
}