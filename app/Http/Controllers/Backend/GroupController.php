<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\GroupStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\Group\GroupContract;
use Illuminate\Support\Facades\Log;

class GroupController extends Controller
{
    protected $repository;

    public function __construct(GroupContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {
            /*if (Auth::user()->hasRole('admin')) {
                $groups = $this->repository->getAll();
            }
            else if(Auth::user()->hasRole('company')) {
                $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
                $groups_data = $this->repository->getAllGroupByCompany($getUserCompany->id);
                $groups = $groups_data->filter(function ($item){
                    $item->group_count = 0;
                    if($item->group_employees) {
                        foreach ($item->group_employees as $emp )
                        {
                            if($emp->employee) {
                                $item->group_count++;
                            }
                        }
                    }
                    //$item->group_count = $item->group_employees()->count();
                    return $item;
                });
            }*/

            return view('backend.groups.index');

        } catch (\Exception $e) {
            Log::error('Group management error:' . $e->getMessage());
            return redirect()->route('groups.index')->with('failure', 'Something went wrong');
        }
    }
    public function get_ajax_data(Request $request)
    {
        try {
            $inputs = $request->only('query');
            if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                $searchKey = $inputs['query']['generalSearch'];
            }
            if(Auth::user()->hasRole('company')) {
                $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
                if(!empty($searchKey)) {
                    $groups_data = $this->repository->getAllGroupByCompany($getUserCompany->id,$searchKey);
                }
                else {
                    $groups_data = $this->repository->getAllGroupByCompany($getUserCompany->id);
                }

                $groups = $groups_data->filter(function ($item){
                    $item->group_count = 0;
                    if($item->group_employees) {
                        foreach ($item->group_employees as $emp )
                        {
                            if($emp->employee && $emp->employee->status == 1) {
                                $item->group_count++;
                            }
                        }
                    }
                    //$item->group_count = $item->group_employees()->count();
                    return $item;
                });
                return response()->json(['status'=>'success','data'=>$groups,'code'=>200],200);
            }
            else {
                return '';
            }



        } catch (\Exception $e) {
            Log::error('Group management error:' . $e->getMessage());
            return '';
        }
    }

    public function show_people(Request $request) {
        $inputs = $request->only('group_id');
        if(!empty($inputs['group_id'])) {
            $group = $this->repository->find($inputs['group_id']);
            return view('backend.groups.show_people',compact('group'));
        }
    }
    public function create()
    {
        $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
        if(!empty($getUserCompany)) {
            $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
            return view('backend.groups.form',compact('employees'));
        }
        else {
            return redirect()->route('dashboard')->with('failure', 'Company Not Found');
        }

    }

    public function store(GroupStoreRequest $request)
    {
        try {
            $input = $request->only('name', 'employee_id', 'description');
            if(empty($input['description'])) {
                $input['description'] = '';
            }
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if( !empty($getUserCompany) ) {
                $input['company_id'] =$getUserCompany->id;
                $group = $this->repository->store($input);
                if($group) {
                    if(!empty($input['employee_id'])) {
                        foreach ($input['employee_id'] as $emp_id) {
                            $group_emp['group_id'] = $group->id;
                            $group_emp['employee_id'] = $emp_id;
                            $this->repository->store_group_emp($group_emp);
                        }
                    }

                }
                return redirect()->route('groups.index')->with('success', 'New Group added successfully');
            }



        } catch (\Exception $e) {
            Log::error('Group management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('groups.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $group = $this->repository->find($id);
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            if(!empty($getUserCompany)) {
                $employees = $this->repository->getAllEmpByCompany($getUserCompany->id);
                return view('backend.groups.form',compact('employees','group'));
            }
            else {
                return redirect()->route('dashboard')->with('failure', 'Company Not Found');
            }

        } catch (\Exception $e) {
            Log::error('Group management error:' . $e->getMessage());
            return redirect()->route('groups.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(GroupStoreRequest $request, $id)
    {

        try {
            $input = $request->only('name', 'employee_id', 'description');
            if(empty($input['description'])) {
                $input['description'] = '';
            }
            $this->repository->update($id, $input);
            $this->repository->delete_group_emp($id);
            if(!empty($input['employee_id'])) {
                foreach ($input['employee_id'] as $emp_id) {
                    $group_emp['group_id'] = $id;
                    $group_emp['employee_id'] = $emp_id;
                    $this->repository->store_group_emp($group_emp);
                }
            }
            return redirect()->route('groups.index')->with('success', 'Group updated successfully');

        } catch (\Exception $e) {
            Log::error('Group management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('groups.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return redirect()->route('groups.index')->with('success', 'Group deleted successfully');

        } catch (\Exception $e) {
            Log::error('Group management error:' . $e->getMessage());
            return redirect()->route('groups.index')->with('failure', 'Something went wrong');
        }
    }
}
