<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Dashboard\DashboardContract;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;

class DashboardController extends Controller {

    protected $repository;

    public function __construct(DashboardContract $repository) {
        $this->repository = $repository;
    }

    //return all the reservation record from database
    public function index() {
        try {
            $user_count = User::all()->count();
            if (Auth::user()->hasRole('admin')) {
                $today_guest_count = \App\Models\GuestUser::where('from_date', '<=', date('Y-m-d'))
                                ->where('to_date', '>=', date('Y-m-d'))->get()->count();

                $avg_tempreture = \App\Models\EmployeeTemperature::selectRaw('avg(temperature) as temp')->whereDate('temp_date', date('Y-m-d'))->first();
            } else {
                $getUserCompany = \App\Models\Company::where('user_id', Auth::user()->id)->first();
                $today_guest_count = \App\Models\GuestUser::where('from_date', '<=', date('Y-m-d'))
                                ->where('to_date', '>=', date('Y-m-d'))
                                ->where('company_id', $getUserCompany->id)->get()->count();
                $avg_tempreture = \App\Models\EmployeeTemperature::selectRaw('avg(temperature) as temp')
                                ->leftJoin('employee', 'employee.id', '=', 'employee_temperature.employee_id')
                                ->leftJoin('guest_users', 'guest_users.id', '=', 'employee_temperature.employee_id')
                                ->leftJoin('companies as emp_comp', 'employee.company_id', '=', 'emp_comp.id')
                                ->leftJoin('companies as guest_comp', 'guest_users.company_id', '=', 'guest_comp.id')
                                ->where(function($query) use($getUserCompany) {
                                    $query->where('emp_comp.id', $getUserCompany->id);
                                    $query->orWhere('guest_comp.id', $getUserCompany->id);
                                })
                                ->whereDate('temp_date', date('Y-m-d'))->first();
            }
                      
            return view('backend.dashboard', compact('user_count','avg_tempreture','today_guest_count'));
        } catch (\Exception $e) {
            Log::error('Dashboard error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }

}
