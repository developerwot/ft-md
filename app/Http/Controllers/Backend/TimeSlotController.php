<?php

namespace App\Http\Controllers\Backend;

use App\BuildingAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\StoreTimeslotRequest;
use App\LocationAccess;
use App\TimeslotAccess;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TimeSlotController extends Controller
{
    public function index(){
        return view('backend.timeslot_access.index');
    }

    public function tableAjax(){
        $locations=  TimeslotAccess::with('location','building')->where('company_id',auth()->user()->company->id)->where('is_policy_added',null)->get();
        return response()->json(['status'=>1,'message'=>'table fetched','data'=>$locations],200);
    }

    public function create(){
        $buildings= BuildingAccess::where([['company_id','=',auth()->user()->company->id],['building_status','=',1]])->get();
        return view('backend.timeslot_access.form',compact('buildings'));
    }

    public function locationAjax($id){
       $locations= LocationAccess::where([['building_access_id','=',$id],['location_status','=',1]])->get();
       if(count($locations->toArray())>0){
//      $count=count($locations->toArray());
           return response()->json(['status'=>1,'message'=>'locations fetched','data'=>$locations],200);
       }else{
           return response()->json(['status'=>0,'message'=>'locations failed'],422);
       }

    }
    public function store(StoreTimeslotRequest $request){

        try {
            $inputs = $request->except('_token');
            $startTime=Carbon::parse($inputs['start_time'])->format('H:i:s') ;
            $endTime=Carbon::parse($inputs['end_time'])->format('H:i:s') ;;
            $inputs['start_time']=$startTime;
            $inputs['end_time']=$endTime;
            $locationId=$inputs['location_access_id'];
            $availability = TimeslotAccess::where(function ($query) use ($startTime, $endTime,$locationId) {
                $query
                    ->where(function ($query) use ($startTime, $endTime,$locationId) {
                        $query
                            ->where('location_access_id','=',$locationId)
                            ->where('start_time', '<', $startTime)
                            ->where('end_time', '>', $startTime);
                    })
                    ->orWhere(function ($query) use ($startTime, $endTime,$locationId) {
                        $query
                            ->where('location_access_id','=',$locationId)
                            ->where('start_time', '<', $endTime)
                            ->where('end_time', '>', $endTime);
                    });
            })->count();
            if($availability){
                return redirect()->back()->withInput()->with('failure','time Slot already exist or overlapping existing time slot');
            }
            TimeslotAccess::create($inputs);
            return redirect(route('timeslot-access.index'))->with('success','Time slot created');

        }catch(\Exception $e){
            return redirect(route('timeslot-access.index'))->with('failure','Something went wrong');
        }


    }
    public function edit($id){
        $buildings= BuildingAccess::where([['company_id','=',auth()->user()->company->id],['building_status','=',1]])->get();
        $timeslot=TimeslotAccess::where('id',$id)->first();
//        $location=  LocationAccess::with('buildings')->where('id',$id)->first();
//      dd($location);
        return view('backend.timeslot_access.form',compact('buildings','timeslot'));
    }

    public function update(StoreTimeslotRequest $request ,$id){
        try{
            $inputs=$request->except('_token');
            $startTime=Carbon::parse($inputs['start_time'])->format('H:i:s') ;
            $endTime=Carbon::parse($inputs['end_time'])->format('H:i:s') ;;
            $inputs['start_time']=$startTime;
            $inputs['end_time']=$endTime;
            $locationId=$inputs['location_access_id'];
            $availability = TimeslotAccess::where(function ($query) use ($startTime, $endTime,$locationId,$id) {
                $query
                    ->where(function ($query) use ($startTime, $endTime,$locationId,$id) {
                        $query
                            ->where('id','!=',$id)
                            ->where('location_access_id','=',$locationId)
                            ->where('start_time', '<', $startTime)
                            ->where('end_time', '>', $startTime);
                    })
                    ->orWhere(function ($query) use ($startTime, $endTime,$locationId,$id) {
                        $query
                            ->where('id','!=',$id)
                            ->where('location_access_id','=',$locationId)
                            ->where('start_time', '<', $endTime)
                            ->where('end_time', '>', $endTime);
                    });
            })->count();
       if($availability){
           return redirect()->back()->withInput()->with('failure','time Slot already exist or overlapping existing time slot');
       }
            TimeslotAccess::where('id',$id)->update($inputs);
            return redirect(route('timeslot-access.index'))->with('success','timeslot updated!');
        }catch(\Exception $e){
            return redirect(route('timeslot-access.index'))->with('failure','Something went wrong');
        }

    }

    public function deletetimeslot($id){
        try{
            TimeslotAccess::where('id',$id)->delete();
            return redirect(route('timeslot-access.index'))->with('success','timeslot deleted!');
        }catch (\Exception $e){
            return redirect(route('timeslot-access.index'))->with('failure','something went wrong');
        }
    }
    public function tableAjax2(Request $request){
        $locations=  TimeslotAccess::with('location','building')->where([['company_id',auth()->user()->company->id],['location_access_id',$request->location_id]])->where('is_policy_added',null)->get();
        return response()->json(['status'=>1,'message'=>'table fetched','data'=>$locations],200);
    }
}
