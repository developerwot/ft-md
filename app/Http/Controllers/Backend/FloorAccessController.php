<?php

namespace App\Http\Controllers\Backend;

use App\BuildingAccess;
use App\DeskAccess;
use App\FloorAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\FloorstoreRequest;
use App\Http\Requests\Backend\FloorUpdateeRequest;
use Illuminate\Http\Request;

class FloorAccessController extends Controller
{
    public function index(){
        return view('backend.floor.index');
    }
    public function tableAjax(){
        $locations=  FloorAccess::with('buildings')->where('company_id',auth()->user()->company->id)->get();
        return response()->json(['status'=>1,'message'=>'table fetched','data'=>$locations],200);
    }

    public function create(){
        $buildings= BuildingAccess::where([['company_id','=',auth()->user()->company->id],['building_status','=',1]])->get();
        return view('backend.floor.form',compact('buildings'));
    }
    public function store(FloorstoreRequest $request){
        try {
            $inputs = $request->except('_token');
//            $building_limit=BuildingAccess::where('id',$inputs['building_access_id'])->value('building_limit');
//            if($inputs['floor_limit']>$building_limit){
//                return redirect()->back()->withInput()->with('failure','Building limit is for '.$building_limit.' people');
//            }else{
//                $total_locations=  FloorAccess::where('building_access_id',$inputs['building_access_id'])->get();
//                if($total_locations){
//                    $occupied=collect($total_locations)->sum('floor_limit');
//                    $remain=$building_limit-$occupied;
//                    if($remain==0){
//                        return redirect()->back()->withInput()->with('failure','building limit reached');
//                    }
//                    elseif($inputs['floor_limit']>$remain){
//                        return redirect()->back()->withInput()->with('failure','Only '.$remain.' people limit remain now');
//                    }
//                    else{
                        FloorAccess::create($inputs);
                        return redirect(route('floor-access.index'))->with('success','Floor added!');
//                    }
//                }
//            }

        }catch(\Exception $e){
            return redirect(route('floor-access.index'))->with('failure','Something went wrong');
        }


    }
    public function edit($id){
        $buildings=BuildingAccess::where([['company_id','=',auth()->user()->company->id],['building_status','=',1]])->get();
        $location=  FloorAccess::with('buildings')->where('id',$id)->first();
//      dd($location);
        return view('backend.floor.form',compact('buildings','location'));
    }

    public function update(FloorUpdateeRequest $request ,$id){
        try{
            $inputs=  $request->except('_token');
//            $building_limit=BuildingAccess::where('id',$inputs['building_access_id'])->value('building_limit');
//            if($inputs['floor_limit']>$building_limit){
//                return redirect()->back()->withInput()->with('failure','Building limit is for '.$building_limit.' people');
//            }else {
//                $total_locations = FloorAccess::where('building_access_id', $inputs['building_access_id'])->get();
//                if ($total_locations) {
//                    $occupied = collect($total_locations)->where('id','!=',$id)->sum('floor_limit');
//                    $remain = $building_limit - $occupied;
//                    if ($remain == 0) {
//                        return redirect()->back()->withInput()->with('failure', 'building limit reached');
//                    } elseif ($inputs['floor_limit'] > $remain) {
//                        return redirect()->back()->withInput()->with('failure', 'Only ' . $remain . ' people limit remain now');
//                    } else {
                        FloorAccess::where('id', $id)->update($inputs);
                        return redirect(route('floor-access.index'))->with('success', 'Location updated!');
//                    }
//                }
//            }

        }catch(\Exception $e){
            return redirect(route('floor-access.index'))->with('failure','Something went wrong');
        }

    }

    public function deleteFloor($id){
        try{
            FloorAccess::where('id',$id)->delete();
            DeskAccess::where('floor_id',$id)->delete();
            return redirect(route('floor-access.index'))->with('success','Location deleted!');
        }catch (\Exception $e){
            return redirect(route('floor-access.index'))->with('failure','something went wrong');
        }
    }

    public function tableAjax2(Request $request){
        $locations=  FloorAccess::with('buildings')->where([['company_id',auth()->user()->company->id],['building_access_id',$request->building_id]])->get();
        return response()->json(['status'=>1,'message'=>'table fetched','data'=>$locations],200);
    }
}
