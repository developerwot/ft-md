<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\EmpTemperatureStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\EmployeeTemperature;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Backend\EmpTemp\EmpTemperatureContract;
use Illuminate\Support\Facades\Log;

class EmployeeTempratureController extends Controller {

    protected $repository;
    protected $company_id;

    public function __construct(EmpTemperatureContract $repository) {

        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index() {
        try {

            /* if (Auth::user()->hasRole('admin')) {
              $emp_temp_data =  $this->repository->getAll();
              }
              else {
              $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
              $this->company_id  = $getUserCompany->id;

              $emp_temp_data = $this->repository->getAllEmpTempByCompany($this->company_id);
              }

              $emp_temp = $emp_temp_data->filter(function ($item){
              if($item->employee) {
              $item->name = $item->employee->first_name.' '.$item->employee->last_name;
              $item->last_name = $item->employee->last_name;
              $item->date = date('d-M-Y',strtotime($item->temp_date));
              $item->time = date('h:i A',strtotime($item->temp_date));

              $company = Company::where('id',$item->employee->company_id)->first();
              if(!empty($company)) {
              $item->company = $company->business_name;
              }
              $item->status ='';
              $item->temperature = number_format($item->temperature,2) + 0;
              return  $item->email = $item->employee->email;
              }
              return $item;

              }); */
           $email= Setting::where('company_id',auth()->user()->company->id)->value('report_email');
            return view('backend.emp_temp.index',compact('email'));
        } catch (\Exception $e) {
            Log::error('People Temp management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }

    public function getLists(Request $request) {
        try {

            $searchKey = '';
            $inputs = $request->only('temp_date', 'search', 'query');
            if (!empty($inputs['temp_date']) && !empty($inputs['search']) && $inputs['search'] == 1) {
                $temp_date = date('Y-m-d', strtotime($inputs['temp_date']));
            }
            if (!empty($inputs['query'])) {
                if (isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }
            }

            if (!empty($temp_date)) {
                $current_date = $temp_date;
            } else {
                $current_date = date("Y-m-d");
            }
            if (Auth::user()->hasRole('admin')) {
//                $emp_temp_data = $this->repository->getAll($current_date, $searchKey);

                $emp_temp_data = EmployeeTemperature::
                        selectRaw("
                            employee_temperature.*,
                        CONCAT(IF(employee_temperature.is_guest = 1,guest_user_data.first_name,employee.first_name),' ',
                        IF(employee_temperature.is_guest = 1,guest_user_data.last_name,employee.last_name)) as name,
                        IF(employee_temperature.is_guest = 1,guest_users.email_address,employee.email) as email,
                        IF(employee_temperature.is_guest = 1,guest_comp.business_name,emp_comp.business_name) as company
                        
                        ")
                        ->leftJoin('employee', 'employee.id', '=', 'employee_temperature.employee_id')
                        ->leftJoin('guest_users', 'guest_users.id', '=', 'employee_temperature.employee_id')
                        ->leftJoin('guest_user_data', 'guest_user_data.guest_user_id', '=', 'guest_users.id')
                        ->leftJoin('companies as emp_comp', 'employee.company_id', '=', 'emp_comp.id')
                        ->leftJoin('companies as guest_comp', 'guest_users.company_id', '=', 'guest_comp.id')
                        ->whereDate("temp_date", "=", $current_date)
                        ->where(function($query) use ($searchKey) {
                            $query->where(\DB::raw('IF(employee_temperature.is_guest = 1,guest_user_data.first_name,employee.first_name)'), 'LIKE', "%$searchKey%");
                            $query->orWhere(\DB::raw('IF(employee_temperature.is_guest = 1,guest_user_data.last_name,employee.last_name)'), 'LIKE', "%$searchKey%");
                            $query->orWhere(\DB::raw('IF(employee_temperature.is_guest = 1,guest_users.email_address,employee.email)'), 'LIKE', "%$searchKey%");
                            $query->orWhere(\DB::raw('IF(employee_temperature.is_guest = 1,guest_comp.business_name,emp_comp.business_name)'), 'LIKE', "%$searchKey%");

                            $query->orWhere('temperature', 'LIKE', "%$searchKey%");
                            $query->orWhere('temp_date', 'LIKE', "%$searchKey%");
                        })
                        ->get();
            } else {

                $getUserCompany = Company::where('user_id', Auth::user()->id)->first();

                $emp_temp_data = EmployeeTemperature::
                        selectRaw("
                            employee_temperature.*,
                        CONCAT(IF(employee_temperature.is_guest = 1,guest_user_data.first_name,employee.first_name),' ',
                        IF(employee_temperature.is_guest = 1,guest_user_data.last_name,employee.last_name)) as name,
                        IF(employee_temperature.is_guest = 1,guest_users.email_address,employee.email) as email,
                        IF(employee_temperature.is_guest = 1,guest_comp.business_name,emp_comp.business_name) as company
                        
                        ")
                        ->leftJoin('employee', 'employee.id', '=', 'employee_temperature.employee_id')
                        ->leftJoin('guest_users', 'guest_users.id', '=', 'employee_temperature.employee_id')
                        ->leftJoin('guest_user_data', 'guest_user_data.guest_user_id', '=', 'guest_users.id')
                        ->leftJoin('companies as emp_comp', 'employee.company_id', '=', 'emp_comp.id')
                        ->leftJoin('companies as guest_comp', 'guest_users.company_id', '=', 'guest_comp.id')
                        ->whereDate("temp_date", "=", $current_date)
                        ->where(function($query) use($getUserCompany) {
                            $query->where(function($query2) use($getUserCompany) {
                                $query2->where('emp_comp.id', $getUserCompany->id);
                                $query2->where('employee_temperature.is_guest', '<>', 1);
                            });

                            $query->orWhere(function($query2) use($getUserCompany) {
                                $query2->where('guest_comp.id', $getUserCompany->id);
                                $query2->where('employee_temperature.is_guest', 1);
                            });
                        })
                        ->where(function($query) use ($searchKey) {
                            $query->where(\DB::raw('IF(employee_temperature.is_guest = 1,guest_user_data.first_name,employee.first_name)'), 'LIKE', "%$searchKey%");
                            $query->orWhere(\DB::raw('IF(employee_temperature.is_guest = 1,guest_user_data.last_name,employee.last_name)'), 'LIKE', "%$searchKey%");
                            $query->orWhere(\DB::raw('IF(employee_temperature.is_guest = 1,guest_users.email_address,employee.email)'), 'LIKE', "%$searchKey%");
                            $query->orWhere(\DB::raw('IF(employee_temperature.is_guest = 1,guest_comp.business_name,emp_comp.business_name)'), 'LIKE', "%$searchKey%");

                            $query->orWhere('temperature', 'LIKE', "%$searchKey%");
                            $query->orWhere('temp_date', 'LIKE', "%$searchKey%");
                        })
                        ->get();

//                if(!empty($searchKey)) {
//                    $emp_temp_data = $this->repository->getAllEmpTempByCompany($this->company_id,$current_date,$searchKey);
//                }
//                else {
//                    $emp_temp_data = $this->repository->getAllEmpTempByCompany($this->company_id,$current_date);
//                }
            }

            $emp_temp = $emp_temp_data->filter(function ($item) {
//                if($item->employee) {
//                    $item->name = $item->employee->first_name.' '.$item->employee->last_name;
//                    $item->last_name = $item->employee->last_name;
                $item->date = date('d-M-Y', strtotime($item->temp_date));
                $item->time = date('h:i A', strtotime($item->temp_date));
//
////                    $company = Company::where('id',$item->employee->company_id)->first();
////                    if(!empty($company)) {
////                        $item->company = $company->business_name;
////                    }
                if ($item->temperature < 37.5) {
                    $item->status = 'Green';
                } else {
                    $item->status = 'Red';
                }
//
//                    $item->temperature = number_format($item->temperature,2) + 0;
//                    return  $item->email = $item->employee->email;
//                }
                return $item;
            });
            return response()->json(['status' => 'success', 'data' => $emp_temp, 'code' => 200], 200);
        } catch (\Exception $e) {
            Log::error('People Temp management error:' . $e->getMessage());
            return '';
        }
    }

    public function create() {
        $getUserCompany = Company::where('user_id', Auth::user()->id)->first();
        if (!empty($getUserCompany)) {
            $this->company_id = $getUserCompany->id;
            $employees = $this->repository->getAllEmpByCompany($this->company_id);
            return view('backend.emp_temp.form', compact('employees'));
        } else {
            return redirect()->route('dashboard')->with('failure', 'Company Not Found');
        }
    }

    public function store(EmpTemperatureStoreRequest $request) {
        try {
            $input = $request->only('temperature', 'employee_id');

            $getEmp = EmployeeTemperature::where('employee_id', $input['employee_id'])->first();
            if (!empty($getEmp)) {
                $input['temp_date'] = date("Y-m-d H:i:s");
                $this->repository->update($getEmp->id, $input);
            } else {
                $emp_temp_data['employee_id'] = $input['employee_id'];
                $emp_temp_data['temperature'] = $input['temperature'];
                $emp_temp_data['temp_date'] = date("Y-m-d H:i:s");
                $this->repository->store($emp_temp_data);
            }
            return redirect()->route('emp_temp.index')->with('success', 'New People Temp added successfully');
        } catch (\Exception $e) {
            Log::error('People Temp management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('emp_temp.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id) {
        try {
            $emp_temp = $this->repository->find($id);
            return view('backend.emp_temp.form', compact('emp_temp'));
        } catch (\Exception $e) {
            Log::error('People Temp management error:' . $e->getMessage());
            return redirect()->route('emp_temp.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(EmpTemperatureStoreRequest $request, $id) {

        try {
            $input = $request->only('temperature', 'employee_id');
            $this->repository->update($id, $input);
            return redirect()->route('emp_temp.index')->with('success', 'People Temp updated successfully');
        } catch (\Exception $e) {
            Log::error('People Temp management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('emp_temp.index')->with('Something went wrong');
        }
    }

    public function destroy($id) {
        try {
            $this->repository->delete($id);
            return redirect()->route('emp_temp.index')->with('success', 'People Temp deleted successfully');
        } catch (\Exception $e) {
            Log::error('People Temp management error:' . $e->getMessage());
            return redirect()->route('emp_temp.index')->with('failure', 'Something went wrong');
        }
    }
    public function updateEmail(Request $request)
    {
        try {
            Setting::where('company_id', auth()->user()->company->id)->update([
                'report_email' => $request->email
            ]);
            return redirect(route('emp_temp.index'))->with('success','Report Email update');
        }catch (\Exception $e){
            return redirect(route('emp_temp.index'))->with('failure','something went wrong');
        }
    }

}
