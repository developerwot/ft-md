<?php

namespace App\Http\Controllers\Backend;

use App\BookingPolicy;
use App\BuildingAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\LocationStoreRequest;
use App\Http\Requests\Backend\LocationUpdateRequest;
use App\LocationAccess;
use App\LocationDevice;
use App\TimeslotAccess;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class LocationAccessController extends Controller
{
    public function index(){
        return view('backend.location_access.index');
    }

    public function tableAjax(){
      $locations=  LocationAccess::with('buildings')->where('company_id',auth()->user()->company->id)->get();
    return response()->json(['status'=>1,'message'=>'table fetched','data'=>$locations],200);
    }

    public function create(){
       $buildings= BuildingAccess::where([['company_id','=',auth()->user()->company->id],['building_status','=',1]])->get();
       return view('backend.location_access.form',compact('buildings'));
    }
    public function store(LocationStoreRequest $request){
        try {
            $inputs = $request->except('_token');
            $building_limit=BuildingAccess::where('id',$inputs['building_access_id'])->value('building_limit');
if($inputs['location_limit']>$building_limit){
    return redirect()->back()->withInput()->with('failure','Building limit is for '.$building_limit.' people');
}else{
  $total_locations=  LocationAccess::where('building_access_id',$inputs['building_access_id'])->get();
  if($total_locations){
      $occupied=collect($total_locations)->sum('location_limit');
      $remain=$building_limit-$occupied;
      if($remain==0){
          return redirect()->back()->withInput()->with('failure','building limit reached');
      }
      elseif($inputs['location_limit']>$remain){
          return redirect()->back()->withInput()->with('failure','Only '.$remain.' people limit remain now');
      }
      else{
          $QR_string = 'intelly-scan_L-o-c-a-t-i-o-n_scan_qr_' .str_replace(' ', '',  $inputs['location_name']) . '_'  . $inputs['building_access_id'];
          $inputs['location_QR'] = $QR_string;
          QrCode::size(400)
              ->format('png')
              ->merge('http://3.125.234.142:8080/images/site_logo.png', 0.3, true)
              ->errorCorrection('H')
              ->generate($QR_string, storage_path('app/public/QR_codes/'.$QR_string.'.png'));
//          dd($inputs);

          LocationAccess::create($inputs);
          return redirect(route('location-access.index'))->with('success','Location added!');
      }
  }
}

        }catch(\Exception $e){
            return redirect(route('location-access.index'))->with('failure','Something went wrong');
        }


    }
    public function edit($id){
        $buildings=BuildingAccess::where([['company_id','=',auth()->user()->company->id],['building_status','=',1]])->get();
      $location=  LocationAccess::with('buildings')->where('id',$id)->first();
//      dd($location);
    return view('backend.location_access.form',compact('buildings','location'));
    }

    public function update(LocationUpdateRequest $request ,$id){
      try{
          $inputs=  $request->except('_token');
          $building_limit=BuildingAccess::where('id',$inputs['building_access_id'])->value('building_limit');
          if($inputs['location_limit']>$building_limit){
              return redirect()->back()->withInput()->with('failure','Building limit is for '.$building_limit.' people');
          }else {
              $total_locations = LocationAccess::where('building_access_id', $inputs['building_access_id'])->get();
              if ($total_locations) {
                  $occupied = collect($total_locations)->where('id','!=',$id)->sum('location_limit');
                  $remain = $building_limit - $occupied;
                  if ($remain == 0) {
                      return redirect()->back()->withInput()->with('failure', 'building limit reached');
                  } elseif ($inputs['location_limit'] > $remain) {
                      return redirect()->back()->withInput()->with('failure', 'Only ' . $remain . ' people limit remain now');
                  } else {
                      LocationAccess::where('id', $id)->update($inputs);
                      return redirect(route('location-access.index'))->with('success', 'Location updated!');
                  }
              }
          }

      }catch(\Exception $e){
          return redirect(route('location-access.index'))->with('failure','Something went wrong');
      }

    }

    public function deleteLocation($id){
       try{
           LocationAccess::where('id',$id)->delete();
           TimeslotAccess::where('location_access_id',$id)->delete();
           LocationDevice::where('location_id',$id)->delete();
           BookingPolicy::where('location_id',$id)->delete();
           return redirect(route('location-access.index'))->with('success','Location deleted!');
       }catch (\Exception $e){
           return redirect(route('location-access.index'))->with('failure','something went wrong');
       }
    }

public function tableAjax2(Request $request){
    $locations=  LocationAccess::with('buildings')->where([['company_id',auth()->user()->company->id],['building_access_id',$request->building_id]])->get();
    return response()->json(['status'=>1,'message'=>'table fetched','data'=>$locations],200);
}
}
