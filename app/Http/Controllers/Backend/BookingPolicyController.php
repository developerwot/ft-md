<?php

namespace App\Http\Controllers\Backend;

use App\BookingPolicy;
use App\BuildingAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BookingPolicyRequest;
use App\LocationDevice;
use App\TimeslotAccess;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookingPolicyController extends Controller
{

    public function index(){
      $booking=  BookingPolicy::where('company_id',auth()->user()->company->id)->get();
        return view('backend.booking_policy.index',compact('booking'));
    }

    public function create(){
        $buildings=BuildingAccess::where('company_id',auth()->user()->company->id)->get();
        return view('backend.booking_policy.form',compact('buildings'));
    }
    public function store(BookingPolicyRequest $request){
//dd($request->all());
        if($request->is_permitted==1){
            $rules = [
                'slots' => 'required|array|min:1',
                'slots.*.start' => 'required',
                'slots.*.end' => 'required'
            ];

            $customMessages = [
                'slots.required' => 'You have to add at least one slot.',
                'slots.*.start.required' => 'start time can not be empty',
                'slots.*.end.required' => 'start time can not be empty'

            ];
            $this->validate($request, $rules, $customMessages);
        }
        $drange = explode('-', $request->date_range);
        $from=date('Y-m-d', strtotime(trim($drange[0])));
        $to=date('Y-m-d', strtotime(trim($drange[1])));
        $exist=BookingPolicy::where('building_id','=',$request->building_id)->whereIn('location_id',$request->location_id)->get();
        $available=0;
        foreach ($exist as $val) {
            if ($val->from_date <= $from && $val->to_date >= $from) {
                $available++;
            } elseif (($val->from_date <= $to) && ($val->to_date >= $to)) {
                $available++;
            }
        }
        if($available==0){
            if($request->is_permitted==1){
                $slotss=array_values($request->slots);
                foreach ($request->location_id as $location){
                    $ids=[];
                    for($k=0;$k < collect($slotss)->count();$k++){
//                        $start_time=Carbon::parse($slotss[$k]['start'])->format('H:i:s');
//                        $end_time=Carbon::parse($slotss[$k]['end'])->format('H:i:s');
//                        $exist=TimeslotAccess::where([['company_id','=',auth()->user()->company->id],['building_access_id','=',$request->building_id],['location_access_id','=',$location],['is_policy_added','=',1]])->get();
//                        $available=0;
//                        foreach ($exist as $val) {
//                            if ($val->start_time <= $start_time && $val->end_time > $start_time) {
//                                $available++;
//                            } elseif (($val->end_time <= $end_time) && ($val->start_time >= $end_time)) {
//                                $available++;
//                            }
//                        }
//                       if($available==0){
                           $time= TimeslotAccess::create([
                               'company_id'=>auth()->user()->company->id,
                               'building_access_id'=>$request->building_id,
                               'location_access_id'=>$location,
                               'start_time'=>Carbon::parse($slotss[$k]['start'])->format('H:i:s'),
                               'end_time'=>Carbon::parse($slotss[$k]['end'])->format('H:i:s'),
                               'status'=>1,
                               'is_policy_added'=>1
                           ]);
                           $ids[]=$time->id;
//                       }
                    }
                    $input['company_id']=auth()->user()->company->id;
                    $input['building_id']=$request->building_id;
                    $input['location_id']=$location;
                    $input['slot_ids']=$ids;
                    $input['is_permitted']=$request->is_permitted;
                    $input['from_date']=$from;
                    $input['to_date' ]=$to;
                    BookingPolicy::create($input);
                }
            }
            else{
                foreach ($request->location_id as $location){
                    $input['company_id']=auth()->user()->company->id;
                    $input['building_id']=$request->building_id;
                    $input['location_id']=$location;
                    $input['is_permitted']=$request->is_permitted;
                    $input['from_date']=$from;
                    $input['to_date' ]= $to;
                    BookingPolicy::create($input);
                }
            }
            return redirect(route('booking-policy.index'))->with('success','Booking Policy added');
        }else{
            return redirect()->back()->withInput()->with('failure','Booking policy is already added for this date range');
        }
    }

    public function ajaxTable(Request $request){
       $bookings= BookingPolicy::with('location','building')->where('company_id',auth()->user()->company->id)->get();
//       dd($bookings);
       return response()->json(['status'=>1,'success'=>true,'data'=>$bookings],200);
    }

    public function edit($id){
     $booking_policy=   BookingPolicy::where('id',$id)->first();

        $buildings=BuildingAccess::where('company_id',auth()->user()->company->id)->get();
   if($booking_policy->slot_ids==null){
       $time_slots= null;
   }else{
       $time_slots=TimeslotAccess::whereIn('id',$booking_policy->slot_ids)->get();
   }
    return  view('backend.booking_policy.form',compact('buildings','booking_policy','time_slots'));
    }

    public function update(BookingPolicyRequest $request ,$id)
    {
        if ($request->is_permitted == 1) {
            $rules = [
                'slots' => 'required|array|min:1',
                'slots.*.start' => 'required',
                'slots.*.end' => 'required'
            ];

            $customMessages = [
                'slots.required' => 'You have to add at least one slot.',
                'slots.*.start.required' => 'start time can not be empty',
                'slots.*.end.required' => 'start time can not be empty'

            ];

            $this->validate($request, $rules, $customMessages);
        }
        $drange = explode('-', $request->date_range);
        $from = date('Y-m-d', strtotime(trim($drange[0])));
        $to = date('Y-m-d', strtotime(trim($drange[1])));
        $exist = BookingPolicy::where('id','!=',$id)->where('building_id', '=', $request->building_id)->whereIn('location_id', $request->location_id)->get();
        $available = 0;
        foreach ($exist as $val) {
            if ($val->from_date <= $from && $val->to_date >= $from) {
                $available++;
            } elseif (($val->from_date <= $to) && ($val->to_date >= $to)) {
                $available++;
            }
        }

        if ($available == 0) {
            $previous_slots= BookingPolicy::where('id',$id)->value('slot_ids');
            if($previous_slots!=null){
                TimeslotAccess::whereIn('id',$previous_slots)->delete();
            }


            if ($request->is_permitted == 1) {
                $slotss=array_values($request->slots);
//                dd($slotss);
                foreach ($request->location_id as $location){
                    $ids=[];
                    for($k=0;$k < collect($slotss)->count();$k++){
//                        $start_time=Carbon::parse($slotss[$k]['start'])->format('H:i:s');
//                        $end_time=Carbon::parse($slotss[$k]['end'])->format('H:i:s');
//                        $exist=TimeslotAccess::where([['company_id','=',auth()->user()->company->id],['building_access_id','=',$request->building_id],['location_access_id','=',$location],['is_policy_added','=',1]])->get();
//                        $available=0;
//                        foreach ($exist as $val) {
//                            if ($val->start_time <= $start_time && $val->end_time > $start_time) {
//                                $available++;
//                            } elseif (($val->end_time <= $end_time) && ($val->start_time >= $end_time)) {
//                                $available++;
//                            }
//                        }
//                        dd($exist);
//                        if($available==0) {
                            $time = TimeslotAccess::create([
                                'company_id' => auth()->user()->company->id,
                                'building_access_id' => $request->building_id,
                                'location_access_id' => $location,
                                'start_time'=>Carbon::parse($slotss[$k]['start'])->format('H:i:s'),
                                'end_time'=>Carbon::parse($slotss[$k]['end'])->format('H:i:s'),
                                'status' => 1,
                                'is_policy_added' => 1
                            ]);
                            $ids[] = $time->id;
                        }
//                    }

                    $input['company_id'] = auth()->user()->company->id;
                    $input['building_id'] = $request->building_id;
                    $input['location_id'] = $location;
                    $input['slot_ids'] = $ids;
                    $input['is_permitted'] = $request->is_permitted;
                    $input['from_date'] = $from;
                    $input['to_date'] = $to;
                    BookingPolicy::where('id',$id)->update($input);
                }
            }
            else {
                foreach ($request->location_id as $location) {
                    $input['company_id'] = auth()->user()->company->id;
                    $input['building_id'] = $request->building_id;
                    $input['location_id'] = $location;
                    $input['slot_ids']=null;
                    $input['is_permitted'] = $request->is_permitted;
                    $input['from_date'] = $from;
                    $input['to_date'] = $to;
                    BookingPolicy::where('id',$id)->update($input);
                }
            }
            return redirect(route('booking-policy.index'))->with('success','Booking Policy updated');
        }else{
            return redirect(route('booking-policy.index'))->with('failure','Booking Policy already added in this date range');
        }


    }

    public function deletePolicy($id){
        $slots=BookingPolicy::where('id',$id)->value('slot_ids');
//        dd($slots);
        try {
            if ($slots != null) {
                TimeslotAccess::whereIn('id', $slots)->delete();
            }
            BookingPolicy::where('id', $id)->delete();
            return redirect(route('booking-policy.index'))->with('success','Booking Policy deleted!');
        }
        catch (\Exception $e){
            return redirect(route('booking-policy.index'))->with('failure','something went wrong!');
        }
    }
}
