<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\UserStoreRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\UserUpdateRequest;
use App\Repositories\Backend\User\UserContract;
use App\Role;
use Illuminate\Support\Facades\Log;


class UserController extends Controller
{
    protected $repository;

    public function __construct(UserContract $repository)
    {
        $this->middleware('admin_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {
            $users = $this->repository->getAll();
            return view('backend.users.index', compact('users'));

        } catch (\Exception $e) {
            Log::error('User management error:' . $e->getMessage());
            return redirect()->route('users.index')->with('failure', 'Something went wrong');
        }
    }

    public function create()
    {
        $roles = Role::all();
        return view('backend.users.form', compact('roles'));
    }

    public function store(UserStoreRequest $request)
    {
        try {
            $input = $request->only('name', 'email', 'roles');
            $user = $this->repository->store($input);
            return redirect()->route('users.index')->with('success', 'New User added successfully');

        } catch (\Exception $e) {
            Log::error('User management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('users.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $user = $this->repository->find($id);
            $roles = Role::all();
            return view('backend.users.form', compact('user','roles'));

        } catch (\Exception $e) {
            Log::error('User management error:' . $e->getMessage());
            return redirect()->route('users.index')->with('failure', 'Something went wrong');
        }
    }
    public function show($id)
    {
        try {
            $user = $this->repository->find($id);
            $roles = Role::all();
            return view('backend.users.form', compact('user','roles'));

        } catch (\Exception $e) {
            Log::error('User management error:' . $e->getMessage());
            return redirect()->route('users.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(UserUpdateRequest $request, $id)
    {

        try {
            $input = $request->only('name', 'email', 'roles');
            $this->repository->update($id, $input);
            return redirect()->route('users.index')->with('success', 'User updated successfully');

        } catch (\Exception $e) {
            Log::error('User management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('users.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return redirect()->route('users.index')->with('success', 'User deleted successfully');

        } catch (\Exception $e) {
            Log::error('User management error:' . $e->getMessage());
            return redirect()->route('users.index')->with('failure', 'Something went wrong');
        }
    }
}
