<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\RoleStoreRequest;
use App\Http\Requests\Backend\UserStoreRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Role\RoleContract;
use App\Repositories\Backend\User\UserContract;
use Illuminate\Support\Facades\Log;

class RoleController extends Controller
{
    protected $repository;

    public function __construct(RoleContract $repository)
    {
        $this->middleware('admin_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {
            $roles = $this->repository->getAll();
            return view('backend.roles.index', compact('roles'));

        } catch (\Exception $e) {
            Log::error('Role management error:' . $e->getMessage());
            return redirect()->route('roles.index')->with('failure', 'Something went wrong');
        }
    }

    public function create()
    {
        return view('backend.roles.form');
    }

    public function store(RoleStoreRequest $request)
    {
        try {
            $input = $request->only('name', 'display_name', 'description');
            $role = $this->repository->store($input);
            return redirect()->route('roles.index')->with('success', 'New Role added successfully');

        } catch (\Exception $e) {
            Log::error('Role management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('roles.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $role = $this->repository->find($id);
            return view('backend.roles.form', compact('role'));

        } catch (\Exception $e) {
            Log::error('Role management error:' . $e->getMessage());
            return redirect()->route('roles.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(RoleStoreRequest $request, $id)
    {

        try {
            $input = $request->only('name', 'display_name', 'description');
            $this->repository->update($id, $input);
            return redirect()->route('roles.index')->with('success', 'Role updated successfully');

        } catch (\Exception $e) {
            Log::error('Role management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('roles.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return redirect()->route('roles.index')->with('success', 'Role deleted successfully');

        } catch (\Exception $e) {
            Log::error('Role management error:' . $e->getMessage());
            return redirect()->route('roles.index')->with('failure', 'Something went wrong');
        }
    }
}
