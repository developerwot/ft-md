<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BookingStoreRequest;
use App\Models\Company;
use App\Repositories\Backend\Booking\BookingContract;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class BookingController extends Controller
{
    protected $repository;

    public function __construct(BookingContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }

    public function index()
    {
        try {

            /* $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
             $building_policy_data = $this->repository->getAllByCompany($getUserCompany->id);

             $bookings = $building_policy_data->filter(function ($item){

                $item->date = date("d-M-Y",strtotime($item->booking_date));
                $item->time = date("H:i A",strtotime($item->booking_time));
                if($item->people) {
                    $item->people_name = $item->people->first_name.' '.$item->people->last_name;
                    $item->email = $item->people->email;
                }
                if($item->building) {
                    $item->building_name = $item->building->name;
                }
                else {
                    $item->building_name = '';
                }
                return $item;
            });*/
             return view('backend.bookings.index');

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return redirect()->route('dashboard')->with('failure', 'Something went wrong');
        }
    }

    public function get_ajax_data(Request $request)
    {
        try {

            $inputs = $request->only('query');
            if(!empty($inputs['query'])) {
                if(isset($inputs['query']['generalSearch']) && !empty($inputs['query']['generalSearch'])) {
                    $searchKey = $inputs['query']['generalSearch'];
                }
            }

             $getUserCompany = Company::where('user_id',Auth::user()->id)->first();


             if(!empty($searchKey)) {
                 $building_policy_data = $this->repository->getAllByCompany($getUserCompany->id,$searchKey);
             }
             else {
                 $building_policy_data = $this->repository->getAllByCompany($getUserCompany->id);
             }


             $bookings = $building_policy_data->filter(function ($item){

                $item->date = date("d-M-Y",strtotime($item->booking_date));
                $item->time = date("H:i A",strtotime($item->booking_time));
                if($item->people) {
                    $item->people_name = $item->people->first_name.' '.$item->people->last_name;
                    $item->email = $item->people->email;
                }
                if($item->building) {
                    $item->building_name = $item->building->name;
                }
                else {
                    $item->building_name = '';
                }
                return $item;
            });
            return response()->json(['status'=>'success','data'=>$bookings,'code'=>200],200);

        } catch (\Exception $e) {
            Log::error('Device management error:' . $e->getMessage());
            return '';
        }
    }

    public function create()
    {
        $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
        if(!empty($getUserCompany)) {

            $buildings = $this->repository->getAllBuildingByCompany($getUserCompany->id);
            $employees = $this->repository->getAllEmployeeByCompany($getUserCompany->id);
            return view('backend.bookings.form',compact('buildings','employees'));
        }
        else {
            return redirect()->route('dashboard')->with('failure', 'Company Not Found');
        }

    }

    public function store(BookingStoreRequest $request)
    {

        try {
            $input = $request->only('employee_id','building_id','booking_date','booking_time');
            $input['booking_date'] = date("Y-m-d",strtotime($input['booking_date']));
            $input['booking_time'] = date("H:i:s",strtotime($input['booking_time']));
            $bookings_exists = $this->repository->getBookingsByDate($input['building_id'],$input['booking_date']);
            $building =$this->repository->findBuildingById($input['building_id']);
            if($bookings_exists->count() == $building->building_limit) {
                return redirect()->back()->withInput()->withErrors(['This building has reached the maximum limit of persons']);
            }
            //dd($input);
            $bookings_exists_by_user = $this->repository->getBookingsByDate($input['building_id'],$input['booking_date'],$input['employee_id'],$input['booking_time']);
            if($bookings_exists_by_user->count() > 0) {
                return redirect()->back()->withInput()->withErrors(['Booking already available for person in building for date']);
            }

            $booking = $this->repository->store($input);
            return redirect()->route('bookings.index')->with('success', 'New Booking added successfully');


        } catch (\Exception $e) {
            Log::error('Booking management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('bookings.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $getUserCompany = Company::where('user_id',Auth::user()->id)->first();
            $booking = $this->repository->find($id);
            $employees = $this->repository->getAllEmployeeByCompany($getUserCompany->id);
            $buildings = $this->repository->getAllBuildingByCompany($getUserCompany->id);
            return view('backend.bookings.form',compact('booking','buildings','employees'));

        } catch (\Exception $e) {
            Log::error('Booking management error:' . $e->getMessage());
            return redirect()->route('bookings.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(BookingStoreRequest $request, $id)
    {

        try {
            //Start to checkexisting with edit parameter id
            $input = $request->only('employee_id','building_id','booking_date','booking_time');
            $input['booking_date'] = date("Y-m-d",strtotime($input['booking_date']));
            $input['booking_time'] = date("H:i:s",strtotime($input['booking_time']));

            $bookings_exists = $this->repository->getBookingsByDate($input['building_id'],$input['booking_date'],'','',$id);
            $building =$this->repository->findBuildingById($input['building_id']);
            if($bookings_exists->count() == $building->building_limit) {
                return redirect()->back()->withErrors(['This building has reached the maximum limit of persons']);
            }

            $bookings_exists_by_user = $this->repository->getBookingsByDate($input['building_id'],$input['booking_date'],$input['employee_id'],$input['booking_time'],$id);

            if($bookings_exists_by_user->count() > 0) {
                return redirect()->back()->withErrors(['Booking already available for person in building for date']);
            }

            $booking = $this->repository->find($id);
            $this->repository->update($id, $input);

            return redirect()->route('bookings.index')->with('success', 'Booking updated successfully');

        } catch (\Exception $e) {
            Log::error('Booking management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('bookings.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $booking = $this->repository->find($id);
            if($booking) {
                $this->repository->delete($id);
            }
            return redirect()->route('bookings.index')->with('success', 'Booking deleted successfully');

        } catch (\Exception $e) {
            Log::error('Booking management error:' . $e->getMessage());
            return redirect()->route('bookings.index')->with('failure', 'Something went wrong');
        }
    }
}