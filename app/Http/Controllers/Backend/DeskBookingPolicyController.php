<?php

namespace App\Http\Controllers\Backend;

use App\BuildingAccess;
use App\DeskAccess;
use App\DeskBookingPolicy;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeskBookingPolicyRequest;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DeskBookingPolicyController extends Controller
{
    public function index()
    {
        $booking = DeskBookingPolicy::where('company_id', auth()->user()->company->id)->get();
        return view('backend.desk_policies.index', compact('booking'));
    }

    public function create()
    {
        $buildings = BuildingAccess::where('company_id', auth()->user()->company->id)->get();
        return view('backend.desk_policies.form', compact('buildings'));
    }

    public function store(DeskBookingPolicyRequest $request)
    {
        if ($request->is_permitted == 1) {
            $rules = [
                'desks' => 'required|array|min:1',
                'desks.*' => 'required'
            ];

            $customMessages = [
                'desks' => 'You have add at-least one Desk',
                'desk.*.required' => 'Desk name is required'

            ];
            $this->validate($request, $rules, $customMessages);
        }
        $drange = explode('-', $request->date_range);
        $from = date('Y-m-d', strtotime(trim($drange[0])));
        $to = date('Y-m-d', strtotime(trim($drange[1])));
        $exist = DeskBookingPolicy::where('building_id', '=', $request->building_id)->whereIn('floor_id', $request->floor_id)->get();
        $available = 0;
        foreach ($exist as $val) {
            if ($val->from_date <= $from && $val->to_date >= $from) {
                $available++;
            } elseif (($val->from_date <= $to) && ($val->to_date >= $to)) {
                $available++;
            }
        }
        if ($available == 0) {
            if ($request->is_permitted == 1) {
                $desks = array_values($request->desks);
                foreach ($request->floor_id as $location) {
                    $ids = [];
                    for ($k = 0; $k < collect($desks)->count(); $k++) {
                        $QR_string = 'intelly-scan_D-e-s-k_scan_qr_' . str_replace(' ', '', $desks[$k]) . '_' . $location . '_' . $request->building_id . 'Booking_Policy';
                        QrCode::size(400)
                            ->format('png')
                            ->merge('http://3.125.234.142:8080/images/site_logo.png', 0.3, true)
                            ->errorCorrection('H')
                            ->generate($QR_string, storage_path('app/public/QR_codes/' . $QR_string . '.png'));
                        $time = DeskAccess::create([
                            'company_id' => auth()->user()->company->id,
                            'building_id' => $request->building_id,
                            'floor_id' => $location,
                            'desk_name' => $desks[$k],
                            'desk_QR' => $QR_string,
                            'desk_status' => 1,
                            'is_policy_added' => 1
                        ]);
                        $ids[] = $time->id;
                    }
                    $input['company_id'] = auth()->user()->company->id;
                    $input['building_id'] = $request->building_id;
                    $input['floor_id'] = $location;
                    $input['desk_ids'] = $ids;
                    $input['is_permitted'] = $request->is_permitted;
                    $input['from_date'] = $from;
                    $input['to_date'] = $to;
                    DeskBookingPolicy::create($input);
                }
            } else {
                foreach ($request->floor_id as $location) {
                    $input['company_id'] = auth()->user()->company->id;
                    $input['building_id'] = $request->building_id;
                    $input['floor_id'] = $location;
                    $input['is_permitted'] = $request->is_permitted;
                    $input['from_date'] = $from;
                    $input['to_date'] = $to;
                    DeskBookingPolicy::create($input);
                }
            }
            return redirect(route('desk-booking-policy.index'))->with('success', 'Booking Policy added');
        } else {
            return redirect()->back()->withInput()->with('failure', 'Booking policy is already added for this date range');
        }
    }

    public function ajaxTable(Request $request)
    {
        $bookings = DeskBookingPolicy::with('floor', 'building')->where('company_id', auth()->user()->company->id)->get();
        return response()->json(['status' => 1, 'success' => true, 'data' => $bookings], 200);
    }

    public function edit($id)
    {
        $booking_policy = DeskBookingPolicy::where('id', $id)->first();

        $buildings = BuildingAccess::where('company_id', auth()->user()->company->id)->get();
        if ($booking_policy->desk_ids == null) {
            $desks = null;
        } else {
            $desks = DeskAccess::whereIn('id', $booking_policy->desk_ids)->get();
        }
        return view('backend.desk_policies.form', compact('buildings', 'booking_policy', 'desks'));
    }

    public function update(DeskBookingPolicyRequest $request, $id)
    {
        if ($request->is_permitted == 1) {
            $rules = [
                'desks' => 'required|array|min:1',
                'desks.*' => 'required'
            ];

            $customMessages = [
                'desks' => 'You have add at-least one Desk',
                'desk.*.required' => 'Desk name is required'

            ];
            $this->validate($request, $rules, $customMessages);
        }
        $drange = explode('-', $request->date_range);
        $from = date('Y-m-d', strtotime(trim($drange[0])));
        $to = date('Y-m-d', strtotime(trim($drange[1])));
        $exist = DeskBookingPolicy::where('id', '!=', $id)->where('building_id', '=', $request->building_id)->whereIn('floor_id', $request->floor_id)->get();

        $available = 0;
        foreach ($exist as $val) {
            if ($val->from_date <= $from && $val->to_date >= $from) {
                $available++;
            } elseif (($val->from_date <= $to) && ($val->to_date >= $to)) {
                $available++;
            }
        }

        if ($available == 0) {
            $previous_slots = DeskBookingPolicy::where('id', $id)->value('desk_ids');
            if ($previous_slots != null) {
                DeskAccess::whereIn('id', $previous_slots)->delete();
            }


            if ($request->is_permitted == 1) {
                $desks = array_values($request->desks);

                foreach ($request->floor_id as $location) {
                    $ids = [];
                    for ($k = 0; $k < collect($desks)->count(); $k++) {

                        $QR_string = 'intelly-scan_D-e-s-k_scan_qr_' . str_replace(' ', '', $desks[$k]) . '_' . $location . '_' . $request->building_id . 'Booking_Policy';
                        QrCode::size(400)
                            ->format('png')
                            ->merge('http://3.125.234.142:8080/images/site_logo.png', 0.3, true)
                            ->errorCorrection('H')
                            ->generate($QR_string, storage_path('app/public/QR_codes/' . $QR_string . '.png'));
                        $time = DeskAccess::create([
                            'company_id' => auth()->user()->company->id,
                            'building_id' => $request->building_id,
                            'floor_id' => $location,
                            'desk_name' => $desks[$k],
                            'desk_QR' => $QR_string,
                            'desk_status' => 1,
                            'is_policy_added' => 1
                        ]);
                        $ids[] = $time->id;
                    }

                    $input['company_id'] = auth()->user()->company->id;
                    $input['building_id'] = $request->building_id;
                    $input['floor_id'] = $location;
                    $input['desk_ids'] = $ids;
                    $input['is_permitted'] = $request->is_permitted;
                    $input['from_date'] = $from;
                    $input['to_date'] = $to;
                    DeskBookingPolicy::where('id', $id)->update($input);
                }
            } else {
                foreach ($request->floor_id as $location) {
                    $input['company_id'] = auth()->user()->company->id;
                    $input['building_id'] = $request->building_id;
                    $input['floor_id'] = $location;
                    $input['desk_ids'] = null;
                    $input['is_permitted'] = $request->is_permitted;
                    $input['from_date'] = $from;
                    $input['to_date'] = $to;
                    DeskBookingPolicy::where('id', $id)->update($input);
                }
            }
            return redirect(route('desk-booking-policy.index'))->with('success', 'Booking Policy updated');
        } else {
            return redirect(route('desk-booking-policy.index'))->with('failure', 'Booking Policy already added in this date range');
        }


    }

    public function deletePolicy($id)
    {
        $slots = DeskBookingPolicy::where('id', $id)->value('desk_ids');
        try {
            if ($slots != null) {
                DeskAccess::whereIn('id', $slots)->delete();
            }
            DeskBookingPolicy::where('id', $id)->delete();
            return redirect(route('desk-booking-policy.index'))->with('success', 'Booking Policy deleted!');
        } catch (\Exception $e) {
            return redirect(route('desk-booking-policy.index'))->with('failure', 'something went wrong!');
        }
    }
}


