<?php

namespace App\Http\Controllers\Backend;

use App\BuildingAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AddBuildingRequest;
use App\Http\Requests\Backend\UpdateBuildingRequest;
use App\Models\Company;
use App\Repositories\Backend\BuildingAccess\BuildingAccessContract;
use Illuminate\Http\Request;

class BuildingAccessController extends Controller
{
    protected $repository;

    public function __construct(BuildingAccessContract $repository)
    {
        $this->middleware('company_role');
        $this->repository = $repository;
    }


    public function index(){
        return view('backend.building_access.index');
    }
    public function tableAjax(Request $request){
        $buildings=BuildingAccess::where('company_id',auth()->user()->company->id)->get();
        return response()->json(['status'=>1,'success'=>true,'data'=>$buildings],200);
    }

    public function create(){
        return view('backend.building_access.form');
    }

    public function  store(AddBuildingRequest $request)
    {
        try {
            $inputs = $request->except('_token');
            $this->repository->storeBuilding($inputs);
            return  redirect(route('booking-access.index'))->with('success','Building Added!');
        }catch (\Exception $e){
            return redirect(route('booking-access.index'))->with('failure','something went wrong');
        }
    }
    public function edit($id){
       $building= BuildingAccess::where('id',$id)->first();
        return view('backend.building_access.form',compact('building'));
    }

    public function update(UpdateBuildingRequest $request , $id){
        try {
            $data = $request->only('building_name', 'building_limit', 'building_status');
            $this->repository->updateBuilding($id, $data);
            return redirect(route('booking-access.index'))->with('success','Building details updated');
        }catch (\Exception $e){
            return redirect(route('booking-access.index'))->with('failure','something went wrong');
        }
    }

    public function deleteBuilding($id){
        try{
            $this->repository->deleteBuilding($id);
            return redirect(route('booking-access.index'))->with('success','Building removed');
        }
        catch(\Exception $e){
            return redirect(route('booking-access.index'))->with('failure','something went wrong');
        }
    }
}
