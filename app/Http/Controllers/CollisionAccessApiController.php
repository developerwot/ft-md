<?php

namespace App\Http\Controllers;

use App\CollisionAccess;
use App\Models\Employee;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\DB;

class CollisionAccessApiController extends Controller
{
    public function storeCollision(Request $request)
    {

        $input = json_decode($request->getContent(), true);
        if (empty($input) && !isset($input)) {
            return response()->json([
                "status" => 0,
                "message" => "something went wrong.!"
            ], 422);
        }
        $data = $input;
        if (!isset($data['api_key']) || (isset($data['api_key']) && $data['api_key'] != env('REST_API_KEY'))) {
            return response()->json([
                "status" => 0,
                "message" => "Api key not found"
            ], 422);
        }
        if (!isset($data['employee_id']) && $data['employee_id'] == '') {
            return response()->json([
                "status" => 0,
                "message" => "Employee is not found.!"
            ], 422);
        }
       try{
           $employee = Employee::findOrFail($data['employee_id']);
       }catch(\Exception $e){
           return response()->json([
               "status" => 1,
               "message" => "Employee not found!"
           ], 200);
       }
        $user = $employee->user;
        $fromUser = $user->id;
        $parsedDateTime = Carbon::createFromTimestampMs($data['time'])->toDateTimeString();
//        dd($parsedDateTime);
        foreach ($data['data'] as $collision) {
            $toUser = User::where('major_id', $collision['majorId'])->where('minor_id', $collision['minorId'])->first();
            if ($toUser) {
                $collisionData = [
                    'from_user' => $fromUser,
                    'to_user' => $toUser->id,
                    'contacted_at' => $parsedDateTime
                ];
                $this->createRealAndReverseRecord($collisionData, $data['time']);
            }
        }
        return response()->json([
            "status" => 1,
            "message" => "collision data stored successfully.!"
        ], 200);
    }

    public function createRealAndReverseRecord($collision, $time)
    {
        $parsedDateTime = Carbon::createFromTimestampMs($time)->toDateTimeString();

        // RealRecord
        if ($this->checkIfRecordIsAlreadyCreatedInLastFiveMinutes($collision, $time) == true) {
//            dd(1);
            CollisionAccess::create($collision);
        }

        // reverseRecord
        $reverseCollision = [
            'from_user' => $collision['to_user'],
            'to_user' => $collision['from_user'],
            'contacted_at' => $parsedDateTime
        ];
        if ($this->checkIfRecordIsAlreadyCreatedInLastFiveMinutes($reverseCollision, $time) == true) {
//            dd(2);
            CollisionAccess::create($reverseCollision);
        }
    }

    public function checkIfRecordIsAlreadyCreatedInLastFiveMinutes($collision, $time)
    {
        $minuteOlderThenCurrentTime = Carbon::createFromTimestampMs($time);

        $reverseRecordCreatedInLast5Minutes = CollisionAccess::where('from_user', $collision['from_user'])
            ->where('to_user', $collision['to_user'])
            ->whereDate('contacted_at', '<=', $minuteOlderThenCurrentTime->toDateTimeString())
            ->whereDate('contacted_at', '=', Carbon::today())
            ->orderBy('contacted_at', 'desc')->latest()->first();
//dd($reverseRecordCreatedInLast5Minutes);
//        $given=new \DateTime($reverseRecordCreatedInLast5Minutes->contacted_at);
//        $given->setTimezone(new \DateTimeZone("UTC"));

//dd($reverseRecordCreatedInLast5Minutes->contacted_at ,Carbon::parse($reverseRecordCreatedInLast5Minutes->contacted_at, $reverseRecordCreatedInLast5Minutes->timezone)->setTimezone('UTC'),$given->format('Y-m-d H:i:s') ,$minuteOlderThenCurrentTime->toDateTimeString(),date('i', strtotime($minuteOlderThenCurrentTime->toDateTimeString())), date('i', strtotime($reverseRecordCreatedInLast5Minutes->contacted_at)));



        if ($reverseRecordCreatedInLast5Minutes != null) {
            if($minuteOlderThenCurrentTime<Carbon::parse($reverseRecordCreatedInLast5Minutes->contacted_at)){
                return false;
            }
            $lastEntry=Carbon::parse($reverseRecordCreatedInLast5Minutes->contacted_at);
            $difference = $lastEntry->diffInMinutes($minuteOlderThenCurrentTime);

            if ($difference >= 5) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }

    }

    public function getOverAllStates(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (empty($input) && !isset($input)) {
            return response()->json([
                "status" => 0,
                "message" => "something went wrong.!"
            ], 422);
        }
        $data = $input;
        if (!isset($data['api_key']) || (isset($data['api_key']) && $data['api_key'] != env('REST_API_KEY'))) {
            return response()->json([
                "status" => 0,
                "message" => "Api key not found"
            ], 422);
        }
        try{
            $employee = Employee::findOrFail($data['employee_id']);
        }catch(\Exception $e){
            return response()->json([
                "status" => 1,
                "message" => "Employee not found!"
            ], 200);
        }
        $user = $employee->user;
        $fromUser = $user->id;

        $from = date('Y-m-d', strtotime('-13 days', strtotime(date('Y-m-d'))));
        $to = date('Y-m-d', strtotime('+1 days', strtotime(date('Y-m-d'))));
        $contacted = CollisionAccess::where('from_user', $fromUser)->whereBetween('contacted_at', [$from, $to])->get();
        $contactedCount = $contacted->count();
        $contacts = [];
        foreach ($contacted as $val) {
            if (!in_array(User::where('id', $val['to_user'])->first(['major_id', 'minor_id']), $contacts)) {
                array_push($contacts, User::where('id', $val['to_user'])->first(['major_id', 'minor_id']));
            }

        }
        return response()->json(['success' => true,
            'status' => 1,
            'message' => 'Overall states fetched',
            'contactsCount' => $contactedCount,
            'contacts' => $contacts
        ]);
    }

    public function getCollisionPerDayReport(Request $request)
    {
        $input = json_decode($request->getContent(), true);
        if (empty($input) && !isset($input)) {
            return response()->json([
                "status" => 0,
                "message" => "something went wrong.!"
            ], 422);
        }
        $data = $input;
        if (!isset($data['api_key']) || (isset($data['api_key']) && $data['api_key'] != env('REST_API_KEY'))) {
            return response()->json([
                "status" => 0,
                "message" => "Api key not found"
            ], 422);
        }
        try{
            $employee = Employee::findOrFail($data['employee_id']);
        }catch(\Exception $e){
            return response()->json([
                "status" => 1,
                "message" => "Employee not found!"
            ], 200);
        }

        $user = $employee->user;
        $fromUser = $user->id;
//dd($fromUser);
        $from = date('Y-m-d', strtotime('-13 days', strtotime(date('Y-m-d'))));
        $to = date('Y-m-d', strtotime('+1 days', strtotime(date('Y-m-d'))));
        $contacted = DB::table('collision_accesses')
            ->whereBetween('contacted_at', [$from, $to])
            ->where('from_user', $fromUser)
            ->join('users', 'users.id', '=', 'collision_accesses.to_user')
            ->select(DB::raw('collision_accesses.contacted_at,month(collision_accesses.contacted_at) as month,EXTRACT(DAY FROM collision_accesses.contacted_at)as date'), DB::raw('users.major_id,users.minor_id'))
            ->orderBy('collision_accesses.contacted_at', 'desc')
//            ->distinct()
            ->get();
        $contacted = collect($contacted)->groupBy('date');
        $formatted = [];
        foreach ($contacted as $key => $val) {
            foreach ($val as $k => $final) {
                $formatted[$key]['month'] = $final->month;
                $formatted[$key]['count'] = $val->count();
                $formatted[$key]['dayOfMonth'] = $key;
                $formatted[$key]['contacts'][$k]['major'] = $final->major_id;
                $formatted[$key]['contacts'][$k]['minor'] = $final->minor_id;
                $formatted[$key]['contacts']=array_unique($formatted[$key]['contacts'],SORT_REGULAR);
            }
        }

        return response()->json(['success' => true, 'status' => 1, 'message' => 'last 14 days contacts fetched', 'data' => array_values($formatted)], 200);
    }
}
