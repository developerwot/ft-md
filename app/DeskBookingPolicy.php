<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeskBookingPolicy extends Model
{
    protected $guarded=[];

    protected $casts = [
        'desk_ids' => 'array',
    ];

    public function building(){
        return $this->belongsTo(BuildingAccess::class,'building_id');
    }

    public function floor(){
        return $this->belongsTo(FloorAccess::class,'floor_id');
    }
}
