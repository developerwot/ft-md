<?php

/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;

function isActiveRoute($route, $output = "m-menu__item--active")
{
    if (Route::currentRouteName() == $route) return $output;
}


/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active for sub menu like nav.
|
*/
function areActiveRoutes(Array $routes, $output = "m-menu__item--open m-menu__item--active")
{
    foreach ($routes as $route)
    {
        if (Route::currentRouteName() == $route) return $output;
    }

}


function generateOtpCode()
{

    $code = rand(111111,999999);
    if(DB::table('password_resets')->where('token', $code)->first())
    {
        generateOtpCode();
    }
    return $code;
}

const user_role = 2;

function checkUser(){
    $digits = 5;
    do {
        $major = (int)str_pad( rand(1, 65535),$digits,0);
    } while ( \App\User::where( 'major_id', $major )->exists() || $major>65535 );

    do {
        $minor =(int)str_pad( rand(1, 65535),$digits,0);
    } while ( \App\User::where( 'minor_id', $minor)->exists()  || $minor>65535 );
    return $data=['minor'=>$minor,'major'=>$major];
}

