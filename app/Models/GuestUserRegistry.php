<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuestUserRegistry extends Model
{

	protected $table = 'guest_user_registry';

	protected $fillable = ['guest_user_id','people_file','training_status'];

    public function guest_user() {
        return $this->belongsTo('App\Models\GuestUser', 'guest_user_id');
    }

}
