<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class GuestUserData extends Model
{
    //use SoftDeletes;
	protected $table = 'guest_user_data';

	protected $fillable = ['first_name','last_name','company_id',
                          'dob','address',
                          'city','postal_code',
                          'telephone_number',
                          'address_proof','identity_proof',
                          'company_name','guest_user_id'
                            ];

    public function guest_user() {
        return $this->belongsTo('App\Models\GuestUser', 'guest_user_id');
    }

}
