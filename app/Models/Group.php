<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;
	protected $table = 'groups';

	protected $fillable = ['name', 'description','company_id'];

    public function group_employees() {
        return $this->hasMany('App\Models\GroupEmployee', 'group_id');
    }
    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

}
