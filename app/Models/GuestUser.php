<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuestUser extends Model
{
    use SoftDeletes;
	protected $table = 'guest_users';

	protected $fillable = ['is_first_name','is_last_name','company_id','guest_user_company_id',
                          'is_dob','is_address',
                          'is_city','is_postal_code',
                          'is_telephone_number','from_date',
                          'to_date','email_address',
                          'receiver_employee_id','instructions_upon_arrival',
                          'is_address_proof','is_identity_proof',
                          'is_company_name','verify_code','is_completed','completed_at'
                            ];

    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'receiver_employee_id');
    }
    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
    public function guest_registry() {
        return $this->hasOne('App\Models\GuestUserRegistry', 'guest_user_id');
    }

}
