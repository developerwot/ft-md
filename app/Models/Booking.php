<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
	protected $table = 'bookings';

	protected $fillable = ['building_id','employee_id','booking_date','booking_time'];

  /*  public function device_people() {
        return $this->hasMany('App\Models\DevicePeople', 'device_id');
    }*/
    public function building() {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }
    public function people() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($device) { // before delete() method call this

            //$device->device_people()->delete();
            // do the rest of the cleanup...
        });
    }
}
