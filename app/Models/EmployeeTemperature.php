<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class EmployeeTemperature extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'employee_temperature';
    protected $fillable = [
        'employee_id', 'temperature','temp_date','is_guest','created_at','updated_at','deleted_at'
    ];
    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }
}
