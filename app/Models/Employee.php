<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'employee';
    protected $fillable = [
        'first_name', 'last_name', 'email','status',
        'company_id','user_id'
    ];
    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function groups() {
        return $this->belongsToMany('App\Models\Group', 'group_employees');
    }
    public function employee_temperature() {
        return $this->hasMany('App\Models\EmployeeTemperature', 'employee_id');
    }
    public function employee_temperature_log() {
        return $this->hasMany('App\Models\EmployeeTemperatureLog', 'employee_id');
    }
    public function people_registry() {
        return $this->hasMany('App\Models\PeopleRegistry', 'employee_id');
    }
    public function group_emp() {
        return $this->hasMany('App\Models\GroupEmployee', 'employee_id');
    }
    public function device_people() {
        return $this->hasOne('App\Models\DevicePeople', 'employee_id');
    }
    public function people_attendance() {
        return $this->hasMany('App\Models\PeopleAttendance', 'employee_id');
    }
    public function contact_tracing() {
        return $this->hasMany('App\Models\ContactTracing', 'employee_id');
    }
    public function emp_contact_tracing() {
        return $this->hasMany('App\Models\ContactTracing', 'contact_employee_id');
    }
    public function booking() {
        return $this->hasMany('App\Models\Booking', 'employee_id');
    }
    
    public static function boot() {
        parent::boot();

        static::deleting(function($employee) { // before delete() method call this

                $employee->employee_temperature()->delete();
                $employee->employee_temperature_log()->delete();
                $employee->user()->delete();
                $employee->device_people()->delete();
                $employee->contact_tracing()->delete();
                $employee->emp_contact_tracing()->delete();
                $employee->booking()->delete();

            // do the rest of the cleanup...
        });
    }
}
