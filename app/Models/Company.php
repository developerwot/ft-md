<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    public function employees() {
        return $this->hasMany('App\Models\Employee', 'company_id');
    }
    public function groups() {
        return $this->hasMany('App\Models\Group', 'company_id');
    }
    public function devices() {
        return $this->hasMany('App\Models\Device', 'company_id');
    }
    public function buildings() {
        return $this->hasMany('App\Models\Building', 'company_id');
    }
    public function settings() {
        return $this->hasOne('App\Models\Setting', 'company_id');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($company) { // before delete() method call this
            $company->employees->each(function($emp) use($company) {
               /* if ($company->isForceDeleting()) {
                    $emp->employee_temperature()->withTrashed()->forceDelete();
                    $emp->user()->withTrashed()->forceDelete();
                }
                else {*/
                    $emp->employee_temperature()->delete();
                    $emp->employee_temperature_log()->delete();
                    $emp->user()->delete();
               // }
            });
           /* if ($company->isForceDeleting()) {
                $company->employees()->withTrashed()->forceDelete();
            }
            else {*/
                $company->employees()->delete();
                $company->groups()->delete();
                $company->devices()->delete();
                $company->buildings()->delete();
           // }


        });
    }
}
