<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeopleAttendance extends Model
{

	protected $table = 'people_attendance';

    protected $fillable = ['employee_id', 'date','check_in','check_out','break_in','break_out'];

    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }


}
