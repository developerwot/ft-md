<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'host',
        'port',
        'video_stream_url',
        'video_stream_url_out',
        'face_detect_at_seconds',
        'recognize_at_seconds',
        'temperature_check',
        'temperature_check_end',
        'temperature_threshold',
        'door_open_for_green_at',
        'door_open_for_red_at',
        'low',
        'medium',
        'high',
        'low_color',
        'medium_color',
        'high_color',
        'hr_email',
        'guest_user_link',
        'camera_name',
        'building_id',
        'report_email'
    ];
    public function company() {
        return $this->belongsTo('App\Models\Company','company_id');
    }
}
