<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupEmployee extends Model
{

	protected $table = 'group_employees';

	protected $fillable = ['group_id', 'employee_id'];

    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }
    public function group() {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }
}
