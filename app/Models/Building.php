<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Building extends Model
{
    use SoftDeletes;
	protected $table = 'buildings';

	protected $fillable = ['name','company_id','building_limit'];

  /*  public function device_people() {
        return $this->hasMany('App\Models\DevicePeople', 'device_id');
    }*/
    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
    public function device_people() {
        return $this->hasMany('App\Models\DevicePeople', 'building_id');
    }
    public function building_policy() {
        return $this->hasMany('App\Models\BuildingPolicy', 'building_id');
    }

    public function bookings() {
        return $this->hasMany('App\Models\Booking', 'building_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($device) { // before delete() method call this

            $device->building_policy()->delete();
            $device->bookings()->delete();
            // do the rest of the cleanup...
        });
    }
}
