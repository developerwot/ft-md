<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/*use Illuminate\Database\Eloquent\SoftDeletes;*/

class PeopleRegistry extends Model
{
    /*use SoftDeletes;*/
	protected $table = 'people_registry';

	protected $fillable = ['employee_id', 'people_file','file_type','training_status'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if(!isset($model->training_status)) {
                $model->training_status = 0;
            }
            $model->file_type = 'Video';
        });
        static::saving(function ($model) {
            $model->file_type = 'Video';
        });
    }
    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }


}
