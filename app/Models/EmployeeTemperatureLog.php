<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeTemperatureLog extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'employee_temperature_logs';
    protected $fillable = [
        'employee_id', 'temperature','temp_date'
    ];
    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }
}
