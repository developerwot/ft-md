<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuildingPolicy extends Model
{
    use SoftDeletes;
	protected $table = 'building_access_polices';

	protected $fillable = ['policy','building_id','group_id','always_range','permit','from_date','to_date','from_hour','to_hour'];

  /*  public function device_people() {
        return $this->hasMany('App\Models\DevicePeople', 'device_id');
    }*/
    public function building() {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }
    public function group() {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($device) { // before delete() method call this

            //$device->device_people()->delete();
            // do the rest of the cleanup...
        });
    }
}
