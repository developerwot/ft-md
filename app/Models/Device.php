<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;
	protected $table = 'devices';

	protected $fillable = ['beaconId','company_id'];

    public function device_people() {
        return $this->hasMany('App\Models\DevicePeople', 'device_id');
    }
    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($device) { // before delete() method call this

            $device->device_people()->delete();
            // do the rest of the cleanup...
        });
    }
}
