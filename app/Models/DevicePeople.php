<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DevicePeople extends Model
{
    use SoftDeletes;
	protected $table = 'device_peoples';

	protected $fillable = ['employee_id','device_id','building_id'];

    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }
    public function device() {
        return $this->belongsTo('App\Models\Device', 'device_id');
    }

    public function building() {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }


}
