<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuestUserDocuments extends Model
{

	protected $table = 'guest_user_documents';

	protected $fillable = ['guest_user_id','address_proof','identity_proof'];

    public function guest_user() {
        return $this->belongsTo('App\Models\GuestUser', 'guest_user_id');
    }

}
