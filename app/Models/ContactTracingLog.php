<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ContactTracingLog extends Model
{

    //use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'contact_tracing_logs';
    protected $fillable = [
        //'employee_id', 'contact_employee_id','contact_time'
        'employee_id', 'contact_employee_id','contact_time'
    ];
    public function employee() {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }
    public function contact_employee() {
        return $this->belongsTo('App\Models\Employee', 'contact_employee_id');
    }
}
