<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocationAccess extends Model
{

    protected  $guarded=[];
    use SoftDeletes;
    public function buildings(){
      return  $this->belongsTo(BuildingAccess::class,'building_access_id');
    }

    public function timeslot(){
        return $this->hasMany(TimeslotAccess::class);
    }
    public function locationDevice(){
        return $this->hasMany(LocationDevice::class);
    }
}
