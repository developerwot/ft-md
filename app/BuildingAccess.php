<?php

namespace App;

use App\Models\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuildingAccess extends Model
{
    use SoftDeletes;
    protected  $guarded=[];


    public function company(){
     return   $this->belongsTo(Company::class);
    }

    public function location(){
         return $this->hasMany(LocationAccess::class);
    }
    public function timeslot(){
        return $this->hasMany(TimeslotAccess::class);
    }

    public function locationDevice(){
        return $this->hasMany(LocationDevice::class,'building_id');
    }
    public function floor(){
        return $this->hasMany(FloorAccess::class);
    }

    public function desk(){
        return $this->hasMany(DeskAccess::class,'building_id');
    }
}
